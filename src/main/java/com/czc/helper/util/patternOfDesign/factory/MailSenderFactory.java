package com.czc.helper.util.patternOfDesign.factory;

/**
 * Created by cuizhongcheng on 2019/10/12
 */
public class MailSenderFactory implements AbstractFactory {
    @Override
    public Sender product() {
        return new MailSender();
    }
}
