package com.czc.helper.util.patternOfDesign.adapter;

/**
 * 直接继承Source，可将method1变为Targetable接口的方法实现
 *
 * Created by cuizhongcheng on 2019/10/16
 */
public class ClassAdapter extends Source implements Targetable {

    // @Override 可要可不要
    public void method2() {
        System.out.println("method2");
    }

}
