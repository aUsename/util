package com.czc.helper.util.patternOfDesign.behavior.observer;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public class Observer2 implements ObserverInterface {
    @Override
    public void update() {
        System.out.println("Observer2");
    }
}
