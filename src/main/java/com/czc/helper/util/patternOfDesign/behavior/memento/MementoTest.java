package com.czc.helper.util.patternOfDesign.behavior.memento;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public class MementoTest {
    public static void main(String[] args) {
        Original original = new Original("123");
        Storage storage = new Storage(original.createMemento());

        System.out.println(original.getValue());
        original.setValue("456");
        System.out.println(original.getValue());
        original.restoreMemento(storage.getMemento());
        System.out.println(original.getValue());
    }
}
