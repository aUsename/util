package com.czc.helper.util.patternOfDesign.adapter;

/**
 * Created by cuizhongcheng on 2019/10/16
 */
public class ObjectAdapter implements Targetable {

    private Source source;

    // 构造方法，注入被适配对象
    public ObjectAdapter(Source source) {
        super();
        this.source = source;
    }

    @Override
    public void method1() {
        source.method1();
    }

    @Override
    public void method2() {
        System.out.println("ObjectAdapter method2");
    }


}
