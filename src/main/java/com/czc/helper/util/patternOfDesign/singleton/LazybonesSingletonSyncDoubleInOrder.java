package com.czc.helper.util.patternOfDesign.singleton;

/**
 * Created by cuizhongcheng on 2019/10/9
 * 解决无序写问题懒汉式单例
 *
 *
 * 1、线程A进入getInstance()方法。
 * 2、因为instance是空的 ，所以线程A进入位置//1的第一个synchronized块。
 * 3、线程A执行位置//2的代码，把instance赋值给本地变量temp。instance为空，所以temp也为空。
 * 4、因为temp为空，所以线程A进入位置//3的第二个synchronized块。（后来想想这个锁有点多余）
 * 5、线程A执行位置//4的代码，把temp设置成非空，但还没有调用构造方法！（“无序写”问题）
 * 6、如果线程A阻塞，线程B进入getInstance()方法。
 * 7、因为instance为空，所以线程B试图进入第一个synchronized块。但由于线程A已经在里面了。所以无法进入。线程B阻塞。
 * 8、线程A激活，继续执行位置//4的代码。调用构造方法。生成实例。
 * 9、将temp的实例引用赋值给instance。退出两个synchronized块。返回实例。
 * 10、线程B激活，进入第一个synchronized块。
 * 11、线程B执行位置//2的代码，把instance实例赋值给temp本地变量。
 * 12、线程B判断本地变量temp不为空，所以跳过if块。返回instance实例。
 *
 */
public class LazybonesSingletonSyncDoubleInOrder {

    private static LazybonesSingletonSyncDoubleInOrder singleton = null;

    // 私有构造
    private LazybonesSingletonSyncDoubleInOrder() {
    }

    public static LazybonesSingletonSyncDoubleInOrder getSingleton() {
        if (singleton == null) {
            synchronized (LazybonesSingletonSyncDoubleInOrder.class) {  // 1
                LazybonesSingletonSyncDoubleInOrder temp = singleton;   // 2
                if (temp == null) {
                    synchronized (LazybonesSingletonSyncDoubleInOrder.class) { // 3
                        temp = new LazybonesSingletonSyncDoubleInOrder(); // 4
                    }
                }
                singleton = temp; // 5
            }
        }
        return singleton;
    }

    public Object readResolve() {
        return singleton;
    }
}
