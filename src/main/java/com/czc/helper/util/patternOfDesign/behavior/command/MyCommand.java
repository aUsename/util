package com.czc.helper.util.patternOfDesign.behavior.command;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public class MyCommand implements Command {

    private Receiver receiver;

    public MyCommand(Receiver receiver){
        this.receiver = receiver;
    }

    @Override
    public void exe() {
        receiver.action();
    }
}
