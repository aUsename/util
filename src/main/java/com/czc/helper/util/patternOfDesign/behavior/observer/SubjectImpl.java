package com.czc.helper.util.patternOfDesign.behavior.observer;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public class SubjectImpl extends AbstractSubject {
    public void operation() {
        System.out.println("update self!");
        notifyObservers();
    }
}
