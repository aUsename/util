package com.czc.helper.util.patternOfDesign.adapter.interfaceAdapter;

/**
 * 实现所有方法
 * Created by cuizhongcheng on 2019/10/16
 */
public abstract class AbstractWrapper implements ManyMethodInterface {
    public void method1(){}
    public void method2(){}
    public void method3(){}
    public void method4(){}
    public void method5(){}
    public void method6(){}
}
