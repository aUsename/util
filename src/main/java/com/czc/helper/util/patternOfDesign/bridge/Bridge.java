package com.czc.helper.util.patternOfDesign.bridge;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class Bridge extends BridgeInterface {

    public void method() {
        getSource().method();
    }

}
