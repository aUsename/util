package com.czc.helper.util.patternOfDesign.facade;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class CPU {

    public void startup() {
        System.out.println("cpu start");
    }

    public void shutdown() {
        System.out.println("cpu shutdown");
    }

}
