package com.czc.helper.util.patternOfDesign.bridge;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public abstract class BridgeInterface {

    private SourceInterface source;

    public void method() {
        source.method();
    }

    public SourceInterface getSource() {
        return source;
    }

    public void setSource(SourceInterface source) {
        this.source = source;
    }
    
}
