package com.czc.helper.util.patternOfDesign.behavior.chainOfResponsibility;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public interface HandlerInterface {
    public void operator();
}
