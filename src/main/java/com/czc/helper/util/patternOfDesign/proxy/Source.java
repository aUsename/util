package com.czc.helper.util.patternOfDesign.proxy;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class Source implements SourceInterface{
    @Override
    public void method1() {
        System.out.println("source method1");
    }
}
