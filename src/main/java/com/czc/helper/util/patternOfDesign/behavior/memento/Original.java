package com.czc.helper.util.patternOfDesign.behavior.memento;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
@Getter
@Setter
@AllArgsConstructor
public class Original {
    private String value;

    public Memento createMemento(){
        return new Memento(value);
    }

    public void restoreMemento(Memento memento){
        this.value = memento.getValue();
    }
}
