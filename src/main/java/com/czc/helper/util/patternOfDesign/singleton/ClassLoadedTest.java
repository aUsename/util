package com.czc.helper.util.patternOfDesign.singleton;

/**
 * Created by cuizhongcheng on 2019/10/9
 * 加载顺序：静态属性-静态代码块-非静态属性-构造方法
 */
public class ClassLoadedTest {

    // 静态属性 只加载一次
    public static long date = System.currentTimeMillis(); //1

    // 非静态属性
    public int notStatic = 1; //3,5

    // 静态代码块 只加载一次
    static {
        System.out.println("ClassLoadedTest:" + System.currentTimeMillis()); //2
    }

    // 构造方法
    public ClassLoadedTest() {
        System.out.println(System.currentTimeMillis()); //4,6
    }

    // 内部类 getDate()方法调用时才会加载而且也只加载一次
    static class interior {
        public static long date = System.currentTimeMillis(); //8
    }

    public long getDate() {
        return interior.date; //7,9
    }

    public static void main(String[] args) {
        ClassLoadedTest t = new ClassLoadedTest();
        ClassLoadedTest t1 = new ClassLoadedTest();
        System.out.println(t.getDate());
        System.out.println(t1.getDate());
    }
}
