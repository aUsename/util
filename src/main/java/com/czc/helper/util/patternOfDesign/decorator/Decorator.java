package com.czc.helper.util.patternOfDesign.decorator;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class Decorator implements SourceInterface{

    // 持有被装饰对象实例
    private SourceInterface sourceInterface;

    public Decorator(SourceInterface sourceInterface){
        super();
        this.sourceInterface = sourceInterface;
    }

    // 扩展功能
    @Override
    public void method1() {
        System.out.println("before");
        sourceInterface.method1();
        System.out.println("after");
    }
}
