package com.czc.helper.util.patternOfDesign.composite;

/**
 * Created by cuizhongcheng on 2019/10/19
 */
public class Tree {

    Node root = null;

    public Tree(String name) {
        root = new Node(name);
    }

    public static void main(String[] args) {
        Tree tree = new Tree("A");
        Node nodeB = new Node("B");
        Node nodeC = new Node("C");
        nodeB.add(nodeC);
        tree.root.add(nodeB);
        System.out.println("build the tree finished!");
    }
}
