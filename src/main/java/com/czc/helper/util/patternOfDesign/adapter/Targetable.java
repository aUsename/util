package com.czc.helper.util.patternOfDesign.adapter;

/**
 * Created by cuizhongcheng on 2019/10/12
 */
public interface Targetable {

    public void method1();

    public void method2();

}
