package com.czc.helper.util.patternOfDesign.factory;

/**
 * Created by cuizhongcheng on 2019/10/12
 */
public class SmsSender implements Sender {
    @Override
    public void send() {
        System.out.println("SmsSender");
    }
}
