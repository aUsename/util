package com.czc.helper.util.patternOfDesign.adapter.interfaceAdapter;

/**
 * Created by cuizhongcheng on 2019/10/16
 */
public interface ManyMethodInterface {
    void method1();
    void method2();
    void method3();
    void method4();
    void method5();
    void method6();
}
