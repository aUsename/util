package com.czc.helper.util.patternOfDesign.singleton;

/**
 * Created by cuizhongcheng on 2019/10/9
 * 双重锁定懒汉式单例
 *
 * 注：
 * singleton = new LazybonesSingletonSyncDouble(); 这行其实做了两个事情：1、调用构造方法，创建了一个实例。2、把这个实例赋值给instance这个实例变量
 * 但是JVM不保证这两步是有序的，所以可能先画出了一些分配给Singleton实例的空白内存，并赋值给instance成员，再做实例初始化操作，
 * 而在实例初始化之前，程序就会离开synchronized块，导致别的线程可能拿到一个未初始化的实例（不是null）
 */
public class LazybonesSingletonSyncDouble {

    private static LazybonesSingletonSyncDouble singleton = null;

    // 私有构造
    private LazybonesSingletonSyncDouble() {
    }

    public static LazybonesSingletonSyncDouble getSingleton() {
        if (singleton == null) {
            synchronized (LazybonesSingletonSyncDouble.class) { // 也可以写成 synchronized (singleton)
                if (singleton == null) {
                    singleton = new LazybonesSingletonSyncDouble(); // 这一行包括了两个无序的操作
                }
            }
        }
        return singleton;
    }

    public Object readResolve() {
        return singleton;
    }
}
