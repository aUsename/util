package com.czc.helper.util.patternOfDesign.behavior.command;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public interface Command {
    public void exe();
}
