package com.czc.helper.util.patternOfDesign.behavior.iterator;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public interface Iterator {

    // 前移
    public Object previous();

    // 后移
    public Object next();

    public boolean hasNext();

    // 取得第一个元素
    public Object first();
}
