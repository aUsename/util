package com.czc.helper.util.patternOfDesign.singleton;

import lombok.ToString;

import java.io.Serializable;

/**
 * Created by cuizhongcheng on 2019/9/29
 * 内部类实现的懒汉式单例
 */
@ToString
public class Singleton implements Serializable {

    private Singleton() {
    }

    // 内部类
    private static class SingletonFactory {
        private static Singleton instance = new Singleton();
    }

    // 本方法被调用时，内部类才会被加载
    public static Singleton getInstance() {
        return SingletonFactory.instance;
    }

    public Object readResolve() {
        return getInstance();
    }


}
