package com.czc.helper.util.patternOfDesign.singleton;

/**
 * Created by cuizhongcheng on 2019/10/9
 * 简单懒汉式单例
 */
public class LazybonesSingleton {

    private static LazybonesSingleton singleton = null;

    // 私有构造
    private LazybonesSingleton() {
    }

    public static LazybonesSingleton getSingleton() {
        if (singleton == null) {
            singleton = new LazybonesSingleton();
        }
        return singleton;
    }

    public Object readResolve() {
        return singleton;
    }

}
