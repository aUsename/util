package com.czc.helper.util.patternOfDesign.behavior.strategy;

/**
 * 统一算法接口
 *
 * Created by cuizhongcheng on 2019/10/23
 */
public interface ICalculator {
    public int calculate(String exp);
}
