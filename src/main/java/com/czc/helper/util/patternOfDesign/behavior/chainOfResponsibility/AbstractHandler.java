package com.czc.helper.util.patternOfDesign.behavior.chainOfResponsibility;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public abstract class AbstractHandler {

    private HandlerInterface handler;

    public HandlerInterface getHandler() {
        return handler;
    }

    public void setHandler(HandlerInterface handlerInterface) {
        this.handler = handlerInterface;
    }
}
