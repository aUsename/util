package com.czc.helper.util.patternOfDesign.factory;

/**
 * Created by cuizhongcheng on 2019/10/12
 */
public interface Sender {
    public void send();
}
