package com.czc.helper.util.patternOfDesign.behavior.observer;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public abstract class AbstractSubject {
    private Vector<ObserverInterface> vector = new Vector<ObserverInterface>();

    public void add(ObserverInterface observer) {
        vector.add(observer);
    }

    public void del(ObserverInterface observer) {
        vector.remove(observer);
    }

    public void notifyObservers() {
        Enumeration<ObserverInterface> enumo = vector.elements();
        while(enumo.hasMoreElements()){
            enumo.nextElement().update();
        }
    }
}
