package com.czc.helper.util.patternOfDesign.facade;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class Memory {

    public void startup() {
        System.out.println("memory start");
    }

    public void shutdown() {
        System.out.println("memory shutdown");
    }

}
