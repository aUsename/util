package com.czc.helper.util.patternOfDesign.behavior.strategy;

/**
 * Created by cuizhongcheng on 2019/10/23
 */
public class Multiply extends AbstractCalculator implements ICalculator {

    @Override
    public int calculate(String exp) {
        int arrayInt[] = split(exp, "\\*");
        return arrayInt[0] * arrayInt[1];
    }
}