package com.czc.helper.util.patternOfDesign.singleton;

/**
 * Created by cuizhongcheng on 2019/10/10
 * 饿汉式单例
 */
public class HungrySingleton {

    // 在类加载时进行初始化一次，保证线程安全
    private static HungrySingleton singleton = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getSingleton() {
        return singleton;
    }
}
