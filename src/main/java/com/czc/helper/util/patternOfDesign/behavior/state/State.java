package com.czc.helper.util.patternOfDesign.behavior.state;

import lombok.Data;

/**
 * 状态类
 * Created by cuizhongcheng on 2019/11/4
 */
@Data
public class State {

    private String value;

    public void method1(){
        System.out.println("method1");
    }

    public void method2(){
        System.out.println("method2");
    }
}
