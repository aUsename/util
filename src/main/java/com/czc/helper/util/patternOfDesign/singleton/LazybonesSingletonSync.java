package com.czc.helper.util.patternOfDesign.singleton;

/**
 * Created by cuizhongcheng on 2019/10/9
 * 同步法懒汉式单例
 */
public class LazybonesSingletonSync {

    private static LazybonesSingletonSync singleton = null;

    // 私有构造
    private LazybonesSingletonSync() {
    }

    // 加上synchronized关键字，每次getSingleton时均需要加锁
    public static synchronized LazybonesSingletonSync getSingleton() {
        if (singleton == null) {
            singleton = new LazybonesSingletonSync();
        }
        return singleton;
    }

    public Object readResolve() {
        return singleton;
    }
}
