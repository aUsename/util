package com.czc.helper.util.patternOfDesign.composite;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by cuizhongcheng on 2019/10/19
 */
public class Node {

    private String name;
    private Node parent;
    private Vector<Node> children = new Vector<>();

    // 构造
    public Node(String name){
        this.name = name;
    }

    // 添加孩子节点
    public void add(Node node){
        children.add(node);
    }

    // 删除孩子节点
    public void remove(Node node){
        children.remove(node);
    }

    // 取得孩子节点
    public Enumeration<Node> getChildren(){
        return children.elements();
    }


    // get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }
}
