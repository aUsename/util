package com.czc.helper.util.patternOfDesign.adapter.interfaceAdapter;

/**
 * 不需要再实现众多方法
 * Created by cuizhongcheng on 2019/10/16
 */
public class OneMethodClass extends AbstractWrapper {

    public void method1() {
        System.out.println("method1");
    }

}
