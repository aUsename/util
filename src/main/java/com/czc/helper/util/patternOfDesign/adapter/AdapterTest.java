package com.czc.helper.util.patternOfDesign.adapter;

/**
 * Created by cuizhongcheng on 2019/10/16
 */
public class AdapterTest {
    public static void main(String[] args) {
        Targetable targetable = new ClassAdapter();
        targetable.method1();
        targetable.method2();
    }
}
