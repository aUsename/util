package com.czc.helper.util.patternOfDesign.proxy;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class Proxy implements SourceInterface {

    // 持有被代理对象实例
    private Source source;

    // 自己构造一个被代理对象
    public Proxy(){
        super();
        this.source = new Source();
    }

    @Override
    public void method1() {
        System.out.println("before");
        source.method1();
        System.out.println("after");
    }
}
