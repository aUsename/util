package com.czc.helper.util.patternOfDesign.behavior.iterator;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public interface Collection {

    public Iterator iterator();

    public Object get(int i);

    public int size();
}
