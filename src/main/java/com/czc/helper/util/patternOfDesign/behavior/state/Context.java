package com.czc.helper.util.patternOfDesign.behavior.state;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by cuizhongcheng on 2019/11/4
 */
@Data
@AllArgsConstructor
public class Context {

    private State state;

    public void method(){
        if (state.getValue().equals("state1")) {
            state.method1();
        } else if (state.getValue().equals("state2")) {
            state.method2();
        }
    }
}
