package com.czc.helper.util.patternOfDesign.factory;

/**
 * Created by cuizhongcheng on 2019/10/12
 */
public class StaticFactory {
    public static Sender produceMail(){
        return new MailSender();
    }

    public static Sender produceSms(){
        return new SmsSender();
    }
}
