package com.czc.helper.util.patternOfDesign.behavior.templateMethod;

/**
 * Created by cuizhongcheng on 2019/10/23
 */
public class Plus extends AbstractCalculator {

    @Override
    public int calculate(int num1,int num2) {
        return num1 + num2;
    }
}
