package com.czc.helper.util.patternOfDesign.behavior.chainOfResponsibility;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public class Handler extends AbstractHandler implements HandlerInterface {

    private String name;

    public Handler(String name) {
        this.name = name;
    }

    @Override
    public void operator() {
        System.out.println(name + " deal!");
        if (getHandler() != null) {
            getHandler().operator();
        }
    }
}
