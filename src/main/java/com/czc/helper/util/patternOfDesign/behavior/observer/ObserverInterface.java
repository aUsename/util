package com.czc.helper.util.patternOfDesign.behavior.observer;

/**
 * Created by cuizhongcheng on 2019/10/23
 */
public interface ObserverInterface {
    public void update();
}
