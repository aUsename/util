package com.czc.helper.util.patternOfDesign.bridge;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class SourceB implements SourceInterface {

    @Override
    public void method() {
        System.out.println("SourceB");
    }

}
