package com.czc.helper.util.patternOfDesign.facade;

/**
 * Created by cuizhongcheng on 2019/10/17
 */
public class FacadeComputer {
    private CPU cpu;
    private Memory memory;
    private Disk disk;

    public FacadeComputer() {
        cpu = new CPU();
        memory = new Memory();
        disk = new Disk();
    }

    public void startup() {
        cpu.startup();
        memory.startup();
        disk.startup();
    }

    public void shutdown() {
        cpu.shutdown();
        memory.shutdown();
        disk.shutdown();
    }
}
