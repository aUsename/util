package com.czc.helper.util.patternOfDesign.behavior.command;

/**
 * Created by cuizhongcheng on 2019/10/26
 */
public class Invoker {
    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void action(){
        command.exe();
    }
}
