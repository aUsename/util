package com.czc.helper.util.dataStructureAndAlgorithm.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by cuizhongcheng on 2019/11/28
 */
public class ConsumerAndThen {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        work(list, t -> {
            System.out.println("consumer1 is: " + t);
        }, t -> {
            System.out.println("consumer2 is: " + t);
        });
    }

    public static void work(List<String> list, Consumer<String> consumer1, Consumer<String> consumer2) {
        list.forEach(t -> consumer1.andThen(consumer2).accept(t));
        System.out.println("=================");
        list.forEach(
                consumer1.andThen(consumer2)
        );
    }
}
