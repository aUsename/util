package com.czc.helper.util.dataStructureAndAlgorithm.collection;

/**
 * Created by cuizhongcheng on 2019/12/2
 */
public class AbstractCollectionHugeCapacityTest {

    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    public static void main(String[] args) {
        System.out.println("MAX_ARRAY_SIZE = " + MAX_ARRAY_SIZE);
        System.out.println("MAX_VALUE = " + Integer.MAX_VALUE);
        int cap = 2147483645;
        System.out.println("cap = " + cap);
        System.out.println("cap >> 1 = " + (cap >> 1));
        int newCap = cap + (cap >> 1) + 1;
        System.out.println("newCap = " + newCap);
        if (newCap - MAX_ARRAY_SIZE > 0){
            newCap = hugeCapacity(cap + 1);
        }
        System.out.println("newCap = " + newCap);
    }

    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0){
            // overflow
            throw new OutOfMemoryError("Required array size too large");
        }
        return (minCapacity > MAX_ARRAY_SIZE) ? Integer.MAX_VALUE : MAX_ARRAY_SIZE;
    }
}
