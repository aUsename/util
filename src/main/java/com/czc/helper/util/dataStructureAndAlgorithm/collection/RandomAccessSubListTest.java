package com.czc.helper.util.dataStructureAndAlgorithm.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

/**
 * Created by cuizhongcheng on 2019/12/5
 */
public class RandomAccessSubListTest {
    public static void main(String[] args) {
        ArrayList<String> s = new ArrayList<>();
        s.add("1");
        s.add("2");
        s.add("3");
        s.add("4");
        s.add("5");
        s.add("6");
        System.out.println(s);
        List s1 = s.subList(0,5);
        System.out.println(s1);
        if (s1 instanceof RandomAccess){
            System.out.println("s1");
        }
        List s2 = s1.subList(0,3);
        System.out.println(s2);
        if (s2 instanceof RandomAccess){
            System.out.println("s2");
        }
    }
}
