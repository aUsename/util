package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * Created by cuizhongcheng on 2019/11/16
 */
public class QuickSort {
    private int[] array;
    private int length;

    public QuickSort(int[] array) {
        this.array = array;
        this.length = array.length;
    }

    public void display() {
        for (int a : array) {
            System.out.print(a);
            System.out.print(" ");
        }
    }

    public void sort() {
        quickSort(0, length - 1);
    }

    private void quickSort(int left, int right) {
        if (right - left <= 0) {
            return;
        }
        int leftP = partitionIt(left, right);
        quickSort(left, leftP);
        quickSort(leftP + 1, right);
    }

    private int partitionIt(int left, int right) {
        int div = array[right];
        int leftP = left;
        int rightR = right;

        while (true) {
            while (leftP < right && array[leftP] < div) {
                leftP++;
            }
            while (rightR > left && array[rightR] > div) {
                rightR--;
            }
            if (leftP < rightR) {
                swap(leftP, rightR);
                leftP++;
                rightR--;
            } else {
                return leftP - 1;
            }
        }
    }

    private void swap(int a, int b) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

}
