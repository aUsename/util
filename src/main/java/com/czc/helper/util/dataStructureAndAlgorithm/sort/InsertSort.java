package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * 插入排序
 * Created by cuizhongcheng on 2019/10/30
 */
public class InsertSort {

    private int[] array;
    private int length;

    public InsertSort(int[] array) {
        this.array = array;
        this.length = array.length;
    }

    // 最终版
    public void sort() {
        for (int i = 1; i < length; i++) {
            int temp = array[i];
            int j = i;
            while (j > 0 && array[j - 1] > temp) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
        }
    }

    // 合并循环，但是array[j] = temp; 执行次数依旧较高
//    public void sort() {
//        for (int i = 1; i < length; i++) {
//            int temp = array[i];
//            for (int j = i - 1; j >= 0; j--) {
//                if (array[j] > temp){
//                    array[j+1] = array[j];
//                    array[j] = temp;
//                }
//            }
//
//        }
//    }

    // 初版
//    public void sort() {
//        for (int i = 1; i < length; i++) {
//            // 里面的两个for循环可以合并
//            for (int j = 0; j < i; j++) {
//                if (array[j] > array[i]) {
//                    int temp = array[i];
//                    for (int k = i; k > j; k--) {
//                        array[k] = array[k - 1];
//                    }
//                    array[j] = temp;
//                }
//            }
//        }
//    }

    public void display() {
        for (int a : array) {
            System.out.print(a);
            System.out.print(" ");
        }
    }

    private void swap(int a, int b) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
