package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * Created by cuizhongcheng on 2019/11/13
 */
public class MergeSort {
    private int[] array;
    private int length;

    public MergeSort(int[] array) {
        this.array = array;
        this.length = array.length;
    }

    public void sort() {
        // 仅用于归并过程，每归并一次后，根据workSpaceArray更新原数据，以便于下次归并
        int[] workSpaceArray = new int[length];
        mergeSort(workSpaceArray, 0, length - 1);
    }

    private void mergeSort(int[] newArray, int low, int high) {
        if (low == high) {
            return;
        } else {
            int m = (low + high) / 2;
            mergeSort(newArray, low, m);
            mergeSort(newArray, m + 1, high);
            merge(newArray, low, m, high);
        }
    }

    // 归并
    private void merge(int[] workSpaceArray, int low, int m, int high) {

        // 数量
        int nums = high - low + 1;
        // 最低值
        int lowTemp = low;
        int mid = m;

        int index = 0;

        // 归并
        while (low <= mid && m + 1 <= high) {
            if (array[low] <= array[m + 1]) {
                workSpaceArray[index] = array[low];
                low++;
            } else {
                workSpaceArray[index] = array[m + 1];
                m++;
            }
            index++;
        }

        // 后半段数据遍历完，填上前半段
        while (low <= mid) {
            workSpaceArray[index] = array[low];
            low++;
            index++;
        }

        // 前半段数据遍历完，填上后半段
        while (m + 1 <= high) {
            workSpaceArray[index] = array[m + 1];
            m++;
            index++;
        }

        // 更新原array
        for (int i = 0; i < nums; i++) {
            array[lowTemp + i] = workSpaceArray[i];
        }
    }

    public void display() {
        for (int a : array) {
            System.out.print(a);
            System.out.print(" ");
        }
        System.out.println();
    }
}
