package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * Created by cuizhongcheng on 2019/11/14
 */
public class ShellSort {
    private int[] array;
    private int length;

    public ShellSort(int[] array) {
        this.array = array;
        this.length = array.length;
    }

    public void sort() {
        // 计算h
        int h = 1;
        while (h < length / 3) {
            h = h * 3 + 1;
        }

        while (h > 0) {
            for (int i = h; i < length; i++) {
                int temp = array[i];
                int j = i;
                while (j >= h && array[j - h] > temp) {
                    array[j] = array[j - h];
                    j = j - h;
                }
                array[j] = temp;
            }
            h = (h - 1) / 3;
        }

    }

    public void display() {
        for (int a : array) {
            System.out.print(a);
            System.out.print(" ");
        }
        System.out.println();
    }
}
