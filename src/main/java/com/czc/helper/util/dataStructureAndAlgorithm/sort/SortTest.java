package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * Created by cuizhongcheng on 2019/10/29
 */
public class SortTest {
    public static void main(String[] args) {
//        int[] array = {9, 8, 7, 6, 5, 4, 3, 2, 1};
//        int[] array = {2, 1, 9, 8, 7, 6, 5, 4, 3};
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};

//        BubbleSort sort = new BubbleSort(array);
//        SelectSort sort = new SelectSort(array);
//        InsertSort sort = new InsertSort(array);
//        MergeSort sort = new MergeSort(array);
//        ShellSort sort = new ShellSort(array);
        QuickSort sort = new QuickSort(array);

        sort.sort();
        sort.display();
        System.out.println();
    }
}
