package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * 冒泡排序
 * Created by cuizhongcheng on 2019/10/29
 */
public class BubbleSort {

    private int[] array;
    private int length;

    public BubbleSort(int[] array) {
        this.array = array;
        this.length = array.length;
    }

    public void sort() {
        for (int i = length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    swap(j, j + 1);
                }
            }
        }
    }

    public void display() {
        for (int a : array) {
            System.out.print(a);
            System.out.print(" ");
        }
    }

    private void swap(int a, int b) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
