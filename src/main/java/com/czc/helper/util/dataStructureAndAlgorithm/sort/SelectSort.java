package com.czc.helper.util.dataStructureAndAlgorithm.sort;

/**
 * 选择排序
 * Created by cuizhongcheng on 2019/10/29
 */
public class SelectSort {
    private int[] array;
    private int length;

    public SelectSort(int[] array) {
        this.array = array;
        this.length = array.length;
    }

    public void sort() {
        for (int i = 0; i < length - 1; i++) {
            int min = i;
            for (int j = i; j < length; j++) {
                if (array[min] > array[j]) {
                    min = j;
                }
            }
            swap(min, i);
        }
    }

    public void display() {
        for (int a : array) {
            System.out.print(a);
            System.out.print(" ");
        }
    }

    private void swap(int a, int b) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

}
