package com.czc.helper.util.sevenConcurrentModels.threadsLocks;

/**
 * Created by cuizhongcheng on 2018/11/23
 * yield()方法
 * 新线程创建需要时间，可能造成先后关系
 */
public class Yield {
    public static void main(String[] args) throws InterruptedException {
        Thread myThread = new Thread(() -> System.out.println("new Thread!"));
        myThread.start();

        // 当前线程让出处理器，重新竞争处理器
        Thread.yield();
//        Thread.sleep(1);

        System.out.println("my Thread!");

        myThread.join();
    }

}
