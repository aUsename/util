## OSI
### 七层模型
    * 7应用层（567可统一认为是应用层）DNS FTP Telent HTTP WWW SMTP
    * 6表示层      为上层用户提供共同的数据或信息的语法表示变换
    * 5会话层      进程–进程的层次
    * 4运输层 TCP UDP      端–端，也即主机–主机的层次
    * 3网络层（网际层）IP   数据以分组（包）为单位传输，要解决如何使数据分组跨越通信子网从源传送到目的地的问题，这就需要在通信子网中进行路由选择  
    * 2数据链路层    帧传输，通过校验、确认和反馈重发等手段，将不可靠的物理链路改造成对网络层来说无差错的数据链路。物理寻址、同时将原始比特流转变为逻辑传输线路  
    * 1物理层（12可统一认为是网络接口层）   物理机的比特流传输  


--------------------------------------------------------------------------------  
## IP
### 首部
    * 20+字节 版本0.5+首部长度0.5+区分服务1+总长度2 + 标识2+标志+片偏移 + 生存时间1+协议1+首部校验和2 + 源地址4 + 目的地址4 + 可选+填充

--------------------------------------------------------------------------------    
## UDP    
简单的面向数据报的运输层协议。UDP不提供可靠性，它只是把应用程序传给IP层的数据报发送出去，但是并不能保证它们能到达目的地。
### 首部
    * 8字节 源端口+目的端口+总长度+校验和

--------------------------------------------------------------------------------
## TCP
提供的是面向连接、可靠的基于字节流的传输层通信协议。当客户和服务器彼此交换数据前，必须先在双方之间建立一个TCP连接，之后才能传输数据。
TCP提供超时重发，丢弃重复数据，检验数据，流量控制等功能，保证数据能从一端传到另一端
### 首部
    * 20+字节 源端口2，目的端口2，序号4，确认号4，窗口2，校验和2，紧急指针2，各标志位（ACK，SYN，FIN）
### 三次握手四次挥手
    A -> B SYN=1 seq=x （SYN仅在建立连接时设置，消耗1序号）     SYN_SEND（A）  
    B -> A SYN=1 ACK=1 seq=y ack=x+1 （此后一直ACK=1）        SYN_RECV（B）  
    a -> B ACK=1 seq=x+1 ack=y （若携带数据，seq消耗序号，否则不消耗，下一次还是x+1）       ESTABLISHED  
    第三次握手是为了防止已失效的连接请求突然又传送到了B  
    
    A -> B FIN=1 seq=u （FIN不携带数据，但还是消耗1序号）      FIN_WAIT1（A）  
    B -> A ACK=1 seq=v ack=u+1      FIN_WAIT2（A）CLOSE_WAIT(B)  
    B -> A FIN=1 ACK=1 seq=w ack=u+1 (ACK和ack还是需要设置)        LAST_ACK  
    A -> B ACK=1 seq=u+1 ack=w+1        CLOSE  
    A等待2MSL（最长报文段寿命时间，防止第四次挥手丢失，4分钟）        TIME_WAIT(A)  
    
### TCP如何保证可靠传输
    * 三次握手
    * 对收到的请求给出确认响应
    * 对未收到确认响应的，超时重发
    * 对校验出错的报文段直接丢弃，不给响应
    * 丢弃重复数据
    * 将数据截断为合理长度，按字节编号，对失序数据重排序后再交付
    * 流量控制，设置了缓冲区
    * 拥塞控制

### TCP和UDP区别
    * TCP连接，UDP无连接，每一条TCP连接只能是点到点的；UDP支持一对一，一对多，多对一和多对多的交互通信
    * TCP提供可靠的服务，UDP尽最大努力交付
    * UDP实时性更好，效率更高，对系统资源要求更少
    * UDP不对应用程序提交报文信息进行拆分或者合并

--------------------------------------------------------------------------------
## HTTP
请求报文：  
    `
    <METHOD> <URL> HTTP/1.1\r\n 
    <Header1>: <HeaderValue1>\r\n
    <Header2>: <HeaderValue2>\r\n  
    ...  
    <HeaderN>: <HeaderValueN>\r\n  
    \r\n  
    <Body Data....>
    `  
    方法 URL 版本  
    header  
    body  
例：  
    get http://www.xyz.cn/index HTTP/1.1  
    Host: www.xyz.cn  
    Connection: close  
### GET POST区别
需要先确定是浏览器使用的，还是作为接口传输协议使用的场景

    浏览器的get和post
    * get是从服务器上获取数据，post是向服务器传送数据（提交一个form表单并获得结果）
    * get请求会被缓存，post不会，get请求可以被缓存为书签，post不行
    * GET请求没有body，只有url，请求数据放在url的querystring中；POST请求的数据在body中
    
    接口中的get和post 只要是符合http请求格式就可以
    * REST 风格 
        * GET、POST、PUT和DELETE分别获取、创建、替换和删除“资源”，推荐请求体使用json
        * get+资源定位符 用于获取资源或者资源列表
        * post+资源定位符 用于“创建一个资源”
    * get传送的数据量有限制（实际浏览器对URL的长度限制,ie8不超过2k，Chrome是2m），post方法没有数据量限制
    * post方法安全性较高，因为POST用body传输数据，而GET用url传输，但安全性还是需要使用https

### 一次完整的HTTP请求过程
    * 客户端解析缓存，若有缓存且较新，直接展现
    * 若缓存不新或无缓存，重新构造HTTP请求 方法+URL+版本+header+请求实体
    * DNS解析，缓存+递归
    * TCP三次握手
    * 服务端处理，返回http响应
    * 客户端根据http响应里Connection的keep-alive判断是否关闭TCP连接，四次挥手
    * 客户端做前端处理，展示
    
### URI和URL区别
    * URI统一资源标识符，唯一标记一个资源，但不是地址，没法根据URI定位一个资源（能定位即为URL）
    * URL统一资源定位符，是一种具体的URI，不仅唯一标识，还指明了如何定位
    
### HTTP和HTTPS
    * http直接使用TCP套接字与传输层交互，https使用了SSL协议，在应用层下面增加了SSL子层，数据先经过SSL子层加解密之后再与TCP套接字交互
    * http是无状态的连接，https是http+SSL构造的，可加密传输，身份认证的网络协议，https协议需要到CA申请证书，一般收费
    * http使用80端口，https使用443端口  

### HTTPS是如何保证安全性的（简要过程）
    * 协商加密算法。1）浏览器A向服务器B发送A的SSL版本和一些可选的加密算法 2）B选取自己支持的算法如RSA，告知A
    * 服务器鉴别。3）B向A发送包含其RSA公钥的数字证书 4）A使用认证机构CA公开发布的RSA公钥对证书验证
    * 会话密钥计算。A随机产生一个随机数 5）用B的公钥加密后发给B 6）双方根据协商的算法产生共享的对称会话密钥
    * 安全数据传输。7）双方用会话密钥加解密他们之间传输的数据并验证其完整性
    
### 重定向和转发的区别
    * 重定向是客户端行为，转发是服务器行为
    * 重定向：两次请求，浏览器地址发生变化，可访问web之外的资源，传输的数据会丢失
    * 转发：一次请求，地址不变，访问自身web资源，传输的数据不会丢失
    
### HTTP请求头有哪些
    * Host, 请求的域名
    * Connection 请求完之后是否释放连接
    * User-Agent，用户的浏览器版本信息
    * Accept，响应的内容类型
    * Accept-Language, 接受的语言
    * Accept-Encoding, 可接受的编码方式
    * Cookie，本地的 Cookie 信息

--------------------------------------------------------------------------------
## DNS
### dns使用的传输层协议
    * 默认使用UDP，因为更快
    * 报文长度大于512字节时，使用TCP，因为UDP最大数据量是512
    * 区域传送时使用TCP，因为数据同步时数据量通常较大

--------------------------------------------------------------------------------
## 综合
### Cookie和Session的区别
    * cookie是服务器发给客户端的特殊信息，以文本的形式存放，客户端再次请求时会携带cookie，服务器会解析
    * session是服务器端的机制，解析客户端请求并操作session_id，按需保存状态信息
    * session可以使用cookie来实现，也可以使用URL回写来实现（即页面上任何链接都带有session_id参数）
    * 无论客户端怎么设置，session都能正常工作，当客户端禁用cookie时cookie无法使用
    * session可存储任意的java对象，cookie只能存储String类型的对象