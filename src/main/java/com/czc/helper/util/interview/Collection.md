## Collection
继承 Iterable
子类 Set List Map Queue
抽象子类 AbstractCollection

* nested class
* Spliterator   JDK1.8新增
* Stream JDK1.8新增
* default boolean removeIf(Predicate<? super E> filter)



## ArrayList
初始化容量为10
System.arraycopy方法：它就是从指定的源数组将元素中复制到目标数组，复制从指定的位置开始，到设定的复制长度结束，然后从目标数组的指定起始位置依次插入
Arrays.copy方法：它新建了一个数组并且将原数组的内容拷贝到长度为newLength的新数组中，并且返回该新数组。内部调用了System.arraycopy方法
自己实现了序列化和反序列化，因为它实现了writeObject和readObject方法。

-------
transient Object[] elementData 
    保存数据
public ArrayList(int initialCapacity)  
    按传参定容量
public ArrayList()  
    默认空数据
public ArrayList(Collection<? extends E> c) 
    将传递的集合调用toArray()方法转为数组内赋值给elementData，若为空集合，则将elementData设为空数组，若c.toArray()不为Object[]，则通过Arrays.copyOf转一下
-------
public int size()
    维护了内部变量size，直接返回
-------
public boolean contains(Object o)
    调用了indexOf(Object o) 方法
public int indexOf(Object o)
    通过遍历实现，null使用==，非null使用equals
public int lastIndexOf(Object o)
    逻辑同indexOf，倒序
-------
public Object clone()
    通过Arrays.copyOf(elementData, size)实现，并重设modCount
public Object[] toArray()
    return Arrays.copyOf(elementData, size);
public <T> T[] toArray(T[] a)
-------
public boolean add(E e)
    1.确保最小长度为size+1
    2.modCount++;
    3.size+1 > elementData.length时，才执行扩容grow
    4.grow扩容策略：
        新长度=原数组长度增加1/2（右移一位）
        若新长度<所需最小长度，则新长度=所需最小长度
        若新长度>Integer.MAX_VALUE - 8 
            若所需最小长度<0，抛错，若所需最小长度>Integer.MAX_VALUE - 8，新长度=Integer.MAX_VALUE，否则，新长度=Integer.MAX_VALUE - 8
    5.赋值
public void add(int index, E element)
    1.index校验
    2.同add方法校验
    3.System.arraycopy（)移动元素
    4.赋值
public E remove(int index)
    1.index校验，确保index<size
    2.modCount++;
    3.相关元素左移
    4.末位设为null（gc）
public boolean remove(Object o)
    for循环遍历数组，若匹配则删除，若o为null，使用==，否则用equals。删除时逻辑同remove，跳过校验（fastRemove）
public void clear()
    modCount++;
    数组所有元素设为null (gc)
    size=0;
public boolean addAll(Collection<? extends E> c)
    将c.toArray()添加至末尾 使用System.arraycopy方法
public boolean addAll(int index, Collection<? extends E> c)
    批量添加至index处
protected void removeRange(int fromIndex, int toIndex)
    末尾批量左移，末尾设为null
public boolean removeAll(Collection<?> c)
public boolean retainAll(Collection<?> c)
    复用 private boolean batchRemove(Collection<?> c, boolean complement) 
    从index=0开始，设置需要保留的值，并在最后将末尾的值设为null
-------
private void writeObject(java.io.ObjectOutputStream s)
private void readObject(java.io.ObjectInputStream s)
------- 迭代器
private class Itr implements Iterator<E>
    next()
    remove()
        连续调用两次会出错，因为回退至上一个位置只能回退一次
    forEachRemaining(Consumer<? super E> consumer)
        since 1.8
private class ListItr extends Itr implements ListIterator<E>   
    public boolean hasPrevious()
    public int nextIndex()
    public int previousIndex()
    public E previous()
        check modCount 
    public void set(E e)
    public void add(E e)
        在当前位置添加，后面的元素顺移，内部使用ArrayList.this.add(i, e)，add之后不允许立刻调用remove
        check modCount 
        add
        expectedModCount = new modCount
        cursor = i + 1; lastRet = -1;
private class SubList extends AbstractList<E> implements RandomAccess
    SubList仍旧继承RandomAccess，但不再是ArrayList类型，仅是AbstractList类型
    SubList底层的数组为原list的数组
    当原数组被原list操作修改，modCount改变后，SubList不能再被使用
    subList迭代器的modCount，初始化时使用原list的modCount
    set使用原数组，add使用父数组（取决于modCount是否变化）
------- 
public void forEach(Consumer<? super E> action)
public Spliterator<E> spliterator()
static final class ArrayListSpliterator<E> implements Spliterator<E>
public boolean removeIf(Predicate<? super E> filter)
public void replaceAll(UnaryOperator<E> operator)
public void sort(Comparator<? super E> c)

## Vector
跟arraylist区别：
Vector每次请求其大小的双倍空间，而ArrayList每次对size增长50%.
所有方法加synchronized
允许null元素

## LinkList
继承了AbstractSequentialList（AbstractList），Deque，Cloneable，Serializable
允许null元素






















