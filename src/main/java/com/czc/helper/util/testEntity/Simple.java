package com.czc.helper.util.testEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by cuizhongcheng on 2017/12/26
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Simple {
    private int index;
    private String city;
}
