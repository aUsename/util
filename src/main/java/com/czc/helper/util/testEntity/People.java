package com.czc.helper.util.testEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by cuizhongcheng on 2017/12/26
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class People {
    private String id;
    private String name;
    private int age;
    private List<Integer> integerList;
    private List<Simple> simpleList;
}
