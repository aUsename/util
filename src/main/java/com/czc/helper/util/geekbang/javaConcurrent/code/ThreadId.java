package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.concurrent.atomic.AtomicLong;

/**
 * ThreadLocal使用示例 会为每个线程分配一个唯一的线程 Id
 * <p>
 * Created by cuizhongcheng on 2022/8/16
 */
public class ThreadId {

    static final AtomicLong nextId = new AtomicLong(0);
    //定义ThreadLocal变量
    static final ThreadLocal<Long> tl = ThreadLocal.withInitial(
            () -> nextId.getAndIncrement()
    );

    //此方法会为每个线程分配一个唯一的Id
    static long get() {
        return tl.get();
    }
}
