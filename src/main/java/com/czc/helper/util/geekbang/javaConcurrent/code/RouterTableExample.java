package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * RPC调用服务本地路由表实现
 * <p>
 * 将Router定义为一个不可变类，当有路由修改或者上下线时，创建或删除Router对象，而不是在Router对象里修改状态等
 * <p>
 * Created by cuizhongcheng on 2022/8/11
 */
public class RouterTableExample {

    //Key:接口名
    //Value:路由集合
    ConcurrentHashMap<String, CopyOnWriteArraySet<Router>> rt = new ConcurrentHashMap<>();

    //根据接口名获取路由表
    public Set<Router> get(String iface) {
        return rt.get(iface);
    }

    //删除路由
    public void remove(Router router) {
        Set<Router> set = rt.get(router.iface);
        if (set != null) {
            set.remove(router);
        }
    }

    //增加路由
    public void add(Router router) {
        Set<Router> set = rt.computeIfAbsent(
                router.iface, r -> new CopyOnWriteArraySet<>());
        set.add(router);
    }

}


//路由信息
final class Router {
    private final String ip;
    private final Integer port;
    final String iface;

    //构造函数
    public Router(String ip, Integer port, String iface) {
        this.ip = ip;
        this.port = port;
        this.iface = iface;
    }

    //重写equals方法
    public boolean equals(Object obj) {
        if (obj instanceof Router) {
            Router r = (Router) obj;
            return iface.equals(r.iface) &&
                    ip.equals(r.ip) &&
                    port.equals(r.port);
        }
        return false;
    }

    public int hashCode() {
        //省略hashCode相关代码
        return 0;
    }
}