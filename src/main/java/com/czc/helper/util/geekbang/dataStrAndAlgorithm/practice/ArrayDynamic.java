package com.czc.helper.util.geekbang.dataStrAndAlgorithm.practice;

/**
 * Created by cuizhongcheng on 2022/9/7
 */
public class ArrayDynamic {

    private String[] array;
    private int size;
    private int count;

    public ArrayDynamic(int size) {
        this.array = new String[size];
        this.count = 0;
        this.size = size;
    }

    public boolean add(String newObj) {
        if (count == size) {
            expand();
        }
        array[count] = newObj;
        count++;
        return true;
    }

    private void expand() {
        String[] newArray = new String[size * 2];
        System.arraycopy(array, 0, newArray, 0, size);
        this.array = newArray;
        size = size * 2;
    }

    public void print(){
        for (int i = 0; i < count; i++) {
            System.out.println( i + " is " + array[i]);
        }
        System.out.println("-------");
    }

    public static void main(String[] args) {
        ArrayDynamic arrayDynamic = new ArrayDynamic(2);
        arrayDynamic.add("a");
        arrayDynamic.add("b");
        arrayDynamic.add("c");
        arrayDynamic.add("d");
        arrayDynamic.add("e");
        arrayDynamic.print();
    }


}
