package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * CountDownLatch使用示例
 * <p>
 * Created by cuizhongcheng on 2022/8/8
 */
public class CountDownLatchExample {
    public static void main(String[] args) throws InterruptedException {
        // 创建2个线程的线程池
        Executor executor = Executors.newFixedThreadPool(2);
        // 存在未对账订单时
        while (true) {
            // 计数器初始化为2
            CountDownLatch latch =
                    new CountDownLatch(2);

            executor.execute(() -> {
                // 查询未对账订单
                // pos = getPOrders();
                latch.countDown();
            });

            executor.execute(() -> {
                // 查询派送单
                // dos = getDOrders();
                latch.countDown();
            });

            // 等待两个查询操作结束
            latch.await();

            // 执行对账操作
            //diff = check(pos, dos);

            // 差异写入差异库
            // save(diff);
        }
    }
}
