package com.czc.helper.util.geekbang.dataStrAndAlgorithm.practice;

/**
 * Created by cuizhongcheng on 2022/9/19
 * <p>
 * 归并排序
 */
public class MergeSort {

    private int[] data;
    private int length;

    public MergeSort(int[] data) {
        this.data = data;
        length = data.length;
    }

    public void sort() {
        if (length <= 1) return;
        // 定义一个workArray，保存中间数据，每次merge后更新至原数组，这样workArray可以不断复用
        int[] workArray = new int[length];
        sort(workArray, 0, length - 1);
    }

    private void sort(int[] workArray, int start, int end) {
        if (end - start == 0) {
            return;
        }
        int mid = (end + start) / 2;
        sort(workArray, start, mid);
        sort(workArray, mid + 1, end);
        merge(workArray, start, mid, end);
    }

    /**
     * 合并
     */
    private void merge(int[] workArray, int start, int mid, int end) {
        int begin1 = start;
        int begin2 = mid + 1;
        int workIndex = start;
        while (begin1 <= mid && begin2 <= end) {
            if (data[begin1] >= data[begin2]) {
                workArray[workIndex] = data[begin1];
                begin1++;
            } else {
                workArray[workIndex] = data[begin2];
                begin2++;
            }
            workIndex++;
        }
        while (begin1 <= mid) {
            workArray[workIndex] = data[begin1];
            workIndex++;
            begin1++;
        }
        while (begin2 <= end) {
            workArray[workIndex] = data[begin2];
            workIndex++;
            begin2++;
        }
        // 复制回原数组，workArray即可重复利用
        for (int i = start; i <= end; i++) {
            data[i] = workArray[i];
        }
    }

    private void display() {
        for (int a : data) {
            System.out.print(a + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] data = {1, 2, 8, 4, 7, 3, 5, 6};
        MergeSort sort = new MergeSort(data);
        sort.sort();
        sort.display();
    }


}
