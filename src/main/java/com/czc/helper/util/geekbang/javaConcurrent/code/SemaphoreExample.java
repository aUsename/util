package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.concurrent.Semaphore;

/**
 * Created by cuizhongcheng on 2022/8/5
 */
public class SemaphoreExample {


    static int count;
    //初始化信号量
    static final Semaphore s = new Semaphore(1);

    //用信号量保证互斥
    static void addOne() throws InterruptedException {
        s.acquire();
        try {
            count += 1;
        } finally {
            s.release();
        }
    }

}
