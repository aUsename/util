package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 利用 CompletionService 实现 Dubbo 中的 Forking Cluster
 * <p>
 * Created by cuizhongcheng on 2022/8/10
 */
public class DubboForkingExample {
    public static void main(String[] args) {

        // 创建线程池
        ExecutorService executor = Executors.newFixedThreadPool(3);

        // 创建CompletionService
        CompletionService<Integer> cs = new ExecutorCompletionService<>(executor);

        // 用于保存Future对象
        // 这个list只是为了后面取消任务，实际获取执行结果用cs就行了
        List<Future<Integer>> futures = new ArrayList<>(3);

        //提交异步任务，并保存future到futures
        futures.add(
                cs.submit(() -> {
                    //  geocoderByS1()
                    return 0;
                }));

        futures.add(
                cs.submit(() -> {
                    // geocoderByS2()
                    return 1;
                }));

        futures.add(
                cs.submit(() -> {
                    // geocoderByS3()
                    return 3;
                }));

        // 获取最快返回的任务执行结果
        Integer r = 0;
        try {
            // 只要有一个成功返回，则break
            for (int i = 0; i < 3; ++i) {
                r = cs.take().get();
                //简单地通过判空来检查是否成功返回
                if (r != null) {
                    break;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            //取消所有任务
            for (Future<Integer> f : futures)
                f.cancel(true);
        }

        System.out.println(r);
    }
}
