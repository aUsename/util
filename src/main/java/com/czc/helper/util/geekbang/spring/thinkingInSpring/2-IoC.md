## IOC容器
### JavaBeans 也是一种IoC容器
- 可以使用Introspector.getBeanInfo()方法，获取对应类的BeanInfo信息
- （spring3.0之前，大量使用PropertyEditorSupport实现输入类型转换）可以使用BeanInfo的getPropertyDescriptors方法，获取到PropertyDescriptor[]，PropertyDescriptor允许添加属性编辑器 PropertyEditor。 自定义类继承PropertyEditor接口的通用实现类PropertyEditorSupport，重写setAsText方法，再将这个自定义类用PropertyDescriptor.setPropertyEditorClass添加进去，即可实现属性编辑

### Spring IoC 容器
- 容器是Spring框架的核心。Spring容器使用DI管理构成应用的组件，它会创建相互协作的组件之间的关联
- Spring自带了多个容器实现，可以归为两种不同的类型
    - Bean工厂，BeanFactory接口定义，是最简单的容器，提供基本的DI支持，默认实现是DefaultListableBeanFactory
    - 应用上下文，ApplicationContext接口定义，是基于BeanFactory的，并提供应用框架级别的服务，例如从属性文件解析文本信息以及发布应用事件给感兴趣的事件监听者。
    - 我们可以在bean工厂和应用上下文之间任选一种，但bean工厂对大多数应用来说往往太低级了，因此，应用上下文要比bean工厂更受欢迎。

### BeanFactory 和 ApplicationContext 区别
- ApplicationContext 是 BeanFactory 的子接口，提供的功能是BeanFactory的超集，所以他俩都是IoC容器
- 但是我们获取的ApplicationContext和BeanFactory并不是同一对象，而是ApplicationContext组合了一个BeanFactory的实现，可以看AbstractApplicationContext.getBean(String name)相关方法，底层用的是getBeanFactory().getBean(name)。而getBeanFactory方法的实现，可以看AbstractRefreshableApplicationContext.getBeanFactory()，实际使用的是DefaultListableBeanFactory beanFactory;
- 所以我们从依赖注入直接获取的，是DefaultListableBeanFactory类型的对象，而自己获取的上下文，是ApplicationContext类型的对象，不是同一个对象。

### ApplicationContext
IoC容器角色，还提供了其他特性
- 面向切面（AOP）
- 配置元信息
- 资源管理
- 事件
- 国际化
- 注解
- Environment抽象

### 示例 自己创建BeanFactory作为IoC容器
    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
    XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
    reader.loadBeanDefinitions("classpath:/META-INF/xx.xml")
此时的IoC容器是BeanFactory，而不是ApplicationContext

### 示例 自己创建ApplicationContext作为IoC容器
    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
    applicationContext.register(xx.class);
    applicationContext.refresh(); // 启动
在xx.class中，通过@Bean注解定义了Bean对象。
示例：ApplicationContextIoC.class

    
### Spring IoC容器的启动停止大致步骤
以 ApplicationContext 为例
启动
- 启动的时候，需要调用ApplicationContext.refresh()方法，启动过程都是在这个方法里做
    - 创建BeanFactory，进行初步初始化，加入内建Bean依赖，非Bean依赖
    - 加入允许自定义的对BeanFactory扩展
    - 加入允许自定义对对Bean的扩展
    - 加入定义国际化事件等诸多特性
停止
- 调用ApplicationContext.close()
    - 销毁所有的Bean
    - 关闭BeanFactory
    - 调用允许的自定义扩展

### BeanFactory 和 FactoryBean 的区别
- BeanFactory 是IoC的底层容器
- FactoryBean 是创建Bean的一种方式，帮助实现复杂的初始化逻辑





## 依赖查找
### Spring IoC 依赖查找
（视频第22）分为三种，根据名称，类型，注解进行查找

先获取到BeanFactory，（Spring应用上下文）
- 根据Bean名称查找
    - 实时查找
        BeanFactory.getBean(String name)
    - 延时查找
        objectFactory = (ObjectFactory)BeanFactory.getBean(String name)
        objectFactory.getObject
        - spring5.1之后，BeanFactory提供了getBeanProvider(Class)方法，返回的是ObjectFactory的实现类ObjectProvider类型
- 根据Bean类型查找
    - 单个Bean对象
        BeanFactory.getBean(Class<T> requiredType)
    - 集合Bean对象
        if(BeanFactory instanceof ListableBeanFactory) 
            Map<String,T> map = ListableBeanFactory.getBeansOfType(Class<T> requiredType)
        - 有getBeansOfType(Class)获取同类型Bean实例列表的方法，还有getBeanNamesForType(Class)获取同类型Bean名称列表的方法，建议优先用后者，因为前者可能会触发一些Bean的提前初始化
    - 层次依赖查找 
        = HierarchicalBeanFactory
- 根据Java注解查找
    - 单个Bean对象
    - 集合Bean对象
        Map<String,T> map = (Map)ListableBeanFactory.getBeansWithAnnotation(Class<? extends Annotation> annotationType) 



      
       
## 依赖注入
### Spring IoC 依赖注入
依赖注入的依赖处理实际是在DefauleListableBeanFactory的resolveDependency方法中处理的，然后再通过反射的方式注入
（视频第23）
- 可以根据名称和类型注入 autowire = name/type
    - 通过名称会严重依赖手工配置，所以一般使用type，且用autowire自动装配
- 可以注入自定义Bean，容器内建Bean，或非Bean对象
- 可以实时注入，延迟注入

### 依赖注入的方式
- 通过set/构造器注入
- 通过在 属性字段 + @Autowired 注入 字段
    - 通过 @Autowired 注解进行注入时，主要处理类依赖于AutowiredAnnotationBeanPostProcessor
    - 其他注解如 @Resource 依赖CommonAnnotationBeanPostProcessor，但流程与@Autowired基本类似
- 在方法上 + @Autowired 注入 方法参数
- 实现 Aware 相关接口，接口回调，注入内建隐藏依赖





## 依赖来源
### Spring IoC 依赖来源哪里
不止我们自定义的Bean
- Bean
    - 自定义Bean
    - 容器内建Bean对象
        如BeanFactory.getBean(Environment.class)
- 容器内建依赖
        如自动依赖注入的BeanFactory对象
        
### Spring IoC 配置元信息
- Bean定义配置
    - XML文件
    - Properties文件
    - java注解
    - java API
    - （通过 Groovy 方式进行DSL配置）
- IoC容器配置
    - XML文件
    - java注解
    - java API
- 外部化属性配置
    - 基于java注解
        如@Value注解















