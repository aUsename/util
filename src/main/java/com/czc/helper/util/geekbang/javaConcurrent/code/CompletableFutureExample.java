package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * CompletableFuture使用示例，与FutureTaskExample.java实现了相关的功能
 * <p>
 * Created by cuizhongcheng on 2022/8/9
 */
public class CompletableFutureExample {

    //        T1:洗水壶...
    //        T2:洗茶壶...
    //        T1:烧开水...
    //        T2:洗茶杯...
    //        T2:拿茶叶...
    //        T1:拿到茶叶:龙井
    //        T1:泡茶...
    //        上茶:龙井
    public static void main(String[] args) {




        // -----------------------------------------------------------------------------------------
        //任务1：洗水壶->烧开水
        CompletableFuture<Void> f1 = CompletableFuture.runAsync(() -> {
            System.out.println("T1:洗水壶...");
            sleep(1, TimeUnit.SECONDS);

            System.out.println("T1:烧开水...");
            sleep(6, TimeUnit.SECONDS);
        });

        //任务2：洗茶壶->洗茶杯->拿茶叶
        CompletableFuture<String> f2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("T2:洗茶壶...");
            sleep(1, TimeUnit.SECONDS);

            System.out.println("T2:洗茶杯...");
            sleep(2, TimeUnit.SECONDS);

            System.out.println("T2:拿茶叶...");
            sleep(1, TimeUnit.SECONDS);
            return "龙井";
        });
        //任务3：任务1和任务2完成后执行：泡茶
        CompletableFuture<String> f3 = f1.thenCombine(f2, (__, tf) -> {
            System.out.println("T1:拿到茶叶:" + tf);
            System.out.println("T1:泡茶...");
            return "上茶:" + tf;
        });

        //等待任务3执行结果
        System.out.println(f3.join());


        // -----------------------------------------------------------------------------------------
        // 串行关系示例
        CompletableFuture<String> f0 =
                CompletableFuture.supplyAsync(
                        () -> "Hello World")      //①
                        .thenApplyAsync(s -> s + " QQ")  //②
                        .thenApply(String::toUpperCase);//③
        //输出结果
        System.out.println(f0.join());


        // -----------------------------------------------------------------------------------------
        // OR 汇聚关系示例
        CompletableFuture<String> fOR1 =
                CompletableFuture.supplyAsync(() -> {
                    int t = 5;
                    sleep(t, TimeUnit.SECONDS);
                    return String.valueOf(t);
                });

        CompletableFuture<String> fOR2 =
                CompletableFuture.supplyAsync(() -> {
                    int t = 10;
                    sleep(t, TimeUnit.SECONDS);
                    return String.valueOf(t);
                });

        CompletableFuture<String> fOR3 =
                CompletableFuture.supplyAsync(() -> {
                    int t = 2;
                    sleep(t, TimeUnit.SECONDS);
                    return String.valueOf(t);
                });

        CompletableFuture<String> fOR4 =
                fOR1.applyToEither(fOR2, s -> s)
                        .applyToEither(fOR3, s -> s);

        System.out.println(fOR4.join());

        // -----------------------------------------------------------------------------------------
        // 异常捕获
        CompletableFuture<Integer> fEx = CompletableFuture
                .supplyAsync(() -> (7 / 0))
                .thenApply(r -> r + 10)
                .exceptionally(e -> 0);
        System.out.println(fEx.join());

    }

    static void sleep(int t, TimeUnit u) {
        try {
            u.sleep(t);
        } catch (InterruptedException e) {
        }
    }
}
