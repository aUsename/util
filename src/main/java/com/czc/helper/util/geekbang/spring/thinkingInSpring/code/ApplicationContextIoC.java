package com.czc.helper.util.geekbang.spring.thinkingInSpring.code;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by cuizhongcheng on 2023/2/5
 */
// @Component
public class ApplicationContextIoC {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(ApplicationContextIoC.class);
        // 启动上下文
        applicationContext.refresh();

        User u = (User) applicationContext.getBean("user");
//        ApplicationContextIoC u = (ApplicationContextIoC) applicationContext.getBean("applicationContextIoC");
        System.out.println("user is: " + u);
    }

    @Bean
    private User user() {
        User user = new User(1, "zhang");
        return user;
    }
}
