package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 生产者-消费者模式-批量执行示例
 * <p>
 * Created by cuizhongcheng on 2022/8/15
 */
public class BatchConsumerExample {

    //任务队列
    BlockingQueue<Task> bq = new LinkedBlockingQueue<>(2000);

    //启动5个消费者线程
    //执行批量任务
    void start() {
        ExecutorService es = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            es.execute(() -> {
                try {
                    while (true) {
                        //获取批量任务
                        List<Task> ts = pollTasks();
                        //执行批量任务
                        execTasks(ts);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    //从任务队列中获取批量任务
    List<Task> pollTasks() throws InterruptedException {
        List<Task> ts = new LinkedList<>();
        // 阻塞式获取一条任务
        // 重要 当队列没有任务时，让线程阻塞，以避免无谓循环浪费资源
        Task t = bq.take();
        while (t != null) {
            ts.add(t);
            // 非阻塞式获取一条任务
            t = bq.poll();
        }
        return ts;
    }

    //批量执行任务
    void execTasks(List<Task> ts) {
        //省略具体代码无数
    }
}

class Task {

}
