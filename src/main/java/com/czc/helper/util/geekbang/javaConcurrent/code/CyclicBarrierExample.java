package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.Vector;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by cuizhongcheng on 2022/8/8
 */
public class CyclicBarrierExample<P, D> {

    // 订单队列
    Vector<P> pos;
    // 派送单队列
    Vector<D> dos;
    // 执行回调的线程池
    Executor executor = Executors.newFixedThreadPool(1);
    // 回调函数，使用了线程池执行
    // CyclicBarrier的回调函数执行在一个回合里最后执行await()的线程上，而且同步调用回调函数check()，调用完check之后，才会开始第二回合。所以check如果不另开一线程异步执行，就起不到性能优化的作用了
    final CyclicBarrier barrier = new CyclicBarrier(2, () -> {
        executor.execute(() -> check());
    });

    void check() {
        P p = pos.remove(0);
        D d = dos.remove(0);

        // 执行对账操作
        // diff = check(p, d);

        // 差异写入差异库
        // save(diff);
    }

    P getPOrders() {
        return (P) new Object();
    }

    D getDOrders() {
        return (D) new Object();
    }

    void checkAll() {
        // 循环查询订单库
        Thread T1 = new Thread(() -> {
            // 存在未对账订单时
            while (true) {
                // 查询订单库
                pos.add(getPOrders());

                // 等待
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }
        });
        T1.start();

        // 循环查询运单库
        Thread T2 = new Thread(() -> {
            // 存在未对账订单时
            while (true) {
                // 查询运单库
                dos.add(getDOrders());

                // 等待
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }
        });
        T2.start();
    }
}
