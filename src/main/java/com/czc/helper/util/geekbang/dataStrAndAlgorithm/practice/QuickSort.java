package com.czc.helper.util.geekbang.dataStrAndAlgorithm.practice;

/**
 * Created by cuizhongcheng on 2022/9/19
 * <p>
 * 快速排序
 */
public class QuickSort {

    private int[] data;
    private int length;

    public QuickSort(int[] data) {
        this.data = data;
        length = data.length;
    }

    public void sort() {
        if (length <= 1) return;
        sort(0, length - 1);
    }

    private void sort(int start, int end) {
        if (start >= end) {
            return;
        }
        // 选取卯点
        int point = data[end];
        int mid = start;
        for (int i = start; i <= end; i++) {
            if (data[i] <= point) {
                if (mid != i) {
                    swap(mid, i);
                }
                mid++;
            }
        }
        sort(start, mid - 2);
        sort(mid, end);
    }

    private void swap(int left, int right) {
        int temp = data[left];
        data[left] = data[right];
        data[right] = temp;
    }


    private void display() {
        for (int a : data) {
            System.out.print(a + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] data = {1, 2, 8, 4, 7, 3, 5, 6};
        QuickSort sort = new QuickSort(data);
        sort.sort();
        sort.display();
    }


}
