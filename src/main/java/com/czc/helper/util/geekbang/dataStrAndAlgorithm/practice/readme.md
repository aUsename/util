## 模块练习
### 数组
- 实现一个支持动态扩容的数组
    - ArrayDynamic
- 实现一个大小固定的有序数组，支持动态增删改操作
    - ArrayRW
- 实现两个有序数组合并为一个有序数组
- https://leetcode.cn/problems/3sum/ 三数之和
    - leetcode  A15ThreeSum
- https://leetcode.cn/problems/majority-element/ 求众数
    - leetcode  A169MajorityElement
- https://leetcode.cn/problems/first-missing-positive/ 求缺失的第一个正数
    - leetcode  A41FirstMissingPositive


### 链表
- 实现单链表、循环链表、双向链表，支持增删操作
- https://leetcode.cn/problems/reverse-linked-list/ 链表反转
    - leetcode  A206ReverseLinkedList
- https://leetcode.cn/problems/merge-two-sorted-lists/ 合并两个有序链表
    - leetcode  A21MergeTwoSortedLists
- https://leetcode.cn/problems/remove-nth-node-from-end-of-list 删除链表倒数第n个结点
    - leetcode  A19RemoveNthNodeFromEndOfList
- 实现求链表的中间结点 876
    - leetcode  A876MiddleOfTheLinkedList
- https://leetcode.cn/problems/linked-list-cycle/ 环形链表 
    - leetcode  A141LinkedListCycle
- https://leetcode.cn/problems/linked-list-cycle-ii/ 环形链表II 
    - leetcode  A142LinkedListCycleii
- https://leetcode.cn/problems/merge-k-sorted-lists/ 合并k个升序链表
    - leetcode  A23MergeKSortedLists
- https://leetcode.cn/problems/palindrome-linked-list/ 判断回文链表
    - leetcode  A234PalindromeLinkedList
- https://leetcode.cn/problems/swap-nodes-in-pairs 交换链表元素（经典题）
    - leetcode A24SwapNodesInPairs 


### 栈
- 用数组实现一个顺序栈
- 用链表实现一个链式栈
- 编程模拟实现一个浏览器的前进、后退功能
- 在函数调用中的应用
- 在表达式求值中的应用
- https://leetcode.cn/problems/valid-parentheses/ 有效的括号
    - leetcode  A20ValidParentheses
- https://leetcode.cn/problems/longest-valid-parentheses/ 最长有效的括号
    - leetcode  A32LongestValidParentheses
- https://leetcode.cn/problems/evaluate-reverse-polish-notation/ 逆波兰表达式求值
    - leetcode  A150EvaluateReversePolishNotation


### 队列
- 数组实现一个顺序队列
- 用链表实现一个链式队列
- 实现一个循环队列
- https://leetcode.cn/problems/design-circular-deque/ 设计一个双端队列
    - leetcode  A641DesignCircularDeque
- https://leetcode.cn/problems/sliding-window-maximum/ 滑动窗口最大值
    - leetcode  A239SlidingWindowMaximum


### 递归
- 编程实现斐波那契数列求值 f(n)=f(n-1)+f(n-2)
    - https://leetcode.cn/problems/fei-bo-na-qi-shu-lie-lcof/
    - Fibonacci 
- 编程实现求阶乘 n!
- https://leetcode.cn/problems/permutations/ 全排列
    - leetcode A46Permutations
- https://leetcode.cn/problems/climbing-stairs/ 爬楼梯
    - leetcode A70ClimbingStairs
写出二叉树遍历、DFS 的递归代码


### 排序
实现归并排序、快速排序、插入排序、冒泡排序、选择排序
    - 归并    MergeSort
    - 快排    QuickSort
如何在 O(n) 的时间复杂度内查找一个无序数组中的第 K 大元素
    - 快排解决


### 二分查找
- 实现一个有序数组的二分查找算法
- 实现模糊二分查找算法（比如大于等于给定值的第一个元素）（二分查找的四个变体）
- https://leetcode.cn/problems/sqrtx/ x 的平方根
    - leetcode A69Sqrtx


### 散列表
- 实现一个基于链表法解决冲突问题的散列表
- 实现一个 LRU 缓存淘汰算法
    - 用链表顺序表示访问顺序
    - 用散列表来快速访问缓存


### 字符串
- 实现一个字符集，只包含 a～z 这 26 个英文字母的 Trie 树
- 实现朴素的字符串匹配算法
- https://leetcode.cn/problems/reverse-string/ 反转字符串
    - leetcode A344rReverseString
- https://leetcode.cn/problems/reverse-words-in-a-string/ 翻转字符串里的单词
    - leetcode A151ReverseWordsInAString
- https://leetcode.cn/problems/string-to-integer-atoi/ 字符串转换整数 (atoi)
    - leetcode A8StringToIntegerAtoi
- https://leetcode.cn/problems/longest-palindromic-substring 寻找最长回文子串
    - leetcode A5LongestPalindromicSubstring


### 二叉树
- 实现一个二叉查找树，并且支持插入、删除、查找操作实现
- 查找二叉查找树中某个节点的后继、前驱节点
- 实现二叉树前、中、后序以及按层遍历
- https://leetcode.cn/problems/invert-binary-tree/ 翻转二叉树
    - leetcode A226InvertBinaryTree
- https://leetcode.cn/problems/maximum-depth-of-binary-tree/ 二叉树的最大深度
    - leetcode A104MaximumDepthOfBinaryTree
- https://leetcode.cn/problems/validate-binary-search-tree/ 验证二叉查找树
    - leetcode A98ValidateBinarySearchTree
- https://leetcode.cn/problems/path-sum/ 路径总和
    - leetcode A112PathSum


### 堆 
- 实现一个小顶堆、大顶堆、优先级队列
- 实现堆排序
- 利用优先级队列合并 K 个有序数组
- 求一组动态数据集合的最大 Top K
代码实现堆、堆排序，并且掌握堆的三种应用（优先级队列、Top k、中位数）


### 图
- 实现有向图、无向图、有权图、无权图的邻接矩阵和邻接表表示方法
- 实现图的深度优先搜索、广度优先搜索
- 实现 Dijkstra 算法、A* 算法
- 实现拓扑排序的 Kahn 算法、DFS 算法
- https://leetcode.cn/problems/number-of-islands/description/ 岛屿的个数
    - leetcode A200NumberOfIslands
- https://leetcode.cn/problems/valid-sudoku/ 有效的数独
    - leetcode A36ValidSudoku


### 回溯
- https://leetcode.cn/problems/n-queens/ N皇后
    - leetcode A51NQueens 
- 利用回溯算法求解 0-1 背包问题
- https://leetcode.cn/problems/regular-expression-matching/ 正则表达式匹配
    - leetcode A10RegularExpressionMatching


### 分治
- 利用分治算法求一组数据的逆序对个数


### 动态规划
- 0-1 背包问题
- 编程实现莱文斯坦最短编辑距离
- 编程实现查找两个字符串的最长公共子序列
- 编程实现一个数据序列的最长递增子序列
- https://leetcode.cn/problems/minimum-path-sum/ 最小路径和
    - leetcode A64MinimumPathSum
- https://leetcode.cn/problems/coin-change/ 零钱兑换
    - leetcode A322CoinChange
- https://leetcode.cn/problems/best-time-to-buy-and-sell-stock/ 买卖股票的最佳时机
    - leetcode A121BestTimeToBuyAndSellStock
- https://leetcode.cn/problems/maximum-product-subarray/ 乘积最大子序列
    - leetcode A152MaximumProductSubarray
- https://leetcode.cn/problems/triangle/ 三角形最小路径和
    - leetcode A120Triangle


### 海量问题处理
- 比如说，我们有 10GB 的订单数据，我们希望按订单金额（假设金额都是正整数）进行排序，但是我们的内存有限，只有几百 MB，没办法一次性把 10GB 的数据都加载到内存中。这个时候该怎么办呢？
- 如果你所在的省有 50 万考生，如何通过成绩快速排序得出名次呢？
- 假设我们有 10 万个手机号码，希望将这 10 万个手机号码从小到大排序，你有什么比较快速的排序方法呢？
- 假设我们有 1000 万个整型数据，每个数据占 8 个字节，如何设计数据结构和算法，快速判断某个整数是否出现在这 1000 万数据中？ 我们希望这个功能不要占用太多的内存空间，最多不要超过 100MB，你会怎么做呢？






## 实战测试题
### 一
假设猎聘网有 10 万名猎头顾问，每个猎头顾问都可以通过做任务（比如发布职位），来积累积分，然后通过积分来下载简历。假设你是猎聘网的一名工程师，如何在内存中存储这 10 万个猎头 ID 和积分信息，让它能够支持这样几个操作：
- 根据猎头的 ID 快速查找、删除、更新这个猎头的积分信息；
- 查找积分在某个区间的猎头 ID 列表；
- 查询积分从小到大排在第 x 位的猎头 ID 信息；
- 查找按照积分从小到大排名在第 x 位到第 y 位之间的猎头 ID 列表。


### 二
电商交易系统中，订单数据一般都会很大，我们一般都分库分表来存储。假设我们分了 10 个库并存储在不同的机器上，在不引入复杂的分库分表中间件的情况下，我们希望开发一个小的功能，能够快速地查询金额最大的前 K 个订单（K 是输入参数，可能是 1、10、1000、10000，假设最大不会超过 10 万）。如果你是这个功能的设计开发负责人，你会如何设计一个比较详细的、可以落地执行的设计方案呢？
- 数据库中，订单表的金额字段上建有索引，我们可以通过 select order by limit 语句来获取数据库中的数据；
- 我们的机器的可用内存有限，比如只有几百 M 剩余可用内存。希望你的设计尽量节省内存，不要发生 Out of Memory Error。


### 三
我们知道，CPU 资源是有限的，任务的处理速度与线程个数并不是线性正相关。相反，过多的线程反而会导致 CPU 频繁切换，处理性能下降。所以，线程池的大小一般都是综合考虑要处理任务的特点和硬件环境，来事先设置的。
当我们向固定大小的线程池中请求一个线程时，如果线程池中没有空闲资源了，这个时候线程池如何处理这个请求？是拒绝请求还是排队请求？各种处理策略又是怎么实现的呢？


### 四
通过 IP 地址来查找 IP 归属地的功能，不知道你有没有用过？没用过也没关系，你现在可以打开百度，在搜索框里随便输一个 IP 地址，就会看到它的归属地。
这个功能并不复杂，它是通过维护一个很大的 IP 地址库来实现的。地址库中包括 IP 地址范围和归属地的对应关系。比如，当我们想要查询 202.102.133.13 这个 IP 地址的归属地时，我们就在地址库中搜索，发现这个 IP 地址落在[202.102.133.0, 202.102.133.255]这个地址范围内，那我们就可以将这个 IP 地址范围对应的归属地“山东东营市”显示给用户了。
在庞大的地址库中逐一比对 IP 地址所在的区间，是非常耗时的。假设在内存中有 12 万条这样的 IP 区间与归属地的对应关系，如何快速定位出一个 IP 地址的归属地呢？


### 五
假设我们现在希望设计一个简单的海量图片存储系统，最大预期能够存储 1 亿张图片，并且希望这个海量图片存储系统具有下面这样几个功能：
- 存储一张图片及其它的元信息，主要的元信息有：图片名称以及一组 tag 信息。比如图片名称叫玫瑰花，tag 信息是{红色，花，情人节}；
- 根据关键词搜索一张图片，比如关键词是“情人节 花”“玫瑰花”；
- 避免重复插入相同的图片。这里，我们不能单纯地用图片的元信息，来比对是否是同一张图片，因为有可能存在名称相同但图片内容不同，或者名称不同图片内容相同的情况。
我们希望自主开发一个简单的系统，不希望借助和维护过于复杂的三方系统，比如数据库（MySQL、Redis 等）、分布式存储系统（GFS、Bigtable 等），并且我们单台机器的性能有限，比如硬盘只有 1TB，内存只有 2GB，如何设计一个符合我们上面要求，操作高效，且使用机器资源最少的存储系统呢？


### 六
- 我们知道，散列表的查询效率并不能笼统地说成是 O(1)。它跟散列函数、装载因子、散列冲突等都有关系。如果散列函数设计得不好，或者装载因子过高，都可能导致散列冲突发生的概率升高，查询效率下降。
- 在极端情况下，有些恶意的攻击者，还有可能通过精心构造的数据，使得所有的数据经过散列函数之后，都散列到同一个槽里。如果我们使用的是基于链表的冲突解决方法，那这个时候，散列表就会退化为链表，查询的时间复杂度就从 O(1) 急剧退化为 O(n)。
- 如果散列表中有 10 万个数据，退化后的散列表查询的效率就下降了 10 万倍。更直观点说，如果之前运行 100 次查询只需要 0.1 秒，那现在就需要 1 万秒。这样就有可能因为查询操作消耗大量 CPU 或者线程资源，导致系统无法响应其他请求，从而达到拒绝服务攻击（DoS）的目的。这也就是散列表碰撞攻击的基本原理。
- 如何设计一个可以应对各种异常情况的工业级散列表，来避免在散列冲突的情况下，散列表性能的急剧下降，并且能抵抗散列碰撞攻击？





## 实战测试题答案
### 一
- 这个问题既要通过 ID 来查询，又要通过积分来查询，所以，对于猎头这样一个对象，我们需要将其组织成两种数据结构，才能支持这两类操作。
- 我们按照 ID，将猎头信息组织成散列表。这样，就可以根据 ID 信息快速地查找、删除、更新猎头的信息。我们按照积分，将猎头信息组织成跳表这种数据结构，按照积分来查找猎头信息，就非常高效，时间复杂度是 O(logn)。
- 我刚刚讲的是针对第一个、第二个操作的解决方案。第三个、第四个操作是类似的，按照排名来查询，这两个操作该如何实现呢？
- 我们可以对刚刚的跳表进行改造，每个索引结点中加入一个 span 字段，记录这个索引结点到下一个索引结点的包含的链表结点的个数。这样就可以利用跳表索引，快速计算出排名在某一位的猎头或者排名在某个区间的猎头列表。
- 实际上，这些就是 Redis 中有序集合这种数据类型的实现原理。在开发中，我们并不需要从零开始代码实现一个散列表和跳表，我们可以直接利用 Redis 的有序集合来完成。


### 二
- 解决这个题目的基本思路我想你应该能想到，就是借助归并排序中的合并函数，这个我们在排序（下）以及堆的应用那一节中讲过。
- 我们从每个数据库中，通过 select order by limit 语句，各取局部金额最大的订单，把取出来的 10 个订单放到优先级队列中，取出最大值（也就是大顶堆堆顶数据），就是全局金额最大的订单。然后再从这个全局金额最大订单对应的数据库中，取出下一条订单（按照订单金额从大到小排列的），然后放到优先级队列中。一直重复上面的过程，直到找到金额前 K（K 是用户输入的）大订单。
- 从算法的角度看起来，这个方案非常完美，但是，从实战的角度来说，这个方案并不高效，甚至很低效。因为我们忽略了，数据库读取数据的性能才是这个问题的性能瓶颈。所以，我们要尽量减少 SQL 请求，每次多取一些数据出来，那一次性取出多少才合适呢？这就比较灵活、比较有技巧了。一次性取太多，会导致数据量太大，SQL 执行很慢，还有可能触发超时，而且，我们题目中也说了，内存有限，太多的数据加载到内存中，还有可能导致 Out of Memory Error。
- 所以，一次性不能取太多数据，也不能取太少数据，到底是多少，还要根据实际的硬件环境做 benchmark 测试去找最合适的。


### 三
- 这个问题的答案涉及队列这种数据结构。队列可以应用在任何有限资源池中，用于排队请求，比如数据库连接池等。实际上，对于大部分资源有限的场景，当没有空闲资源时，基本上都可以通过“队列”这种数据结构来实现请求排队。
- 这个问题的具体答案，在队列那一节我已经讲得非常详细了，你可以回去看看，这里我就不赘述了。


### 四
- 这个问题可以用二分查找来解决，不过，普通的二分查找是不行的，我们需要用到二分查找的变形算法，查找最后一个小于等于某个给定值的数据。不过，二分查找最难的不是原理，而是实现。要实现一个二分查找的变形算法，并且实现的代码没有 bug，可不是一件容易的事情，不信你自己写写试试。
- 关于这个问题的解答以及写出 bug free 的二分查找代码的技巧，我们在二分查找（下）那一节有非常详细的讲解，你可以回去看看，我这里就不赘述了。


### 五
- 这个问题可以分成两部分，第一部分是根据元信息的搜索功能，第二部分是图片判重。
- 第一部分，我们可以借助搜索引擎中的倒排索引结构。关于倒排索引我会在实战篇详细讲解，我这里先简要说下。
- 如题目中所说，一个图片会对应一组元信息，比如玫瑰花对应{红色，花，情人节}，牡丹花对应{白色，花}，我们可以将这种图片与元信息之间的关系，倒置过来建立索引。“花”这个关键词对应{玫瑰花，牡丹花}，“红色”对应{玫瑰花}，“白色”对应{牡丹花}，“情人节”对应{玫瑰花}。
- 当我们搜索“情人节 花”的时候，我们拿两个搜索关键词分别在倒排索引中查找，“花”查找到了{玫瑰花，牡丹花}，“情人节”查找到了{玫瑰花}，两个关键词对应的结果取交集，就是最终的结果了。
- 第二部分关于图片判重，我们要基于图片本身来判重，所以可以用哈希算法，对图片内容取哈希值。我们对哈希值建立散列表，这样就可以通过哈希值以及散列表，快速判断图片是否存在。
- 我这里只说说我的思路，这个问题中还有详细的内存和硬盘的限制。要想给出更加详细的设计思路，还需要根据这些限制，给出一个估算。详细的解答，我都放在哈希算法（下）那一节里了，你可以自己回去看。


### 六
- 我经常把这道题拿来作为面试题考察候选人。散列表可以说是我们最常用的一种数据结构了，编程语言中很多数据类型，都是用散列表来实现的。尽管很多人能对散列表都知道一二，知道有几种散列表冲突解决方案，知道散列表操作的时间复杂度，但是理论跟实践还是有一定距离的。光知道这些基础的理论并不足以开发一个工业级的散列表。
- 所以，我在散列表（中）那一节中详细给你展示了一个工业级的散列表要处理哪些问题，以及如何处理的，也就是这个问题的详细答案。