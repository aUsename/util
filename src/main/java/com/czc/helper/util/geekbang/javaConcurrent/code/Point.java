package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.concurrent.locks.StampedLock;

/**
 * StampedLock官方示例
 * <p>
 * Created by cuizhongcheng on 2022/8/8
 */
class Point {
    private int x, y;
    final StampedLock sl = new StampedLock();

    //计算到原点的距离
    double distanceFromOrigin() {
        // 乐观读
        long stamp = sl.tryOptimisticRead();
        // 读入局部变量，
        // 读的过程数据可能被修改
        int curX = x, curY = y;
        //判断执行读操作期间，是否存在写操作，如果存在，则sl.validate返回false
        if (!sl.validate(stamp)) {
            // 升级为悲观读锁
            stamp = sl.readLock();
            try {
                curX = x;
                curY = y;
            } finally {
                //释放悲观读锁
                sl.unlockRead(stamp);
            }
        }
        // 执行业务操作
        return Math.sqrt(curX * curX + curY * curY);
    }
}
