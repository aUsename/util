package com.czc.helper.util.geekbang.spring.thinkingInSpring.code;

import lombok.Data;

/**
 * Created by cuizhongcheng on 2023/2/5
 */
@Data
public class User {
    private int age;
    private String name;

    public User(int age, String name) {
        this.age = age;
        this.name = name;
    }
}
