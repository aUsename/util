package com.czc.helper.util.geekbang.dataStrAndAlgorithm.practice;

/**
 * Created by cuizhongcheng on 2022/9/7
 * <p>
 * 好像实现的不对，要求是有序数组，这里只实现了一个动态增删的数组
 */
public class ArrayRW {

    private String[] array;
    private int size;
    private int count;

    public ArrayRW(int size) {
        this.array = new String[size];
        this.count = 0;
        this.size = size;
    }

    public boolean insert(int index, String newStr) {
        if (count >= size) return false;
        if (index > count || index < 0) return false;

        for (int i = count; i > index; i--) {
            array[i] = array[i - 1];
        }

        array[index] = newStr;
        count++;
        return true;
    }

    public boolean remove(int index) {
        if (index < 0 || index > count - 1) return false;
        if (count <= 0) return false;

        for (int i = index; i < count - 1; i++) {
            array[i] = array[i + 1];
        }

        array[count - 1] = null;
        count--;
        return true;
    }

    public void print() {
        for (int i = 0; i < count; i++) {
            System.out.println(i + " is " + array[i]);
        }
        System.out.println("-------");
    }

    public static void main(String[] args) {
        ArrayRW arrayRW = new ArrayRW(10);
        arrayRW.insert(0, "a");
        arrayRW.print();
        arrayRW.insert(1, "b");
        arrayRW.print();
        arrayRW.insert(2, "d");
        arrayRW.print();
        arrayRW.insert(2, "c");
        arrayRW.print();
        arrayRW.remove(0);
        arrayRW.print();
        arrayRW.remove(0);
        arrayRW.print();
    }

}
