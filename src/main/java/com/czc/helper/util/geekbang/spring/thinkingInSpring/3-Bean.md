## Bean 生命周期
- 前置准备：配置 -> 解析（读取）
- Bean元信息注册：其实就是对应的register方法
- BeanDefinition 合并
- 类加载
- 实例化：前置操作 -> 反射进行实例化/CGiLAB进行提升 -> 后置操作
- 属性赋值：前置操作 -> 属性赋值
- 初始化：众多Aware接口回调（也算是可以做属性赋值） -> BeanPostProcessor接口前置方法 -> 初始化方法（@PostConstruct注解 -> 实现InitializingBean接口 -> 自定义接口）-> BeanPostProcessor接口后置方法
- 初始化完成：SmartInitializingSingleton#afterSingletonsInstantiated
- 销毁：销毁前回调（DestructionAwareBeanPostProcessor接口前置方法,包含@PreDstroy注解标注但方法）-> 实现DisposableBean接口 -> 自定义接口
- 垃圾回收：spring应用上下文关闭 -> GC


### Bean作用域
Bean作用域指的是BeanDefinition的作用域
- 单例（Singleton）：在整个应用中不一定唯一，但是在同一个上下文里只创建bean的一个实例。 
- 原型（Prototype）：每次注入或者通过Spring应用上下文获取的时候，都会创建一个新的bean实例。 
- 会话（Session）：在Web应用中，为每个会话创建一个bean实例。 
- 请求（Rquest）：在Web应用中，为每个请求创建一个bean实例。

### BeanDefinition
BeanDefinition 是 spring中 定义Bean的配置元信息接口，包含
- Bean的类名
- Bean行为配置元素，如作用域，自动绑定的模式，生命周期回调等
- 其他Bean引用，又可称合作者（Collaborators）或者依赖（Dependencies）
- 配置设置，比如Bean属性（Properties）

### BeanDefinition配置方式 & 解析器
- 资源配置 （BeanDefinitionReader）
    - XML文件配置 
    - properties文件配置
- java注解配置 (AnnotatedBeanDefinitionReader)
- java API配置元信息

### 获取 BeanDefinition
通过 BeanDefinitionBuilder beanDefinitionBuilder = genericBeanDefinition(Class<?> beanClass) 可以拿到对应类的BeanDefinition （beanDefinitionBuilder.getBeanDefinition）

### BeanDefinition注册
Bean的注册一般情况下都是基于BeanDefinition的注册，特殊情况：使用AnnotationConfigApplicationContext的registerSingleton方法，直接注册一个单例的对象
（视频第35）
- XML文件配置 
    - <bean name="..." .../>
- properties文件配置
- java注解配置
    - @Bean
        - 与2-IoC里 "示例 自己创建ApplicationContext作为IoC容器" 相同
    - @Component
        - 与@Bean的区别在于，示例代码 ApplicationContextIoC 中，@Bean打在方法上，可以把@Component放到类上，也可以实现注入
    - @Import
        - 在使用类上，加上注解@Import(xx.class)，可以注入xx类
- java API配置元信息
    - 命名方式 BeanDefinitionRegistry#registerBeanDefinition(String beanName, BeanDefinition)
        - 我们常用的 AnnotationConfigApplicationContext ，就是一个 BeanDefinition 接口的实现类
    - 非命名方式 BeanDefinitionReaderUtil#registerWithGeneratedName(AbstractBeanDefinition, BeanDefinitionRegistry)
        - 区别就在于不需要传入beanName
    - 配置类方式 AnnotatedBeanDefinitionReader#register()
        - AnnotationConfigApplicationContext.register 底层用的就是这个方法
        
### Spring Bean 实例化
（视频第36）
- 常规方式
    - 通过构造器（XML，注解，javaAPI）
    - 通过静态工厂方法（XML，javaAPI）
        - 如XML配置时，指定 factory-method 为对应的静态方法
    - 通过Bean工厂方法（XML，javaAPI）
        - 如XML配置时，指定 factory-bean 为对应的工厂类，factory-method 为对应的方法
    - 通过FactoryBean（XML，注解，javaAPI）
        - 一个实现了FactoryBean接口的类，返回对应的Bean对象实例 （FactoryBean是创建Bean的一种方式，帮助实现复杂的初始化逻辑）
- 特殊方式
    - ServiceLoaderFactoryBean
        - ServiceLoader是java的一个传统API，使用ServiceLoader可以加载相应资源文件（META-INF/services目录），是JDK对反转控制的一种实现
        - 与FactoryBean方法配合使用
    - AutowireCapableBeanFactory#createBean(java.lang.Class, int ,boolean)
        - ApplicationContext.getAutowireCapableBeanFactory().createBean() ，传入的class需要是个工厂类
    - BeanDefinitionRegistry#registerBeanDefinition(String beanName, BeanDefinition)
        - 同BeanDefinition注册时的方法
        
### Spring Bean 初始化
（视频第37）
- (顺序1)@PostConstruct标注方法
- (顺序2)实现InitializingBean接口的afterPropertiesSet()方法
- (顺序3)自定义初始化方法
    - XML: <bean init-method="init" ... />
    - Java注解: @Bean(initMethod="init")
    - Java API: AbstractBeanDefinition#setInitMethodName(String)

### Bean 延迟初始化
非延迟初始化Bean，在Spring上下文启动完成后就被初始化，而延迟初始化的Bean是按需初始化
- XML
- 注解 @Lazy(true)

        
### Spring Bean 销毁
（视频第39）
- (顺序1)@PreDestroy标注方法
- (顺序2)实现DisposableBean接口的destroy()方法
- (顺序3)自定义销毁方法
    - XML: <bean destroy="destroy" ... />
    - Java注解: @Bean(destroy="destroy")
    - Java API: AbstractBeanDefinition#setDestroyMethodName(String)
    
### BeanPostProcessor的使用场景有哪些
- BeanPostProcessor提供了Bean初始化前和初始化后的生命周期回调，分别对应postProcessBeforeInitialization() 和 postProcessAfterInitialization()方法，允许对关心对Bean进行扩展，甚至替换
- 其中 ApplicationContext相关对Aware回调，也是基于BeanPostProcessor实现，即ApplicationContextAwareProcessor

### BeanFactoryPostProcessor 与 BeanPostProcessor的区别
- BeanFactoryPostProcessor是BeanFactory（实际是ConfigurableListableBeanFactory）的后置处理器，用于扩展BeanFactory，或通过BeanFactory进行依赖查找和依赖注入
- BeanFactoryPostProcessor必须由ApplicationContext执行，BeanFactory无法直接与其交互
- BeanPostProcessor则直接与BeanFactory关联，属于N对1的关系


### AbstractAutowireCapableBeanFactory类的doCreateBean()方法
- 1.实例化bean
- 2.应用后置处理器修改bean definition
- 3.饥饿加载单例bean，解决循环依赖问题
- 4.如何解决循环依赖问题的呢，就是提前将实例化的bean放入缓存，以beanName->bean的引用的方式放入map，在createBean的上一步getBean的时候会从缓存中先拿bean。
- 5.初始化bean实例
- 6.设置属性，如果属性是依赖注入的其他bean，走一遍getBean方法
- 7.初始化，调用aware方法、bean后置处理器的初始化前方法、初始化方法、bean后置处理器的初始化后方法
- 8.注册需要执行销毁回调的单例bean，比如在销毁方法中释放资源。









