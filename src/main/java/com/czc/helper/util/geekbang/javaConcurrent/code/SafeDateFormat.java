package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * ThreadLocal使用示例 线程安全的DateFormat
 * <p>
 * Created by cuizhongcheng on 2022/8/16
 */
public class SafeDateFormat {

    //定义ThreadLocal变量
    static final ThreadLocal<DateFormat> tl = ThreadLocal.withInitial(
            () -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    );

    static DateFormat get() {
        return tl.get();
    }

    //不同线程执行下面代码
    //返回的df是不同的
    // DateFormat df = SafeDateFormat.get();

}
