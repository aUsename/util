## AOP术语
### 通知 Advice
Spring切面可以应用5种类型的通知
- 前置通知（Before）：在目标方法被调用之前调用通知功能； 
- 后置通知（After）：在目标方法完成之后调用通知，此时不会关心方法的输出是什么； 
- 返回通知（After-returning）：在目标方法成功执行之后调用通知； 
- 异常通知（After-throwing）：在目标方法抛出异常后调用通知； 
- 环绕通知（Around）：通知包裹了被通知的方法，在被通知的方法调用之前和调用之后执行自定义的行为。

### 切点 Pointcut
- 通知定义了切面的“什么”和“何时”，那么切点就定义了“何处”。
- 切点的定义会匹配通知所要织入的一个或多个连接点。我们通常使用 明确的类和方法名称，或是利用正则表达式定义所匹配的类和方法名称来指定这些切点。有些AOP框架允许我们创建动态的切点，可以根据运 行时的决策（比如方法的参数值）来决定是否应用通知。

### 切面 Aspect
- 切面是通知和切点的结合。通知和切点共同定义了切面的全部内容——它是什么，在何时和何处完成其功能。

### 连接点 join point
- 我们的应用可能有数以千计的时机应用通知。这些时机被称为连接点。连接点是在应用执行过程中能够插入切面的一个点。这个点可 以是调用方法时、抛出异常时、甚至修改一个字段时。切面代码可以利用这些点插入到应用的正常流程之中，并添加新的行为

### 引入 Introduction
- 引入允许我们向现有的类添加新方法或属性。

### 织入 Weaving
织入是把切面应用到目标对象并创建新的代理对象的过程。切面在指定的连接点被织入到目标对象中。在目标对象的生命周期里有多个点可以 进行织入：
- 编译期：切面在目标类编译时被织入。这种方式需要特殊的编译器。AspectJ的织入编译器就是以这种方式织入切面的。 
- 类加载期：切面在目标类加载到JVM时被织入。这种方式需要特殊的类加载器（ClassLoader），它可以在目标类被引入应用之前增 强该目标类的字节码。AspectJ 5的加载时织入（load-time weaving，LTW）就支持以这种方式织入切面。 
- 运行期：切面在应用运行的某个时刻被织入。一般情况下，在织入切面时，AOP容器会为目标对象动态地创建一个代理对象。Spring AOP就是以这种方式织入切面的。




## Spring 对 AOP 的支持
Spring提供了4种类型的AOP支持：
- 基于代理的经典Spring AOP； 
- 纯POJO切面；
- @AspectJ注解驱动的切面； 
- 注入式AspectJ切面（适用于Spring各版本）

前三种都是Spring AOP实现的变体，Spring AOP构建在动态代理基础之上，因此，Spring对AOP的支持局限于方法拦截。

### Spring在运行时通知对象
- 通过在代理类中包裹切面，Spring在运行期把切面织入到Spring管理的bean中。
- 代理类封装了目标类，并拦截被通知方法的调 用，再把调用转发给真正的目标bean。当代理拦截到方法调用时，在调用目标bean方法之前，会执行切面逻辑。
- 直到应用需要被代理的bean时，Spring才创建代理对象。如果使用的是ApplicationContext的话，在ApplicationContext从BeanFactory中加载所有bean的时候，Spring才会创建被代理的对象。因为Spring运行时才创建代理对象，所 以我们不需要特殊的编译器来织入Spring AOP的切面。

### Spring只支持方法级别的连接点
- AOP方案可以支持多种连接点模型。因为Spring基于动态代理，所以Spring只支持方法连接点。




## 通过切点选择连接点
### AspectJ切点表达式语言
- Spring AOP 中，需要使用AspectJ切点表达式语言来定义切点，如@annotation()限定匹配带有指定注解，execution执行匹配
- Spring还引入了一个新的bean()指示器，它允许我们在切点表达式中使用bean的ID来标识bean。bean()使用 bean ID或bean名称作为参数来限制切点只匹配特定的bean。



## 使用注解创建切面
- 使用@Aspect注解可以定义一个切面
- @Before @After @AfterReturn @AfterThrowing @Around通知注解，放在方法上,表明它们应该在什么时候调用
    - @Around 接受ProceedingJoinPoint作为参数
- @Pointcut注解能够在一个@AspectJ切面内定义可重用的切点。然后其他通知注解可以直接使用这个切点
- 还需要将这个切面注册为Spring的一个Bean，同时启用自动代理@EnableAspectJAutoProxy（springboot应该就不用了）
    - AspectJ自动代理都会为使用@Aspect注解的bean创建一个代理，这个代理会围绕着所有该切面的切点所匹配的bean。

### 使用注解对被包装Bean添加新方法
AOP是对被包装Bean做动态代理，即在方法调用前后做动作，但也提供对被包装Bean添加新方法的功能
- 在@Aspect注解的切面中，使用@DeclareParents(value="",defaultImpl=xx.class)注解，再将这个切面注册到Spring中
    - value属性 指定了哪种类型的bean要引入该接口
    - defaultImpl属性 指定为引入功能提供实现的类
    - 注解本身，所标注的静态属性指明了要引入了接口



