package com.czc.helper.util.geekbang.javaConcurrent.code;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

/**
 * Akka简单使用示例
 * <p>
 * Created by cuizhongcheng on 2022/8/18
 */
public class HelloActor extends UntypedAbstractActor {
    @Override
    public void onReceive(Object message) {
        System.out.println("Hello " + message);
    }

    public static void main(String[] args) {
        //创建Actor系统
        ActorSystem system = ActorSystem.create("HelloSystem");
        //创建HelloActor
        ActorRef helloActor = system.actorOf(Props.create(HelloActor.class));
        //发送消息给HelloActor
        helloActor.tell("Actor", ActorRef.noSender());
    }
}


