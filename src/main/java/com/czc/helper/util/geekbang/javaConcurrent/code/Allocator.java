package com.czc.helper.util.geekbang.javaConcurrent.code;

import java.util.List;

/**
 * java synchronized实现管程
 * 本例子是一次性申请两个对象的使用权，所以对两个对象一起加锁
 * <p>
 * Created by cuizhongcheng on 2022/8/2
 */
class Allocator {
    private List als;

    // 一次性申请所有资源
    synchronized void apply(Object from, Object to) {
        // 经典写法 （范式，使用while语句）
        while (als.contains(from) || als.contains(to)) {
            try {
                // synchronized锁的是非静态方法，即锁的是当前实例对象，所以这里相当于this.wait()
                wait();
            } catch (Exception e) {
            }
        }
        als.add(from);
        als.add(to);
    }

    // 归还资源
    synchronized void free(Object from, Object to) {
        als.remove(from);
        als.remove(to);
        // 相当于this.notifyAll();
        // 尽量使用notifyAll()，通知所有持有this锁的线程
        notifyAll();
    }
}
