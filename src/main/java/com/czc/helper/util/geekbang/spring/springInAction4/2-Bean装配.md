# Bean主要的装配的方式
- 在XML中进行显式配置
- 在Java中进行显式配置
- 隐式的bean发现机制和自动装配（建议）



## Spring自动动化装配Bean
尽可能地使用自动配置的机制。显式配置越少越好。Spring从两个角度来实现自动化装配：
- 组件扫描（component scanning）：Spring会自动发现应用上下文中所创建的bean
- 自动装配（autowiring）：Spring自动满足bean之间的依赖。

### 基于注解和扫描实现Bean注册和发现
- 自定义类，带有@Component注解
    - 也可以使用@Named注解，与@Component作用基本相同
- 自定义Java配置类，使用@ComponentScan注解，这个注解能够在Spring中启用组件扫描
    - @ComponentScan默认会扫描与配置类相同的包
    - @ComponentScan带有basePackages，可以设置多个扫描基础包
    - @ComponentScan还提供了basePackageClasses参数，可以指定相关class，并将这些类所在的包作为组件扫描的基础包。
    - 也可以使用xml的方式启用组件扫描

### 基于注解实现自动装配
- 使用@Autowired，即可实现构造器，set方法的依赖自动装配
    - java提供的替代类 @Inject，同@Named相对@Component



## 基于java代码装配Bean
有时候我们依赖的Bean是第三方包内的，无法加上@Component注解，可以在Java中进行显式配置

### 注册Bean
- 首先需要一个配置类，带有@Configuration注解的类，声明为配置类
- 在这个类中，创建方法，该方法使用@Bean注解，返回对应的对象，即可将该对象注册为spring中的一个Bean
- 方法加上@Bean注解后，Spring将会拦截所有对它的调用，并确保直接返回该方法所创建的bean，而不是每次都对其进行实际的调用
- 同时使用类@Bean注解的其他方法，若依赖其他带有@Bean注解的对象（也可以是通过扫描发现或者xml注册的），spring也可以实现自动装配



# 高级装配特性

## profile
- 在类或者方法上，使用用@Profile注解。它会告诉Spring这个配置类/方法的bean只有在对应profile激活时才会创建。如：@Profile("dev") @Profile("prod")
- 如果设置了spring.profiles.active属性，那么它的值就会用来确定哪个profile是激活的。如果没有设置spring.profiles.active，Spring将会查找spring.profiles.default的值。如果均没有设置的话，那就没有激活的profile，因此只会创建那些没有定义在profile中的bean。设置方式：
    - 作为DispatcherServlet的初始化参数； 
    - 作为Web应用的上下文参数； 
    - 作为JNDI条目； 
    - 作为环境变量； 
    - 作为JVM的系统属性； 
    - 在集成测试类上，使用@ActiveProfiles注解设置

## 条件化配置Bean
- 可以在Bean上使用@Conditional(xx.class)注解，而xx.class需要继承Condition接口，实现matches()方法，返回boolean来决定对应Bean是否创建
- spring4之后，@Profile也借用@Conditional来实现

## 消除Bean自动装配的歧义
- 根据类型注入，对相同类型，可以使用@Primary注解标识首选Bean
- 在Bean和使用方，使用限定符@Qualifier注解，来确认需要装备的Bean
- 对需要使用多个限定符的场景，由于Java不允许在同一个条目上重复出现相同类型的多个注解，所以可以对每个特性（限定符），自定义一个注解，在自定义注解中，再使用@Qualifier注解(Java8允许出现重复的注解，只要这个注解本身在定义的时候带有@Repeatable注解就可以。)

## Bean的作用域
修改Bean的作用域，需要与@Scope(ConfigurableBeanFactory.xx)注解配合使用
- 单例（Singleton）：在整个应用中，只创建bean的一个实例。 
- 原型（Prototype）：每次注入或者通过Spring应用上下文获取的时候，都会创建一个新的bean实例。 
- 会话（Session）：在Web应用中，为每个会话创建一个bean实例。 
- 请求（Rquest）：在Web应用中，为每个请求创建一个bean实例。

- @Scope中的proxyMode属性（如ScopedProxyMode.INTERFACES），用于解决将会话或请求作用域的bean注入到单例bean中所遇到的问题。
    - 对单例bean来说，不会装配具体的依赖bean对象，而是装配一个代理，当真是调用相关方法时，代理会再调用至具体的对象
    - 若单例bean依赖的对象是接口而不是具体的类，那ScopedProxyMode.INTERFACES代理没有问题，但若其是个具体实现类，它必须使用CGLib来生成基于类的代理。所以，如果bean类型是具体类的话，我们必须要 将proxyMode属性设置为ScopedProxyMode.TARGET_CLASS，以此来表明要以生成目标类扩展的方式创建代理。

## 运行时注入值
Spring提供了两种在运行时求值的方式： 
- 属性占位符（Property placeholder）。
- Spring表达式语言（SpEL）。

### 注入外部的值
- 使用@PropertySource注解，引用类路径中一个相关文件，再引入Environment，可直接从Environment用getProperty()获取对应的值
- Environment中提供了四种变种的getProperty()方法
    - String getProperty(String key)
    - String getProperty(String key,String defaultValue)
    - T getProperty(String key,class<T> type)
    - T getProperty(String key,class<T> type,T defaultValue)
- 直接从Environment中检索属性是非常方便的，尤其是在Java配置中装配bean的时候。但是，Spring也提供了通过占位符装配属性的方法，这些占位符的值会来源于一个属性源。
- 在Spring装配中，占位符的形式为使用“${ ... }”包装的属性名称。我们可以使用@Value注解，它的使用方式与@Autowired注解非常相似。

### 使用Spring表达式语言进行装配
SpEL拥有很多特性，包括： 
- 使用bean的ID来引用bean；
    - SpEL表达式也可以引用其他的bean或其他bean的属性，如：#{ A.b }
    - 可以通过systemProperties对象引用系统属性：#{ systemProperties['abc'] }
- 调用方法和访问对象的属性；
    - 如 #{ T(system).currentTimeMillis() }  。T()表达式会将java.lang.System视为Java中对应的类型，因此可以调用 其static修饰的currentTimeMillis()方法。
- 对值进行算术、关系和逻辑运算；
- 正则表达式匹配；
- 集合操作

在动态注入值到Spring bean时，SpEL是一种很便利和强大的方式。我们有时会忍不住编写很复杂的表达式。但需要注意的是，不要让你的表达式太智能。你的表达式越智能，对它的测试就越重要。SpEL毕竟只是 String类型的值，可能测试起来很困难。鉴于这一点，我建议尽可能让表达式保持简洁，这样测试不会是什么大问题。

