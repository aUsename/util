package com.czc.helper.util.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by cuizhongcheng on 2018/10/30
 */
@RestController
public class MainController {

    @GetMapping("/main")
    public String mainMethod(){
        System.out.println("got!");
        System.out.println(new Date().getTime());
        try {
            Thread.sleep(1000 * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "main";
    }
}
