package com.czc.helper.util.helper.spider;

/**
 * @author cuizhongcheng
 * @date 2018/1/26
 */
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    // 目标网址
    private static final String URL = "http://www.budejie.com/video/";

    // 获取视频标签的正则
    private static final String LABEL_REG = "data-mp4=\"(.*?)\"";

    // 获取视频下载链接的正则
    private static final String URL_REG = "[a-zA-z]+://[^\\s]*";

    private static int Num = 0;

    public static void main(String[] args) {
        Main main=new Main();
        for (int i = 0; i < 51; i++) {
            main.exc(i);
        }
    }

    public void exc(int s){
        //获得html文本内容
        String HTML = getHtml(URL+s);
        //获取视频所在的Label
        List<String> labelList = getLabel(HTML);
        //获取视频下载链接
        List<String> videoUrlList = getVideoUrl(labelList);
        //下载视频
        Download(videoUrlList);
    }

    /**
     * 获取目标网页的源代码
     *
     * @param url 目标网页地址
     * @return 网页HTML代码
     * @throws Exception
     */
    private String getHtml(String url){
        try {
            URL url1 = new URL(url);
            URLConnection connection = url1.openConnection();
            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader br = new BufferedReader(inputStreamReader);

            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = br.readLine()) != null) {
                stringBuffer.append(line, 0, line.length());
                stringBuffer.append('\n');
            }

            br.close();
            inputStreamReader.close();
            inputStream.close();

            return stringBuffer.toString();
        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取视频所在的Label
     *
     * @param html 目标网页的html源代码
     * @return 视频的Label列表
     */
    private List<String> getLabel(String html){
        Matcher matcher = Pattern.compile(LABEL_REG).matcher(html);
        List<String> LabelList = new ArrayList<>();
        while (matcher.find()){
            LabelList.add(matcher.group());
        }
        return LabelList;
    }

    /**
     * 获取视频下载链接列表
     *
     * @param labelList 视频的Label列表
     * @return 视频下载链接列表
     */
    private List<String> getVideoUrl(List<String> labelList){
        List<String> VideoUrl=new ArrayList<>();
        for (String label : labelList){
            Matcher matcher = Pattern.compile(URL_REG).matcher(label);
            while (matcher.find()){
                VideoUrl.add(matcher.group().substring(0, matcher.group().length()-1));
            }
        }
        return VideoUrl;
    }

    /**
     * 下载视频
     *
     * @param videoUrl 视频下载链接列表
     */
    private void Download(List<String> videoUrl) {
        try {
            for (String url : videoUrl) {
                //String name = url.substring(url.lastIndexOf("/") + 1, url.length());
                String name = Num+".mp4";
                URL uri = new URL(url);
                InputStream in = uri.openStream();
                FileOutputStream fo = new FileOutputStream(new File("F:/mp4/"+name));
                byte[] buf = new byte[1024];
                int length = 0;
                System.out.println("开始下载:" + url);
                while ((length = in.read(buf, 0, buf.length)) != -1) {
                    fo.write(buf, 0, length);
                }
                in.close();
                fo.close();
                Num++;
                System.out.println(name + "下载完成");
            }
        } catch (Exception e) {
            System.out.println("下载失败");
        }
    }
}