package com.czc.helper.util.helper;

import java.util.Collection;
import java.util.Map;

/**
 * 小小工具类似,监测各种类型数据是否为空
 *
 * @author Marco
 */
public class Detect {

    /**
     * 判断字符串是否为空
     *
     * @param value 字符串
     * @return 是否为空
     */
    public static boolean notEmpty(String value) {
        return value != null && value.trim().length() > 0;
    }

    /**
     * 判断集合是否为空
     *
     * @param collection 集合
     * @return 是否为空
     */
    public static boolean notEmpty(Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    /**
     * 判断Map是否为空
     *
     * @param map Map
     * @return 是否为空
     */
    public static boolean notEmpty(Map<?, ?> map) {
        return map != null && !map.isEmpty();
    }
}
