package com.czc.helper.util.helper;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
/**
 * Redis 工具
 * 使用方法：
 * 1、直接调用快捷方法，如：
 * ////////////  @Resource
 * ////////////  RedisHelper redisHelper;
 * ////////////  redisHelper.setAdd(key, value);
 * ////////////  redisHelper.setRemove(key, value);
 * 2、先获取对应数据类型的客户端在操作：
 * ////////////  @Resource
 * ////////////  RedisHelper redisHelper;
 * ////////////  SetClient<String> client = redisHelper.setClient(String.class, key);
 * ////////////  client.add(value);
 * ////////////  client.remove(value);
 * 以上两种方式效果、效率相同：
 * ----只有少数基础redis操作可以使用第 1 种
 * ----如果会较多的操作redis，用第二种方法更好
 * Created by yangtao on 2017/9/5.
 * @author cuizhongcheng
 * @date 2017/12/26
 */
@Component
public class RedisHelper {

    /**
     * key + 泛型实际类型共享同一个类型的客户端，null 会额外缓存一个客户端
     */
    //Json工具
    @Resource
    private ObjectMapper objectMapper;
    //Redis工具
    @Autowired(required = false)
    private StringRedisTemplate stringRedisTemplate;

    /* *****************************************************
     * ******************** 基本操作 ***********************
     * *****************************************************/

    /**
     * 删除redis key
     *
     * @param key 要删除的key
     */
    public void delete(String key) {
        getTemplate().delete(key);
    }

    /**
     * 批量删除redis key
     *
     * @param keys 要删除的key集合
     */
    public void delete(Collection<String> keys) {
        getTemplate().delete(keys);
    }

    /**
     * 判断redis key是否存在
     *
     * @param key 要检查的key
     * @return true为存在
     */
    public boolean hasKey(String key) {
        return getTemplate().hasKey(key);
    }

    /**
     * 获取满足表达式的key集合
     *
     * @param pattern 表达式，*表示全部
     * @return key集合
     */
    public Set<String> keys(String pattern) {
        return getTemplate().keys(pattern);
    }

    /* *****************************************************
     * ******************** Value操作 **********************
     * *****************************************************/

    /**
     * 设置值
     *
     * @param key   redis key
     * @param value value
     */
    public void valueSet(String key, Object value) {
        getTemplate().opsForValue().set(key, string(value));
    }

    /**
     * 设置值
     *
     * @param key     redis key
     * @param value   value
     * @param timeout 超时时间
     * @param unit    超时时间单位
     */
    public void valueSet(String key, Object value, long timeout, TimeUnit unit) {
        getTemplate().opsForValue().set(key, string(value), timeout, unit);
    }

    /**
     * 如果key不存在，设置值（set nx）
     * @param key redis key
     * @param value value
     * @return 是否设置成功
     */
    public boolean valueSetIfAbsent(String key, Object value) {
        return getTemplate().opsForValue().setIfAbsent(key, string(value));
    }

    /**
     * 获取锁
     * @param key 锁条件
     * @param expireSeconds 过期时间
     * @return 是否成功获取锁
     */
    public boolean lock(String key, Long expireSeconds) {
        boolean absent = valueSetIfAbsent(key, System.currentTimeMillis());
        if (absent) {
            //设置过期时间
            valueExpire(key, expireSeconds, TimeUnit.SECONDS);
            return true;
        }
        Long expire = valueGetExpire(key);
        //如果未设置过期时间，设置过期时间，本次不处理
        if (expire == -1L || expireSeconds < expire) {
            valueExpire(key, expireSeconds, TimeUnit.SECONDS);
        }
        return false;
    }
    /**
     * 设置过期时间
     * @param timeout 过期时间
     * @param unit 单位
     * @return 是否设置成功
     */
    public boolean valueExpire(String key, long timeout, TimeUnit unit) {
        return getTemplate().expire(key, timeout, unit);
    }

    public Long valueGetExpire(String key) {
        return getTemplate().getExpire(key);
    }

    /**
     * 批量设置值
     *
     * @param entries 要批量set的键值对
     */
    public void valueSet(Map<?, ?> entries) {
        Map<String, String> keyValues = new HashMap<>(capacity(entries.size()));
        entries.forEach((key, value) -> keyValues.put(string(key), string(value)));
        getTemplate().opsForValue().multiSet(keyValues);
    }

    /**
     * 获取值（字符串）
     *
     * @param key redis key
     * @return 值
     */
    public String valueGet(String key) {
        return valueGet(key, String.class);
    }

    /**
     * 获取值
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return 值
     */
    public <T> T valueGet(String key, Class<T> valueType) {
        return entity(getTemplate().opsForValue().get(key),valueType);
    }

    /**
     * 获取值 List
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return 值
     */
    public <T> List<T> valueGetList(String key, Class<T> valueType) {
        return list(getTemplate().opsForValue().get(key), valueType);
    }

    /**
     * 获取值 Set
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return 值
     */
    public <T> Set<T> valueGetSet(String key, Class<T> valueType) {
        return set(getTemplate().opsForValue().get(key), valueType);
    }

    /**
     * 获取值 Key为String的Map
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return 值
     */
    public <T> Map<String, T>valueGetMap(String key, Class<T> valueType) {
        return map(getTemplate().opsForValue().get(key), valueType);
    }

    /**
     * 批量获取值（字符串）
     *
     * @param keys redis keys
     * @return 值
     */
    public List<String> valueMultiGet(Collection<String> keys) {
        return getTemplate().opsForValue().multiGet(keys);
    }

    /**
     * 批量获取值
     *
     * @param keys      redis keys
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return 值
     */
    public <T> List<T> valueMultiGet(Collection<String> keys, Class<T> valueType) {
        List<String> result = getTemplate().opsForValue().multiGet(keys);
        List<T> values = new ArrayList<>(result.size());
        result.forEach(item -> values.add(entity(item, valueType)));
        return values;
    }

    /**
     * get并set字符串值（字符串）
     *
     * @param key   redis key
     * @param value 新值
     * @return 值
     */
    public String valueGetAndSet(String key, String value) {
        return getTemplate().opsForValue().getAndSet(key, value);
    }

    /**
     * get并set值
     *
     * @param key       redis key
     * @param value     新值
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return 值
     */
    public <T> T valueGetAndSet(String key, T value, Class<T> valueType) {
        return entity(getTemplate().opsForValue().getAndSet(key, string(value)), valueType);
    }

    /* *****************************************************
     * ********************* SET 操作 **********************
     * *****************************************************/

    /**
     * 获取指定key的Set全部值（字符串）
     *
     * @param key redis key
     * @return Set集合
     */
    public Set<String> setMembers(String key) {
        return setMembers(key, String.class);
    }

    /**
     * 获取指定key的Set全部值
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值的泛型
     * @return Set集合
     */
    public <T> Set<T> setMembers(String key, Class<T> valueType) {
        Set<String> result = getTemplate().opsForSet().members(key);
        Set<T> set = new HashSet<>(capacity(result.size()));
        result.forEach(item -> set.add(entity(item, valueType)));
        return set;
    }

    /**
     * 向指定key的set中添加值
     *
     * @param key   rediskey
     * @param items 要添加的值，集合
     * @return 添加后set的长度
     */
    public Long setAdd(String key, Collection<?> items) {
        return setAdd(key, items.toArray());
    }

    /**
     * 向指定key的set中添加值
     *
     * @param key  rediskey
     * @param item 要添加的值，个数可变
     * @return 添加后set的长度
     */
    public Long setAdd(String key, Object... item) {
        String[] items = new String[item.length];
        for (int i = 0; i < item.length; i++) {
            items[i] = string(item[i]);
        }
        return getTemplate().opsForSet().add(key,items);
    }

    /**
     * 从指定key的set中移除值
     *
     * @param key  redis key
     * @param item 值 集合
     * @return 移除的元素数量
     */
    public Long setRemove(String key, Collection<?> item) {
        return setRemove(key, item.toArray());
    }

    /**
     * 从指定key的set中移除值
     *
     * @param key  redis key
     * @param item 值
     * @return 移除的元素数量
     */
    public Long setRemove(String key, Object... item) {
        /*String[] items = new String[item.length];
        for (int i = 0; i < item.length; i++) {
            items[i] = string(item[i]);
        }*/
        return getTemplate().opsForSet().remove(key, item);
    }

    /**
     * 判断指定key的set中是否包含某个值
     *
     * @param key  redis key
     * @param item 值
     * @return true为存在
     */
    public boolean setContain(String key, Object item) {
        return getTemplate().opsForSet().isMember(key, item);
    }

    /**
     * 随机移除并返回一个值（字符串）
     *
     * @param key redis key
     * @return 值
     */
    public String setPop(String key) {
        return setPop(key, String.class);
    }

    /**
     * 随机移除并返回一个值
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值泛型
     * @return 值
     */
    public <T> T setPop(String key, Class<T> valueType) {
        return entity(getTemplate().opsForSet().pop(key), valueType);
    }

    /**
     * 随机返回一个值，不移除（字符串）
     *
     * @param key redis key
     * @return 值
     */
    public String setRandom(String key) {
        return setRandom(key, String.class);
    }

    /**
     * 随机返回一个值，不移除
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <T>       值泛型
     * @return 值
     */
    public <T> T setRandom(String key, Class<T> valueType) {
        return entity(getTemplate().opsForSet().randomMember(key), valueType);
    }

    /**
     * 随机返回指定个数的值，不移除（字符串）
     *
     * @param key   redis key
     * @param count 要返回的个数
     * @return 值
     */
    public List<String> setRandom(String key, long count) {
        return setRandom(key, count, String.class);
    }

    /**
     * 随机返回指定个数的值，不移除
     *
     * @param key       redis key
     * @param count     要返回的个数
     * @param valueType 值类型
     * @param <T>       值泛型
     * @return 值
     */
    public <T> List<T> setRandom(String key, long count, Class<T> valueType) {
        List<String> result = getTemplate().opsForSet().randomMembers(key, count);
        List<T> random = new ArrayList<>(result.size());
        result.forEach(item -> random.add(entity(item, valueType)));
        return random;
    }

    /**
     * 获取Set元素个数
     *
     * @param key redis key
     * @return set的长度
     */
    public Long setSize(String key) {
        return getTemplate().opsForSet().size(key);
    }

    /**
     * 判断是否存在
     *
     * @param key Set Key
     * @param o   Object key
     * @return 是否存在
     */
    public boolean setIsMember(String key, Object o) {
        return getTemplate().opsForSet().isMember(key, o);
    }

    /* *****************************************************
     * ********************* ZSET 操作 *********************
     * *****************************************************/

    /**
     * 添加值到ZSET
     * <p>
     * 不存在就添加，存在就更新分数
     *
     * @param key   redis key
     * @param item  要添加的元素
     * @param score 分值
     * @return 是否添加成功
     */
    public boolean zsetAdd(String key, Object item, double score) {
        return getTemplate().opsForZSet().add(key, string(item), score);
    }

    /**
     * 从ZSET中移除值
     *
     * @param key    redis key
     * @param values 要移除的值列表  可变参数
     * @return 移除的个数
     */
    public Long zsetRemove(String key, Object... values) {
        /*String[] items = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            items[i] = string(values[i]);
        }*/
        return getTemplate().opsForZSet().remove(key, values);
    }

    /**
     * 移除指定 索引 范围的值
     *
     * @param key   redis key
     * @param start 开始
     * @param end   结束
     */
    public Long zsetRemove(String key, long start, long end) {
        return  getTemplate().opsForZSet().removeRange(key, start, end);
    }

    /**
     * 移除指定 分数 范围的值
     *
     * @param key redis key
     * @param min 最低分
     * @param max 最高分
     */
    public Long zsetRemove(String key, double min, double max) {
        return getTemplate().opsForZSet().removeRangeByScore(key, min, max);
    }

    /**
     * 返回指定 索引 范围的值（字符串）
     *
     * @param key   redis key
     * @param start 开始
     * @param end   结束
     * @return 值的集合
     */
    public Set<String> zsetRange(String key, long start, long end) {
        return zsetRange(key, start, end, String.class);
    }

    /**
     * 返回指定 索引 范围的值
     *
     * @param key       redis key
     * @param start     开始
     * @param end       结束
     * @param valueType 值类型
     * @return 值的集合
     */
    public <T> Set<T> zsetRange(String key, long start, long end, Class<T> valueType) {
        Set<String> result = getTemplate().opsForZSet().range(key, start, end);
        Set<T> range = new HashSet<>(capacity(result.size()));
        result.forEach(item -> range.add(entity(item, valueType)));
        return range;
    }

    /**
     * 返回指定 分数 范围的值
     *
     * @param key redis key
     * @param min 最低分
     * @param max 最高分
     * @return 值的集合
     */
    public Set<String> zsetRange(String key, double min, double max) {
        return zsetRange(key, min, max, String.class);
    }

    /**
     * 返回指定 分数 范围的值
     *
     * @param key       redis key
     * @param min       最低分
     * @param max       最高分
     * @param valueType 值类型
     * @return 值的集合
     */
    public <T> Set<T> zsetRange(String key, double min, double max, Class<T> valueType) {
        Set<String> result = getTemplate().opsForZSet().rangeByScore(key, min, max);
        Set<T> range = new HashSet<>(capacity(result.size()));
        result.forEach(item -> range.add(entity(item, valueType)));
        return range;
    }

    /**
     * 返回指定 分数 范围的值个数
     *
     * @param key redis key
     * @param min 最低分
     * @param max 最高分
     * @return 值的个数
     */
    public Long zsetCount(String key, double min, double max) {
        return getTemplate().opsForZSet().count(key, min, max);
    }

    /**
     * 获取值在ZSET中的排名
     *
     * @param key   redis key
     * @param value 值
     * @return 值的排名
     */
    public Long zsetRank(String key, Object value) {
        return getTemplate().opsForZSet().rank(key, value);
    }

    /**
     * 获取值在ZSET中的分数
     *
     * @param key   redis key
     * @param value 值
     * @return 值的分数
     */
    public Double zsetScore(String key, Object value) {
        return getTemplate().opsForZSet().score(key, value);
    }

    /**
     * 增加值的分数
     *
     * @param key   redis key
     * @param value 值
     * @param delta 要增加的分数，负值为减
     * @return 值的分数
     */
    public Double zsetIncrementScore(String key, Object value, double delta) {
        return getTemplate().opsForZSet().incrementScore(key, string(value), delta);
    }

    /**
     * 获取ZSET长度
     *
     * @param key redis key
     * @return 长度
     */
    public Long zsetSize(String key) {
        return getTemplate().opsForZSet().size(key);
    }

    /**
     * 获取 zset 基数
     *
     * @param key redis key
     * @return 基数
     */
    public Long zsetzCard(String key) {
        return getTemplate().opsForZSet().zCard(key);
    }

    /* *****************************************************
     * ********************* LIST 操作 *********************
     * *****************************************************/

    /**
     * 右侧入队
     *
     * @param key   redis key
     * @param items 要入队的元素集合
     * @return list长度
     */
    public Long listRightPush(String key, Collection<?> items) {
        return listRightPush(key, items.toArray());
    }

    /**
     * 右侧入队
     *
     * @param key   redis key
     * @param items 要入队的元素
     * @return list长度
     */
    public Long listRightPush(String key, Object... items) {
        String[] item = new String[items.length];
        for (int i = 0; i < items.length; i++) {
            item[i] = string(items[i]);
        }
        return getTemplate().opsForList().rightPushAll(key, item);
    }

    /**
     * 左侧入队
     *
     * @param key   redis key
     * @param items 要入队的元素集合
     * @return list长度
     */
    public Long listLeftPush(String key, Collection<?> items) {
        return listLeftPush(key, items.toArray());
    }

    /**
     * 左侧入队
     *
     * @param key   redis key
     * @param items 要入队的元素
     * @return list长度
     */
    public Long listLeftPush(String key, Object... items) {
        String[] item = new String[items.length];
        for (int i = 0; i < items.length; i++) {
            item[i] = string(items[i]);
        }
        return getTemplate().opsForList().leftPushAll(key, item);
    }

    /**
     * 右侧出队（字符串）
     *
     * @param key redis key
     * @return 出队的元素
     */
    public String listRightPop(String key) {
        return listRightPop(key, String.class);
    }

    /**
     * 右侧出队
     *
     * @param key       redis key
     * @param valueType 元素类型
     * @param <T>       泛型
     * @return 出队的元素
     */
    public <T> T listRightPop(String key, Class<T> valueType) {
        return entity(getTemplate().opsForList().rightPop(key), valueType);
    }

    /**
     * 左侧出队（字符串）
     *
     * @param key redis key
     * @return 出队的元素
     */
    public String listLeftPop(String key) {
        return listLeftPop(key, String.class);
    }

    /**
     * 左侧出队
     *
     * @param key       redis key
     * @param valueType 元素类型
     * @param <T>       泛型
     * @return 出队的元素
     */
    public <T> T listLeftPop(String key, Class<T> valueType) {
        return entity(getTemplate().opsForList().leftPop(key), valueType);
    }

    /**
     * 获取指定索引的元素（字符串）
     *
     * @param key   redis key
     * @param index 索引
     * @return 值
     */
    public String listIndex(String key, long index) {
        return listIndex(key, index, String.class);
    }

    /**
     * 获取指定索引的元素
     *
     * @param key       redis key
     * @param index     索引
     * @param valueType 值类型
     * @param <T>       值泛型
     * @return 值
     */
    public <T> T listIndex(String key, long index, Class<T> valueType) {
        return entity(getTemplate().opsForList().index(key, index), valueType);
    }

    /**
     * 设置值
     *
     * @param index 索引位置
     * @param value 新的值
     */
    public void listSet(String key, long index, Object value) {
        getTemplate().opsForList().set(key, index, string(value));
    }

    /**
     * 取出指定范围的值（字符串）
     *
     * @param key   redis key
     * @param start 开始位置
     * @param end   结束位置
     * @return List集合
     */
    public List<String> listRange(String key, long start, long end) {
        return listRange(key, start, end, String.class);
    }

    /**
     * 取出指定范围的值
     *
     * @param key       redis key
     * @param start     开始位置
     * @param end       结束位置
     * @param valueType 值类型
     * @param <T>       值泛型
     * @return List集合
     */
    public <T> List<T> listRange(String key, long start, long end, Class<T> valueType) {
        List<String> result = getTemplate().opsForList().range(key, start, end);
        List<T> range = new ArrayList<>(result.size());
        result.forEach(item -> range.add(entity(item, valueType)));
        return range;
    }

    /**
     * 获取 LIST 长度
     *
     * @param key redis key
     * @return LIST 长度
     */
    public Long listSize(String key) {
        return getTemplate().opsForList().size(key);
    }

    /* *****************************************************
     * ********************* MAP 操作 **********************
     * *****************************************************/

    /**
     * put到map
     *
     * @param key    redis key
     * @param mKey   map key
     * @param mValue map value
     */
    public void mapPut(String key, Object mKey, Object mValue) {
        getTemplate().opsForHash().put(key, mKey, mValue);
    }

    /**
     * put一个map到已有map
     *
     * @param key redis key
     * @param map 要put的map
     */
    public void mapPut(String key, Map<?, ?> map) {
        getTemplate().opsForHash().putAll(key, map);
    }

    /**
     * 从map中get一个值（字符串）
     *
     * @param key  redis key
     * @param mKey map key
     * @return 取到的值
     */
    public String mapGet(String key, Object mKey) {
        return mapGet(key, mKey, String.class);
    }

    /**
     * 从map中get一个值
     *
     * @param key       redis key
     * @param mKey      map key
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 取到的值
     */
    public <V> V mapGet(String key, Object mKey, Class<V> valueType) {
        return entity(string(getTemplate().opsForHash().get(key, mKey)), valueType);
    }

    /**
     * 从map中get一个List值
     *
     * @param key  redis key
     * @param mKey map key
     * @return 取到的值
     */
    public List<String> mapGetList(String key, Object mKey) {
        return list(string(getTemplate().opsForHash().get(key, mKey)),String.class);
    }

    /**
     * 从map中get一个List值
     *
     * @param key       redis key
     * @param mKey      map key
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 取到的值
     */
    public <V> List<V> mapGetList(String key, Object mKey, Class<V> valueType) {
        return list(string(getTemplate().opsForHash().get(key, mKey)), valueType);
    }

    /**
     * 从map中get一个Set值
     *
     * @param key  redis key
     * @param mKey map key
     * @return 取到的值
     */
    public Set<String> mapGetSet(String key, Object mKey) {
        return set(string(getTemplate().opsForHash().get(key, mKey)), String.class);
    }

    /**
     * 从map中get一个Set值
     *
     * @param key       redis key
     * @param mKey      map key
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 取到的值
     */
    public <V> Set<V> mapGetSet(String key, Object mKey, Class<V> valueType) {
        return set(string(getTemplate().opsForHash().get(key, mKey)), valueType);
    }

    /**
     * 从map中get一个Map值
     *
     * @param key  redis key
     * @param mKey map key
     * @return 取到的值
     */
    public Map<String, String> mapGetMap(String key, Object mKey) {
        return mapGetMap(key, mKey, String.class);
    }

    /**
     * 从map中get一个Map值
     *
     * @param key       redis key
     * @param mKey      map key
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 取到的值
     */
    public <V> Map<String, V> mapGetMap(String key, Object mKey, Class<V> valueType) {
        return mapGetMap(key, mKey, String.class, valueType);
    }

    /**
     * 从map中get一个Map值
     *
     * @param key       redis key
     * @param mKey      map key
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 取到的值
     */
    public <K, V> Map<K, V> mapGetMap(String key, Object mKey, Class<K> keyType, Class<V> valueType) {
        return map(string(getTemplate().opsForHash().get(key, mKey)), keyType, valueType);
    }

    /**
     * 从map中批量get值
     *
     * @param key       redis key
     * @param mKeys     map keys
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 取到的值
     */
    public <V> List<V> mapMultiGet(String key, Collection<?> mKeys, Class<V> valueType) {
        List<Object> keyList = new ArrayList<>(mKeys.size());
        mKeys.forEach(tkey -> keyList.add(string(tkey)));
        List<Object> result = getTemplate().opsForHash().multiGet(key, keyList);
        List<V> values = new ArrayList<>(result.size());
        result.forEach(value -> values.add(entity(string(value), valueType)));
        return values;
    }

    /**
     * 判断map是否包含指定key
     *
     * @param key  redis key
     * @param mKey map key
     * @return true为包含
     */
    public boolean mapHasKey(String key, Object mKey) {
        return getTemplate().opsForHash().hasKey(key, mKey);
    }

    /**
     * 从map移除一个key
     *
     * @param key  redis key
     * @param mKey map key
     * @return 移除的个数
     */
    public Long mapRemove(String key, Collection<?> mKey) {
        return mapRemove(key, mKey.toArray());
    }

    /**
     * 从map移除一个key
     *
     * @param key  redis key
     * @param mKey map key
     * @return 移除的个数
     */
    public Long mapRemove(String key, Object... mKey) {
        /*String[] items = new String[mKey.length];
        for (int i = 0; i < mKey.length; i++) {
            items[i] = string(mKey[i]);
        }
        return getTemplate().opsForHash().delete(key, items);*/
        return getTemplate().opsForHash().delete(key, mKey);

    }

    /**
     * 获取map的所有key（字符串）
     *
     * @param key redis key
     * @return key的Set集合
     */
    public Set<String> mapKeys(String key) {
        return mapKeys(key, String.class);
    }

    /**
     * 获取map的所有key
     *
     * @param key     redis key
     * @param keyType key类型
     * @param <K>     key的泛型
     * @return key的Set集合
     */
    public <K> Set<K> mapKeys(String key, Class<K> keyType) {
        Set<Object> result = getTemplate().opsForHash().keys(key);
        Set<K> keys = new HashSet<>(capacity(result.size()));
        result.forEach(item -> keys.add(entity(string(item), keyType)));
        return keys;
    }

    /**
     * 取map的所有value（String）
     *
     * @param key redis key
     * @return 值的List集合
     */
    public List<String> mapValues(String key) {
        return mapValues(key, String.class);
    }

    /**
     * 取map的所有value
     *
     * @param key       redis key
     * @param valueType 值类型
     * @param <V>       值泛型
     * @return 值的List集合
     */
    public <V> List<V> mapValues(String key, Class<V> valueType) {
        List<Object> result = getTemplate().opsForHash().values(key);
        List<V> values = new ArrayList<>(result.size());
        result.forEach(item -> values.add(entity(string(item), valueType)));
        return values;
    }

    /**
     * 获取map（key和value都为字符串）
     *
     * @param key redis key
     * @return map
     */
    public Map<String, String> mapEntries(String key) {
        return mapEntries(key, String.class, String.class);
    }

    /**
     * 获取map（key为字符串）
     *
     * @param key       redis key
     * @param valueType map value类型
     * @param <V>       map value 泛型
     * @return map
     */
    public <V> Map<String, V> mapEntries(String key, Class<V> valueType) {
        return mapEntries(key, String.class, valueType);
    }

    /**
     * 获取map
     *
     * @param key       redis key
     * @param keyType   map key类型
     * @param valueType map value类型
     * @param <K>       map key 泛型
     * @param <V>       map value 泛型
     * @return map
     */
    public <K, V> Map<K, V> mapEntries(String key, Class<K> keyType, Class<V> valueType) {
        Map<Object, Object> result = getTemplate().opsForHash().entries(key);
        Map<K, V> entries = new HashMap<>(result.size());
        result.forEach((tkey, value) -> entries.put(entity(string(tkey), keyType), entity(string(value), valueType)));
        return entries;
    }

    /**
     * 获取map长度
     *
     * @param key redis key
     * @return map的长度
     */
    public Long mapSize(String key) {
        return getTemplate().opsForHash().size(key);
    }

    /* *****************************************************
     * ********************** 扩展工具 *********************
     * *****************************************************/

    /**
     * 获取指定长度的HashMap的实际初始长度（避免 resize() 带来的性能消耗）
     *
     * @param expectedSize 指定的长度
     * @return 实际应该初始化的长度
     */
    public int capacity(int expectedSize) {
        if (expectedSize < 3) {
            return expectedSize + 1;
        }
        if (expectedSize < (1 << (Integer.SIZE - 2))) {
            return (int) ((float) expectedSize / 0.75F + 1.0F);
        }
        return Integer.MAX_VALUE;
    }

    /**
     * 任意对象转Json字符串
     *
     * @param object 对象
     * @return Json字符串
     */
    public String string(Object object) {
        if (object instanceof String) {
            return (String) object;
        }
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException("数据有误：" + e.getMessage());
        }
    }

    /**
     * 字符串转对象
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> T entity(String json, Class<T> valueType) {
        if (json == null) {
            return null;
        }

        if (valueType.isAssignableFrom(String.class)) {
            return (T) json;
        }
        try {
            return objectMapper.readValue(json, valueType);
        } catch (IOException e) {
            throw new RuntimeException("数据有误：" + e.getMessage());
        }
    }

    /**
     * 字符串转List
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> List<T> list(String json, Class<T> valueType) {
        if (json == null) {
            return new ArrayList<>();
        }
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, valueType);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误：" + e.getMessage());
        }
    }

    /**
     * 字符串转Set
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> Set<T> set(String json, Class<T> valueType) {
        if (json == null) {
            return new HashSet<>();
        }
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(Set.class, valueType);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误：" + e.getMessage());
        }
    }

    /* *****************************************************
     * ********************* 通用方法 **********************
     * *****************************************************/

    /**
     * 字符串转Map, key为字符串
     *
     * @param json      字符串
     * @param valueType value类型
     * @param <V>       value的泛型
     * @return 对象
     */
    public <V> Map<String, V> map(String json, Class<V> valueType) {
        return map(json, String.class, valueType);
    }

    /**
     * 字符串转Map
     *
     * @param json      字符串
     * @param keyType   key类型
     * @param valueType value类型
     * @param <K>       key的泛型
     * @param <V>       value的泛型
     * @return 对象
     */
    public <K, V> Map<K, V> map(String json, Class<K> keyType, Class<V> valueType) {
        if (json == null) {
            return new HashMap<>();
        }
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(HashMap.class, keyType, valueType);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误：" + e.getMessage());
        }
    }

    private StringRedisTemplate getTemplate() {
        if (stringRedisTemplate == null) {
            throw new RuntimeException("本工具依赖 Spring Redis ，请添加相关依赖");
        }
        return stringRedisTemplate;
    }

}

