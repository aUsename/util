package com.czc.helper.util.helper.locksDemo;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author cuizhongcheng
 * @date 2018/1/18
 */
public class ReentrantLockDemo implements Runnable{
    static ReentrantLock lock = new ReentrantLock();
    static int i = 0;
    @Override
    public void run() {
        for (int j = 0; j < 10000000; j++) {
            lock.lock();
            lock.lock();
            try {
                i++;
            } finally {
                lock.unlock();
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ReentrantLockDemo reentrantLockDemo = new ReentrantLockDemo();
        Thread thread1 = new Thread(reentrantLockDemo);
        Thread thread2 = new Thread(reentrantLockDemo);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println(i);
    }
}
