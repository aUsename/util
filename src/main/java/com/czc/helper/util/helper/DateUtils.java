package com.czc.helper.util.helper;

import com.google.common.math.DoubleMath;

import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期相关操作工具
 *
 * @author Marco
 */
public class DateUtils {

    public enum TIME_UNIT {MILLIS, SECS, MINS, HOURS, DAYS}

    private static String INVALID_DATE_ERR = "Please provide a valid Date.";
    private static String INVALID_CAL_ERR = "Please provide a valid Calendar.";
    private static String STRING_FMT_ERR = "Please provide a valid String.";

    private static final long MILLIS_IN_SEC = 1000;
    private static final long MILLIS_IN_MIN = MILLIS_IN_SEC * 60;
    private static final long MILLIS_IN_HOUR = MILLIS_IN_MIN * 60;
    private static final long MILLIS_IN_DAY = MILLIS_IN_HOUR * 24;

    private static final int[] CONSTELLATION_DAY = new int[]{20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 23, 22};

    private static final String[] CONSTELLATIONS = new String[]{"摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座"};

    /**
     * 根据生日获取星座的函数
     *
     * @param birthday 生日
     * @return 星座
     * @throws NullPointerException 异常
     */
    public static String toConstellation(Date birthday) {
        if (birthday == null)
            throw new NullPointerException(INVALID_DATE_ERR);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(birthday);

        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return day < CONSTELLATION_DAY[month - 1] ? CONSTELLATIONS[month - 1] : CONSTELLATIONS[month];
    }

    /**
     * 根据生日计算年龄
     *
     * @param birthday 生日
     * @return 年龄
     */
    public static int calculateAge(Date birthday) {
        Calendar cal = Calendar.getInstance();

        if (cal.before(birthday)) {
            return 0;
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthday);

        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

        int age = yearNow - yearBirth;

        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            } else {
                age--;
            }
        }
        return age;
    }

    /**
     * 获取当前UTC时间
     *
     * @return UTC时间
     */
    public static Date getCurrentUTC() {
        Calendar cal = Calendar.getInstance();
        int zoneOffset = cal.get(Calendar.ZONE_OFFSET);
        int dstOffset = cal.get(Calendar.DST_OFFSET);
        cal.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        return cal.getTime();
    }

    /**
     * 格式化Date成指定format
     *
     * Formats a Date object, given a format string consistent with
     * <code>SimpleDateFormat</code> class.
     *
     * @param date   - A Date object to be formatted.
     * @param format - The desired format (for <code>SimpleDateFormat</code> class.
     * @return A date formatted as a string.
     */
    public static String formatDate(Date date, String format) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        if (format == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (format.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
        return dateFormatter.format(date);
    }

    /**
     * 将字符串转换为Date
     *
     * Creates a date from a formatted string consistent with the <code>
     * SimpleDateFormat</code> class.
     *
     * @param formattedString - A formatted date as a string consistent with the
     *                        <code>SimpleDateFormat</code> class.
     * @param format  - The desired format consistent with the <code>
     *                 SimpleDateFormat</code> class.
     * @return A Date object created from the date in the formattedString.
     * @throws ParseException
     */
    public static Date getDateFromString(String formattedString,String format) throws ParseException {
        if (formattedString == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (formattedString.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);

        return dateFormatter.parse(formattedString);

    }

    /**
     * 格式化Calendar成指定format
     *
     * Formats a Calendar object, given a format string consistent with the <code>
     * SimpleDateFormat</code> class..
     *
     * @param calendar - A Calendar object to be formatted.
     * @param format   - The desired format consistent with the <code>
     *                 SimpleDateFormat</code> class.
     * @return A date formatted as a string.
     */
    public static String formatCalendar(Calendar calendar, String format) {
        if (calendar == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        if (format == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (format.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);

        return dateFormatter.format(calendar.getTime());
    }

    /**
     * 将字符串转换为Calendar
     *
     * Creates a date from a formatted string consistent with the <code>
     * SimpleDateFormat</code> class.
     *
     * @param formattedString - A formatted date as a string consistent with the
     *                        <code>SimpleDateFormat</code> class.
     * @param format  - The desired format consistent with the <code>
     *                 SimpleDateFormat</code> class.
     * @return A Calendar object created from the date in the formattedString.
     * @throws ParseException
     */
    public static Calendar getCalendarFromString(String formattedString,String format) throws ParseException {
        if (formattedString == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (formattedString.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
        Date date = dateFormatter.parse(formattedString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return cal;
    }

    /**
     * 从calendar获取日期
     *
     * Formats the specified calendar into a string with short date format (MM/dd/yyyy).
     * For example, 01/01/2013.
     *
     * @param cal - The calendar to format.
     * @return A String formatted into a short date (MM/dd/yyyy).
     */
    public static String formatToShortDate(Calendar cal) {
        if (cal == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        return formatCalendar(cal, "MM/dd/yyyy");
    }

    /**
     * 从Date获取日期
     *
     * Formats the specified date into a string with short date format (MM/dd/yyyy).
     * For example, 01/01/2013.
     *
     * @param date - The date to format.
     * @return A String formatted into a short date (MM/dd/yyyy).
     */
    public static String formatToShortDate(Date date) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        return formatDate(date, "MM/dd/yyyy");
    }

    /**
     * 从Calendar获取时间
     *
     * Formats the specified calendar into a string with short time format
     * (HH:mm aa). For example, 09:00 AM.
     *
     * @param cal - The calendar to format.
     * @return A String formatted into a short time.
     */
    public static String formatToShortTime(Calendar cal) {
        if (cal == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        return formatCalendar(cal, "hh:mm aa");
    }

    /**
     * 从Date获取时间
     *
     * Formats the specified date into a string with short time format
     * (HH:mm aa). For example, 09:00 AM.
     *
     * @param date - The date to format.
     * @return A String formatted into a short time.
     */
    public static String formatToShortTime(Date date) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        return formatDate(date, "hh:mm aa");
    }


    /**
     * 获取两个Calendar的时间差
     *
     * Returns the time difference between two calendars in the specified time
     * unit. If the first calendar's date occurs after the second calendar's,
     * returns a negative value.
     *
     * @param cal1     - The first calendar.
     * @param cal2     - The second calendar.
     * @param timeUnit - The <code>TIME_UNIT</code> to return the difference in.
     * @return Time difference between the calendars. If cal1 is after cal2,
     * returns a negative value.
     */
    public static long getDiff(Calendar cal1, Calendar cal2,
                               TIME_UNIT timeUnit) {

        if (cal1 == null || cal2 == null || timeUnit == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }

        long timeDiffInMillis = cal2.getTimeInMillis() - cal1.getTimeInMillis();

        switch (timeUnit) {
            case MILLIS:
                return timeDiffInMillis;
            case SECS:
                return timeDiffInMillis / MILLIS_IN_SEC;
            case MINS:
                return timeDiffInMillis / MILLIS_IN_MIN;
            case HOURS:
                return timeDiffInMillis / MILLIS_IN_HOUR;
            case DAYS:
                return timeDiffInMillis / MILLIS_IN_DAY;
            default:
                return timeDiffInMillis;
        }

    }

    /**
     * 获取两个date的时间差
     *
     * Returns the time difference between two dates in the specified time
     * unit. If the first date occurs after the second date, returns a negative.
     *
     * @param date1    - The first date.
     * @param date2    - The second date.
     * @param timeUnit - The <code>TIME_UNIT</code> to return the difference in.
     * @return Time difference between the calendars. If date1 is after date2,
     * returns a negative value.
     */
    public static long getDiff(Date date1, Date date2, TIME_UNIT timeUnit) {
        if (date1 == null || date2 == null || timeUnit == null) {
            throw new NullPointerException();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        return getDiff(cal1, cal2, timeUnit);

    }

    /**
     * 获取给定Date与当前时间的时间差
     *
     * @param date 给定日期
     * @return
     */
    public static String timeHint (Date date) {
        if (null != date) {
            Date now     = new Date();
            Long seconds = DoubleMath.roundToLong((now.getTime() - date.getTime()) / 1000, RoundingMode.FLOOR);
            if (seconds < 60) {
                return "1分钟前";
            }
            if (seconds < 60 * 60) {
                return DoubleMath.roundToLong(seconds / 60,RoundingMode.FLOOR) + "分钟前";
            }

            if (seconds < 60 * 60 * 24) {
                return DoubleMath.roundToLong(seconds / 3600,RoundingMode.FLOOR) + "小时前";
            }

            if (seconds < 60 * 60 * 24 * 30) {
                return DoubleMath.roundToLong(seconds / 86400,RoundingMode.FLOOR) + "天前";
            }
        }

        return "30天前";

    }

    /**
     * 改变当前Calendar的值
     *
     * Given a <code>Calendar</code>, adds the given amount of time to the
     * calendar. If the given amount of time is negative, it subtracts the time,
     * producing a date earlier than the given date.
     *
     * @param cal      - The Calendar to add time to.
     * @param amount   - The amount of time to add, in the time unit specified in
     *                 <code>timeUnit</code>.
     * @param timeUnit - The unit of time to add.
     * @return A Calendar with the new date/time.
     */
    public static Calendar addTime(Calendar cal, long amount, TIME_UNIT timeUnit) {
        if (cal == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        switch (timeUnit) {
            case MILLIS:
                cal.setTimeInMillis(cal.getTimeInMillis() + amount);
                return cal;
            case SECS:
                long secondsToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_SEC;
                cal.setTimeInMillis(secondsToAdd);
                return cal;
            case MINS:
                long minsToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_MIN;
                cal.setTimeInMillis(minsToAdd);
                return cal;
            case HOURS:
                long hoursToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_HOUR;
                cal.setTimeInMillis(hoursToAdd);
                return cal;
            case DAYS:
                long daysToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_DAY;
                cal.setTimeInMillis(daysToAdd);
                return cal;
            default:
                return null;
        }
    }

    /**
     * 改变当前Date的值
     *
     * Given a <code>Date</code>, adds the given amount of time to the
     * date. If the given amount of time is negative, it subtracts the time,
     * producing a date earlier than the given date.
     *
     * @param date     - The <code>Date</code> to add time to.
     * @param amount   - The amount of time to add, in the time unit specified in
     *                 <code>timeUnit</code>.
     * @param timeUnit - The unit of time to add.
     * @return A <code>Date</code> with the new date/time.
     */
    public static Date addTime(Date date, long amount, TIME_UNIT timeUnit) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        addTime(cal, amount, timeUnit);
        return cal.getTime();
    }

}