package com.czc.helper.util.helper.init;

/**
 * Created by cuizhongcheng on 2018/7/18
 */
public class Mugs extends MugsFather{

    Mug c1;
    Mug c2;

    Mug c4 = new Mug(4);


    {
        c1 = new Mug(1);
        c2 = new Mug(2);
        System.out.println("c1 & c2 initialized");
    }



    static {
        Mug c3 = new Mug(3);
        i = 7;
    }



    Mugs() {
        System.out.println("Mugs()");
    }


    public static void main(String[] args) {
        System.out.println("Inside main()");
        Mugs x = new Mugs();
        System.out.println(i);

    }

    static int i;

    static {
        System.out.println(i);
    }

    static {
        Mug c5 = new Mug(5);
    }
}

class Mug {
    Mug(int marker) {
        System.out.println("Mug(" + marker + ")");
    }
}

class MugsFather{

    static Mug c6 = new Mug(6);
    Mug c7 = new Mug(7);

    MugsFather(){
        System.out.println("MugFather");
    }
}
