package com.czc.helper.util.helper;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
 * Created by cuizhongcheng on 2017/12/26
 * Created by yangtao on 2017/10/10.
 * 任何对象       ->      Json字符串
 * Json         ->      对象
 * Json         ->      List        传入list项的类型
 * Json         ->      Set         传入set项的类型
 * Json         ->      Map         key和value为字符串
 * Json         ->      Map         key为字符串，value为object
 * Json         ->      Map         key和value均为object
 */
//@Component
public class JsonHelper {

//    @Resource
//    private ObjectMapper objectMapper;
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 任意对象转Json字符串
     *
     * @param object 对象
     * @return Json字符串
     */
    public String string(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof String) {
            return (String) object;
        }
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转对象
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> T entity(String json, Class<T> valueType) {
        try {
            return objectMapper.readValue(json, valueType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转带泛型的对象
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param type      对象是泛型时的泛型类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T, V> T entity(String json, Class<T> valueType, Class<V> type) {
        if (json == null)
            return null;
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(valueType, type);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }


    /**
     * 字符串转List
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> List<T> list(String json, Class<T> valueType) {
        if (json == null) {
            return new ArrayList<>();
        }
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, valueType);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转Set
     *
     * @param json      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> Set<T> set(String json, Class<T> valueType) {
        if (json == null) {
            return new HashSet<>();
        }
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(Set.class, valueType);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转Map, key和value为字符串
     *
     * @param json 字符串
     * @return 对象
     */
    public Map<String, String> map(String json) {
        return map(json, String.class);
    }

    /**
     * 字符串转Map, key为字符串
     *
     * @param json      字符串
     * @param valueType value类型
     * @param <V>       value的泛型
     * @return 对象
     */
    public <V> Map<String, V> map(String json, Class<V> valueType) {
        return map(json, String.class, valueType);
    }

    /**
     * 字符串转Map
     *
     * @param json      字符串
     * @param keyType   key类型
     * @param valueType value类型
     * @param <K>       key的泛型
     * @param <V>       value的泛型
     * @return 对象
     */
    public <K, V> Map<K, V> map(String json, Class<K> keyType, Class<V> valueType) {
        if (json == null) {
            return new HashMap<>();
        }
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(HashMap.class, keyType, valueType);
        try {
            return objectMapper.readValue(json, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }
}
