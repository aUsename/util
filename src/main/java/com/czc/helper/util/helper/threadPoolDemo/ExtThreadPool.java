package com.czc.helper.util.helper.threadPoolDemo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author cuizhongcheng
 * @date 2018/1/22
 *
 * 回调接口
 */
public class ExtThreadPool {
    public static class MyTask implements Runnable{
        public String name;

        public MyTask(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println("Thread ID:"+Thread.currentThread().getId()+", Name = "+name);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = new ThreadPoolExecutor(5,5,0L, TimeUnit.MILLISECONDS,new LinkedBlockingDeque<Runnable>()){
            @Override
            protected void beforeExecute(Thread thread,Runnable runnable){
                System.out.println("before "+((MyTask)runnable).name);
            }

            @Override
            protected void afterExecute(Runnable runnable,Throwable throwable){
                System.out.println("after "+((MyTask)runnable).name);
            }

            @Override
            protected void terminated(){
                System.out.println("quit");
            }
        };

        for (int i = 0; i < 10; i++) {
            MyTask task = new MyTask("Task-GEYM-"+i);
            executorService.execute(task);
            Thread.sleep(10);
        }
        executorService.shutdown();
    }

}
