package com.czc.helper.util.helper;

import org.springframework.util.AntPathMatcher;

/**
 * @author cuizhongcheng
 * @date 2018/1/12
 */
public class URLPathMatcher {

    public static void main(String[] args) {
        URLPathMatcher urlPathMatcher = new URLPathMatcher();
        urlPathMatcher.match();
    }

    public void match(){
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        String pattern = "/abc/a*b/a.jsp";

        System.out.println("pattern: "+pattern);
        System.out.println("/abc/aa/bb/a.jsp: "+antPathMatcher.match(pattern,"/abc/aaaaab/a.jsp"));
        System.out.println("/aBc/aa/bb/a.jsp: "+antPathMatcher.match(pattern,"/aBc/aa/bb/a.jsp"));
        System.out.println("/abc/a.jsp: "+antPathMatcher.match(pattern,"/abc/a.jsp"));

        String p = "/abc/*/a.j";
        System.out.println(antPathMatcher.match(p,"/abc/aa/a.jsp"));
        System.out.println(antPathMatcher.match(p,"/abc/aa/a.j"));
        System.out.println(antPathMatcher.match(p,"/abc"));

    }
}
