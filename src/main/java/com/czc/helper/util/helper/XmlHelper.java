package com.czc.helper.util.helper;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
/**
 * Created by cuizhongcheng on 2017/12/26
 * Created by yangtao on 2017/10/10.
 * 任何对象      ->      xml字符串
 * xml         ->      对象
 * xml         ->      List        传入list项的类型
 * xml         ->      Set         传入set项的类型
 * xml         ->      Map         key和value为字符串
 * xml         ->      Map         key为字符串，value为object
 * xml         ->      Map         key和value均为object
 */
//@Component
public class XmlHelper {

    private final XmlMapper xmlMapper = new XmlMapper();

    /**
     * 任意对象转xml字符串
     *
     * @param object 对象
     * @return xml字符串
     */
    public String string(Object object) {
        try {
            return xmlMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转对象
     *
     * @param xml      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> T entity(String xml, Class<T> valueType) {
        try {
            return xmlMapper.readValue(xml, valueType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转List
     *
     * @param xml      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> List<T> list(String xml, Class<T> valueType) {
        if (xml == null) {
            return new ArrayList<>();
        }
        JavaType javaType = xmlMapper.getTypeFactory().constructParametricType(List.class, valueType);
        try {
            return xmlMapper.readValue(xml, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转Set
     *
     * @param xml      字符串
     * @param valueType 对象类型
     * @param <T>       泛型
     * @return 对象
     */
    public <T> Set<T> set(String xml, Class<T> valueType) {
        if (xml == null) {
            return new HashSet<>();
        }
        JavaType javaType = xmlMapper.getTypeFactory().constructParametricType(Set.class, valueType);
        try {
            return xmlMapper.readValue(xml, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

    /**
     * 字符串转Map, key和value为字符串
     *
     * @param xml 字符串
     * @return 对象
     */
    public Map<String, String> map(String xml) {
        return map(xml, String.class);
    }

    /**
     * 字符串转Map, key为字符串
     *
     * @param xml      字符串
     * @param valueType value类型
     * @param <V>       value的泛型
     * @return 对象
     */
    public <V> Map<String, V> map(String xml, Class<V> valueType) {
        return map(xml, String.class, valueType);
    }

    /**
     * 字符串转Map
     *
     * @param xml      字符串
     * @param keyType   key类型
     * @param valueType value类型
     * @param <K>       key的泛型
     * @param <V>       value的泛型
     * @return 对象
     */
    public <K, V> Map<K, V> map(String xml, Class<K> keyType, Class<V> valueType) {
        if (xml == null) {
            return new HashMap<>();
        }
        JavaType javaType = xmlMapper.getTypeFactory().constructParametricType(HashMap.class, keyType, valueType);
        try {
            return xmlMapper.readValue(xml, javaType);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("数据有误！");
        }
    }

}
