package com.czc.helper.util.helper.locksDemo;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author cuizhongcheng
 * @date 2018/1/18
 */
public class CyclicBarrierDemo {
    public static class Soldier implements Runnable{
        private String soldier;
        private final CyclicBarrier cyclicBarrier;

        Soldier(String soldierName, CyclicBarrier cyclicBarrier) {
            this.soldier = soldierName;
            this.cyclicBarrier = cyclicBarrier;
        }

        @Override
        public void run() {
            try {
                cyclicBarrier.await();
                doWork();
                cyclicBarrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }

        void doWork(){
            try {
                Thread.sleep(Math.abs(new Random().nextInt()%10000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(soldier+":complete");
        }
    }

    public static class BarrierRun implements Runnable{
        boolean flag;
        int N;

        public BarrierRun(int n,boolean flag) {
            this.N = n;
            this.flag = flag;
        }

        @Override
        public void run() {
            if(flag){
                System.out.println("soldier"+N+"个,任务完成");
            }else {
                System.out.println("soldier"+N+"个，集合完毕");
                flag = true;
            }
        }
    }

    public static void main(String[] args) {
        final int N = 10;
        Thread[] threads = new Thread[N];
        boolean flag = false;
        CyclicBarrier cyclicBarrier = new CyclicBarrier(N,new BarrierRun(N,flag));

        System.out.println("集合");

        for (int i = 0; i < N; i++) {
            System.out.println("soldier"+i+"报道");
            threads[i] = new Thread(new Soldier("soldier"+i,cyclicBarrier));
            threads[i].start();
//            if (i == 5){
//                threads[0].interrupt();
//            }
        }

    }
}
