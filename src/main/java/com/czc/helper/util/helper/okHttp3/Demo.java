package com.czc.helper.util.helper.okHttp3;

import okhttp3.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by cuizhongcheng on 2018/9/25
 */
public class Demo {

    // https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp
    // compile group: 'com.squareup.okhttp3', name: 'okhttp', version: '3.11.0'
    public static void main(String[] args) {
        // OkHttpClient client = new OkHttpClient();
        OkHttpClient client = new OkHttpClient().newBuilder()
                .readTimeout(8, TimeUnit.SECONDS)       //设置读取超时时间
                .writeTimeout(8, TimeUnit.SECONDS)      //设置写的超时时间
                .connectTimeout(8, TimeUnit.SECONDS)    //设置连接超时时间
                .build();

        // get
        Request request = new Request.Builder().url("http://www.baidu.com").get().build();


        // post
        RequestBody requestBody = new FormBody.Builder()
                .add("name","111")
                .add("password","123")
                .build();

        Request request1 = new Request.Builder().url("http://www.baidu.com").post(requestBody).build();

        try {
            Response response = client.newCall(request).execute();
            String responseData = response.body().string();
            System.out.println(responseData);

            System.out.println("===============================");

            Response response1 = client.newCall(request1).execute();
            if (response1.body() != null) {
                String responseData1 = response1.body().string();
                System.out.println(responseData1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
