package com.czc.helper.util.helper.atomicDemo;

import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author cuizhongcheng
 * @date 2018/1/17
 */
public class AtomicStampedReferenceDemo {
    static AtomicStampedReference<Integer> money = new AtomicStampedReference<Integer>(19,0);

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            final int timestamp = money.getStamp();
            new Thread(){
                public void run(){
                    while (true){
                        while (true){
                            Integer m = money.getReference();
                            if (m < 20){
                                if (money.compareAndSet(m,m + 20,timestamp,timestamp+1)){
                                    System.out.println("充值成功，余额："+money.getReference());
                                    break;
                                }
                            }else {
                                //System.out.println("不充值");
                                break;
                            }
                        }
                    }
                }
            }.start();
        }

        new Thread(){
            public void run(){
                for (int i = 0; i < 100; i++) {
                    while (true){
                        int time = money.getStamp();
                        int m = money.getReference();
                        if (m > 10){
                            System.out.println("大于10元");
                            if (money.compareAndSet(m,m - 10,time,time +1)){
                                System.out.println("消费成功，余额："+money.getReference());
                                break;
                            }
                        } else {
                            System.out.println("余额不足");
                            break;
                        }
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
