//package com.yupaopao.platform.content.es;
//
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.collect.Lists;
//import com.yupaopao.platform.content.binlog.BinlogRecord;
//import com.yupaopao.platform.content.request.TimeLineEsQuery;
//import com.yupaopao.platform.content.response.TimeLineEsDto;
//import com.yupaopao.platform.content.response.TimeLineEsQueryResult;
//import com.yupaopao.platform.content.response.TimeLineEsStatResult;
//import io.searchbox.client.JestClient;
//import io.searchbox.core.Bulk;
//import io.searchbox.core.BulkResult;
//import io.searchbox.core.Index;
//import io.searchbox.core.Search;
//import io.searchbox.core.SearchResult;
//import io.searchbox.core.Update;
//import io.searchbox.core.search.aggregation.TermsAggregation;
//import lombok.extern.slf4j.Slf4j;
//import org.elasticsearch.common.Strings;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.Operator;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.index.query.RangeQueryBuilder;
//import org.elasticsearch.script.Script;
//import org.elasticsearch.search.aggregations.AggregationBuilder;
//import org.elasticsearch.search.aggregations.AggregationBuilders;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.elasticsearch.search.sort.SortOrder;
//import org.springframework.stereotype.Component;
//import org.springframework.util.CollectionUtils;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//import java.util.List;
//
///**
// * 处理动态的数据消息
// *
// * @author zhangqinghua
// * @date 7:31 PM 2019/2/22
// */
//@Component
//@Slf4j
//public class TimeLineEsManager {
//
//    public static final String INDEX = "bixin_timeline";
//
//    @Resource
//    private JestClient jestClient;
//
//    public void saveTimeLineRecord(List<BinlogRecord> recordList) {
//        if (CollectionUtils.isEmpty(recordList)) {
//            return;
//        }
//        List<TimeLineEsDO> timeLineEsDOList = Lists.newArrayList();
//        recordList.forEach(record -> timeLineEsDOList.add(TimeLineEsConverter.fromTimeLineBinLog(record)));
//        try {
//            saveTimeLine(timeLineEsDOList);
//        } catch (Exception e) {
//            log.error("saveTimeLineRecord {} error", JSONObject.toJSONString(recordList), e);
//        }
//    }
//
//    public boolean saveTimeLine(List<TimeLineEsDO> timeLineEsDOList) throws IOException {
//        if (CollectionUtils.isEmpty(timeLineEsDOList)) {
//            return true;
//        }
//        Bulk.Builder bulk = new Bulk.Builder();
//        for (TimeLineEsDO timeLineEsDO : timeLineEsDOList) {
//            TimeLineEsQuery timeLineEsQuery = new TimeLineEsQuery();
//            timeLineEsQuery.setId(timeLineEsDO.getId());
//            timeLineEsQuery.setPageSize(2);
//            TimeLineEsQueryResult timeLineEsQueryResult = searchQuery(timeLineEsQuery);
//            if (CollectionUtils.isEmpty(timeLineEsQueryResult.getEsTimeLineList())) {
//                timeLineEsDO.setRecommendLevel(0);
//                Index index = new Index.Builder(timeLineEsDO).index(INDEX).type(INDEX).id(timeLineEsDO.getId()).build();
//                bulk.addAction(index);
//            } else {
//                timeLineEsDO.setRecommendLevel(null);
//                String json = JSONObject.toJSONString(timeLineEsDO);
//                String script = "{" + "    \"doc\" : " + json + "}";
//                Update update = new Update.Builder(script).index(INDEX).type(INDEX).id(timeLineEsDO.getId()).build();
//                bulk.addAction(update);
//            }
//        }
//        BulkResult result = jestClient.execute(bulk.build());
//        return result.isSucceeded();
//    }
//
//    public boolean updateRecommendLevel(List<TimeLineEsDO> timeLineEsDOList) throws IOException {
//        if (CollectionUtils.isEmpty(timeLineEsDOList)) {
//            return true;
//        }
//        Bulk.Builder bulk = new Bulk.Builder();
//        timeLineEsDOList.forEach(entity -> {
//            String script = "{" +
//                    "    \"doc\" : {" +
//                    "        \"recommendLevel\" : " +entity.getRecommendLevel() +
//                    "    }" +
//                    "}";
//            Update update = new Update.Builder(script).index(INDEX).type(INDEX).id(entity.getId()).build();
//            bulk.addAction(update);
//        });
//        BulkResult result = jestClient.execute(bulk.build());
//        return result.isSucceeded();
//    }
//
//    public TimeLineEsQueryResult searchQuery(TimeLineEsQuery timeLineEsQuery) throws IOException {
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        boolQueryBuilder = addCatQuery(boolQueryBuilder, timeLineEsQuery.getHasCat(), timeLineEsQuery.getCatId());
//        TimeLineEsQueryResult timeLineEsQueryResult = new TimeLineEsQueryResult();
//        if (boolQueryBuilder == null) {
//            return timeLineEsQueryResult;
//        }
//        boolQueryBuilder = addTagQuery(boolQueryBuilder, timeLineEsQuery.getHasTag(), timeLineEsQuery.getTagId());
//        if (boolQueryBuilder == null) {
//            return timeLineEsQueryResult;
//        }
//
//        addTermQuery(boolQueryBuilder, timeLineEsQuery);
//        addRangeQuery(boolQueryBuilder, timeLineEsQuery);
//
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        int pageNo = timeLineEsQuery.getPageNo();
//        int pageSize = timeLineEsQuery.getPageSize();
//        String query = searchSourceBuilder.query(boolQueryBuilder).sort("createTime", SortOrder.DESC).from(pageNo *
//                pageSize).size(pageSize).toString();
//
//        Search search = new Search.Builder(query).addIndex(INDEX).addType(INDEX).build();
//        SearchResult searchResult = jestClient.execute(search);
//        if (searchResult.isSucceeded()) {
//            List<TimeLineEsDto> resultList = Lists.newArrayList();
//            timeLineEsQueryResult.setTotal(searchResult.getTotal().intValue());
//            List<SearchResult.Hit<TimeLineEsDO, Void>> hits = searchResult.getHits(TimeLineEsDO.class);
//            for (SearchResult.Hit<TimeLineEsDO, Void> hit : hits) {
//                resultList.add(TimeLineEsConverter.convertToDto(hit.source));
//            }
//            timeLineEsQueryResult.setEsTimeLineList(resultList);
//        }
//        return timeLineEsQueryResult;
//    }
//
//    public List<TimeLineEsStatResult> statQueryVideo(List<String> userIdList) throws IOException {
//        List<TimeLineEsStatResult> timeLineEsStatResult = Lists.newArrayList();
//        if (CollectionUtils.isEmpty(userIdList)) {
//            return timeLineEsStatResult;
//        }
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        List<Integer> status = Lists.newArrayList();
//        status.add(1);
//        status.add(6);
//        boolQueryBuilder.must(QueryBuilders.termsQuery("status", status));
//        List<Integer> typeList = Lists.newArrayList();
//        typeList.add(2);
//        typeList.add(5);
//        boolQueryBuilder.must(QueryBuilders.termsQuery("type", typeList));
//        boolQueryBuilder.must(QueryBuilders.termsQuery("userId", userIdList));
//        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("userIdAgg").field("userId.keyword").size(Integer.MAX_VALUE);
//
//        String query = searchSourceBuilder.query(boolQueryBuilder).aggregation(aggregationBuilder).size(0).toString();
//        Search search = new Search.Builder(query).addIndex(INDEX).addType(INDEX).build();
//        SearchResult searchResult = jestClient.execute(search);
//        if (searchResult.isSucceeded()) {
//            TermsAggregation aggregations = searchResult.getAggregations().getTermsAggregation("userIdAgg");
//            List<TermsAggregation.Entry> buckets = aggregations.getBuckets();
//            if (!CollectionUtils.isEmpty(buckets)) {
//                for (TermsAggregation.Entry bucket : buckets) {
//                    TimeLineEsStatResult result = new TimeLineEsStatResult();
//                    result.setUserId(bucket.getKey());
//                    result.setVideoCount(bucket.getCount().intValue());
//                    timeLineEsStatResult.add(result);
//                }
//            }
//        }
//        return timeLineEsStatResult;
//    }
//
//    private void addTermQuery(BoolQueryBuilder boolQueryBuilder, TimeLineEsQuery timeLineEsQuery) {
//        if (!Strings.isNullOrEmpty(timeLineEsQuery.getId())) {
//            boolQueryBuilder.must(QueryBuilders.termQuery("id", timeLineEsQuery.getId()));
//        }
//        if (!Strings.isNullOrEmpty(timeLineEsQuery.getUserId())) {
//            boolQueryBuilder.must(QueryBuilders.termQuery("userId", timeLineEsQuery.getUserId()));
//        }
//        if (timeLineEsQuery.getTypeList() != null) {
//            boolQueryBuilder.must(QueryBuilders.termsQuery("type", timeLineEsQuery.getTypeList()));
//        }
//        if (timeLineEsQuery.getStatusList() != null) {
//            boolQueryBuilder.must(QueryBuilders.termsQuery("status", timeLineEsQuery.getStatusList()));
//        }
//        if (timeLineEsQuery.getIsWell() != null) {
//            boolQueryBuilder.must(QueryBuilders.termQuery("isWell", timeLineEsQuery.getIsWell()));
//        }
//        if (!Strings.isNullOrEmpty(timeLineEsQuery.getContent())) {
//            boolQueryBuilder.must(QueryBuilders.queryStringQuery(timeLineEsQuery.getContent()).field("content")
//                    .defaultOperator(Operator.AND));
//        }
//        if (timeLineEsQuery.getRecommendLevel() != null) {
//            boolQueryBuilder.must(QueryBuilders.termQuery("recommendLevel", timeLineEsQuery.getRecommendLevel()));
//        }
//    }
//
//    private void addRangeQuery(BoolQueryBuilder boolQueryBuilder, TimeLineEsQuery timeLineEsQuery) {
//        addUniqueFieldRangeQuery(boolQueryBuilder, "commentCount", timeLineEsQuery.getMinCommentCount(),
//                timeLineEsQuery.getMaxCommentCount());
//        addUniqueFieldRangeQuery(boolQueryBuilder, "praiseCount", timeLineEsQuery.getMinPraiseCount(),
//                timeLineEsQuery.getMaxPraiseCount());
//        addUniqueFieldRangeQuery(boolQueryBuilder, "videoLength", timeLineEsQuery.getMinVideoLength(),
//                timeLineEsQuery.getMaxVideoLength());
//        addUniqueFieldRangeQuery(boolQueryBuilder, "rewardDiamondCount", timeLineEsQuery.getMinRewardDiamondCount(),
//                timeLineEsQuery.getMaxRewardDiamondCount());
//        addUniqueFieldRangeQuery(boolQueryBuilder, "createTime", timeLineEsQuery.getBeginTime(), timeLineEsQuery
//                .getEndTime());
//    }
//
//    private void addUniqueFieldRangeQuery(BoolQueryBuilder boolQueryBuilder, String field, Object from, Object end) {
//        if (from == null && end == null) {
//            return;
//        }
//        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(field);
//        if (from != null) {
//            rangeQueryBuilder.gte(from);
//        }
//        if (end != null) {
//            rangeQueryBuilder.lt(end);
//        }
//        boolQueryBuilder.filter(rangeQueryBuilder);
//    }
//
//    private BoolQueryBuilder addTagQuery(BoolQueryBuilder boolQueryBuilder, Boolean hasTag, String tagId) {
//        if (hasTag == null) {
//            if (!Strings.isNullOrEmpty(tagId)) {
//                boolQueryBuilder.must(QueryBuilders.termQuery("tags", tagId));
//            }
//        } else {
//            if (hasTag) {
//                if (!Strings.isNullOrEmpty(tagId)) {
//                    boolQueryBuilder.must(QueryBuilders.termQuery("tags", tagId));
//                } else {
//                    Script script = new Script("doc['tags'].values.length > 0");
//                    boolQueryBuilder.filter(QueryBuilders.scriptQuery(script));
//                }
//            } else {
//                if (!Strings.isNullOrEmpty(tagId)) {
//                    return null;
//                }
//                Script script = new Script("doc['tags'].values.length <= 0");
//                boolQueryBuilder.filter(QueryBuilders.scriptQuery(script));
//            }
//        }
//        return boolQueryBuilder;
//    }
//
//    private BoolQueryBuilder addCatQuery(BoolQueryBuilder boolQueryBuilder, Boolean hasCat, String catId) {
//        if (hasCat == null) {
//            if (!Strings.isNullOrEmpty(catId)) {
//                boolQueryBuilder.must(QueryBuilders.termQuery("catId", catId));
//            }
//        } else {
//            if (hasCat) {
//                if (!Strings.isNullOrEmpty(catId)) {
//                    boolQueryBuilder.must(QueryBuilders.termQuery("catId", catId));
//                } else {
//                    boolQueryBuilder.mustNot(QueryBuilders.termQuery("catId.keyword", ""));
//                }
//            } else {
//                if (!Strings.isNullOrEmpty(catId)) {
//                    return null;
//                }
//                boolQueryBuilder.must(QueryBuilders.termQuery("catId.keyword", ""));
//            }
//        }
//        return boolQueryBuilder;
//    }
//}
//
