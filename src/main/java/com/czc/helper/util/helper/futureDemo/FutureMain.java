package com.czc.helper.util.helper.futureDemo;

import java.util.concurrent.*;

/**
 * @author cuizhongcheng
 * @date 2018/1/22
 *
 * Future模式
 */
public class FutureMain {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(1);

        //构造FutureTask
        //方案一
//        FutureTask<String> futureTask = new FutureTask<String>(new RealData("a"));
//        service.submit(futureTask);
        //方案二
        Future<String> futureTask = service.submit(new RealData("a"));

        System.out.println("请求完毕");

        try {
            //其他操作
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //若此时call方法未执行完，则会等待直至call方法的返回
        System.out.println("数据 = "+futureTask.get());
        service.shutdown();
    }

}
