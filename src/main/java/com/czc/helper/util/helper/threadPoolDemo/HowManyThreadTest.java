package com.czc.helper.util.helper.threadPoolDemo;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by cuizhongcheng on 2018/10/30
 */
@Component
@EnableScheduling
public class HowManyThreadTest {
    private static int num = 0;

    @Scheduled(fixedRate = 2 * 500)
    public void job() {
        System.out.println("this time is: " + (num + 1));
        num++;
        for (int i = 0; i < 10000; i++) {
            Th t = new Th();
            t.start();
        }
    }

    class Th extends Thread {
        @Override
        public void run() {
//            String name = Thread.currentThread().getName();
//            System.out.println("name is: " + name + ", time is: " + num);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println("end! name is: " + name);
        }
    }
}
