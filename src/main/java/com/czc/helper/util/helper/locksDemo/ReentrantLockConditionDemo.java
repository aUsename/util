package com.czc.helper.util.helper.locksDemo;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author cuizhongcheng
 * @date 2018/1/18
 */
public class ReentrantLockConditionDemo implements Runnable{
    static ReentrantLock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();

    @Override
    public void run() {
        try {
            lock.lock();
            condition.await();
            System.out.println("Thread is going on");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ReentrantLockConditionDemo reentrantLockConditionDemo = new ReentrantLockConditionDemo();
        Thread thread = new Thread(reentrantLockConditionDemo);
        thread.start();
        Thread.sleep(2000);

        lock.lock();
        System.out.println("signal Thread");
        condition.signal();
        lock.unlock();
    }
}
