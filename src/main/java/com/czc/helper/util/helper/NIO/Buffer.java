package com.czc.helper.util.helper.NIO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author cuizhongcheng
 * @date 2018/1/22
 */
public class Buffer {
    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        //buffer.flip();
        buffer.mapperdFile();
    }

    //将文件映射到内存中
    private void mapperdFile(){
        try {
            RandomAccessFile file = new RandomAccessFile("D:\\map.txt","rw");
            FileChannel channel = file.getChannel();
            MappedByteBuffer byteBuffer = channel.map(FileChannel.MapMode.READ_WRITE,0,file.length());
            while (byteBuffer.hasRemaining()){
                System.out.print((char) byteBuffer.get());
            }
            byteBuffer.put(0,(byte) 98);
            file.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void flip(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(15);
        System.out.println("limit="+byteBuffer.limit()+" capacity="+byteBuffer.capacity()+" position="+byteBuffer.position());
        for (int i = 0; i < 10; i++) {
            byteBuffer.put((byte) i);
        }
        System.out.println("limit="+byteBuffer.limit()+" capacity="+byteBuffer.capacity()+" position="+byteBuffer.position());

        System.out.println("flip");
        //将limit设为position，将position置零，从写到读转换的时候需要使用
        byteBuffer.flip();
        System.out.println("limit="+byteBuffer.limit()+" capacity="+byteBuffer.capacity()+" position="+byteBuffer.position());
        for (int i = 0; i < 5; i++) {
            System.out.print(byteBuffer.get());
        }
        System.out.println();
        System.out.println("limit="+byteBuffer.limit()+" capacity="+byteBuffer.capacity()+" position="+byteBuffer.position());

        System.out.println("flip");
        byteBuffer.flip();
        for (int i = 0; i < 5; i++) {
            System.out.print(byteBuffer.get());
        }
        System.out.println();
        System.out.println("limit="+byteBuffer.limit()+" capacity="+byteBuffer.capacity()+" position="+byteBuffer.position());
        //byteBuffer.flip();
        //System.out.print(byteBuffer.get());
        byteBuffer.flip();
        //byteBuffer.limit(15);
        System.out.println("limit="+byteBuffer.limit()+" capacity="+byteBuffer.capacity()+" position="+byteBuffer.position());
    }
}
