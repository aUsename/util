package com.czc.helper.util.helper.ffmpeg;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cuizhongcheng
 * @date 2018/1/24
 */
public class Test1 {

    public static void main(String[] args) {
        testjt();
    }

    public static boolean testjt(){
        String ffmpeg_path = "D:/ffmpeg/ffmpeg-20180122-2e96f52-win64-static/bin/ffmpeg.exe";
        String veido_path = "D:/ffmpeg/video/wmv9.wmv9";
        String image_path = "D:/ffmpeg/image/wmv9.jpg";

        List<String> commands = new ArrayList<String>();
        commands.add(ffmpeg_path);
        commands.add("-ss");
        commands.add("91");//这个参数是设置截取视频多少秒时的画面
        commands.add("-i");
        commands.add(veido_path);
        commands.add("-y");
        commands.add("-f");
        commands.add("image2");
        commands.add("-t");
        commands.add("0.001");
        commands.add("-s");
        commands.add("700x525"); //这个参数是设置截取图片的大小
        commands.add(image_path);
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            builder.start();
            System.out.println("截取成功");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
