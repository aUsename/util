package com.czc.helper.util.helper.threadPoolDemo.fork;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/**
 * @author cuizhongcheng
 * @date 2018/1/22
 */
public class CountTask extends RecursiveTask<Long> {
    public static final int THRESHOLD = 10000;
    private long start;
    private long end;

    public CountTask(long pos, long lastone) {
        this.start = pos;
        this.end = lastone;
    }

    @Override
    protected Long compute() {
        long sum = 0;
        boolean canCompute = (end - start) < THRESHOLD;
        if (canCompute) {
            for (long i = start; i <= end; i++) {
                sum += i;
            }
        } else {
            long step = (start + end) / 100;
            ArrayList<CountTask> countTasks = new ArrayList<CountTask>();
            long pos = start;
            for (int i = 0; i < 100; i++) {
                long lastone = pos + step;
                if (lastone > end) {
                    lastone = end;
                }
                CountTask subTask = new CountTask(pos, lastone);
                pos += step + 1;
                countTasks.add(subTask);
                subTask.fork();
            }
            for (CountTask t : countTasks) {
                sum += t.join();
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        CountTask countTask = new CountTask(0, 200000L);
        ForkJoinTask<Long> result = forkJoinPool.submit(countTask);
        try {
            long res = result.get();
            System.out.println("sum = " + res);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
