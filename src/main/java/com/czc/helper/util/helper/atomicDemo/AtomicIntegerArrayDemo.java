package com.czc.helper.util.helper.atomicDemo;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @author cuizhongcheng
 * @date 2018/1/17
 */
public class AtomicIntegerArrayDemo {
    static AtomicIntegerArray arr = new AtomicIntegerArray(10);
//    static Integer[] arr = new Integer[10];
    public static class AddThread implements Runnable{

        @Override
        public void run() {
            for (int k = 0; k < 100000; k++) {
                arr.getAndIncrement(k%arr.length());
//                for (int i = 0; i < 10; i++) {
//                    arr[i]++;
//                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
//        for (int i = 0; i < 10; i++) {
//            arr[i] = 0;
//        }
        Thread[] thread = new Thread[10];
        for (int k = 0; k < 10; k++) {
            thread[k] = new Thread(new AtomicIntegerArrayDemo.AddThread());
        }
        for (int i = 0; i < 10; i++) {
            thread[i].start();
        }
        for (int i = 0; i < 10; i++) {
            thread[i].join();
        }
//        for (int i = 0; i < 10; i++) {
//            System.out.println(arr[i]);
//        }
        System.out.println(arr);
    }
}
