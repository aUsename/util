package com.czc.helper.util.helper.threadPoolDemo.simple;

import java.util.List;
import java.util.Vector;

/**
 * @author cuizhongcheng
 * @date 2018/1/19
 */
public class ThreadPool {
    private static ThreadPool instance = null;

    private List<Worker> idleThreads;

    private int threadCounder;
    private boolean isShutdown = false;

    private ThreadPool(){
        this.idleThreads = new Vector(5);
        threadCounder = 0;
    }

    public int getThreadCounder() {
        return threadCounder;
    }

    //获取线程池实例
    public synchronized static ThreadPool getInstance(){
        if (instance == null){
            instance = new ThreadPool();
        }
        return instance;
    }

    //将线程放入池中
    protected synchronized void repool(Worker worker){
        if(!isShutdown){
            idleThreads.add(worker);
        }else {
            worker.shutdown();
        }
    }

    //停止池中所有线程
    public synchronized void shutdown(){
        isShutdown = true;
        for (int threadIndex = 0; threadIndex < idleThreads.size(); threadIndex++) {
            Worker worker = (Worker) idleThreads.get(threadIndex);
            worker.shutdown();
        }
    }

    //执行任务
    public synchronized void start(Runnable target){
        Worker thread = null;
        //若有空闲的线程，则直接使用
        if (idleThreads.size() > 0){
            int lastIndex = idleThreads.size() - 1;
            thread = (Worker) idleThreads.get(lastIndex);
            idleThreads.remove(lastIndex);
            //立即执行这个任务
            thread.setTarget(target);
        }else {
            //若没有空闲的线程，则创建新线程
            threadCounder++;
            //创建新线程
            thread = new Worker("PThread #" + threadCounder,this,target);
            //启动这个线程
            thread.start();
        }

    }
}
