package com.czc.helper.util.helper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;


/**
 * Created by cuizhongcheng on 2018/9/25
 * 分布式事务工具
 */
@Component
public class LockHelper {
    StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();

    // 默认锁超时时间:单位秒
    private int timeout = 3;

    // 加锁的 key 前缀
    private String keyPre = "PAYMENT_LOCK_";

    private Logger logger = LoggerFactory.getLogger(LockHelper.class);

    private long maxTime = 100;

    /**
     * 订单操作分布式事务加锁
     * <p>
     * 满足以下特性：
     * 1、锁同时只能被一个事务拿到
     * 2、锁本身带超时时间
     * 3、由于加锁和设置锁超时不是原子操作，需要在value中存入过期时间防止在极端情况下出现死锁，所以在加锁过程中需要添加对应的判断逻辑
     * <p>
     * 注意：
     * 1、订单的操作，应该是串行的，所以涉及到订单的操作，都应该使用分布式事务控制
     * 2、此处还可以做加锁的重试操作：如果加锁失败，则短暂的暂停后，继续尝试加锁，失败指定次数后再返回失败，不过会消耗更多资源
     * 3、FIXME: 必须保证所有服务的系统时间一致，如果各个服务的系统时间相差接近10秒，通过value判断超时的代码将会失效
     * 4、如果需要提供重入锁(ReentrantLock)的特性，需要在在value中保存当前服务及线程的唯一值，并添加相关逻辑
     *
     * @param key 锁ID
     * @return 是否拿到锁
     */
    private Boolean lock(String key) {
        return lock(key, timeout);
    }

    private Boolean lock(String key, Integer timeout) {
        if (StringUtils.isEmpty(key)) {
            return false;
        }
        String redisKey = keyPre + key;
        // 取到锁的值（方便后面的CAS操作）
        String value = stringRedisTemplate.opsForValue().get(redisKey);

        // 如果值存在，则判断时间有没有超时：防止线程在 setIfAbsent 和 expire之间挂掉时造成的死锁
        if (StringUtils.hasText(value)) {
            Long now = seconds();
            // 已经超时
            if (now > Integer.valueOf(value)) {
                // 这步操作也会有并发问题，所以需要使用 CAS 保证并发安全
                String pre = stringRedisTemplate.opsForValue().getAndSet(redisKey, String.valueOf(now + timeout));
                // 如果 CAS 成功，则表示加锁成功
                if (pre.equals(value)) {
                    // 设置过期时间
                    if (!stringRedisTemplate.expire(redisKey, timeout, TimeUnit.SECONDS)) {
                        stringRedisTemplate.delete(redisKey);
                        return false;
                    }
                    return true;
                } else {
                    // 此时加锁失败，取消本次操作
                    return false;
                }
            }
        }
        // 加锁
        if (!stringRedisTemplate.opsForValue().setIfAbsent(redisKey, String.valueOf(seconds() + timeout))) {
            return false;
        }
        // 设置过期时间
        if (!stringRedisTemplate.expire(redisKey, timeout, TimeUnit.SECONDS)) {
            stringRedisTemplate.delete(redisKey);
            return false;
        }
        return true;
    }

    /**
     * 获取单位为秒的时间戳
     */
    private Long seconds() {
        return System.currentTimeMillis() / 1000;
    }


    /**
     * 必须拿到锁，否则直接抛出异常
     */
    public void mustLock(String key) {
        mustLock(key, timeout);
    }

    public void mustLock(String key, Integer timeout) {
        Long start = System.currentTimeMillis();
        try {
            if (!lock(key, timeout)) {
                throw new RuntimeException("系统繁忙，请稍后再试");
            }
        } finally {
            Long time = System.currentTimeMillis() - start;
            if (time > maxTime) {
                logger.info("redis must lock time : {}ms", time);
            }
        }
    }

    /**
     * 订单操作分布式事务释放锁
     *
     * 释放锁是在拿到锁的情况下才可以操作，故不需要考虑并发
     *
     * @param key 锁ID
     */
    public void unlock(String key) {
        Long start = System.currentTimeMillis();
        try {
            if (StringUtils.isEmpty(key)) {
                throw new RuntimeException("订单号不能为空！");
            }
            stringRedisTemplate.delete(keyPre + key);
        } finally {
            Long time = System.currentTimeMillis() - start;
            if(time > maxTime) {
                logger.info("redis unlock time : {}ms", time);
            }
        }
    }


}
