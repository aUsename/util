package com.czc.helper.util.helper.locksDemo;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author cuizhongcheng
 * @date 2018/1/18
 */
public class ReentrantLockTimeLockDemo implements Runnable {
    static ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        try {
            if (lock.tryLock(5, TimeUnit.SECONDS)){
                Thread.sleep(6000);
            }else {
                System.out.println("get lock failed");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (lock.isHeldByCurrentThread()){
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        ReentrantLockTimeLockDemo reentrantLockTimeLockDemo = new ReentrantLockTimeLockDemo();
        Thread thread1 = new Thread(reentrantLockTimeLockDemo);
        Thread thread2 = new Thread(reentrantLockTimeLockDemo);
        thread1.start();
        thread2.start();
    }
}
