package com.czc.helper.util.helper.threadPoolDemo;

import java.util.concurrent.*;

/**
 * @author cuizhongcheng
 * @date 2018/1/22
 *
 * 拒绝策略
 */
public class RejectThreadPool {
    public static class MyTask implements Runnable{
        @Override
        public void run() {
            System.out.println("Thread ID:"+Thread.currentThread().getId());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyTask m = new MyTask();
        ExecutorService executorService = new ThreadPoolExecutor(5, 5,
                0L, TimeUnit.MILLISECONDS,
                new SynchronousQueue<Runnable>(),
                Executors.defaultThreadFactory(),
                new RejectedExecutionHandler() {
                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        System.out.println(r.toString()+" is discard");
                    }
                });
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            executorService.submit(m);
            Thread.sleep(100);
        }
    }
}
