package com.czc.helper.util.helper.futureDemo;

import java.util.concurrent.Callable;

/**
 * @author cuizhongcheng
 * @date 2018/1/22
 */
public class RealData implements Callable<String> {
    private String para;

    public RealData(String para) {
        this.para = para;
    }

    @Override
    public String call() throws Exception {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 10; i++) {
            buffer.append(para);
            Thread.sleep(100);
        }
        return buffer.toString();
    }
}
