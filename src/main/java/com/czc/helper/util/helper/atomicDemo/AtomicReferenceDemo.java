package com.czc.helper.util.helper.atomicDemo;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author cuizhongcheng
 * @date 2018/1/17
 */
public class AtomicReferenceDemo {
    public final static AtomicReference<String> atomicStr = new AtomicReference<String>("abc");
//    static String atomicStr = "abc";

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            final int num = i;
            new Thread(){
                public void run(){
                    try {
                        Thread.sleep(Math.abs((int)Math.random()*100));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
//                    if (atomicStr.equals("abc")){
//                        atomicStr = "def";
//                        System.out.println("Thread:"+Thread.currentThread().getId()+" change value to def");
//                    }else{
//                        System.out.println("Thread:"+Thread.currentThread().getId()+" failed");
//                    }
                    if (atomicStr.compareAndSet("abc","def")){
                        System.out.println("Thread:"+Thread.currentThread().getId()+" change value to def");
                    }else{
                        System.out.println("Thread:"+Thread.currentThread().getId()+" failed");
                    }
                }
            }.start();
        }
    }
}
