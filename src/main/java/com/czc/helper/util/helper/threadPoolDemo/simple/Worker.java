package com.czc.helper.util.helper.threadPoolDemo.simple;

/**
 * @author cuizhongcheng
 * @date 2018/1/19
 */
public class Worker extends Thread{
    private ThreadPool pool;
    private Runnable target;
    private boolean isShutdown = false;
    private boolean isIdle = false;

    public Worker(String name, ThreadPool pool, Runnable target) {
        super(name);
        this.pool = pool;
        this.target = target;
    }

    public Runnable getTarget() {
        return target;
    }

    public boolean isIdle() {
        return isIdle;
    }

    public void run(){
        while (!isShutdown){
            isIdle = false;
            if (target != null){
                target.run();
            }
            isIdle = true;
            try {
                pool.repool(this);
                synchronized (this){
                    wait();
                }
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
            isIdle = false;
        }
    }

    public synchronized void setTarget(Runnable newTarget){
        target = newTarget;
        //设置任务之后，通知run方法，开始执行这个任务
        notifyAll();
    }

    public synchronized void shutdown(){
        isShutdown = true;
        notifyAll();
    }
}
