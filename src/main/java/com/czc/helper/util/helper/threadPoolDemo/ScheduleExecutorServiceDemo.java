package com.czc.helper.util.helper.threadPoolDemo;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author cuizhongcheng
 * @date 2018/1/19
 *
 * newScheduledThreadPool
 */
public class ScheduleExecutorServiceDemo {
    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(10);
        //如果前面的任务没有完成，则调度不会启动
        service.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    System.out.println(System.currentTimeMillis()/1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },0,2, TimeUnit.SECONDS);

        for (int i = 0; i < 14; i++) {
            service.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                        System.out.println("next: "+System.currentTimeMillis()/1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            },0,2, TimeUnit.SECONDS);
        }
    }
}
