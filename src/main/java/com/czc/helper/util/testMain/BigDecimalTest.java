package com.czc.helper.util.testMain;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by cuizhongcheng on 2019/5/27
 */
public class BigDecimalTest {

    public static void main(String[] args) {
        double origin = 1.3050000006;
        BigDecimal b = BigDecimal.valueOf(origin);
        System.out.println(b);
        System.out.println(b.setScale(2, RoundingMode.HALF_UP));
        try {
            Thread.sleep(0L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
