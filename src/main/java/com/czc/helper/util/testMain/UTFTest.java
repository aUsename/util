package com.czc.helper.util.testMain;

import java.io.UnsupportedEncodingException;

/**
 * Created by cuizhongcheng on 2020/1/9
 */
public class UTFTest {
    public static void main(String[] args) {
        String s = "\uD83D\uDC95";
        System.out.println(s);
        try {
            String s8 = new String(s.getBytes(),"UTF-8");
            System.out.println(s8);
            String s16 = new String(s8.getBytes("UTF-8"),"UTF-32");
            System.out.println(s16);
        } catch (UnsupportedEncodingException e) {
            // ignore
        }
    }
}
