package com.czc.helper.util.testMain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2023/2/27
 */
class LRUCache {
    private Map<Integer,Node> map;
    private int capacity;
    private int size;
    private Node head;
    private Node tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<>(capacity);
        size = 0;
        head = new Node();
        tail = new Node();
        head.next = tail;
        tail.pre = head;
    }

    /**
     * 如果关键字key存在于缓存中，则返回关键字的值，否则返回-1
     */
    public int get(int key) {
        if(map.containsKey(key)){
            Node current = map.get(key);
            // 处理current前后节点
            current.pre.next = current.next;
            current.next.pre = current.pre;
            // 处理当前头节点
            head.next.pre = current;
            current.next = head.next;
            // current与首部head
            head.next = current;
            current.pre = head;

            return current.value;
        } else{
            return -1;
        }
    }

    /**
     * 如果关键字key已经存在，则变更其数据值value;
     * 如果不存在，则向缓存中插入该组key-value
     */
    public void put(int key, int value) {
        if(map.containsKey(key)){
            // 包含，直接替换，并前移当前节点
            Node current = map.get(key);
            current.value = value;
            // 前移当前节点，逻辑同查找时逻辑
            // 处理current前后节点
            current.pre.next = current.next;
            current.next.pre = current.pre;
            // 处理当前头节点
            head.next.pre = current;
            current.next = head.next;
            // current与首部head
            head.next = current;
            current.pre = head;
        } else {
            // 不包含，新增节点
            if(size == capacity){
                // 达到上限，需要先删除
                // 删除map中元素
                Node toRemove = tail.pre;
                map.remove(toRemove.key);
                size--;
                // 删除链表中元素
                tail.pre.pre.next = tail;
                tail.pre = tail.pre.pre;
                // 重新放置
                put(key,value);
            } else {
                // 未达到上限，直接在头部添加
                Node newNode = new Node(key,value);
                // 处理当前头节点
                head.next.pre = newNode;
                newNode.next = head.next;
                // 处理head节点与当前节点
                newNode.pre = head;
                head.next = newNode;
                // 新增
                map.put(key,newNode);
                size++;
            }
        }
    }
}

class Node {
    public Node pre;
    public Node next;
    public int value;
    public int key;
    public Node(){

    }
    public Node(int key,int value){
        this.key = key;
        this.value = value;
    }
}


public class ShowMeBug {

    // run test cases
    public static void main(String[] args) {
        int value;
        LRUCache lRUCache = new LRUCache(2);
        lRUCache.put(1, 1);         // 缓存为 {1=1}，新增关键字 1
        value = lRUCache.get(1);    // 缓存为 {1=1}，查询关键字 1
        assertPrint(1, 1, value);
        lRUCache.put(2, 2);         // 缓存为 {2=2, 1=1}，新增关键字 2
        lRUCache.put(3, 3);         // 缓存为 {3=3, 2=2}，新增关键字 3，关键字 1 作废
        value = lRUCache.get(1);    // 缓存为 {3=3, 2=2}，查询关键字 1
        assertPrint(1, -1, value);
        value = lRUCache.get(3);    // 缓存为 {3=3, 2=2}，查询关键字 3
        assertPrint(3, 3, value);
        value = lRUCache.get(2);    // 缓存为 {2=2, 3=3}，查询关键字 2
        assertPrint(2, 2, value);
        lRUCache.put(4, 4);         // 缓存为 {4=4, 2=2}，新增关键字 4，关键字 3 作废
        value = lRUCache.get(3);    // 缓存为 {4=4, 2=2}，查询关键字 3
        assertPrint(3, -1, value);
        lRUCache.put(2, 8);         // 缓存为 {2=8, 4=4}，更新关键字 2
        lRUCache.put(5, 5);         // 缓存为 {5=5, 2=8}，新增关键字 5，关键字 4 作废
        value = lRUCache.get(2);    // 缓存为 {2=8, 5=5}，查询关键字 2
        assertPrint(2, 8, value);
        value = lRUCache.get(5);    // 缓存为 {5=5, 2=8}，查询关键字 5
        assertPrint(5, 5, value);
        lRUCache.put(6, 6);         // 缓存为 {6=6, 5=5}，新增关键字 6，关键字 2 作废
        lRUCache.put(6, 9);         // 缓存为 {6=9, 5=5}，更新关键字 6
        value = lRUCache.get(4);    // 缓存为 {6=9, 5=5}，查询关键字 4
        assertPrint(4, -1, value);
        value = lRUCache.get(2);    // 缓存为 {6=9, 5=5}，查询关键字 2
        assertPrint(2, -1, value);
        value = lRUCache.get(6);    // 缓存为 {6=9, 5=5}，查询关键字 6
        assertPrint(6, 9, value);
    }

    public static void assertPrint(int key, int expect, int actual) {
        String result = (expect == actual) ? "pass" : "not_pass";
        String output = String.format("get key %s, expect: %2d, actual: %2d, result: %s", key, expect, actual, result);
        System.out.println(output);
    }
}
