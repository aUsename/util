package com.czc.helper.util.testMain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2023/2/21
 */
public class applovinMain {
    public static void main(String[] args) {
        System.out.println(getResult("abacde"));
        System.out.println(getResult("aabbcccc"));
        System.out.println(getResult("xrrrttyayayayayayayayayaysdiiidiiiimmddddxx"));
    }

    public static int getResult(String s) {
        int result = 0;
        int indexL = 0;
        int indexR = 0;

        int len = s.length();
        Map<Character, Integer> map = new HashMap<>(2);
        int tempLen = 0;
        // map 元素个数
        int size = 0;
        while (indexR < len) {
            // 包含时，累加
            if (map.containsKey(s.charAt(indexR))) {
                map.put(s.charAt(indexR), map.get(s.charAt(indexR)) + 1);
                tempLen++;
                result = Math.max(result, tempLen);
            } else {
                // 不包含，字符数已经为2，处理左指针
                if (size == 2) {
                    // 左移左指针直到被移除的key数量为0；
                    while (indexL <= indexR) {
                        tempLen--;
                        if (map.get(s.charAt(indexL)) == 1) {
                            size--;
                            map.remove(s.charAt(indexL));
                            indexL++;
                            indexR--;
                            break;
                        } else {
                            map.put(s.charAt(indexL), map.get(s.charAt(indexL)) - 1);
                            indexL++;
                        }
                    }
                } else {
                    // 不包含，且字符数小于2，新增
                    tempLen++;
                    size++;
                    map.put(s.charAt(indexR), 1);
                    result = Math.max(result, tempLen);
                }
            }
            indexR++;
        }
        return result;

    }
}
