package com.czc.helper.util.testMain.javaCommonTest;

import java.util.*;

/**
 * Created by cuizhongcheng on 2020/5/28
 */
public class ArrayListTest {
    public static void main(String[] args) {
        ArrayListTest test = new ArrayListTest();
//        test.ArraysCopyOfTest();
//        test.newA();
        test.subListIteratoTest();
    }

    private void newA(){
        ArrayList<String> arr = new ArrayList<>(2);
        arr.add("a");
        System.out.println(arr.size());
        System.out.println(arr);
    }

    /**
     * Arrays.copyOf 返回一个新数组,所以ArrayList.trimToSize，会产生一个新的elementData
     */
    private void ArraysCopyOfTest(){
        String[] s = {"1","2"};
        String[] t = Arrays.copyOf(s,1);
        System.out.println(t.length);
        System.out.println(s.length);
        t[0] = "3";
        System.out.println(s[0]);

    }

    private void iteratorAddTest(){
        ArrayList<Integer> a = new ArrayList<>();

        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);

        Iterator<Integer> s =  a.listIterator();
        s.next();
        s.next();
        ((ListIterator<Integer>) s).add(6);
        System.out.println(a);
        s.remove();

    }

    private void removeTest(){
        ArrayList<Integer> a = new ArrayList<>();

        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);

        Iterator<Integer> s =  a.iterator();
        s.remove();
        s.remove();
    }

    private void removeAllTest(){
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();

        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);

        b.add(1);
        b.add(3);
        b.add(5);

        a.removeAll(b);
        System.out.println(a);
    }

    private void subListRandomAccessTest(){
        ArrayList<Integer> s = new ArrayList<>();
        s.add(1);
        s.add(2);
        if (s instanceof RandomAccess) {
            System.out.println("s");
        }
        List<Integer> t = s.subList(0,1);
        if (t instanceof RandomAccess) {
            System.out.println("s RandomAccess");
        }
        if (t instanceof ArrayList) {
            System.out.println("s ArrayList");
        }

    }

    private void subListModified(){
        ArrayList<Integer> s = new ArrayList<>();
        s.add(1);
        s.add(2);
        List<Integer> t = s.subList(0,2);
        System.out.println("t: " + t);
        System.out.println("s: " + s);
        t.add(3);
        System.out.println("t: " + t);
        System.out.println("s: " + s);
        s.set(0,0);
        System.out.println("s: " + s);
        System.out.println("t: " + t);
        s.add(4);
        System.out.println("s: " + s);
        System.out.println("t: " + t);
    }

    public void subListIteratoTest(){
        ArrayList<Integer> s = new ArrayList<>();
        s.add(1);
        s.add(2);
        s.add(3);
        s.add(4);
        List<Integer> t = s.subList(0,3);
        System.out.println("t: " + t);
        Iterator i = t.iterator();
        System.out.println("1111");
    }

}
