package com.czc.helper.util.testMain;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by cuizhongcheng on 2019/8/3
 */
public class UrlDecodeTest {
    public static void main(String[] args) {
        try {
            String keyWord = URLDecoder.decode("10.00%7C10%B1%C8%D0%C4%B1%D2%B3%E4%D6%B5%3D10%D4%AA%2F%CA%E4%C8%EB%A1%BE%B1%C8%D0%C4ID%A1%BF%A3%AC%C7%EB%CE%F0%CA%E4%C8%EB%CA%D6%BB%FA%BA%C5%2F%B9%D9%B7%BD%D6%B1%B3%E4%7C%B1%C8%D0%C4%7C%7C%7C%7B%22buyerIp%22%3A%22113.129.182.143%22%2C%22buyerIpv6%22%3A%22%22%7D",
                    "GB2312");
            String[] result = keyWord.split("\\|");
            for (int i = 0; i < result.length; i++) {
                if (result[i].contains("buyerIp")) {
                    System.out.println(result[i]);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
