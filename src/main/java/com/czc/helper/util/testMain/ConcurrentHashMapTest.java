package com.czc.helper.util.testMain;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by cuizhongcheng on 2018/8/10
 */
public class ConcurrentHashMapTest {

    private static ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        ConcurrentHashMapTest test = new ConcurrentHashMapTest();
        MyThread myThread1 = new MyThread();
        myThread1.run("123");
        try {
            Thread.sleep(300000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MyThread myThread2 = new MyThread();
        myThread2.run("456");
    }

    public void get(String str){
        if (!map.containsKey("abc")){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            map.put("abc", str);
            System.out.println(map.get("abc"));
            map.put("abc","abc");
        }
        System.out.println(str);
        System.out.println(map.get("abc"));
    }


}

class MyThread extends Thread {
    public void run(String str){
        ConcurrentHashMapTest test = new ConcurrentHashMapTest();
        test.get(str);
    }
}