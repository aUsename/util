package com.czc.helper.util.testMain;

import com.czc.helper.util.helper.MD5Helper;

/**
 * @author cuizhongcheng
 * @date 2017/12/26
 */
public class MD5HelperTest {
    public static void main(String args[]){
        MD5Helper md5Helper = new MD5Helper();
        String str = "1234";
        System.out.println("md5(base64): "+md5Helper.EncoderByMd5(str));
        System.out.println("md5: "+md5Helper.md5(str));
        System.out.println("md5StaticByMarco: "+ MD5Helper.md5Static(str));
        System.out.println("md5StaticByNanNan: "+ MD5Helper.md5ByNanNan(str));
    }
}
