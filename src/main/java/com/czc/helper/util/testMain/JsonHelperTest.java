package com.czc.helper.util.testMain;

import com.czc.helper.util.helper.JsonHelper;
import com.czc.helper.util.testEntity.People;
import com.czc.helper.util.testEntity.Simple;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by cuizhongcheng on 2017/12/26
 */
//@Component
public class JsonHelperTest {
//    @Resource
//    private JsonHelper jsonHelper;
    private JsonHelper jsonHelper = new JsonHelper();

    /**
     * 任意对象转Json字符串
     * @return
     */
    public String  objectTojson(){
        String result = jsonHelper.string(getPeople());
        System.out.println("任意对象转Json字符串 result is: "+result);
        return result;
    }

    /**
     * 字符串转对象
     * @return
     */
    public People jsonToObject(){
        People result = jsonHelper.entity(objectTojson(),People.class);
        System.out.println("字符串转对象 result is: "+result);
        return result;
    }

    /**
     * 获取entity
     * @return
     */
    public People getPeople(){
        List<Integer> integerList = new ArrayList();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        List<Simple> simpleList = new ArrayList<>();
        simpleList.add(new Simple(1,"nanjing"));
        simpleList.add(new Simple(2,"beijing"));
        simpleList.add(new Simple(3,"shanghai"));
        People people = new People("0110","li",18,integerList,simpleList);
        return people;
    }

    public static void main(String[] args){
        JsonHelperTest jsonHelperTest = new JsonHelperTest();
        //jsonHelperTest.objectTojson();
        jsonHelperTest.jsonToObject();
    }
}
