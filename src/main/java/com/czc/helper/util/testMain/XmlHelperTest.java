package com.czc.helper.util.testMain;

import com.czc.helper.util.helper.XmlHelper;

/**
 * Created by cuizhongcheng on 2017/12/26
 */
public class XmlHelperTest {

    private XmlHelper xmlHelper = new XmlHelper();
    private JsonHelperTest jsonHelperTest = new JsonHelperTest();

    /**
     * 任意对象转xml字符串
     * @return
     */
    public String  objectTojson(){
        String result = xmlHelper.string(jsonHelperTest.getPeople());
        System.out.println("任意对象转xml字符串 result is: "+result);
        return result;
    }

    /**
     * 获取entity
     * @return
     */


    public static void main(String[] args){
        XmlHelperTest xmlHelperTest = new XmlHelperTest();
        xmlHelperTest.objectTojson();
    }
}
