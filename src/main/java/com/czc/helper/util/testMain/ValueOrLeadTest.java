package com.czc.helper.util.testMain;

import com.czc.helper.util.testEntity.Simple;

/**
 * Created by cuizhongcheng on 2018/7/16
 */
public class ValueOrLeadTest {

    /**
     * mainMethod1()
     * 基础数据类型，值传递
     * 引用类型，引用的值传递，指向同一对象，对象的值会被修改
     * mainMethod2()
     * 但是，引用本身不会被修改，因为是值传递
     * @param args
     */
    public static void main(String[] args) {
        ValueOrLeadTest valueOrLeadTest = new ValueOrLeadTest();
        valueOrLeadTest.mainMethod1();
        System.out.println("===================");
        valueOrLeadTest.mainMethod2();
    }

    public void mainMethod1(){
        String str = "STR";
        Simple simple = new Simple();
        simple.setIndex(0);
        simple.setCity("shanghai");

        System.out.println("before:");
        System.out.println("str is :"+str);
        System.out.println("simple.index is :"+simple.getIndex());
        System.out.println("simple.city is :"+simple.getCity());

        change1(str,simple);

        System.out.println("after:");
        System.out.println("str is :"+str);
        System.out.println("simple.index is :"+simple.getIndex());
        System.out.println("simple.city is :"+simple.getCity());
    }

    public void change1(String str,Simple simple){
        str = "str";
        simple.setCity("beijing");
        simple.setIndex(1);
    }

    public void mainMethod2(){
        Simple simple = new Simple();
        simple.setIndex(0);
        simple.setCity("shanghai");

        System.out.println("before:");
        System.out.println("simple.index is :"+simple.getIndex());
        System.out.println("simple.city is :"+simple.getCity());

        change2(simple);

        System.out.println("after:");
        System.out.println("simple.index is :"+simple.getIndex());
        System.out.println("simple.city is :"+simple.getCity());
    }

    public void change2(Simple simple){
        Simple newSimple = new Simple();
        newSimple.setCity("beijing");
        newSimple.setIndex(1);
        simple = newSimple;
    }
}
