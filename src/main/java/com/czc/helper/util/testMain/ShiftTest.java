package com.czc.helper.util.testMain;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by cuizhongcheng on 2018/7/17
 */
@Slf4j
public class ShiftTest {
    /**
     * char short等，在进行移位时会自动补全为int
     * @param args
     */
    public static void main(String[] args) {
        ShiftTest shiftTest = new ShiftTest();
        shiftTest.urShift1();
        shiftTest.urShift2();
    }

    public void urShift1(){
        int i = -1;
        i >>>= 10;
        log.info("i :" + i);

        long l = -1;
        l >>>= 10;
        log.info("l :" + l);

        short s = -1;
        s >>>= 10;
        log.info("s :" + s);

        byte b = -1;
        b >>>= 25;
        log.info("b :" + b);
    }

    public void urShift2(){
        int i = -1;
        i >>= 10;
        log.info("i :" + i);

        long l = -1;
        l >>= 10;
        log.info("l :" + l);

        short s = -1;
        s >>= 10;
        log.info("s :" + s);

        byte b = -1;
        b >>= 10;
        log.info("b :" + b);

    }
}
