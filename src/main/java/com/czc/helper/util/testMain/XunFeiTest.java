package com.czc.helper.util.testMain;

import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by cuizhongcheng on 2023/4/2
 */
public class XunFeiTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {// 注意，如果输入是多个测试用例，请通过while循环处理多个测试用例
            String str1 = sc.nextLine();
            String str2 = sc.nextLine();

            HashSet<Character> set = new HashSet<>();
            for (int i = 0; i < str2.length(); i++) {
                set.add(str2.charAt(i));
            }

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < str1.length(); i++) {
                if (!set.contains(str1.charAt(i))) {
                    result.append(str1.charAt(i));
                }
            }
            System.out.println(result.toString());
        }

    }
}

//    They are students.
//        aeiou