package com.czc.helper.util.testMain;

import com.czc.helper.util.helper.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author cuizhongcheng
 * @date 2017/12/26
 */
public class DateUtilsTest {
    public static void main(String[] args) {
        DateUtilsTest dateUtilsTest = new DateUtilsTest();
        //dateUtilsTest.getCurrent();
        //dateUtilsTest.Create();
        dateUtilsTest.getYear();

    }

    public void getCurrentUTC(){
        Date date = DateUtils.getCurrentUTC();
        System.out.println(date);
    }

    public void getCurrent(){
        Calendar calendar = Calendar.getInstance();
        System.out.println(DateUtils.formatCalendar(calendar,"MM/dd/yyyy"));//12/26/2017
        System.out.println(DateUtils.formatCalendar(calendar,"dd/MM/yyyy HH:mm:ss"));//26/12/2017 16:58:09
        System.out.println(DateUtils.formatCalendar(calendar,"dd/MM/yyyy HH:mm aa"));//26/12/2017 16:58 下午
    }

    public void Create(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String formatted = dateFormat.format(DateUtils.getCurrentUTC());
            System.out.println(DateUtils.getDateFromString(formatted,"dd-MM-yyyy HH:mm:ss"));
            System.out.println(DateUtils.getDateFromString("26/12/2017 16:58:09","dd/MM/yyyy HH:mm:ss"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void getYear(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        System.out.println(year);
    }
}
