package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/9
 *
 * <p>
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * 如果不存在公共前缀，返回空字符串 ""。
 * 示例 1：
 * 输入：strs = ["flower","flow","flight"]
 * 输出："fl"
 * 示例 2：
 * 输入：strs = ["dog","racecar","car"]
 * 输出：""
 * 解释：输入不存在公共前缀。
 * 提示：
 * 1 <= strs.length <= 200
 * 0 <= strs[i].length <= 200
 * strs[i] 仅由小写英文字母组成
 * <p>
 * 链接：https://leetcode.cn/problems/longest-common-prefix
 */
public class A14LongestCommonPrefix {
    public String longestCommonPrefix(String[] strs) {
        StringBuilder stringBuilder = new StringBuilder();
        char c = ' ';
        int index = 0;
        while (true) {
            for (int i = 0; i < strs.length; i++) {
                if (strs[i].length() > index) {
                    if (i == 0) {
                        c = strs[i].charAt(index);
                    } else {
                        if (strs[i].charAt(index) != c) {
                            return stringBuilder.toString();
                        }
                    }
                } else {
                    // 某个字符串遍历完，直接返回
                    return stringBuilder.toString();
                }
            }
            stringBuilder.append(c);
            index++;
        }
    }
}
