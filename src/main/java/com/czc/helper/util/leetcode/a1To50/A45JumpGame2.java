package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/21
 *
 * <p>
 * 给你一个非负整数数组 nums ，你最初位于数组的第一个位置。
 * 数组中的每个元素代表你在该位置可以跳跃的最大长度。
 * 你的目标是使用最少的跳跃次数到达数组的最后一个位置。
 * 假设你总是可以到达数组的最后一个位置。
 * 示例 1:
 * 输入: nums = [2,3,1,1,4]
 * 输出: 2
 * 解释: 跳到最后一个位置的最小跳跃数是 2。
 * 从下标为 0 跳到下标为 1 的位置，跳 1 步，然后跳 3 步到达数组的最后一个位置。
 * 示例 2:
 * 输入: nums = [2,3,0,1,4]
 * 输出: 2
 * 提示:
 * 1 <= nums.length <= 10^4
 * 0 <= nums[i] <= 1000
 * <p>
 * 链接：https://leetcode.cn/problems/jump-game-ii
 * 跳跃游戏 II
 */
public class A45JumpGame2 {

    public int jump(int[] nums) {
        int len = nums.length;
        // 每一位置表示到达此位置时的最少跳跃次数
        int[] pd = new int[len];
        for (int i = 1; i < len; i++) {
            pd[i] = Integer.MAX_VALUE;
        }
        pd[0] = 0;
        for (int i = 0; i < len; i++) {
            // 第i位置，往后可以走至 i + nums[i]处
            for (int j = i + 1; j <= i + nums[i]; j++) {
                if (j < len) {
                    pd[j] = Math.min(pd[i] + 1, pd[j]);
                } else {
                    break;
                }
            }
        }
        return pd[len - 1];
    }
}
