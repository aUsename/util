package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/14
 * <p>
 * 给你两棵二叉树的根节点 p 和 q ，编写一个函数来检验这两棵树是否相同。
 * 如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。
 * 示例 1：
 * 输入：p = [1,2,3], q = [1,2,3]
 * 输出：true
 * 示例 2：
 * 输入：p = [1,2], q = [1,null,2]
 * 输出：false
 * 示例 3：
 * 输入：p = [1,2,1], q = [1,1,2]
 * 输出：false
 * 提示：
 * 两棵树上的节点数目都在范围 [0, 100] 内
 * -10^4 <= Node.val <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/same-tree
 * 相同的树
 */
public class A100SameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (!isSameNode(p, q)) {
            return false;
        }
        if (p == null) {
            return true;
        }
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    private boolean isSameNode(TreeNode p, TreeNode q) {
        if (p == null) {
            return q == null;
        }
        return q != null && p.val == q.val;
    }
}
