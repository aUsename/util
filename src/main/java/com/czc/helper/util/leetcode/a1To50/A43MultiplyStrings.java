package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/10/21
 *
 * <p>
 * 给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。
 * 注意：不能使用任何内置的 BigInteger 库或直接将输入转换为整数。
 * 示例 1:
 * 输入: num1 = "2", num2 = "3"
 * 输出: "6"
 * 示例 2:
 * 输入: num1 = "123", num2 = "456"
 * 输出: "56088"
 * 提示：
 * 1 <= num1.length, num2.length <= 200
 * num1 和 num2 只能由数字组成。
 * num1 和 num2 都不包含任何前导零，除了数字0本身。
 * <p>
 * 链接：https://leetcode.cn/problems/multiply-strings
 * 字符串相乘
 */
public class A43MultiplyStrings {
    public String multiply(String num1, String num2) {
        String result = "0";
        if (num1.equals("0") || num2.equals("0")) {
            return result;
        }
        for (int i = 0; i < num2.length(); i++) {
            result = addStrings(result, multiplySingle(num1, num2.charAt(num2.length() - 1 - i) - '0', i));
        }
        return result;
    }

    private String multiplySingle(String num1, int num2, int count) {
        if (num2 == 0) {
            return "0";
        }
        StringBuilder builder = new StringBuilder();
        int ct = 0;
        int temp;
        for (int i = 0; i < num1.length(); i++) {
            temp = (num1.charAt(num1.length() - 1 - i) - '0') * num2 + ct;
            ct = temp / 10;
            temp = temp % 10;
            builder = builder.insert(0, temp);
        }
        if (ct > 0) {
            builder = builder.insert(0, ct);
        }
        for (int i = 0; i < count; i++) {
            builder.append('0');
        }

        return builder.toString();
    }


    /**
     * 使用了 A415AddStrings 两数相加的代码
     */
    private String addStrings(String num1, String num2) {
        int index = 0;
        StringBuilder builder = new StringBuilder();
        int ct = 0;
        int a, b, temp;

        while (index < num1.length() || index < num2.length()) {
            if (index < num1.length()) {
                a = num1.charAt(num1.length() - 1 - index) - '0';
            } else {
                a = 0;
            }

            if (index < num2.length()) {
                b = num2.charAt(num2.length() - 1 - index) - '0';
            } else {
                b = 0;
            }

            temp = a + b + ct;
            ct = temp / 10;
            temp = temp % 10;
            builder = builder.insert(0, temp);
            index++;
        }

        if (ct > 0) {
            builder = builder.insert(0, ct);
        }

        return builder.toString();
    }
}
