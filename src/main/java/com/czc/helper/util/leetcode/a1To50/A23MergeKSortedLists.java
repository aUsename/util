package com.czc.helper.util.leetcode.a1To50;

import com.czc.helper.util.leetcode.helper.ListNode;

import java.util.PriorityQueue;

/**
 * Created by cuizhongcheng on 2022/9/13
 *
 * <p>
 * 给你一个链表数组，每个链表都已经按升序排列。
 * 请你将所有链表合并到一个升序链表中，返回合并后的链表。
 * 示例 1：
 * 输入：lists = [[1,4,5],[1,3,4],[2,6]]
 * 输出：[1,1,2,3,4,4,5,6]
 * 解释：链表数组如下：
 * [
 * 1->4->5,
 * 1->3->4,
 * 2->6
 * ]
 * 将它们合并到一个有序链表中得到。
 * 1->1->2->3->4->4->5->6
 * 示例 2：
 * 输入：lists = []
 * 输出：[]
 * 示例 3：
 * 输入：lists = [[]]
 * 输出：[]
 * 提示：
 * k == lists.length
 * 0 <= k <= 10^4
 * 0 <= lists[i].length <= 500
 * -10^4 <= lists[i][j] <= 10^4
 * lists[i] 按 升序 排列
 * lists[i].length 的总和不超过 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/merge-k-sorted-lists
 * 合并K个升序链表
 */
public class A23MergeKSortedLists {

    /**
     * 自己实现1，每次找出数组中，最小的那个节点
     * <p>
     * todo leetcode的优先级队列实现，基本原理跟这个实现一致，不过利用了PriorityQueue，不需要自己去寻找最小节点，比较次数也会更少（只在构造优先级队列时比较）
     */
    public ListNode mergeKLists(ListNode[] lists) {
        ListNode head = new ListNode();
        ListNode tail = head;
        ListNode cur = findMin(lists);
        while (cur != null) {
            tail.next = cur;
            tail = tail.next;
            cur = findMin(lists);
        }
        return head.next;
    }

    /**
     * 寻找最小节点
     */
    private ListNode findMin(ListNode[] lists) {
        if (lists.length == 0) {
            return null;
        }
        int min = 0;
        int t = 0;
        ListNode minNode = null;
        for (int i = 0; i < lists.length; i++) {
            if (lists[i] != null) {
                if (minNode == null || min >= lists[i].val) {
                    min = lists[i].val;
                    minNode = lists[i];
                    t = i;
                }
            }
        }
        if (minNode == null) return null;
        lists[t] = lists[t].next;
        return new ListNode(min);
    }

    /**
     * 自己实现2，相当于分治，每次合并两个链表
     */
    public ListNode mergeKLists1(ListNode[] lists) {
        // 这里也可以每次合并result和list[i]，这样就不用判断lists.length了
        if (lists.length == 0) {
            return null;
        } else if (lists.length == 1) {
            return lists[0];
        }
        for (int i = lists.length - 1; i > 0; i--) {
            ListNode current = merge2Lists(lists[i], lists[i - 1]);
            lists[i - 1] = current;
        }
        return lists[0];
    }

    private ListNode merge2Lists(ListNode list1, ListNode list2) {
        ListNode head = new ListNode();
        ListNode current = head;
        while (list1 != null && list2 != null) {
            if (list1.val >= list2.val) {
                current.next = list2;
                list2 = list2.next;
                current = current.next;
            } else {
                current.next = list1;
                list1 = list1.next;
                current = current.next;
            }
        }
        current.next = list1 == null ? list2 : list1;
        return head.next;
    }


    public static void main(String[] args) {
        A23MergeKSortedLists mergeKSortedLists = new A23MergeKSortedLists();

        int[] nums1 = {1, 4, 5};
        ListNode list1 = mergeKSortedLists.getList(nums1);
        mergeKSortedLists.print(list1);
        System.out.println();

        int[] nums2 = {1, 3, 4};
        ListNode list2 = mergeKSortedLists.getList(nums2);
        mergeKSortedLists.print(list2);
        System.out.println();

        int[] nums3 = {2, 6};
        ListNode list3 = mergeKSortedLists.getList(nums3);
        mergeKSortedLists.print(list3);
        System.out.println();


        ListNode[] lists = new ListNode[3];
        lists[0] = list1;
        lists[1] = list2;
        lists[2] = list3;
        ListNode result = mergeKSortedLists.mergeKLists1(lists);
        mergeKSortedLists.print(result);
    }


    private ListNode getList(int[] nums) {
        ListNode head = new ListNode();
        ListNode current = head;
        for (int num : nums) {
            current.next = new ListNode(num);
            current = current.next;
        }
        return head.next;
    }

    private void print(ListNode list) {
        while (list != null) {
            System.out.println(list.val);
            list = list.next;
        }
    }

    // ----------- leetcode官方实现
    class Status implements Comparable<Status> {
        int val;
        ListNode ptr;

        Status(int val, ListNode ptr) {
            this.val = val;
            this.ptr = ptr;
        }

        public int compareTo(Status status2) {
            return this.val - status2.val;
        }
    }

    PriorityQueue<Status> queue = new PriorityQueue<Status>();

    public ListNode mergeKLists2(ListNode[] lists) {
        for (ListNode node : lists) {
            if (node != null) {
                queue.offer(new Status(node.val, node));
            }
        }
        ListNode head = new ListNode(0);
        ListNode tail = head;
        while (!queue.isEmpty()) {
            Status f = queue.poll();
            tail.next = f.ptr;
            tail = tail.next;
            if (f.ptr.next != null) {
                queue.offer(new Status(f.ptr.next.val, f.ptr.next));
            }
        }
        return head.next;
    }


}
