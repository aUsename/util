package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/15
 * <p>
 * 给定两个整数数组 preorder 和 inorder ，其中 preorder 是二叉树的先序遍历， inorder 是同一棵树的中序遍历，请构造二叉树并返回其根节点。
 * 示例 1:
 * 输入: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
 * 输出: [3,9,20,null,null,15,7]
 * 示例 2:
 * 输入: preorder = [-1], inorder = [-1]
 * 输出: [-1]
 * 提示:
 * 1 <= preorder.length <= 3000
 * inorder.length == preorder.length
 * -3000 <= preorder[i], inorder[i] <= 3000
 * preorder 和 inorder 均 无重复 元素
 * inorder 均出现在 preorder
 * preorder 保证 为二叉树的前序遍历序列
 * inorder 保证 为二叉树的中序遍历序列
 * <p>
 * 链接：https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-inorder-traversal
 * 从前序与中序遍历序列构造二叉树
 */
public class A105ConstructBinaryTreeFromPreorderAndInorderTraversal {

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        int len = preorder.length;
        if (len == 0) {
            return null;
        }
        TreeNode head = new TreeNode();
        turnNext(preorder, inorder, head, true, 0, len - 1, 0, len - 1);
        return head.left;
    }

    private void turnNext(int[] preorder, int[] inorder, TreeNode current, boolean left,
                          int preLeft, int preRight, int inLeft, int inRight) {
        if (preLeft > preRight) return;
        TreeNode treeNode = new TreeNode(preorder[preLeft]);
        if (left) {
            current.left = treeNode;
        } else {
            current.right = treeNode;
        }
        for (int mid = inLeft; mid <= inRight; mid++) {
            if (treeNode.val == inorder[mid]) {
                turnNext(preorder, inorder, treeNode, true, preLeft + 1, preLeft + mid - inLeft, inLeft, mid - 1);
                turnNext(preorder, inorder, treeNode, false, preRight + mid + 1 - inRight, preRight, mid + 1, inRight);
                return;
            }
        }
    }
}
