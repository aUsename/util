package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by cuizhongcheng on 2022/11/11
 *
 * <p>
 * 给你一个整数 n ，求恰由 n 个节点组成且节点值从 1 到 n 互不相同的 二叉搜索树 有多少种？返回满足题意的二叉搜索树的种数。
 * 示例 1：
 * 输入：n = 3
 * 输出：5
 * 示例 2：
 * 输入：n = 1
 * 输出：1
 * 提示：
 * 1 <= n <= 19
 * <p>
 * 链接：https://leetcode.cn/problems/unique-binary-search-trees
 * 不同的二叉搜索树
 */
public class A96UniqueBinarySearchTrees {

    private int result;
    private int[] arr;

    /**
     * 直接把A95UniqueBinarySearchTrees的代码拿来用了，会超时
     * 其实就是看19个节点能有多少种不同的二叉树
     */
    public int numTrees(int n) {
        arr = new int[n];
        arr[0] = 1;
        Deque<Integer> deque = new ArrayDeque<>();
        deque.addLast(2);
        deque.addLast(3);
        next(n, 1, deque);
        return result;
    }

    private void next(int n, int index, Deque<Integer> deque) {

        // 构造树
        if (index == n) {
            result++;
            return;
        }

        int current = deque.pollFirst();

        // 当前值加入树中
        arr[index] = current;

        deque.addLast(current * 2);
        deque.addLast(current * 2 + 1);
        // 下一位置
        next(n, index + 1, deque);
        // 还原
        deque.pollLast();
        deque.pollLast();


        // 当前值不加入树中
        if (!deque.isEmpty()) {
            next(n, index, deque);
        }
        deque.addFirst(current);
    }


    /**
     * 自己实现，动态规划
     *
     * @param n
     * @return
     */
    public int numTrees1(int n) {
        // result[n] = result[0] * result[n-0 -1] + ... + result[n-0 -1] * result[0]
        int[] result = new int[n + 1];
        result[0] = 1;
        result[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                result[i] = result[i] + result[j] * result[i - 1 - j];
            }
        }
        return result[n];
    }

    public static void main(String[] args) {
        A96UniqueBinarySearchTrees uniqueBinarySearchTrees = new A96UniqueBinarySearchTrees();
        System.out.println(uniqueBinarySearchTrees.numTrees1(19));
    }
}
