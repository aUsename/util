package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/31
 *
 * <p>
 * 给定一个单词数组 words 和一个长度 maxWidth ，重新排版单词，使其成为每行恰好有 maxWidth 个字符，且左右两端对齐的文本。
 * 你应该使用 “贪心算法” 来放置给定的单词；也就是说，尽可能多地往每行中放置单词。必要时可用空格 ' ' 填充，使得每行恰好有 maxWidth 个字符。
 * 要求尽可能均匀分配单词间的空格数量。如果某一行单词间的空格不能均匀分配，则左侧放置的空格数要多于右侧的空格数。
 * 文本的最后一行应为左对齐，且单词之间不插入额外的空格。
 * 注意:
 * 单词是指由非空格字符组成的字符序列。
 * 每个单词的长度大于 0，小于等于 maxWidth。
 * 输入单词数组 words 至少包含一个单词。
 * 示例 1:
 * 输入: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
 * 输出:
 * [
 * "This    is    an",
 * "example  of text",
 * "justification.  "
 * ]
 * 示例 2:
 * 输入:words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
 * 输出:
 * [
 * "What   must   be",
 * "acknowledgment  ",
 * "shall be        "
 * ]
 * 解释: 注意最后一行的格式应为 "shall be    " 而不是 "shall     be",
 * 因为最后一行应为左对齐，而不是左右两端对齐。
 * 第二行同样为左对齐，这是因为这行只包含一个单词。
 * 示例 3:
 * 输入:words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"]，maxWidth = 20
 * 输出:
 * [
 * "Science  is  what we",
 * "understand      well",
 * "enough to explain to",
 * "a  computer.  Art is",
 * "everything  else  we",
 * "do                  "
 * ]
 * 提示:
 * 1 <= words.length <= 300
 * 1 <= words[i].length <= 20
 * words[i] 由小写英文字母和符号组成
 * 1 <= maxWidth <= 100
 * words[i].length <= maxWidth
 * <p>
 * 链接：https://leetcode.cn/problems/text-justification
 * 文本左右对齐
 */
public class A68TextJustification {
    private String[] words;
    private int len;
    private int maxWidth;
    private List<String> result = new ArrayList<>();

    public List<String> fullJustify(String[] words, int maxWidth) {
        this.words = words;
        this.maxWidth = maxWidth;
        len = words.length;

        int start = 0, end, tempSize;
        while (start < len) {
            end = start;
            tempSize = 0;
            while (end < len) {
                if (end == start) {
                    tempSize = tempSize + words[end].length();
                    end++;
                } else if (tempSize + 1 + words[end].length() <= maxWidth) {
                    tempSize = tempSize + 1 + words[end].length();
                    end++;
                } else {
                    trim(start, end - 1, tempSize);
                    break;
                }
            }
            if (end == len) {
                trimLast(start, end);
                break;
            }
            start = end;
        }

        return result;
    }

    // 普通行
    private void trim(int start, int end, int strSize) {
        strSize = strSize - (end - start);
        int spaceSize = maxWidth - strSize;
        int spaceCount = end - start;

        StringBuilder builder = new StringBuilder();
        builder.append(words[start]);

        if (spaceCount == 0) {
            while (builder.length() < maxWidth) {
                builder.append(' ');
            }
        } else {
            int everyLen = spaceSize / (spaceCount);
            int modCount = spaceSize % (spaceCount);
            for (int i = 0; i < spaceCount; i++) {
                for (int j = 0; j < everyLen; j++) {
                    builder.append(' ');
                }
                if (i < modCount) {
                    builder.append(' ');
                }
                builder.append(words[start + 1 + i]);
            }
        }
//        System.out.println(builder.toString());
        result.add(builder.toString());
    }

    // 最后一行
    private void trimLast(int start, int end) {
        StringBuilder builder = new StringBuilder();
        int tempIndex = start;
        while (tempIndex < end) {
            if (tempIndex != start) {
                builder.append(' ').append(words[tempIndex]);
            } else {
                builder.append(words[tempIndex]);
            }
            tempIndex++;
        }
        while (builder.length() < maxWidth) {
            builder.append(' ');
        }
//        System.out.println(builder.toString());
        result.add(builder.toString());
    }

    public static void main(String[] args) {
        A68TextJustification justification = new A68TextJustification();
        String[] words = {"This", "is", "an", "example", "of", "text", "justification."};
        justification.fullJustify(words, 16);
    }
}
