package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/8
 * <p>
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 * 如果反转后整数超过 32 位的有符号整数的范围 [−2^31,  2^31 − 1] ，就返回 0。
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 * 示例 1：
 * 输入：x = 123
 * 输出：321
 * 示例 2：
 * 输入：x = -123
 * 输出：-321
 * 示例 3：
 * 输入：x = 120
 * 输出：21
 * 示例 4：
 * 输入：x = 0
 * 输出：0
 * 提示：
 * -2^31 <= x <= 2^31 - 1
 * <p>
 * 链接：https://leetcode.cn/problems/reverse-integer
 */
public class A7ReverseInteger {
    public int reverse(int x) {
        int result = 0;
        int isNe = 1;
        if (x < 0) {
            x = -x;
            isNe = -1;
        }

        while (x > 0) {
            result = result * 10 + x % 10;
            x = x / 10;

            if (x > 0 && result > Integer.MAX_VALUE / 10) return 0;
            if (result < 0) return 0;
        }

        return result * isNe;
    }


    public static void main(String[] args) {
        A7ReverseInteger reverseInteger = new A7ReverseInteger();
        System.out.println(reverseInteger.reverse(1534236469));
    }
}
