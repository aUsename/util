package com.czc.helper.util.leetcode.a1To50;

import java.util.Stack;

/**
 * Created by cuizhongcheng on 2022/9/14
 *
 * <p>
 * 给你一个只包含 '(' 和 ')' 的字符串，找出最长有效（格式正确且连续）括号子串的长度。
 * 示例 1：
 * 输入：s = "(()"
 * 输出：2
 * 解释：最长有效括号子串是 "()"
 * 示例 2：
 * 输入：s = ")()())"
 * 输出：4
 * 解释：最长有效括号子串是 "()()"
 * 示例 3：
 * 输入：s = ""
 * 输出：0
 * 提示：
 * 0 <= s.length <= 3 * 104
 * s[i] 为 '(' 或 ')'
 * <p>
 * 链接：https://leetcode.cn/problems/longest-valid-parentheses
 */
public class A32LongestValidParentheses {
    /**
     * 自己实现，暴力法，两次循环
     * 时间复杂O(n^2) 空间O(1)
     */
    public int longestValidParentheses(String s) {
        int maxLen = 0;
        char[] strList = s.toCharArray();
        for (int i = 0; i < strList.length; i++) {
            int point = 0;
            int len = 0;
            for (int j = i; j < strList.length; j++) {
                if (strList[j] == '(') {
                    point++;
                    len++;
                } else {
                    point--;
                    if (point < 0) {
                        break;
                    }
                    len++;
                    // point = 0时，子字符串有效
                    if (point == 0) {
                        if (maxLen < len) {
                            maxLen = len;
                        }
                    }
                }
            }
        }
        return maxLen;
    }


    /**
     * 自己实现，利用栈
     * 时间复杂O(n) 空间O(n)
     */
    public int longestValidParentheses1(String s) {
        int maxLen = 0;
        char[] strList = s.toCharArray();
        Stack<Integer> stack = new Stack<>();
        int begin = 0;
        for (int i = 0; i < strList.length; i++) {
            if (strList[i] == '(') {
                if (stack.empty() && begin == -1) {
                    begin = i;
                }
                stack.push(i);
            } else {
                if (stack.empty()) {
                    begin = -1;
                    continue;
                }
                stack.pop();
                if (stack.empty()) {
                    // 栈是空的时候，没法通过peek来获取第一个合法（ 的位置，所以维护了一个begin来保存
                    maxLen = maxLen >= i - begin + 1 ? maxLen : i - begin + 1;
                } else {
                    int temp1 = stack.peek();
                    maxLen = maxLen >= i - temp1 ? maxLen : i - temp1;
                }
            }
        }
        return maxLen;
    }

    /**
     * leetcode解法 两次遍历法
     * 思想： 对括号匹配问题，左右两次遍历
     */
    public int longestValidParentheses2(String s) {
        int maxLen = 0;
        char[] strList = s.toCharArray();

        int point = 0, begin = 0;
        // 左遍历
        for (int i = 0; i < strList.length; i++) {
            if (strList[i] == '(') {
                point++;
            } else {
                point--;
            }
            if (point == 0) {
                maxLen = maxLen > i - begin + 1 ? maxLen : i - begin + 1;
            } else if (point < 0) {
                point = 0;
                begin = i + 1;
            }
        }

        point = 0;
        begin = strList.length - 1;
        // 右遍历
        for (int i = strList.length - 1; i >= 0; i--) {
            if (strList[i] == ')') {
                point++;
            } else {
                point--;
            }

            if (point == 0) {
                maxLen = maxLen > begin - i + 1 ? maxLen : begin - i + 1;
            } else if (point < 0) {
                point = 0;
                begin = i - 1;
            }
        }

        return maxLen;
    }

    public static void main(String[] args) {
        A32LongestValidParentheses longestValidParentheses = new A32LongestValidParentheses();
        int re = longestValidParentheses.longestValidParentheses2("())()(()((");
        System.out.println(re);
    }
}
