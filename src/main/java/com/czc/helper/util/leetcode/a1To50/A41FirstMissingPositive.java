package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/9/13
 * <p>
 * 给你一个未排序的整数数组 nums ，请你找出其中没有出现的最小的正整数。
 * 请你实现时间复杂度为 O(n) 并且只使用常数级别额外空间的解决方案。
 * 示例 1：
 * 输入：nums = [1,2,0]
 * 输出：3
 * 示例 2：
 * 输入：nums = [3,4,-1,1]
 * 输出：2
 * 示例 3：
 * 输入：nums = [7,8,9,11,12]
 * 输出：1
 * 提示：
 * 1 <= nums.length <= 5 * 10^5
 * -2^31 <= nums[i] <= 2^31 - 1
 * <p>
 * 链接：https://leetcode.cn/problems/first-missing-positive
 */
public class A41FirstMissingPositive {

    /**
     * 思想 原地哈希法
     * <p>
     * 由于要求只使用常数级别额外空间，所以需要使用原数组的空间，关键点在于：修改原数组中的值，但却不影响我们使用原数组的每个元素值（只打标签）
     * <p>
     * 在本题中，我们可以将所有负数转化为不影响结果的正数，然后用负号做标签
     * 满足条件的结果，只可能存在于1 ～ n+1中，可以将负数转为>= n+1的数
     * 打标签：遍历数组，若当前数位于 1 ～ n之间，则将数组对应位置至为负数
     * 寻找：找到数组中第一个不为负数的位置，若全为负数，则结果为n+1
     *
     * @return
     */
    public int firstMissingPositive(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] <= 0) {
                nums[i] = nums.length + 1;
            }
        }
        for (int num : nums) {
            // int count = Math.abs(num);
            int count = num;
            if (count < 0) {
                count = -count;
            }
            // 这里是使用下标0 ～ n-1 来表示 1 ～ n数字存不存在
            if (count <= nums.length && nums[count - 1] > 0) {
                nums[count - 1] = -nums[count - 1];
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                return i + 1;
            }
        }
        return nums.length + 1;
    }

    public static void main(String[] args) {
        A41FirstMissingPositive firstMissingPositive = new A41FirstMissingPositive();
//        int[] nums = {3, 4, -1, 1};
        int[] nums = {1};
//        int[] nums = {1, 2, 0};
        int result = firstMissingPositive.firstMissingPositive(nums);
        System.out.println(result);
    }
}
