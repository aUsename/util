package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/11/3
 * <p>
 * 已知存在一个按非降序排列的整数数组 nums ，数组中的值不必互不相同。
 * 在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转 ，使数组变为 [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]（下标 从 0 开始 计数）。例如， [0,1,2,4,4,4,5,6,6,7] 在下标 5 处经旋转后可能变为 [4,5,6,6,7,0,1,2,4,4] 。
 * 给你 旋转后 的数组 nums 和一个整数 target ，请你编写一个函数来判断给定的目标值是否存在于数组中。如果 nums 中存在这个目标值 target ，则返回 true ，否则返回 false 。
 * 你必须尽可能减少整个操作步骤。
 * 示例 1：
 * 输入：nums = [2,5,6,0,0,1,2], target = 0
 * 输出：true
 * 示例 2：
 * 输入：nums = [2,5,6,0,0,1,2], target = 3
 * 输出：false
 * 提示：
 * 1 <= nums.length <= 5000
 * -10^4 <= nums[i] <= 10^4
 * 题目数据保证 nums 在预先未知的某个下标上进行了旋转
 * -10^4 <= target <= 10^4
 * 进阶：
 * 这是 搜索旋转排序数组 的延伸题目，本题中的 nums  可能包含重复元素。
 * 这会影响到程序的时间复杂度吗？会有怎样的影响，为什么？
 * <p>
 * 链接：https://leetcode.cn/problems/search-in-rotated-sorted-array-ii
 * 搜索旋转排序数组2
 */
public class A81SearchInRotatedSortedArray2 {

    public boolean search(int[] nums, int target) {
        if (nums.length == 0) return false;
        return search(nums, target, 0, nums.length - 1);
    }

    private boolean search(int[] nums, int target, int left, int right) {
        if (left > right) {
            return false;
        }

        int mid = (left + right) / 2;
        if (nums[mid] == target) {
            return true;
        }

        // 若left < right,一定不包含分割点
        if (nums[left] < nums[right]) {
            if (nums[mid] < target) {
                return search(nums, target, mid + 1, right);
            } else {
                return search(nums, target, left, mid - 1);
            }
        } else if (nums[left] > nums[right]) {
            // 一定包含分割点
            if (nums[left] < nums[mid]) {
                // 此时mid位于分割点左边
                if (nums[mid] > target) {
                    return search(nums, target, left, mid - 1) || search(nums, target, mid + 1, right);
                } else {
                    return search(nums, target, mid + 1, right);
                }
            } else if (nums[left] > nums[mid]) {
                // 此时mid位于分割点右边
                if (nums[mid] < target) {
                    return search(nums, target, left, mid - 1) || search(nums, target, mid + 1, right);
                } else {
                    return search(nums, target, left, mid - 1);
                }
            } else {
                return search(nums, target, mid + 1, right);
            }

        } else {
            // 不确定是是否有分割点
            return search(nums, target, left, mid - 1) || search(nums, target, mid + 1, right);
        }
    }

    public static void main(String[] args) {
        A81SearchInRotatedSortedArray2 sortedArray2 = new A81SearchInRotatedSortedArray2();
        System.out.println(sortedArray2.search(new int[]{1, 3, 5}, 5));
    }
}
