package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/11/9
 * <p>
 * 给你单链表的头指针 head 和两个整数 left 和 right ，其中 left <= right 。请你反转从位置 left 到位置 right 的链表节点，返回 反转后的链表 。
 * 示例 1：
 * 输入：head = [1,2,3,4,5], left = 2, right = 4
 * 输出：[1,4,3,2,5]
 * 示例 2：
 * 输入：head = [5], left = 1, right = 1
 * 输出：[5]
 * 提示：
 * 链表中节点数目为 n
 * 1 <= n <= 500
 * -500 <= Node.val <= 500
 * 1 <= left <= right <= n
 * 进阶： 你可以使用一趟扫描完成反转吗？
 * <p>
 * 链接：https://leetcode.cn/problems/reverse-linked-list-ii
 * 反转链表 II
 */
public class A92ReverseLinkedList2 {
    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (left == right) return head;

        ListNode resultPre = new ListNode(0, head);

        ListNode current = resultPre;
        for (int i = 1; i < left; i++) {
            current = current.next;
        }
        // 此时 current位于需要倒置链表节点的前一位，缓存此位置 pre
        ListNode pre = current;

        // 此时 current位于需要倒置链表节点第一位，last
        current = current.next;
        ListNode last = current;

        // 此后current一直指向需要被放到pre后的那个位置，共有right-left个节点需要处理
        for (int i = left; i < right; i++) {
            // current指向下一需要转移位置
            current = last.next;
            // 结尾的节点往后移动一位
            last.next = current.next;
            // 需要被移动的节点换到pre后面
            current.next = pre.next;
            pre.next = current;
        }

        return resultPre.next;
    }
}
