package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/14
 * <p>
 * 给你一个二叉树的根节点 root ， 检查它是否轴对称。
 * 示例 1：
 * 输入：root = [1,2,2,3,4,4,3]
 * 输出：true
 * 示例 2：
 * 输入：root = [1,2,2,null,3,null,3]
 * 输出：false
 * 提示：
 * 树中节点数目在范围 [1, 1000] 内
 * -100 <= Node.val <= 100
 * 进阶：你可以运用递归和迭代两种方法解决这个问题吗？
 * <p>
 * 链接：https://leetcode.cn/problems/symmetric-tree
 * 对称二叉树
 */
public class A101SymmetricTree {

    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSameTree(root.left, root.right);
    }

    private boolean isSameTree(TreeNode p, TreeNode q) {
        if (!isSameNode(p, q)) {
            return false;
        }
        if (p == null) {
            return true;
        }
        return isSameTree(p.left, q.right) && isSameTree(p.right, q.left);
    }

    private boolean isSameNode(TreeNode p, TreeNode q) {
        if (p == null) {
            return q == null;
        }
        return q != null && p.val == q.val;
    }
}
