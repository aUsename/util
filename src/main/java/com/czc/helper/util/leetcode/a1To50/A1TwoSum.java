package com.czc.helper.util.leetcode.a1To50;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/9/6
 * <p>
 * <p>
 * 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
 * 你可以按任意顺序返回答案。
 * 示例 1：
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[0,1]
 * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 * 示例 2：
 * 输入：nums = [3,2,4], target = 6
 * 输出：[1,2]
 * 示例 3：
 * 输入：nums = [3,3], target = 6
 * 输出：[0,1]
 * <p>
 * 链接：https://leetcode.cn/problems/two-sum
 */
public class A1TwoSum {

    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer re = map.get(target - nums[i]);
            if (re != null) {
                result[0] = re;
                result[1] = i;
                return result;
            }
            map.put(nums[i], i);

        }
        return result;
    }

    public static void main(String[] args) {
        A1TwoSum twoSum = new A1TwoSum();
        int[] a = {3, 3};
        int[] s = twoSum.twoSum(a, 6);
        System.out.println(s[0] + "-----" + s[1]);
    }

}
