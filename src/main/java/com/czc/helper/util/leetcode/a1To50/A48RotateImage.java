package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/10/24
 * <p>
 * 给定一个 n × n 的二维矩阵 matrix 表示一个图像。请你将图像顺时针旋转 90 度。
 * 你必须在 原地 旋转图像，这意味着你需要直接修改输入的二维矩阵。请不要 使用另一个矩阵来旋转图像。
 * 示例 1：
 * 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * 输出：[[7,4,1],[8,5,2],[9,6,3]]
 * 示例 2：
 * 输入：matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
 * 输出：[[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]
 * 提示：
 * n == matrix.length == matrix[i].length
 * 1 <= n <= 20
 * -1000 <= matrix[i][j] <= 1000
 * <p>
 * 链接：https://leetcode.cn/problems/rotate-image
 * 旋转图像
 */
public class A48RotateImage {
    /**
     * 第x行，第y列 转换规则：
     * (x,y) -> (y,n-x-1)
     */
    public void rotate(int[][] matrix) {
        int len = matrix.length;
        for (int i = 0; i < (len) / 2; i++) {
            for (int j = 0; j < (len) / 2; j++) {
                rotate(matrix, len, i, j);
            }
        }
        if (len % 2 == 1) {
            int l = len / 2;
            for (int i = 0; i < l; i++) {
                rotate(matrix, len, l, i);
            }
        }
    }

    private void rotate(int[][] matrix, int len, int x, int y) {
        int targetValue = matrix[x][y];
        for (int i = 0; i < 4; i++) {
            targetValue = trans(matrix, len, x, y, targetValue);
            int temp = x;
            x = y;
            y = len - temp - 1;
        }
    }

    private int trans(int[][] matrix, int len, int x, int y, int targetValue) {
        int temp = matrix[y][len - x - 1];
        matrix[y][len - x - 1] = targetValue;
        return temp;
    }

    private void print(int[][] matrix, int len) {
        System.out.println();
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        A48RotateImage image = new A48RotateImage();
        int[][] matrix = {
                {0, 1, 0, 0},
                {0, 0, 0, 2},
                {4, 0, 0, 0},
                {0, 0, 3, 0},
        };
        image.print(matrix, matrix.length);
        image.rotate(matrix, 4, 0, 1);
        image.print(matrix, matrix.length);
    }
}
