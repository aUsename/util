package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/9/17
 *
 * <p>
 * 按照国际象棋的规则，皇后可以攻击与之处在同一行或同一列或同一斜线上的棋子。
 * n 皇后问题 研究的是如何将 n 个皇后放置在 n×n 的棋盘上，并且使皇后彼此之间不能相互攻击。
 * 给你一个整数 n ，返回所有不同的 n 皇后问题 的解决方案。
 * 每一种解法包含一个不同的 n 皇后问题 的棋子放置方案，该方案中 'Q' 和 '.' 分别代表了皇后和空位。
 * 示例 1：
 * 输入：n = 4
 * 输出：[[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
 * 解释：如上图所示，4 皇后问题存在两个不同的解法。
 * 示例 2：
 * 输入：n = 1
 * 输出：[["Q"]]
 * 提示：
 * 1 <= n <= 9
 * <p>
 * 链接：https://leetcode.cn/problems/n-queens
 * N皇后
 */
public class A51NQueens {

    /**
     * 自己实现，递归&回溯
     * todo 自己的实现复杂度主要，判断是否可以放置Q时，inputQ方法过于复杂，实际可以使用3个集合对象，分别存储每列，以及两个方向（下标之差或之和是固定值）上是否有Q。（实现见A52NQueens2）
     */
    public List<List<String>> solveNQueens(int n) {
        List<List<String>> result = new ArrayList<>();

        if (n == 1) {
            int[][] space = new int[n][n];
            space[0][0] = 1;
            result.add(setResult(space, n));
            return result;
        }

        // 第一行，从0-n依次填Q
        for (int i = 0; i < n; i++) {
            int[][] space = new int[n][n];
            space[0][i] = 1;
            inputNext(space, n, 1, result);
        }

        return result;
    }

    private void inputNext(int[][] space, int n, int index, List<List<String>> result) {

        for (int j = 0; j < n; j++) {
            if (inputQ(space, n, index, j)) {
                space[index][j] = 1;

                if (index == n - 1) {
                    result.add(setResult(space, n));
                    space[index][j] = 0;
                    return;
                }

                inputNext(space, n, index + 1, result);
                space[index][j] = 0;

            }
        }

    }

    private List<String> setResult(int[][] space, int n) {
        printSpace(space, n);
        List<String> result = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String str = "";
            for (int j = 0; j < n; j++) {
                if (space[i][j] == 1) {
                    str = str + "Q";
                } else {
                    str = str + ".";
                }
            }
            result.add(str);
        }
        return result;
    }

    /**
     * (i,j)位置填Q
     * 0=未填 1=Q 2=.
     */
    private boolean inputQ(int[][] space, int n, int i, int j) {

        for (int k = 0; k < n; k++) {
            // 同一行不能再有Q
            if (k != j && space[i][k] == 1) {
                return false;
            } else {
                space[i][k] = 2;
            }

            // 同一列不能再有Q
            if (k != i && space[k][j] == 1) {
                return false;
            } else {
                space[k][j] = 2;
            }

            // 左上至右下斜线不能再有Q
            if (i >= j) {
                if (k < j) {
                    if (space[k + i - j][k] == 1) {
                        return false;
                    } else {
                        space[k + i - j][k] = 2;
                    }
                }
                if (k > i) {
                    if (space[k][k - i + j] == 1) {
                        return false;
                    } else {
                        space[k][k - i + j] = 2;
                    }
                }
            } else {
                if (k < i) {
                    if (space[k][k + j - i] == 1) {
                        return false;
                    } else {
                        space[k][k + j - i] = 2;
                    }
                }
                if (k > j) {
                    if (space[k - j + i][k] == 1) {
                        return false;
                    } else {
                        space[k - j + i][k] = 2;
                    }
                }
            }

            // 右上至左下斜线不能再有Q
            if (i + j < n) {
                if (k < i) {
                    if (space[k][i + j - k] == 1) {
                        return false;
                    } else {
                        space[k][i + j - k] = 2;
                    }

                }
                if (k > i && k <= i + j) {
                    if (space[k][i + j - k] == 1) {
                        return false;
                    } else {
                        space[k][i + j - k] = 2;
                    }
                }
            } else {
                if (k < i && k >= (i + j - n + 1)) {
                    if (space[k][i + j - k] == 1) {
                        return false;
                    } else {
                        space[k][i + j - k] = 2;
                    }

                }
                if (k > i) {
                    if (space[k][i + j - k] == 1) {
                        return false;
                    } else {
                        space[k][i + j - k] = 2;
                    }
                }
            }
        }

        space[i][j] = 1;
        return true;
    }

    private void printSpace(int[][] space, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(space[i][j] + "");
            }
            System.out.println();
        }
        System.out.println("---------");
    }

    public static void main(String[] args) {
        int n = 4;
        int[][] space = new int[n][n];
        A51NQueens queens = new A51NQueens();
//        queens.inputQ(space, n, 2, 2);
//        queens.printSpace(space, n);
        queens.solveNQueens(5);
    }
}
