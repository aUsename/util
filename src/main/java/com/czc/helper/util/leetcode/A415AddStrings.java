package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/10/21
 * <p>
 * 给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和并同样以字符串形式返回。
 * 你不能使用任何內建的用于处理大整数的库（比如 BigInteger）， 也不能直接将输入的字符串转换为整数形式。
 * 示例 1：
 * 输入：num1 = "11", num2 = "123"
 * 输出："134"
 * 示例 2：
 * 输入：num1 = "456", num2 = "77"
 * 输出："533"
 * 示例 3：
 * 输入：num1 = "0", num2 = "0"
 * 输出："0"
 * 提示：
 * 1 <= num1.length, num2.length <= 10^4
 * num1 和num2 都只包含数字 0-9
 * num1 和num2 都不包含任何前导零
 * <p>
 * 链接：https://leetcode.cn/problems/add-strings
 */
public class A415AddStrings {
    public String addStrings(String num1, String num2) {
        int index = 0;
        StringBuilder builder = new StringBuilder();
        int ct = 0;
        int a, b, temp;

        while (index < num1.length() || index < num2.length()) {
            if (index < num1.length()) {
                a = num1.charAt(num1.length() - 1 - index) - '0';
            } else {
                a = 0;
            }

            if (index < num2.length()) {
                b = num2.charAt(num2.length() - 1 - index) - '0';
            } else {
                b = 0;
            }

            temp = a + b + ct;
            ct = temp / 10;
            temp = temp % 10;
            builder = builder.insert(0, temp);
            index++;
        }

        if (ct > 0) {
            builder = builder.insert(0, ct);
        }

        return builder.toString();
    }
}
