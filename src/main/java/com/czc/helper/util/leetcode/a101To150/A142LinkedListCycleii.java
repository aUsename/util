package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.ListNode;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by cuizhongcheng on 2022/9/13
 *
 * <p>
 * 给定一个链表的头节点  head ，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。
 * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。 为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。如果 pos 是 -1，则在该链表中没有环。注意：pos 不作为参数进行传递，仅仅是为了标识链表的实际情况。
 * 不允许修改 链表。
 * 示例 1：
 * 输入：head = [3,2,0,-4], pos = 1
 * 输出：返回索引为 1 的链表节点
 * 解释：链表中有一个环，其尾部连接到第二个节点。
 * 示例 2：
 * 输入：head = [1,2], pos = 0
 * 输出：返回索引为 0 的链表节点
 * 解释：链表中有一个环，其尾部连接到第一个节点。
 * 示例 3：
 * 输入：head = [1], pos = -1
 * 输出：返回 null
 * 解释：链表中没有环。
 * 提示：
 * 链表中节点的数目范围在范围 [0, 104] 内
 * -105 <= Node.val <= 105
 * pos 的值为 -1 或者链表中的一个有效索引
 * 进阶：你是否可以使用 O(1) 空间解决此题？
 * <p>
 * 链接：https://leetcode.cn/problems/linked-list-cycle-ii
 */
public class A142LinkedListCycleii {

    /**
     * 简单实现，使用set
     */
    public ListNode detectCycle(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        ListNode cur = head;
        while (cur != null) {
            if (set.contains(cur)) {
                return cur;
            } else {
                set.add(cur);
                cur = cur.next;
            }
        }
        return null;
    }

    /**
     * 自己实现，空间复杂度O(1)，先用双指针法找到在环中的任意一节点，根据此节点可以遍历整个环，再依次从头节点判断当前节点是否在环中
     *
     * @param head
     * @return
     */
    public ListNode detectCycle1(ListNode head) {
        ListNode cycleNode = getCycleNode(head);
        if (cycleNode == null) {
            return null;
        }

        ListNode current = head;
        while (current != null) {
            ListNode run = cycleNode.next;
            if (run == cycleNode) {
                if (run == current) {
                    return current;
                }
            }
            while (run != cycleNode) {
                if (run == current) {
                    return current;
                }
                run = run.next;
            }
            current = current.next;
        }
        return null;
    }

    private ListNode getCycleNode(ListNode head) {
        ListNode slow = head;
        if (head == null) return null;
        ListNode fast = head.next;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            if (fast == slow) {
                return fast;
            }
        }
        return null;
    }

    /**
     * leetcode实现，也是快慢指针，但是通过数学手段简化了寻找入环点的方法
     * <p>
     * 慢指针第一圈走不完一定会和快指针相遇： 首先，第一步，快指针先进入环 第二步：当慢指针刚到达环的入口时，快指针此时在环中的某个位置(也可能此时相遇) 第三步：设此时快指针和慢指针距离为x，若在第二步相遇，则x = 0； 第四步：设环的周长为n，那么看成快指针追赶慢指针，需要追赶n-x； 第五步：快指针每次都追赶慢指针1个单位，设慢指针速度1/s，快指针2/s，那么追赶需要(n-x)s 第六步：在n-x秒内，慢指针走了n-x单位，因为x>=0，则慢指针走的路程小于等于n，即走不完一圈就和快指针相遇
     * <p>
     * 当快慢指针相遇时，假设环外链表长度为a，入环点至相遇点长度为b，相遇点至入环点长度为c，快指针已经走了n圈。
     * 快指针移动距离 = a+n(b+c)+b
     * 慢指针移动距离 = a + b
     * 所以 a+n(b+c)+b = 2（a + b）
     * 可得到 a=c+(n−1)(b+c) 即：从相遇点到入环点的距离加上 n−1 圈的环长，恰好等于从链表头部到入环点的距离。
     * 因此，当发现 slow 与 fast 相遇时，我们再额外使用一个指针 ptr起始，它指向链表头部；随后，它和 slow 每次向后移动一个位置。最终，它们会在入环点相遇。
     */
    public ListNode detectCycle2(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode slow = head, fast = head;
        while (fast != null) {
            slow = slow.next;
            if (fast.next != null) {
                fast = fast.next.next;
            } else {
                return null;
            }
            if (fast == slow) {
                ListNode ptr = head;
                while (ptr != slow) {
                    ptr = ptr.next;
                    slow = slow.next;
                }
                return ptr;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        listNode.next = listNode;

        A142LinkedListCycleii linkedListCycleii = new A142LinkedListCycleii();
        ListNode result = linkedListCycleii.detectCycle1(listNode);
        System.out.println(result.val);
    }


}
