package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/2
 *
 * <p>
 * 给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
 * 你可以按 任何顺序 返回答案。
 * 示例 1：
 * 输入：n = 4, k = 2
 * 输出：
 * [
 * [2,4],
 * [3,4],
 * [2,3],
 * [1,2],
 * [1,3],
 * [1,4],
 * ]
 * 示例 2：
 * 输入：n = 1, k = 1
 * 输出：[[1]]
 * 提示：
 * 1 <= n <= 20
 * 1 <= k <= n
 * <p>
 * 链接：https://leetcode.cn/problems/combinations
 * 组合
 */
public class A77Combinations {

    private List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> combine(int n, int k) {
        List<Integer> remain = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            remain.add(i);
        }
        combineNext(remain, remain.size(), k, new ArrayList<>());
        return result;
    }

    /**
     * @param remain  未被选取的数字集合
     * @param size    remain里剩余元素个数
     * @param need    还需要的数字个数
     * @param current 已选取的数字
     */
    private void combineNext(List<Integer> remain, int size, int need, List<Integer> current) {
        if (need == 0) {
            List<Integer> list = new ArrayList<>(current);
            result.add(list);
            return;
        }

        int num = remain.remove(size - 1);
        if (size == need) {
            // 此时，当前值必须选入
            current.add(num);
            combineNext(remain, size - 1, need - 1, current);
            current.remove(current.size() - 1);
        } else {
            // 剩余元素个数 > 还需要的数字个数 当前数字可选，可不选
            // 选当前数字
            current.add(num);
            combineNext(remain, size - 1, need - 1, current);
            current.remove(current.size() - 1);
            // 不选当前数字
            combineNext(remain, size - 1, need, current);
        }
        remain.add(num);
    }

    public static void main(String[] args) {
        A77Combinations combinations = new A77Combinations();
        combinations.combine(4, 2);
    }
}
