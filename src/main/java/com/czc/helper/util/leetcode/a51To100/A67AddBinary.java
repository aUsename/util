package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/10/29
 *
 * <p>
 * 给你两个二进制字符串 a 和 b ，以二进制字符串的形式返回它们的和。
 * 示例 1：
 * 输入:a = "11", b = "1"
 * 输出："100"
 * 示例 2：
 * 输入：a = "1010", b = "1011"
 * 输出："10101"
 * 提示：
 * 1 <= a.length, b.length <= 10^4
 * a 和 b 仅由字符 '0' 或 '1' 组成
 * 字符串如果不是 "0" ，就不含前导零
 * <p>
 * 链接：https://leetcode.cn/problems/add-binary
 * 二进制求和
 */
public class A67AddBinary {
    public String addBinary(String a, String b) {
        StringBuilder builder = new StringBuilder();
        int lenA = a.length();
        int lenB = b.length();
        int carry = 0;
        int index = 0;

        int charA, charB;
        while (index < Math.max(lenA, lenB)) {
            if (index >= lenA) {
                charA = 0;
            } else {
                charA = a.charAt(lenA - index - 1) - '0';
            }
            if (index >= lenB) {
                charB = 0;
            } else {
                charB = b.charAt(lenB - index - 1) - '0';
            }
            int temp = charA + charB + carry;
            switch (temp) {
                case 3: {
                    builder.insert(0, 1);
                    carry = 1;
                    break;
                }
                case 2: {
                    builder.insert(0, 0);
                    carry = 1;
                    break;
                }
                case 1: {
                    builder.insert(0, 1);
                    carry = 0;
                    break;
                }
                default: {
                    builder.insert(0, 0);
                    carry = 0;
                    break;
                }
            }
            index++;
        }
        if (carry == 1) {
            builder.insert(0, 1);
        }

        return builder.toString();
    }
}
