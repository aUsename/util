package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/16
 * <p>
 * 给定两个整数数组 inorder 和 postorder ，其中 inorder 是二叉树的中序遍历， postorder 是同一棵树的后序遍历，请你构造并返回这颗 二叉树 。
 * 示例 1:
 * 输入：inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
 * 输出：[3,9,20,null,null,15,7]
 * 示例 2:
 * 输入：inorder = [-1], postorder = [-1]
 * 输出：[-1]
 * 提示:
 * 1 <= inorder.length <= 3000
 * postorder.length == inorder.length
 * -3000 <= inorder[i], postorder[i] <= 3000
 * inorder 和 postorder 都由 不同 的值组成
 * postorder 中每一个值都在 inorder 中
 * inorder 保证是树的中序遍历
 * postorder 保证是树的后序遍历
 * <p>
 * 链接：https://leetcode.cn/problems/construct-binary-tree-from-inorder-and-postorder-traversal
 * 从中序与后序遍历序列构造二叉树
 */
public class A106ConstructBinaryTreeFromInorderAndPostorderTraversal {

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        int len = inorder.length;
        if (len == 0) {
            return null;
        }
        TreeNode head = new TreeNode();
        turnNext(postorder, inorder, head, true, 0, len - 1, 0, len - 1);
        return head.left;
    }

    private void turnNext(int[] postorder, int[] inorder, TreeNode current, boolean left,
                          int postLeft, int postRight, int inLeft, int inRight) {
        if (postLeft > postRight) return;
        TreeNode treeNode = new TreeNode(postorder[postRight]);
        if (left) {
            current.left = treeNode;
        } else {
            current.right = treeNode;
        }
        for (int mid = inLeft; mid <= inRight; mid++) {
            if (treeNode.val == inorder[mid]) {
                turnNext(postorder, inorder, treeNode, true, postLeft, mid - 1 - inLeft + postLeft, inLeft, mid - 1);
                turnNext(postorder, inorder, treeNode, false, postRight - inRight + mid, postRight - 1, mid + 1, inRight);
                return;
            }
        }
    }
}
