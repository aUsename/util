package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/13
 * <p>
 * 给你两个字符串 haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串的第一个匹配项的下标（下标从 0 开始）。如果 needle 不是 haystack 的一部分，则返回  -1 。
 * 示例 1：
 * 输入：haystack = "sadbutsad", needle = "sad"
 * 输出：0
 * 解释："sad" 在下标 0 和 6 处匹配。
 * 第一个匹配项的下标是 0 ，所以返回 0 。
 * 示例 2：
 * 输入：haystack = "leetcode", needle = "leeto"
 * 输出：-1
 * 解释："leeto" 没有在 "leetcode" 中出现，所以返回 -1 。
 * 提示：
 * 1 <= haystack.length, needle.length <= 10^4
 * haystack 和 needle 仅由小写英文字符组成
 * <p>
 * 链接：https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string
 */
public class A28FindTheIndexOfTheFirstOccurrenceInAString {

    /**
     * 自己实现，RK算法，leetcode官方用KMP实现的，还是算了…懒得掌握
     */
    public int strStr(String haystack, String needle) {
        // 参数校验
        if (haystack.length() < needle.length()) return -1;
        int len = needle.length();
        // 匹配串hash值
        int targetHash = 0;
        // 子串hash值
        int strHash = 0;
        int mode = 1;
        for (int i = 0; i < len; i++) {
            targetHash = targetHash * 26 + needle.charAt(i) - 'a';
            strHash = strHash * 26 + haystack.charAt(i) - 'a';
            mode = mode * 26;
        }
        int i = 0;
        for (; i <= haystack.length() - len; i++) {
            if (targetHash == strHash) {
                // 这里是按位比较，懒得写了
                return i;
            }
            if (i == haystack.length() - len) return -1;
            strHash = (strHash * 26 - mode * (haystack.charAt(i) - 'a')) + haystack.charAt(i + len) - 'a';
        }
        return -1;
    }
}
