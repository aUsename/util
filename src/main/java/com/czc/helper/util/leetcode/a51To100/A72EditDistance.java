package com.czc.helper.util.leetcode.a51To100;

import java.util.Arrays;

/**
 * Created by cuizhongcheng on 2022/10/31
 * <p>
 * 给你两个单词 word1 和 word2， 请返回将 word1 转换成 word2 所使用的最少操作数  。
 * 你可以对一个单词进行如下三种操作：
 * 1.插入一个字符
 * 2.删除一个字符
 * 3.替换一个字符
 * 示例 1：
 * 输入：word1 = "horse", word2 = "ros"
 * 输出：3
 * 解释：
 * horse -> rorse (将 'h' 替换为 'r')
 * rorse -> rose (删除 'r')
 * rose -> ros (删除 'e')
 * 示例 2：
 * 输入：word1 = "intention", word2 = "execution"
 * 输出：5
 * 解释：
 * intention -> inention (删除 't')
 * inention -> enention (将 'i' 替换为 'e')
 * enention -> exention (将 'n' 替换为 'x')
 * exention -> exection (将 'n' 替换为 'c')
 * exection -> execution (插入 'u')
 * 提示：
 * 0 <= word1.length, word2.length <= 500
 * word1 和 word2 由小写英文字母组成
 * <p>
 * 链接：https://leetcode.cn/problems/edit-distance
 * 编辑距离
 */
public class A72EditDistance {

    private int result;

    /**
     * 自己实现，回溯法遍历，执行耗时只5%，勉强不超时
     */
    public int minDistance(String word1, String word2) {
        result = Math.max(word1.length(), word2.length());
        next(word1, 0, word2, 0, 0);
        return result;
    }

    /**
     * index1和index2表示字符串1和2中，待匹配字符的位置
     */
    private void next(String word1, int index1, String word2, int index2, int count) {
        // 两个字符串都遍历完成，结束
        if (index1 == word1.length() && index2 == word2.length()) {
            result = Math.min(result, count);
            return;
        }
        // 字符串1遍历完，字符串2还有剩余，则需要count+字符串2剩余字符次，后面每次都做插入操作
        if (index1 == word1.length()) {
            result = Math.min(result, count + word2.length() - index2);
            return;
        }
        // 字符串2遍历完，字符串1还有剩余，则需要count+字符串1剩余字符次，后面每次都做删除操作
        if (index2 == word2.length()) {
            result = Math.min(result, count + word1.length() - index1);
            return;
        }

        // 待匹配字符相等，可以直接往下走，count不变
        if (word1.charAt(index1) == word2.charAt(index2)) {
            next(word1, index1 + 1, word2, index2 + 1, count);
        } else {
            // 剪枝,修改次数已经超过最小次数，不需要再遍历
            if (count + 1 >= result) {
                return;
            }
            // 字符串1修改当前字符
            next(word1, index1 + 1, word2, index2 + 1, count + 1);

            // 剪枝，长度差过长时，不再增删字符
            if (Math.abs((word1.length() - index1) - (word2.length() - index2)) + count < result) {
                // 字符串1插入一个字符
                next(word1, index1, word2, index2 + 1, count + 1);
                // 字符串1删除当前字符
                next(word1, index1 + 1, word2, index2, count + 1);
            }
        }
    }


    /**
     * 自己实现，动态规划 count[x][y] 代表，遍历到word1第x字符，word2第y字符时，需要的最小次数，为此可以往(x,y+1)(x+1,y)(x+1,y+1)来推
     */
    public int minDistance1(String word1, String word2) {
        int len1 = word1.length();
        int len2 = word2.length();
        int[][] count = new int[len1 + 1][len2 + 1];
        for (int[] item : count) {
            Arrays.fill(item, Math.max(word1.length(), word2.length()));
        }
        count[0][0] = 0;
        for (int i = 0; i <= len1; i++) {
            for (int j = 0; j <= len2; j++) {
                if (i == len1 && j == len2) {
                    return count[len1][len2];
                }
                if (i == len1) {
                    count[i][j + 1] = Math.min(count[i][j + 1], count[i][j] + 1);
                    continue;
                }
                if (j == len2) {
                    count[i + 1][j] = Math.min(count[i + 1][j], count[i][j] + 1);
                    continue;
                }
                if (word1.charAt(i) == word2.charAt(j)) {
                    count[i + 1][j + 1] = Math.min(count[i + 1][j + 1], count[i][j]);
                } else {
                    // 修改
                    count[i + 1][j + 1] = Math.min(count[i + 1][j + 1], count[i][j] + 1);
                    // 插入
                    count[i][j + 1] = Math.min(count[i][j + 1], count[i][j] + 1);
                    // 删除
                    count[i + 1][j] = Math.min(count[i + 1][j], count[i][j] + 1);
                }
            }
        }
        return count[len1][len2];
    }
}
