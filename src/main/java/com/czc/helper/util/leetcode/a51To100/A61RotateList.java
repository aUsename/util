package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/10/28
 *
 * <p>
 * 给你一个链表的头节点 head ，旋转链表，将链表每个节点向右移动 k 个位置。
 * 示例 1：
 * 输入：head = [1,2,3,4,5], k = 2
 * 输出：[4,5,1,2,3]
 * 示例 2：
 * 输入：head = [0,1,2], k = 4
 * 输出：[2,0,1]
 * 提示：
 * 链表中节点的数目在范围 [0, 500] 内
 * -100 <= Node.val <= 100
 * 0 <= k <= 2 * 10^9
 * <p>
 * 链接：https://leetcode.cn/problems/rotate-list
 * 旋转链表
 */
public class A61RotateList {
    public ListNode rotateRight(ListNode head, int k) {
        // 先排除链表长度<2的情况
        if (head == null || head.next == null) return head;

        // 缓存头节点位置
        ListNode start = new ListNode(0, head);
        // 用来指向最后一个节点
        ListNode last;
        // 用来指向需要被移动节点的前一个节点位置
        ListNode moveHead;

        int count = 2;
        ListNode temp = head;
        while (temp.next.next != null && count < k + 2) {
            count++;
            temp = temp.next;
        }
        // 链表已被遍历完 temp.next为最后一个节点 count为链表的节点数
        if (temp.next.next == null) {
            k = k % count;
            temp = head;
            for (int i = 0; i < k; i++) {
                temp = temp.next;
            }
            moveHead = head;
            last = temp;
            while (last.next != null){
                last = last.next;
                moveHead = moveHead.next;
            }

        } else {
            // 链表未被遍历完，temp被后移动了k次
            moveHead = head;
            last = temp;
            while (last.next != null){
                last = last.next;
                moveHead = moveHead.next;
            }
        }

        if (moveHead == last) return head;
        last.next = head;
        start.next = moveHead.next;
        moveHead.next = null;

        return start.next;
    }
}
