package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/8
 *
 * <p>
 * 给定一个长度为 n 的整数数组 height 。有 n 条垂线，第 i 条线的两个端点是 (i, 0) 和 (i, height[i]) 。
 * 找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
 * 返回容器可以储存的最大水量。
 * 说明：你不能倾斜容器。
 * 示例 1：
 * 输入：[1,8,6,2,5,4,8,3,7]
 * 输出：49
 * 解释：图中垂直线代表输入数组 [1,8,6,2,5,4,8,3,7]。在此情况下，容器能够容纳水（表示为蓝色部分）的最大值为 49。
 * 示例 2：
 * 输入：height = [1,1]
 * 输出：1
 * 提示：
 * n == height.length
 * 2 <= n <= 10^5
 * 0 <= height[i] <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/container-with-most-water
 */
public class A11ContainerWithMostWater {

    /**
     * 自己实现，复杂度O(N^2)，两层循环，优化了左移左指针时值变小时可跳过，右移右指针时值变小可跳过的计算逻辑
     * 可进一步优化，在循环中，多次判断了左移左指针和右移右指针时的大小判断。
     * 优化思路：一次循环，随着i变动，左指针在0～i 右指针在i+1 ～ height.length - 1。但是空间复杂度上去了，时间复杂度也没优化太多，只是减少了部分比较
     */
    public int maxArea(int[] height) {
        int result = Math.min(height[0], height[height.length - 1]) * (height.length - 1);
        int maxLeft = height[0];
        for (int i = 0; i < height.length - 1; i++) {
            if (i > 0) {
                if (height[i] > maxLeft) {
                    maxLeft = height[i];
                    result = Math.max(result, Math.min(height[i], height[height.length - 1]) * (height.length - 1 - i));
                } else {
                    continue;
                }
            }

            int maxRight = height[height.length - 1];
            for (int j = height.length - 2; j > i; j--) {
                if (height[j] > maxRight) {
                    maxRight = height[j];
                    result = Math.max(result, Math.min(height[i], height[j]) * (j - i));
                }
            }
        }

        return result;
    }


    /**
     * 参考leetcode思路：
     * 思想：双指针法
     * <p>
     * 在每个状态下，无论长板或短板向中间收窄一格，都会导致水槽 底边宽度 −1-1−1​ 变短：
     * <p>
     * 若向内 移动短板 ，水槽的短板 min(h[i],h[j]) 可能变大，因此下个水槽的面积 可能增大 。
     * 若向内 移动长板 ，水槽的短板 min(h[i],h[j]) 不变或变小，因此下个水槽的面积 一定变小 。
     * <p>
     * 因此，初始化双指针分列水槽左右两端，循环每轮将短板向内移动一格，并更新面积最大值，直到两指针相遇时跳出；即可获得最大面积。
     * <p>
     * ps：左右相等时不跳出循环，因为可能存在1，20，21，1 这样的情况，遍历到底才是正确的
     */
    public int maxArea1(int[] height) {
        int result = 0;
        int left = 0, right = height.length - 1;
        while (left < right) {
            result = Math.max(result, (right - left) * Math.min(height[left], height[right]));
            if (height[left] > height[right]) {
                right--;
            } else {
                left++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        A11ContainerWithMostWater mostWater = new A11ContainerWithMostWater();
        int[] height = {1, 3, 2, 5, 25, 24, 5};
        System.out.println(mostWater.maxArea1(height));
    }

}
