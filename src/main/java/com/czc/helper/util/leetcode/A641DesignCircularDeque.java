package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/9/15
 *
 * <p>
 * 设计实现双端队列。
 * 实现 MyCircularDeque 类:
 * MyCircularDeque(int k) ：构造函数,双端队列最大为 k 。
 * boolean insertFront()：将一个元素添加到双端队列头部。 如果操作成功返回 true ，否则返回 false 。
 * boolean insertLast() ：将一个元素添加到双端队列尾部。如果操作成功返回 true ，否则返回 false 。
 * boolean deleteFront() ：从双端队列头部删除一个元素。 如果操作成功返回 true ，否则返回 false 。
 * boolean deleteLast() ：从双端队列尾部删除一个元素。如果操作成功返回 true ，否则返回 false 。
 * int getFront() )：从双端队列头部获得一个元素。如果双端队列为空，返回 -1 。
 * int getRear() ：获得双端队列的最后一个元素。 如果双端队列为空，返回 -1 。
 * boolean isEmpty() ：若双端队列为空，则返回 true ，否则返回 false  。
 * boolean isFull() ：若双端队列满了，则返回 true ，否则返回 false 。
 * 示例 1：
 * 输入
 * ["MyCircularDeque", "insertLast", "insertLast", "insertFront", "insertFront", "getRear", "isFull", "deleteLast", "insertFront", "getFront"]
 * [[3], [1], [2], [3], [4], [], [], [], [4], []]
 * 输出
 * [null, true, true, true, false, 2, true, true, true, 4]
 * 解释
 * MyCircularDeque circularDeque = new MycircularDeque(3); // 设置容量大小为3
 * circularDeque.insertLast(1);              // 返回 true
 * circularDeque.insertLast(2);              // 返回 true
 * circularDeque.insertFront(3);             // 返回 true
 * circularDeque.insertFront(4);             // 已经满了，返回 false
 * circularDeque.getRear();          // 返回 2
 * circularDeque.isFull();               // 返回 true
 * circularDeque.deleteLast();             // 返回 true
 * circularDeque.insertFront(4);             // 返回 true
 * circularDeque.getFront();       // 返回 4
 * 提示：
 * 1 <= k <= 1000
 * 0 <= value <= 1000
 * insertFront, insertLast, deleteFront, deleteLast, getFront, getRear, isEmpty, isFull  调用次数不大于 2000 次
 * <p>
 * 链接：https://leetcode.cn/problems/design-circular-deque
 */
public class A641DesignCircularDeque {

    // 数组大小
    private int size;
    // 存储数据
    private int[] data;
    // 已存数据个数
    private int count = 0;
    // 头部位置
    private int head = 0;
    // 尾部位置
    private int tail = 1;

    public A641DesignCircularDeque(int k) {
        this.size = k;
        data = new int[k];
    }

    /**
     * 头部插入
     */
    public boolean insertFront(int value) {
        if (count == size) {
            return false;
        }
        count++;
        data[head] = value;
        head = (head - 1 + size) % size;
        return true;
    }

    /**
     * 尾部插入
     */
    public boolean insertLast(int value) {
        if (count == size) {
            return false;
        }
        count++;
        data[tail] = value;
        tail = (tail + 1) % size;
        return true;
    }

    /**
     * 头部删除
     */
    public boolean deleteFront() {
        if (count == 0) {
            return false;
        }
        count--;
        head = (head + 1) % size;
        return true;
    }

    /**
     * 尾部删除
     */
    public boolean deleteLast() {
        if (count == 0) {
            return false;
        }
        count--;
        tail = (tail - 1 + size) % size;
        return true;
    }

    /**
     * 获取头部
     */
    public int getFront() {
        if (count == 0) {
            return -1;
        }
        return data[(head + 1) % size];
    }

    /**
     * 获取尾部
     */
    public int getRear() {
        if (count == 0) {
            return -1;
        }
        return data[(tail - 1 + size) % size];
    }

    /**
     * 是否为空
     */
    public boolean isEmpty() {
        return count == 0;
    }

    /**
     * 是否为满
     */
    public boolean isFull() {
        return count == size;
    }

//    public static void main(String[] args) {
//        A641DesignCircularDeque deque = new A641DesignCircularDeque(3);
//        System.out.println(deque.insertLast(1));
//        System.out.println(deque.insertLast(2));
//        System.out.println(deque.insertFront(3));
//        System.out.println(deque.insertFront(4));
//        System.out.println(deque.getRear());
//        System.out.println(deque.isFull());
//        System.out.println(deque.deleteLast());
//        System.out.println(deque.insertFront(4));
//        System.out.println(deque.getFront());
//    }

//    public static void main(String[] args) {
//        A641DesignCircularDeque deque = new A641DesignCircularDeque(7);
//        System.out.println(deque.insertFront(7));
//        System.out.println(deque.insertLast(0));
//        System.out.println(deque.getFront());
//        System.out.println(deque.insertLast(3));
//        System.out.println(deque.getFront());
//        System.out.println(deque.insertFront(9));
//    }


    public static void main(String[] args) {
        A641DesignCircularDeque deque = new A641DesignCircularDeque(4);
        System.out.println(deque.insertFront(9));
        System.out.println(deque.deleteLast());
        System.out.println(deque.getRear());
        System.out.println(deque.getFront());
        System.out.println(deque.getFront());
        System.out.println(deque.deleteFront());
        System.out.println(deque.insertFront(6));
    }
}
