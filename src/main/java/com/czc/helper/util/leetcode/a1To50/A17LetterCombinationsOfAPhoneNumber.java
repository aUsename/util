package com.czc.helper.util.leetcode.a1To50;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/9
 * <p>
 * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
 * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
 * 2:abc
 * 3:def
 * 4:ghi
 * 5:jkl
 * 6:mno
 * 7:pqrs
 * 8:tuv
 * 9:wxyz
 * 示例 1：
 * 输入：digits = "23"
 * 输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
 * 示例 2：
 * 输入：digits = ""
 * 输出：[]
 * 示例 3：
 * 输入：digits = "2"
 * 输出：["a","b","c"]
 * 提示：
 * 0 <= digits.length <= 4
 * digits[i] 是范围 ['2', '9'] 的一个数字。
 * <p>
 * 链接：https://leetcode.cn/problems/letter-combinations-of-a-phone-number
 */
public class A17LetterCombinationsOfAPhoneNumber {
    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        if (digits.length() == 0) return result;

        result.add("");
        for (int i = 0; i < digits.length(); i++) {
            result = append(result, getChar(digits.charAt(i)));
        }
        return result;
    }

    private List<String> append(List<String> list, String str) {
        List<String> result = new ArrayList<>();
        for (String item : list) {
            for (int i = 0; i < str.length(); i++) {
                result.add(item + str.charAt(i));
            }
        }
        return result;
    }

    private String getChar(char num) {
        switch (num) {
            case '2':
                return "abc";
            case '3':
                return "def";
            case '4':
                return "ghi";
            case '5':
                return "jkl";
            case '6':
                return "mno";
            case '7':
                return "pqrs";
            case '8':
                return "tuv";
            default:
                return "wxyz";
        }
    }


}
