package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/25
 *
 * <p>
 * 给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 * 子数组 是数组中的一个连续部分。
 * 示例 1：
 * 输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
 * 输出：6
 * 解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
 * 示例 2：
 * 输入：nums = [1]
 * 输出：1
 * 示例 3：
 * 输入：nums = [5,4,-1,7,8]
 * 输出：23
 * 提示：
 * 1 <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/maximum-subarray
 * 最大子数组和
 */
public class A53MaximumSubarray {
    /**
     * 自己实现，多次遍历，偏暴力
     */
    public int maxSubArray(int[] nums) {
        List<Integer> list = new ArrayList<>();

        // 去除所有0，并将相邻同号合并
        int tempSum = 0;
        for (int i = 0; i < nums.length; i++) {
            // nums[i]为0时忽略
            if (nums[i] != 0) {
                if (tempSum == 0) {
                    tempSum = nums[i];
                } else if ((tempSum > 0 && nums[i] > 0) || (tempSum < 0 && nums[i] < 0)) {
                    tempSum = tempSum + nums[i];
                } else {
                    list.add(tempSum);
                    tempSum = nums[i];
                }

            }
        }
        list.add(tempSum);

        // 考虑全为正数，全为负数的场景
        if (list.size() == 1) {
            if (list.get(0) < 0) {
                Arrays.sort(nums);
                return nums[nums.length - 1];
            } else {
                return list.get(0);
            }
        }
        // 去除首尾负数
        if (list.get(0) < 0) {
            list.remove(0);
        }
        if (list.get(list.size() - 1) < 0) {
            list.remove(list.size() - 1);
        }
        // 合并
        for (int i = 1; i < list.size(); ) {
            if (list.get(i - 1) + list.get(i) >= 0 && list.get(i) + list.get(i + 1) >= 0) {
                list.set(i - 1, list.get(i - 1) + list.get(i) + list.get(i + 1));
                list.remove(i);
                list.remove(i);
            } else {
                i = i + 2;
            }
        }

        // 寻找结果
        int result = list.get(0);
        int temp = list.get(0);
        int index = 0;
        while (index + 2 < list.size()) {
            if (temp + list.get(index + 1) <= 0) {
                if (list.get(index + 1) + list.get(index + 2) > 0) {
                    result = Math.max(result, temp + list.get(index + 1) + list.get(index + 2));
                }
                temp = list.get(index + 2);
                result = Math.max(result, temp);
            } else {
                temp = temp + list.get(index + 1) + list.get(index + 2);
                result = Math.max(result, temp);
            }
            index += 2;
        }

        return result;
    }

    /**
     * leetcode实现
     * 思想：动态规划
     * 用 f(i)代表以第 i 个数结尾的「连续子数组的最大和」
     * f(i)= max{f(i−1)+nums[i], nums[i]}
     */
    public int maxSubArray1(int[] nums) {
        int pre = 0, maxAns = nums[0];
        for (int x : nums) {
            pre = Math.max(pre + x, x);
            maxAns = Math.max(maxAns, pre);
        }
        return maxAns;
    }

    /**
     * leetcode实现2
     * 思想：线段树求解最长公共上升子序列问题
     */
    public int maxSubArray2(int[] nums) {
        return getInfo(nums, 0, nums.length - 1).mSum;
    }

    public Status getInfo(int[] a, int l, int r) {
        if (l == r) {
            return new Status(a[l], a[l], a[l], a[l]);
        }
        int m = (l + r) >> 1;
        Status lSub = getInfo(a, l, m);
        Status rSub = getInfo(a, m + 1, r);
        return pushUp(lSub, rSub);
    }

    public Status pushUp(Status l, Status r) {
        int iSum = l.iSum + r.iSum;
        int lSum = Math.max(l.lSum, l.iSum + r.lSum);
        int rSum = Math.max(r.rSum, r.iSum + l.rSum);
        int mSum = Math.max(Math.max(l.mSum, r.mSum), l.rSum + r.lSum);
        return new Status(lSum, rSum, mSum, iSum);
    }

    public class Status {
        public int lSum, rSum, mSum, iSum;

        public Status(int lSum, int rSum, int mSum, int iSum) {
            //表示 [l,r] 内以 l 为左端点的最大子段和
            this.lSum = lSum;
            // 表示 [l,r] 内以 r 为右端点的最大子段和
            this.rSum = rSum;
            // 表示 [l,r] 内的最大子段和
            this.mSum = mSum;
            // 表示 [l,r] 的区间和
            this.iSum = iSum;
        }
    }

    public static void main(String[] args) {
        A53MaximumSubarray subarray = new A53MaximumSubarray();
        int[] nums = {-5, 8, -5, 1, 1, -3, 5, 5, -3, -3, 6, 4, -7, -4, -8, 0, -1, -6};
        System.out.println(subarray.maxSubArray(nums));
    }
}
