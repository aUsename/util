package com.czc.helper.util.leetcode.a1To50;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/9/16
 *
 * <p>
 * 给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
 * 示例 1：
 * 输入：nums = [1,2,3]
 * 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 * 示例 2：
 * 输入：nums = [0,1]
 * 输出：[[0,1],[1,0]]
 * 示例 3：
 * 输入：nums = [1]
 * 输出：[[1]]
 * 提示：
 * 1 <= nums.length <= 6
 * -10 <= nums[i] <= 10
 * nums 中的所有整数 互不相同
 * <p>
 * 链接：https://leetcode.cn/problems/permutations
 * 全排列
 */
public class A46Permutations {

    public List<List<Integer>> permute(int[] nums) {

        List<List<Integer>> result = new ArrayList<>();
        int len = nums.length;

        // 数组长度为1时
        if (len == 1) {
            List<Integer> temp = new ArrayList<>(1);
            temp.add(nums[0]);
            result.add(temp);
            return result;
        }

        // 第0个元素和剩余元素的子数组做排列组合，直接插入即可
        int[] newArray = new int[nums.length - 1];
        System.arraycopy(nums, 1, newArray, 0, nums.length - 1);

        List<List<Integer>> tempResult = permute(newArray);

        for (List<Integer> aTempResult : tempResult) {
            for (int j = 0; j <= aTempResult.size(); j++) {
                List<Integer> newList = new ArrayList<>(aTempResult);
                newList.add(j, nums[0]);
                result.add(newList);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        A46Permutations permutations = new A46Permutations();
        int[] nums = {1, 2, 3};
        List<List<Integer>> result = permutations.permute(nums);
        for (List<Integer> list : result) {
            for (Integer item : list) {
                System.out.print(item + " ");
            }
            System.out.println();
        }
    }

}
