package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/13
 *
 * <p>
 * 给定两个整数，被除数 dividend 和除数 divisor。将两数相除，要求不使用乘法、除法和 mod 运算符。
 * 返回被除数 dividend 除以除数 divisor 得到的商。
 * 整数除法的结果应当截去（truncate）其小数部分，例如：truncate(8.345) = 8 以及 truncate(-2.7335) = -2
 * 示例 1:
 * 输入: dividend = 10, divisor = 3
 * 输出: 3
 * 解释: 10/3 = truncate(3.33333..) = truncate(3) = 3
 * 示例 2:
 * 输入: dividend = 7, divisor = -3
 * 输出: -2
 * 解释: 7/-3 = truncate(-2.33333..) = -2
 * 提示：
 * 被除数和除数均为 32 位有符号整数。
 * 除数不为 0。
 * 假设我们的环境只能存储 32 位有符号整数，其数值范围是 [−2^31,  2^31 − 1]。本题中，如果除法结果溢出，则返回 2^31 − 1。
 * <p>
 * 链接：https://leetcode.cn/problems/divide-two-integers
 */
public class A29DivideTwoIntegers {

    /**
     * 自己实现 执行耗时超过时间限制
     * 可优化1：4段类似代码，可以将dividend和divisor转成同符号，由于−2^31会越界，所以将dividend和divisor都转成负数即可
     * 可优化2：通过循环加减来做，耗时过高，可以使用二分查找优化
     */
    public int divide(int dividend, int divisor) {
        if (dividend == Integer.MIN_VALUE) {
            if (divisor == 1) {
                return dividend;
            } else if (divisor == -1) {
                return Integer.MAX_VALUE;
            } else if (dividend == divisor) {
                return 1;
            }
        }
        int result = 0;
        if (dividend < 0 && divisor < 0) {
            while (dividend <= 0) {
                dividend = dividend - divisor;
                result++;
            }
            return result - 1;
        }

        if (dividend > 0 && divisor > 0) {
            while (dividend >= 0) {
                dividend = dividend - divisor;
                result++;
            }
            return result - 1;
        }

        if (dividend < 0 && divisor > 0) {
            while (dividend <= 0) {
                dividend = dividend + divisor;
                result++;
            }
            return -(result - 1);
        }

        if (dividend > 0 && divisor < 0) {
            while (dividend >= 0) {
                dividend = dividend + divisor;
                result++;
            }
            return -(result - 1);
        }

        return result;
    }

    /**
     * 参考leetcode思路实现
     * 思想：二分查找
     * <p>
     * 边界情况：
     * <p>
     * 当被除数-2^31 时：
     * 如果除数为 1，那么我们可以直接返回答案 −2^31；
     * 如果除数为 −1，那么答案为 2^31，产生了溢出。此时我们需要返回 2^31−1。
     * <p>
     * 当除数为 -2^31 时：
     * 如果被除数同样为 -2^31，那么我们可以直接返回答案 1；
     * 对于其余的情况，我们返回答案0。
     * <p>
     * 当被除数为 0 时，我们可以直接返回答案 0。
     */
    public int divide1(int dividend, int divisor) {
        // 考虑被除数为最小值的情况
        if (dividend == Integer.MIN_VALUE) {
            if (divisor == 1) {
                return Integer.MIN_VALUE;
            }
            if (divisor == -1) {
                return Integer.MAX_VALUE;
            }
        }
        // 考虑除数为最小值的情况
        if (divisor == Integer.MIN_VALUE) {
            return dividend == Integer.MIN_VALUE ? 1 : 0;
        }
        // 考虑被除数为 0 的情况
        if (dividend == 0) {
            return 0;
        }

        // 处理符号，全部转为负数
        int nev = 1;
        if (dividend > 0) {
            dividend = -dividend;
            nev = -nev;
        }
        if (divisor > 0) {
            divisor = -divisor;
            nev = -nev;
        }

        int result = 0;
        int mod = divisor;
        int count = 1;
        // 找到 >dividend 的最大mod
        // 这里的((mod << 1) < 0) && (count << 1) > 0很重要，边界判断，容易出错
        while (dividend < mod && ((mod << 1) < 0) && (count << 1) > 0) {
            mod <<= 1;
            count <<= 1;
        }
        if (dividend > mod) {
            mod >>= 1;
            count >>= 1;
        }

        // 相减
        while (mod <= divisor) {
            while (dividend <= mod) {
                dividend = dividend - mod;
                result = result + count;
            }
            // 边界判断
            if (mod == -1) break;

            mod >>= 1;
            count >>= 1;
        }

        return result * nev;
    }

    public static void main(String[] args) {
        A29DivideTwoIntegers divideTwoIntegers = new A29DivideTwoIntegers();
        System.out.println(divideTwoIntegers.divide1(2147483647, 1));
    }
}
