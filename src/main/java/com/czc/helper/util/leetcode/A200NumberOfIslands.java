package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/9/22
 *
 * <p>
 * 给你一个由 '1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量。
 * 岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。
 * 此外，你可以假设该网格的四条边均被水包围。
 * 示例 1：
 * 输入：grid = [
 * ["1","1","1","1","0"],
 * ["1","1","0","1","0"],
 * ["1","1","0","0","0"],
 * ["0","0","0","0","0"]
 * ]
 * 输出：1
 * 示例 2：
 * 输入：grid = [
 * ["1","1","0","0","0"],
 * ["1","1","0","0","0"],
 * ["0","0","1","0","0"],
 * ["0","0","0","1","1"]
 * ]
 * 输出：3
 * 提示：
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 300
 * grid[i][j] 的值为 '0' 或 '1'
 * <p>
 * 链接：https://leetcode.cn/problems/number-of-islands
 */
public class A200NumberOfIslands {


    /**
     * leetcode 思路，自己实现
     * 遇到岛屿后，对岛屿做深度优先遍历
     */
    public int numIslands(char[][] grid) {
        int result = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == '1') {
                    result++;
                    island(grid, i, j);
                }
            }
        }
        return result;
    }

    // 找到一座岛，将这座岛相连的陆地都变成0
    private void island(char[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i == grid.length || j == grid[0].length) return;
        if (grid[i][j] == '1') {
            grid[i][j] = '0';
            island(grid, i, j + 1);
            island(grid, i, j - 1);
            island(grid, i - 1, j);
            island(grid, i + 1, j);
        }
    }
}
