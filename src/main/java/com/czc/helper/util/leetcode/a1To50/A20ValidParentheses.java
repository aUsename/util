package com.czc.helper.util.leetcode.a1To50;

import java.util.Stack;

/**
 * Created by cuizhongcheng on 2022/9/14
 *
 * <p>
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
 * 有效字符串需满足：
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 每个右括号都有一个对应的相同类型的左括号。
 * 示例 1：
 * 输入：s = "()"
 * 输出：true
 * 示例 2：
 * 输入：s = "()[]{}"
 * 输出：true
 * 示例 3：
 * 输入：s = "(]"
 * 输出：false
 * 提示：
 * 1 <= s.length <= 104
 * s 仅由括号 '()[]{}' 组成
 * <p>
 * 链接：https://leetcode.cn/problems/valid-parentheses
 */
public class A20ValidParentheses {
    public boolean isValid(String s) {
        char[] str = s.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (char c : str) {
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if (stack.empty()) return false;
                char temp = stack.pop();
                if (c == ')' && temp != '(') {
                    return false;
                }
                if (c == ']' && temp != '[') {
                    return false;
                }
                if (c == '}' && temp != '{') {
                    return false;
                }
            }
        }

        return stack.empty();
    }
}
