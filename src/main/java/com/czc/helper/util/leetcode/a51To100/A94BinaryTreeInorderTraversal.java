package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/10
 * <p>
 * 给定一个二叉树的根节点 root ，返回 它的 中序 遍历 。
 * 示例 1：
 * 输入：root = [1,null,2,3]
 * 输出：[1,3,2]
 * 示例 2：
 * 输入：root = []
 * 输出：[]
 * 示例 3：
 * 输入：root = [1]
 * 输出：[1]
 * 提示：
 * 树中节点数目在范围 [0, 100] 内
 * -100 <= Node.val <= 100
 * 进阶: 递归算法很简单，你可以通过迭代算法完成吗？
 * <p>
 * 链接：https://leetcode.cn/problems/binary-tree-inorder-traversal
 * 二叉树的中序遍历
 */
public class A94BinaryTreeInorderTraversal {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Deque<TreeNode> stack = new LinkedList<>();

        while (root != null || !stack.isEmpty()) {
            // root不为空时，将当前节点放入栈中，往左遍历
            if (root != null) {
                stack.push(root);
                root = root.left;
            } else {
                // root为空时，从栈中取出元素，往右遍历
                root = stack.pop();
                result.add(root.val);
                root = root.right;
            }
        }

        return result;
    }

    /**
     * 思想：Morris中序遍历（莫里斯遍历）
     */
    public List<Integer> inorderTraversal1(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        TreeNode predecessor;

        while (root != null) {
            if (root.left != null) {
                // 寻找predecessor
                predecessor = root.left;
                while (predecessor.right != null && predecessor.right != root) {
                    predecessor = predecessor.right;
                }
                // 已找到predecessor
                if (predecessor.right == null) {
                    // predecessor.right为null，连接，并向左遍历
                    predecessor.right = root;
                    root = root.left;
                } else {
                    // predecessor.right有值，断开连接，向右遍历
                    result.add(root.val);
                    predecessor.right = null;
                    root = root.right;
                }

            } else {
                result.add(root.val);
                root = root.right;
            }
        }

        return result;
    }
}
