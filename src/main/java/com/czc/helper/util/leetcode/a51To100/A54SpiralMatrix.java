package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/26
 * <p>
 * 给你一个 m 行 n 列的矩阵 matrix ，请按照 顺时针螺旋顺序 ，返回矩阵中的所有元素。
 * 示例 1：
 * 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * 1 2 3
 * 4 5 6
 * 7 8 9
 * 输出：[1,2,3,6,9,8,7,4,5]
 * 示例 2：
 * 输入：matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
 * 1  2  3  4
 * 5  6  7  8
 * 9 10 11 12
 * 输出：[1,2,3,4,8,12,11,10,9,5,6,7]
 * 提示：
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 10
 * -100 <= matrix[i][j] <= 100
 * <p>
 * 链接：https://leetcode.cn/problems/spiral-matrix
 * 螺旋矩阵
 */
public class A54SpiralMatrix {

    private int up = -1, left = -1, down, right;
    private int x = 0, y = 0;
    private List<Integer> result = new ArrayList<>();

    /**
     * 自己实现 同类型题目：A59SpiralMatrix2
     */
    public List<Integer> spiralOrder(int[][] matrix) {
        down = matrix.length;
        right = matrix[0].length;

        while (true) {
            turnRight(matrix);
            if (x == down) {
                break;
            }
            turnDown(matrix);
            if (y == left) {
                break;
            }
            turnLeft(matrix);
            if (x == up) {
                break;
            }
            turnUp(matrix);
            if (y == right) {
                break;
            }
        }

        return result;
    }

    private void turnRight(int[][] matrix) {
        while (y < right) {
            result.add(matrix[x][y]);
            y++;
        }
        y--;
        x++;
        up++;
    }

    private void turnDown(int[][] matrix) {
        while (x < down) {
            result.add(matrix[x][y]);
            x++;
        }
        x--;
        y--;
        right--;
    }

    private void turnLeft(int[][] matrix) {
        while (y > left) {
            result.add(matrix[x][y]);
            y--;
        }
        y++;
        x--;
        down--;
    }

    private void turnUp(int[][] matrix) {
        while (x > up) {
            result.add(matrix[x][y]);
            x--;
        }
        x++;
        y++;
        left++;
    }
}
