package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/3
 * 柱状图中最大的矩形
 * <p>
 * 给定 n 个非负整数，用来表示柱状图中各个柱子的高度。每个柱子彼此相邻，且宽度为 1 。
 * 求在该柱状图中，能够勾勒出来的矩形的最大面积。
 * 示例 1:
 * 输入：heights = [2,1,5,6,2,3]
 * 输出：10
 * 解释：最大的矩形为图中红色区域，面积为 10
 * 示例 2：
 * 输入： heights = [2,4]
 * 输出： 4
 * 提示：
 * 1 <= heights.length <=10^5
 * 0 <= heights[i] <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/largest-rectangle-in-histogram
 */
public class A84LargestRectangleInHistogram {

    private int result = 0;

    /**
     * 自己实现，耗时较高
     */
    public int largestRectangleArea(int[] heights) {
        largest(heights, 0, heights.length - 1);
        return result;
    }

    private void largest(int[] heights, int left, int right) {
        if (left > right) {
            return;
        } else if (right == left) {
            result = Math.max(result, Math.max(heights[left], heights[right]));
            return;
        }

        List<Integer> minList = new ArrayList<>();
        int min = findMin(heights, left, right, minList);
        result = Math.max(result, (right - left + 1) * min);

        largest(heights, left, minList.get(0) - 1);
        largest(heights, minList.get(minList.size() - 1) + 1, right);
        for (int i = 0; i < minList.size() - 1; i++) {
            largest(heights, minList.get(i) + 1, minList.get(i + 1) - 1);
        }
    }

    /**
     * 找出(left,right)之间的最小值，并将下标都填入minList中
     */
    private int findMin(int[] heights, int left, int right, List<Integer> minList) {
        int min = heights[left];
        minList.add(left);
        for (int i = left + 1; i <= right; i++) {
            if (heights[i] == min) {
                minList.add(i);
            } else if (heights[i] < min) {
                minList.clear();
                min = heights[i];
                minList.add(i);
            }
        }
        return min;
    }


    /**
     * leetcode官方实现
     * 思想：单调栈
     * todo 进阶问题 A85MaximalRectangle
     */
    public int largestRectangleArea1(int[] heights) {
        int n = heights.length;
        int[] left = new int[n];
        int[] right = new int[n];

        Deque<Integer> mono_stack = new ArrayDeque<Integer>();
        for (int i = 0; i < n; ++i) {
            while (!mono_stack.isEmpty() && heights[mono_stack.peek()] >= heights[i]) {
                mono_stack.pop();
            }
            left[i] = (mono_stack.isEmpty() ? -1 : mono_stack.peek());
            mono_stack.push(i);
        }

        mono_stack.clear();
        for (int i = n - 1; i >= 0; --i) {
            while (!mono_stack.isEmpty() && heights[mono_stack.peek()] >= heights[i]) {
                mono_stack.pop();
            }
            right[i] = (mono_stack.isEmpty() ? n : mono_stack.peek());
            mono_stack.push(i);
        }

        int ans = 0;
        for (int i = 0; i < n; ++i) {
            ans = Math.max(ans, (right[i] - left[i] - 1) * heights[i]);
        }
        return ans;
    }
}
