package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/17
 *
 * <p>
 * 整数数组的一个 排列  就是将其所有成员以序列或线性顺序排列。
 * 例如，arr = [1,2,3] ，以下这些都可以视作 arr 的排列：[1,2,3]、[1,3,2]、[3,1,2]、[2,3,1] 。
 * 整数数组的 下一个排列 是指其整数的下一个字典序更大的排列。更正式地，如果数组的所有排列根据其字典顺序从小到大排列在一个容器中，那么数组的 下一个排列 就是在这个有序容器中排在它后面的那个排列。如果不存在下一个更大的排列，那么这个数组必须重排为字典序最小的排列（即，其元素按升序排列）。
 * 例如，arr = [1,2,3] 的下一个排列是 [1,3,2] 。
 * 类似地，arr = [2,3,1] 的下一个排列是 [3,1,2] 。
 * 而 arr = [3,2,1] 的下一个排列是 [1,2,3] ，因为 [3,2,1] 不存在一个字典序更大的排列。
 * 给你一个整数数组 nums ，找出 nums 的下一个排列。
 * 必须 原地 修改，只允许使用额外常数空间。
 * 示例 1：
 * 输入：nums = [1,2,3]
 * 输出：[1,3,2]
 * 示例 2：
 * 输入：nums = [3,2,1]
 * 输出：[1,2,3]
 * 示例 3：
 * 输入：nums = [1,1,5]
 * 输出：[1,5,1]
 * 提示：
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 100
 * <p>
 * 链接：https://leetcode.cn/problems/next-permutation
 */
public class A31NextPermutation {
    /**
     * 从右往左遍历，最大的排列是数字由小变大，所以先遍历，找到遇到的第一个降序数字current，则current右边，所有的数字从左往右降序排列
     * 此时问题转变为，寻找current+它右边从左往右降序子数组 的 下一个更大排列（此时current右边一定存在大于current的数字）
     * 此时，找到current右边，大于current的最右边一个元素next，交换current与next，并将原current位置右边数组倒置，即完成
     */
    public void nextPermutation(int[] nums) {
        int len = nums.length;
        if (len <= 1) return;

        for (int i = len - 1; i > 0; i--) {
            // 找到第一个降序数字
            if (nums[i] > nums[i - 1]) {
                int current = i - 1;

                int right = current + 1;
                // 右边大于current的最小数字的位置
                int next = -1;
                while (right < len && nums[right] >= nums[current]) {
                    if (nums[right] > nums[current]) {
                        next = right;
                    } else {
                        break;
                    }
                    right++;
                }
                temp = nums[current];
                nums[current] = nums[next];
                nums[next] = temp;
                doThis(nums, current + 1);
                return;
            }
        }

        // 已经是最大排列了，重排为字典序最小的排列
        doThis(nums, 0);
    }

    private int temp;

    private void doThis(int[] nums, int left) {
        int len = nums.length - left;
        int point = 0;
        while (point < len / 2) {
            temp = nums[left + point];
            nums[left + point] = nums[left + (len - point - 1)];
            nums[left + (len - point - 1)] = temp;
            point++;
        }
    }

    public static void main(String[] args) {
        A31NextPermutation nextPermutation = new A31NextPermutation();
        int[] nums = {5, 4, 7, 5, 3, 2};
        nextPermutation.nextPermutation(nums);
        for (int num : nums) {
            System.out.println(num);
        }
    }
}
