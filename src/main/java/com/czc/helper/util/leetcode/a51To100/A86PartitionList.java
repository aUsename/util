package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/11/4
 * <p>
 * 给你一个链表的头节点 head 和一个特定值 x ，请你对链表进行分隔，使得所有 小于 x 的节点都出现在 大于或等于 x 的节点之前。
 * 你应当 保留 两个分区中每个节点的初始相对位置。
 * 示例 1：
 * 输入：head = [1,4,3,2,5,2], x = 3
 * 输出：[1,2,2,4,3,5]
 * 示例 2：
 * 输入：head = [2,1], x = 2
 * 输出：[1,2]
 * 提示：
 * 链表中节点的数目在范围 [0, 200] 内
 * -100 <= Node.val <= 100
 * -200 <= x <= 200
 * <p>
 * 链接：https://leetcode.cn/problems/partition-list
 * 分隔链表
 */
public class A86PartitionList {
    public ListNode partition(ListNode head, int x) {
        // 小于x
        ListNode left = new ListNode(-1);
        ListNode lTemp = left;
        // 大于等于x
        ListNode right = new ListNode(-1);
        ListNode rTemp = right;

        ListNode temp = head;
        while (temp != null) {
            if (temp.val >= x) {
                rTemp.next = temp;
                rTemp = rTemp.next;
            } else {
                lTemp.next = temp;
                lTemp = lTemp.next;
            }
            temp = temp.next;
        }

//        print(left);
//        print(right);

        if (right.next != null) {
            lTemp.next = right.next;
            lTemp = rTemp;
        }
        lTemp.next = null;
        return left.next;
    }

    public static void main(String[] args) {
        A86PartitionList partitionList = new A86PartitionList();
        ListNode list = partitionList.getList(new int[]{1, 4, 3, 2, 5, 2});
        ListNode result = partitionList.partition(list, 3);
        partitionList.print(result);
    }


    private ListNode getList(int[] nums) {
        ListNode head = new ListNode();
        ListNode current = head;
        for (int num : nums) {
            current.next = new ListNode(num);
            current = current.next;
        }
        return head.next;
    }

    private void print(ListNode list) {
        ListNode temp = list;
        while (temp != null) {
            System.out.print(temp.val + " -> ");
            temp = temp.next;
        }
        System.out.println();
    }
}
