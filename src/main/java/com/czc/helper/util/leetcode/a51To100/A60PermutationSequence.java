package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/28
 *
 * <p>
 * 给出集合 [1,2,3,...,n]，其所有元素共有 n! 种排列。
 * 按大小顺序列出所有排列情况，并一一标记，当 n = 3 时, 所有排列如下：
 * "123"
 * "132"
 * "213"
 * "231"
 * "312"
 * "321"
 * 给定 n 和 k，返回第 k 个排列。
 * 示例 1：
 * 输入：n = 3, k = 3
 * 输出："213"
 * 示例 2：
 * 输入：n = 4, k = 9
 * 输出："2314"
 * 示例 3：
 * 输入：n = 3, k = 1
 * 输出："123"
 * 提示：
 * 1 <= n <= 9
 * 1 <= k <= n!
 * <p>
 * 链接：https://leetcode.cn/problems/permutation-sequence
 * 排列序列
 */
public class A60PermutationSequence {
    public String getPermutation(int n, int k) {
        // 1 1 1*2 1*2*3 ... 1*2*3*..*(n-1)
        int[] helper = new int[n];
        List<Integer> remain = new ArrayList<>();
        helper[0] = 1;
        remain.add(1);
        for (int i = 1; i < n; i++) {
            remain.add(i + 1);
            helper[i] = helper[i - 1] * i;
        }

        StringBuilder builder = new StringBuilder();
        int num = k - 1;

        for (int i = n; i > 0; i--) {
            int temp = num / helper[i - 1];
            builder = builder.append(remain.get(temp));
            remain.remove(temp);
            num = num % helper[i - 1];
        }

        return builder.toString();
    }
}
