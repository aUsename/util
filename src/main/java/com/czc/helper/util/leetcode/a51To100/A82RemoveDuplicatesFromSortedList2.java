package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/11/3
 * <p>
 * 给定一个已排序的链表的头 head ， 删除原始链表中所有重复数字的节点，只留下不同的数字 。返回 已排序的链表 。
 * 示例 1：
 * 输入：head = [1,2,3,3,4,4,5]
 * 输出：[1,2,5]
 * 示例 2：
 * 输入：head = [1,1,1,2,3]
 * 输出：[2,3]
 * 提示：
 * 链表中节点数目在范围 [0, 300] 内
 * -100 <= Node.val <= 100
 * 题目数据保证链表已经按升序 排列
 * <p>
 * 链接：https://leetcode.cn/problems/remove-duplicates-from-sorted-list-ii
 * 删除排序链表中的重复元素2
 */
public class A82RemoveDuplicatesFromSortedList2 {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode begin = new ListNode(0, head);
        ListNode pre = begin;
        int repeat;
        while (pre.next != null && pre.next.next != null) {
            if (pre.next.val == pre.next.next.val) {
                repeat = pre.next.val;
                while (pre.next != null && pre.next.val == repeat) {
                    pre.next = pre.next.next;
                }
            } else {
                pre = pre.next;
            }
        }
        return begin.next;
    }
}
