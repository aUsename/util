package com.czc.helper.util.leetcode.a1To50;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/10/12
 *
 * <p>
 * 给你链表的头节点 head ，每 k 个节点一组进行翻转，请你返回修改后的链表。
 * k 是一个正整数，它的值小于或等于链表的长度。如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。
 * 你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。
 * 示例 1：
 * 输入：head = [1,2,3,4,5], k = 2
 * 输出：[2,1,4,3,5]
 * 示例 2：
 * 输入：head = [1,2,3,4,5], k = 3
 * 输出：[3,2,1,4,5]
 * 提示：
 * 链表中的节点数目为 n
 * 1 <= k <= n <= 5000
 * 0 <= Node.val <= 1000
 * 进阶：你可以设计一个只用 O(1) 额外内存空间的算法解决此问题吗？
 * <p>
 * 链接：https://leetcode.cn/problems/reverse-nodes-in-k-group
 */
public class A25ReverseNodesInKGroup {
    public ListNode reverseKGroup(ListNode head, int k) {
        if (k == 1) return head;
        ListNode result = new ListNode(0, head);
        ListNode currentBefore = result;
        // 这里使用了额外的数组空间，可以优化掉，代码应该复杂些
        ListNode[] tempArr = new ListNode[k];
        while (true) {
            ListNode temp = currentBefore;
            for (int i = 0; i < k; i++) {
                if (temp.next != null) {
                    tempArr[i] = temp.next;
                    temp = temp.next;
                } else {
                    return result.next;
                }
            }
            // switch
            ListNode tempNext = temp.next;
            for (int i = k - 1; i > 0; i--) {
                tempArr[i].next = tempArr[i - 1];
            }
            tempArr[0].next = tempNext;
            currentBefore.next = tempArr[k - 1];
            currentBefore = tempArr[0];
        }
    }
}
