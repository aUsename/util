package com.czc.helper.util.leetcode.a1To50;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/10/25
 *
 * <p>
 * 给你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。
 * 字母异位词 是由重新排列源单词的字母得到的一个新单词，所有源单词中的字母通常恰好只用一次。
 * 示例 1:
 * 输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
 * 输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
 * 示例 2:
 * 输入: strs = [""]
 * 输出: [[""]]
 * 示例 3:
 * 输入: strs = ["a"]
 * 输出: [["a"]]
 * 提示：
 * 1 <= strs.length <= 10^4
 * 0 <= strs[i].length <= 100
 * strs[i] 仅包含小写字母
 * <p>
 * 链接：https://leetcode.cn/problems/group-anagrams
 * 字母异位词分组
 */
public class A49GroupAnagrams {
    /**
     * 计数法
     */
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();
        int[] temp = new int[26];
        for (int i = 0; i < strs.length; i++) {
            for (int j = 0; j < strs[i].length(); j++) {
                temp[strs[i].charAt(j) - 'a']++;
            }
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < 26; j++) {
                builder.append(temp[j]);
                // 需要加分隔符，否则计数变成两位数之后会有歧义
                builder.append('|');
                temp[j] = 0;
            }
            String key = builder.toString();
            if (map.get(key) == null) {
                List<String> list = new ArrayList<>();
                list.add(strs[i]);
                map.put(key, list);
            } else {
                map.get(key).add(strs[i]);
            }
        }

        return new ArrayList<>(map.values());
    }

    public static void main(String[] args) {
        A49GroupAnagrams anagrams = new A49GroupAnagrams();
        String[] strs = {"bdddddddddd", "bbbbbbbbbbc"};
        anagrams.groupAnagrams(strs);
    }
}
