package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/26
 *
 * <p>
 * 以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [start_i, end_i] 。请你合并所有重叠的区间，并返回 一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间 。
 * 示例 1：
 * 输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
 * 输出：[[1,6],[8,10],[15,18]]
 * 解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
 * 示例 2：
 * 输入：intervals = [[1,4],[4,5]]
 * 输出：[[1,5]]
 * 解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。
 * 提示：
 * 1 <= intervals.length <= 10^4
 * intervals[i].length == 2
 * 0 <= start_i <= end_i <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/merge-intervals
 * 合并区间
 */
public class A56MergeIntervals {
    public int[][] merge(int[][] intervals) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < intervals.length; i++) {
            int left = intervals[i][0];
            int index = -1;
            int bt = 0;
            for (int j = 0; j < list.size(); j++) {
                // 找到第一个左边比left大的数组的位置
                if (intervals[list.get(j)][0] >= left) {
                    index = j;
                    break;
                }
                // 找到第一个右边比left大的数组的位置
                if (intervals[list.get(j)][1] >= left) {
                    bt = 1;
                    index = j;
                    break;
                }
            }
            // 此时list里没有left大的数组，直接在list末尾添加i数组
            if (index == -1) {
                list.add(i);
            } else {
                // 在list第index位置添加i数组 其中i数组左边一定小于等于list中靠后的数组
                list.add(index + bt, i);
                while (index < list.size() - 1) {
                    if (!mergeArray(intervals, list, index)) {
                        break;
                    }
                }
            }
        }
        int[][] result = new int[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            result[i][0] = intervals[list.get(i)][0];
            result[i][1] = intervals[list.get(i)][1];
        }
        return result;
    }

    private boolean mergeArray(int[][] intervals, List<Integer> list, int index) {
        // 不重合
        if (intervals[list.get(index)][1] < intervals[list.get(index + 1)][0]) {
            return false;
        } else {
            intervals[list.get(index)][1] = Math.max(intervals[list.get(index)][1], intervals[list.get(index + 1)][1]);
            list.remove(index + 1);
            return true;
        }
    }


    /**
     * leetcode官方实现 用了Arrays.sort进行排序
     */
    public int[][] merge1(int[][] intervals) {
        if (intervals.length == 0) {
            return new int[0][2];
        }
        // 可简化为 Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        Arrays.sort(intervals, new Comparator<int[]>() {
            public int compare(int[] interval1, int[] interval2) {
                return interval1[0] - interval2[0];
            }
        });
        List<int[]> merged = new ArrayList<int[]>();
        for (int i = 0; i < intervals.length; ++i) {
            int L = intervals[i][0], R = intervals[i][1];
            if (merged.size() == 0 || merged.get(merged.size() - 1)[1] < L) {
                merged.add(new int[]{L, R});
            } else {
                merged.get(merged.size() - 1)[1] = Math.max(merged.get(merged.size() - 1)[1], R);
            }
        }
        return merged.toArray(new int[merged.size()][]);
    }


    public static void main(String[] args) {
        A56MergeIntervals mergeIntervals = new A56MergeIntervals();
        int[][] intervals = {{1, 2}, {2, 6}};
        int[][] result = mergeIntervals.merge(intervals);

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }
}
