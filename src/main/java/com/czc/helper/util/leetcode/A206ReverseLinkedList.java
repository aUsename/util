package com.czc.helper.util.leetcode;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/9/13
 *
 * <p>
 * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 * 示例 1：
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1]
 * 示例 2：
 * 输入：head = [1,2]
 * 输出：[2,1]
 * 示例 3：
 * 输入：head = []
 * 输出：[]
 * 提示：
 * 链表中节点的数目范围是 [0, 5000]
 * -5000 <= Node.val <= 5000
 * 进阶：链表可以选用迭代或递归方式完成反转。你能否用两种方法解决这道题？
 * <p>
 * 链接：https://leetcode.cn/problems/reverse-linked-list
 */
public class A206ReverseLinkedList {

    /**
     * 迭代法
     *
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode();
        while (head != null) {
            ListNode newNode = new ListNode(head.val);
            newNode.next = newHead.next;
            newHead.next = newNode;

            head = head.next;
        }

        return newHead.next;
    }

    /**
     * 递归法
     * <p>
     * 比较难理解，原链表 1->2->3->4->5，迭代一次，变更为1->2->3->4<-5，迭代一次，变更为1->2->3<-4<-5，
     *
     * @param head
     * @return
     */
    public ListNode reverseList1(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        // newHead返回的是已反转链表的头部，即5，而尾部是head.next.next
        ListNode newHead = reverseList1(head.next);
        head.next.next = head;
        head.next = null;

        return newHead;
    }


    /**
     * 链表倒置
     *
     * 在影响原节点上改，不新建节点
     */
    private ListNode reverseList2(ListNode head) {
        ListNode newHead = new ListNode();
        ListNode cur = head;
        while (cur != null) {
            ListNode temp = cur.next;
            cur.next = newHead.next;
            newHead.next = cur;
            cur = temp;
        }
        return newHead.next;
    }
}
