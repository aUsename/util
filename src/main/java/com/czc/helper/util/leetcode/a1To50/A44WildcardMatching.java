package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/21
 *
 * <p>
 * 给定一个字符串 (s) 和一个字符模式 (p) ，实现一个支持 '?' 和 '*' 的通配符匹配。
 * '?' 可以匹配任何单个字符。
 * '*' 可以匹配任意字符串（包括空字符串）。
 * 两个字符串完全匹配才算匹配成功。
 * 说明:
 * s 可能为空，且只包含从 a-z 的小写字母。
 * p 可能为空，且只包含从 a-z 的小写字母，以及字符 ? 和 *。
 * 示例 1:
 * 输入:
 * s = "aa"
 * p = "a"
 * 输出: false
 * 解释: "a" 无法匹配 "aa" 整个字符串。
 * 示例 2:
 * 输入:
 * s = "aa"
 * p = "*"
 * 输出: true
 * 解释: '*' 可以匹配任意字符串。
 * 示例 3:
 * 输入:
 * s = "cb"
 * p = "?a"
 * 输出: false
 * 解释: '?' 可以匹配 'c', 但第二个 'a' 无法匹配 'b'。
 * 示例 4:
 * 输入:
 * s = "adceb"
 * p = "*a*b"
 * 输出: true
 * 解释: 第一个 '*' 可以匹配空字符串, 第二个 '*' 可以匹配字符串 "dce".
 * 示例 5:
 * 输入:
 * s = "acdcb"
 * p = "a*c?b"
 * 输出: false
 * <p>
 * 链接：https://leetcode.cn/problems/wildcard-matching
 * 通配符匹配
 */
public class A44WildcardMatching {

    /**
     * 自己实现，回溯+剪纸，怎么都超时…
     */
    public boolean isMatch(String s, String p) {
        if (p.length() == 0) {
            if (s.length() == 0) {
                return true;
            }
            return false;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(p.charAt(0));
        for (int i = 1; i < p.length(); i++) {
            if (builder.charAt(builder.length() - 1) != '*' || p.charAt(i) != '*') {
                builder.append(p.charAt(i));
            }
        }
        p = builder.toString();

        int lastCount = 0;
        for (int i = 0; i < p.length(); i++) {
            if (p.charAt(i) == '*') lastCount++;
        }
        return match(s, 0, p, 0, lastCount);
    }

    private boolean match(String s, int strIndex, String p, int pIndex, int lastCount) {
        // s遍历完，p没有
        if (strIndex > s.length() - 1 && pIndex <= p.length() - 1) {
            // p剩'*'时
            if (p.charAt(pIndex) == '*') {
                return match(s, strIndex, p, pIndex + 1, lastCount - 1);
            } else {
                return false;
            }
        }
        // p遍历完，s没有
        if (strIndex <= s.length() - 1 && pIndex > p.length() - 1) {
            return false;
        }
        // 两个都遍历完，返回
        if (strIndex > s.length() - 1 && pIndex > p.length() - 1) {
            return true;
        }
        if (p.charAt(pIndex) == '?') {
            return match(s, strIndex + 1, p, pIndex + 1, lastCount);
        } else if (p.charAt(pIndex) == '*') {
            if ((p.length() - pIndex - lastCount + 1) > (s.length() - strIndex)) {
                return match(s, strIndex, p, pIndex + 1, lastCount - 1);
            }
            return match(s, strIndex + 1, p, pIndex, lastCount) || match(s, strIndex, p, pIndex + 1, lastCount - 1);
        } else {
            if (p.charAt(pIndex) == s.charAt(strIndex)) {
                return match(s, strIndex + 1, p, pIndex + 1, lastCount);
            } else {
                return false;
            }
        }
    }


    /**
     * 思想 动态规划
     * 尝试通过动态规划解决
     * 数组 boolean[][] dp 。其中dp[i][j]表示s的前i位和p的前j位是否匹配
     */
    public boolean isMatch1(String s, String p) {
        int sLen = s.length();
        int pLen = p.length();
        boolean[][] dp = new boolean[sLen + 1][pLen + 1];
        dp[0][0] = true;
        // s前0位（为空时），p的匹配情况，即前面全为*时匹配，否则不匹配
        for (int i = 0; i < pLen; i++) {
            if (p.charAt(i) == '*') {
                dp[0][i + 1] = dp[0][i];
            } else {
                dp[0][i + 1] = false;
            }
        }
        // 其实不需要，默认值就是false
        for (int i = 0; i < sLen; i++) {
            dp[i + 1][0] = false;
        }

        // 对i，j位置，判断是否匹配
        for (int i = 1; i < sLen + 1; i++) {
            for (int j = 1; j < pLen + 1; j++) {
                if (p.charAt(j - 1) == '*') {
                    dp[i][j] = dp[i][j - 1] || dp[i - 1][j];
                } else if (p.charAt(j - 1) == '?') {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = dp[i - 1][j - 1] && s.charAt(i - 1) == p.charAt(j - 1);
                }
            }
        }
//        print(dp, sLen, pLen);
        return dp[sLen][pLen];
    }

    private void print(boolean[][] dp, int sLen, int pLen) {
        for (int i = 0; i < sLen + 1; i++) {
            for (int j = 0; j < pLen + 1; j++) {
                System.out.print(dp[i][j] + " ");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        A44WildcardMatching matching = new A44WildcardMatching();
        matching.isMatch1("acdcb", "*c?b");
//        System.out.println(matching.isMatch1("acdcb", "a*c?b"));
    }
}
