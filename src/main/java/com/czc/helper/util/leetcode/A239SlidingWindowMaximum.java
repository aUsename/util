package com.czc.helper.util.leetcode;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/9/15
 *
 * <p>
 * 给你一个整数数组 nums，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。你只可以看到在滑动窗口内的 k 个数字。滑动窗口每次只向右移动一位。
 * 返回 滑动窗口中的最大值 。
 * 示例 1：
 * 输入：nums = [1,3,-1,-3,5,3,6,7], k = 3
 * 输出：[3,3,5,5,6,7]
 * 解释：
 * 滑动窗口的位置                最大值
 * ---------------               -----
 * [1  3  -1] -3  5  3  6  7       3
 * 1 [3  -1  -3] 5  3  6  7       3
 * 1  3 [-1  -3  5] 3  6  7       5
 * 1  3  -1 [-3  5  3] 6  7       5
 * 1  3  -1  -3 [5  3  6] 7       6
 * 1  3  -1  -3  5 [3  6  7]      7
 * 示例 2：
 * 输入：nums = [1], k = 1
 * 输出：[1]
 * 提示：
 * 1 <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 * 1 <= k <= nums.length
 */
public class A239SlidingWindowMaximum {

    /**
     * 自己实现，优先级队列，性能比较差，在leetcode上提交会超时
     * 时间复杂度 O(nk)
     */
    public int[] maxSlidingWindow(int[] nums, int k) {
        int[] result = new int[nums.length - k + 1];
        PriorityQueue<Integer> queue = new PriorityQueue<>(k, new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });


        for (int i = 0; i < k - 1; i++) {
            queue.add(nums[i]);
        }

        for (int i = k - 1; i < nums.length; i++) {
            queue.add(nums[i]);
            result[i - k + 1] = queue.peek();
            // todo 这里其实不必要删除所有元素，这样带来了比较高的耗时，只需要判断顶部元素在不在窗口内，不在的话删除即可（删除需要O(k)，插入需要O(logk)）
            queue.remove(nums[i - k + 1]);
        }

        return result;
    }

    /**
     * leetcode 官方实现
     * 用二元数组存储了元素和元素的位置
     * 时间复杂度为 O(nlog⁡n) （最坏情况，优先级队列包含了所有元素）
     */
    public int[] maxSlidingWindow1(int[] nums, int k) {
        int n = nums.length;
        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>() {
            public int compare(int[] pair1, int[] pair2) {
                return pair1[0] != pair2[0] ? pair2[0] - pair1[0] : pair2[1] - pair1[1];
            }
        });
        for (int i = 0; i < k; ++i) {
            pq.offer(new int[]{nums[i], i});
        }
        int[] ans = new int[n - k + 1];
        ans[0] = pq.peek()[0];
        for (int i = k; i < n; ++i) {
            pq.offer(new int[]{nums[i], i});
            // 每当我们向右移动窗口时，我们就可以把一个新的元素放入优先队列中，此时堆顶的元素就是堆中所有元素的最大值。然而这个最大值可能并不在滑动窗口中，在这种情况下，这个值在数组 nums\textit{nums}nums 中的位置出现在滑动窗口左边界的左侧。因此，当我们后续继续向右移动窗口时，这个值就永远不可能出现在滑动窗口中了，我们可以将其永久地从优先队列中移除。
            //我们不断地移除堆顶的元素，直到其确实出现在滑动窗口中。此时，堆顶元素就是滑动窗口中的最大值。为了方便判断堆顶元素与滑动窗口的位置关系，我们可以在优先队列中存储二元组 (num,index)(\textit{num}, \textit{index})(num,index)，表示元素 num\textit{num}num 在数组中的下标为 index\textit{index}index。
            while (pq.peek()[1] <= i - k) {
                pq.poll();
            }
            ans[i - k + 1] = pq.peek()[0];
        }
        return ans;
    }

    /**
     * 自己按照leetcode优先级队列优化方式实现
     * <p>
     * todo 还可以进一步优化，当新元素进入窗口时，若该元素比窗口内的它左边的元素大，那么它左边的元素就可以删除
     */
    public int[] maxSlidingWindow2(int[] nums, int k) {
        int[] result = new int[nums.length - k + 1];
        PriorityQueue<int[]> queue = new PriorityQueue<>(k, (o1, o2) -> {
            if (o2[0] - o1[0] >= 0) {
                return 1;
            }
            return -1;
        });

        for (int i = 0; i < k - 1; i++) {
            queue.add(new int[]{nums[i], i});
        }

        for (int i = k - 1; i < nums.length; i++) {
            queue.add(new int[]{nums[i], i});
            while (queue.peek()[1] < i - k + 1) {
                queue.poll();
            }
            result[i - k + 1] = queue.peek()[0];
        }

        return result;
    }

    /**
     * 思想 单调队列法
     * 按照上方法的继续优化思路，若进入窗口的新元素比窗口内的它左边的元素大，那么它左边的元素就可以删除
     * 使用双端队列，一端做过期，一端做比较删除，会形成一个单调递减的队列，被称作单调队列
     * <p>
     * 时间复杂度O(n)
     * 空间复杂度O(k)
     */
    public int[] maxSlidingWindow3(int[] nums, int k) {
        int[] result = new int[nums.length - k + 1];

        Deque<Integer> deque = new ArrayDeque<>();
        deque.addLast(0);
        for (int i = 1; i < k; i++) {
            // 新增元素，若该元素比它左边的元素大，那么它左边的元素就删除
            while (!deque.isEmpty() && nums[i] >= nums[deque.peekLast()]) {
                deque.pollLast();
            }
            deque.addLast(i);
        }
        result[0] = nums[deque.peek()];
        for (int i = k; i < nums.length; i++) {
            // 删除已经超过窗口的元素
            if (deque.peek() <= i - k) {
                deque.poll();
            }
            // 新增元素，若该元素比它左边的元素大，那么它左边的元素就删除
            while (!deque.isEmpty() && nums[i] >= nums[deque.peekLast()]) {
                deque.pollLast();
            }
            deque.addLast(i);
            result[i - k + 1] = nums[deque.peek()];
        }
        return result;
    }


    public static void main(String[] args) {
        A239SlidingWindowMaximum maximum = new A239SlidingWindowMaximum();
        int[] nums = {1, 3, -1, -3, 5, 3, 6, 7};
        int[] re = maximum.maxSlidingWindow3(nums, 3);
        for (int r : re) {
            System.out.println(r);
        }
    }
}
