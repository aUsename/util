package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/10/29
 *
 * <p>
 * 一个机器人位于一个 m x n 网格的左上角。
 * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
 * 问总共有多少条不同的路径？
 * 示例 1：
 * 输入：m = 3, n = 7
 * 输出：28
 * 示例 2：
 * 输入：m = 3, n = 2
 * 输出：3
 * 解释：
 * 从左上角开始，总共有 3 条路径可以到达右下角。
 * 1. 向右 -> 向下 -> 向下
 * 2. 向下 -> 向下 -> 向右
 * 3. 向下 -> 向右 -> 向下
 * 示例 3：
 * 输入：m = 7, n = 3
 * 输出：28
 * 示例 4：
 * 输入：m = 3, n = 3
 * 输出：6
 * 提示：
 * 1 <= m, n <= 100
 * 题目数据保证答案小于等于 2 * 10^9
 * <p>
 * 链接：https://leetcode.cn/problems/unique-paths
 * 不同路径
 */
public class A62UniquePaths {

    /**
     * 自己实现，但是result一开始用的int，会越界，后面改成double，但是不保险，可能有精度问题，最后看了官方用的long
     * 也可以用动态规划实现，(i,j)位置的路径数目为：f(i,j) = f(i−1,j) + f(i,j−1) 实现可以看A63UniquePaths2
     */
    public int uniquePaths(int m, int n) {

        int min = Math.min(m, n) - 1;
        int max = Math.max(m, n) - 1;

        long result = 1;
        for (int i = 0; i < min; i++) {
            result = result * (max + min - i) / (i + 1);
        }

        return (int) result;
    }
}
