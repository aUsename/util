package com.czc.helper.util.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/9/13
 *
 * <p>
 * 给定一个大小为 n 的数组 nums ，返回其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 * 示例 1：
 * 输入：nums = [3,2,3]
 * 输出：3
 * 示例 2：
 * 输入：nums = [2,2,1,1,1,2,2]
 * 输出：2
 * 提示：
 * n == nums.length
 * 1 <= n <= 5 * 10^4
 * -10^9 <= nums[i] <= 10^9
 * <p>
 * 链接：https://leetcode.cn/problems/majority-element
 */
public class A169MajorityElement {


    /**
     * 排序法
     * 自己的实现，排序后遍历
     *
     * @param nums
     * @return
     */
    public int majorityElement(int[] nums) {
        Arrays.sort(nums);

        int result = 0;
        int current = nums[0];
        int maxCount = 0;
        int currentCount = 0;
        for (int i = 0; i < nums.length; i++) {
            // 与上一个值相同，累计次数
            if (current == nums[i]) {
                currentCount++;
                continue;
            }
            // 与上一个值不同
            if (maxCount < currentCount) {
                result = nums[i - 1];
                maxCount = currentCount;
            }
            currentCount = 1;
            current = nums[i];
        }
        if (maxCount < currentCount) {
            result = nums[nums.length - 1];
        }

        return result;
    }

    /**
     * 排序法
     * leetcode的实现，由于给定的数组总是存在多数元素，所以无论众数是多少，返回 ⌊ n/2 ⌋ 下标对应的值都是正确的
     *
     * @param nums
     * @return
     */
    public int majorityElement1(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

    /**
     * hashmap法
     *
     * @param nums
     * @return
     */
    public int majorityElement2(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }
        int result = 0;
        int maxCount = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() > maxCount) {
                result = entry.getKey();
                maxCount = entry.getValue();
            }
        }

        return result;
    }

    /**
     * 思想 摩尔投票法
     * <p>
     * 随机选定候选人，遇到相同的数，count++，否则count--，当count为0时，更换候选人
     *
     * 只适用于在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素，
     *
     * @param nums
     * @return
     */
    public int majorityElement3(int[] nums) {
        int result = nums[0];
        int count = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == result) {
                count++;
            } else {
                count--;
                // 这里其实就是在count=1时，若遇到不同元素，则更换结果元素，是为了避免abbbbb这样的场景
                // 更换之后，可能遇到abababab这样的场景，所以要求最多元素数量要超过一半
                if (count == 0) {
                    result = nums[i];
                    count = 1;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        A169MajorityElement majorityElement = new A169MajorityElement();
//        int[] nums = {3, 3, 4};
//        int[] nums = {2, 2, 1, 1, 1, 2, 2};
        int[] nums = {1, 2, 1, 3, 1, 4};
        int result = majorityElement.majorityElement3(nums);
        System.out.println(result);
    }

}
