package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/11/11
 * <p>
 * 给定三个字符串 s1、s2、s3，请你帮忙验证 s3 是否是由 s1 和 s2 交错 组成的。
 * 两个字符串 s 和 t 交错 的定义与过程如下，其中每个字符串都会被分割成若干 非空 子字符串：
 * · s = s1 + s2 + ... + sn
 * · t = t1 + t2 + ... + tm
 * · |n - m| <= 1
 * · 交错 是 s1 + t1 + s2 + t2 + s3 + t3 + ... 或者 t1 + s1 + t2 + s2 + t3 + s3 + ...
 * 注意：a + b 意味着字符串 a 和 b 连接。
 * 示例 1：
 * 输入：s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
 * 输出：true
 * 示例 2：
 * 输入：s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
 * 输出：false
 * 示例 3：
 * 输入：s1 = "", s2 = "", s3 = ""
 * 输出：true
 * 提示：
 * 0 <= s1.length, s2.length <= 100
 * 0 <= s3.length <= 200
 * s1、s2、和 s3 都由小写英文字母组成
 * 进阶：您能否仅使用 O(s2.length) 额外的内存空间来解决它?
 * <p>
 * 链接：https://leetcode.cn/problems/interleaving-string
 * 交错字符串
 */
public class A97InterleavingString {
    /**
     * 动态规划
     */
    public boolean isInterleave(String s1, String s2, String s3) {
        int len1 = s1.length(), len2 = s2.length(), len3 = s3.length();
        if (len1 + len2 != len3) {
            return false;
        }

        // (i,j)代表s1的前i位，s2的前j位
        boolean[][] arr = new boolean[len1 + 1][len2 + 1];

        for (int i = 0; i <= len1; i++) {
            for (int j = 0; j <= len2; j++) {
                if (i == 0 && j == 0) {
                    arr[0][0] = true;
                } else if (i == 0) {
                    arr[0][j] = arr[0][j - 1] && s2.charAt(j - 1) == s3.charAt(j - 1);
                } else if (j == 0) {
                    arr[i][0] = arr[i - 1][0] && s1.charAt(i - 1) == s3.charAt(i - 1);
                } else {
                    arr[i][j] = (arr[i][j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1))
                            || (arr[i - 1][j] && s1.charAt(i - 1) == s3.charAt(i + j - 1));
                }
            }
        }

        return arr[len1][len2];
    }

    /**
     * O(s2.length) 空间
     * 思想： 滚动数组 优化
     */
    public boolean isInterleave1(String s1, String s2, String s3) {
        int len1 = s1.length(), len2 = s2.length(), len3 = s3.length();
        if (len1 + len2 != len3) {
            return false;
        }

        // (i,j)代表s1的前i位，s2的前j位
        boolean[] arr = new boolean[len2 + 1];

        for (int i = 0; i <= len1; i++) {
            for (int j = 0; j <= len2; j++) {
                if (i == 0 && j == 0) {
                    arr[0] = true;
                } else if (i == 0) {
                    arr[j] = arr[j - 1] && s2.charAt(j - 1) == s3.charAt(j - 1);
                } else if (j == 0) {
                    arr[0] = arr[0] && s1.charAt(i - 1) == s3.charAt(i - 1);
                } else {
                    arr[j] = (arr[j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1))
                            || (arr[j] && s1.charAt(i - 1) == s3.charAt(i + j - 1));
                }
            }
        }

        return arr[len2];
    }
}
