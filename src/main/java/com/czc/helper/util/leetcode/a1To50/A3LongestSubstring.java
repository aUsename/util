package com.czc.helper.util.leetcode.a1To50;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/9/6
 *
 * <p>
 * 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
 * 示例 1:
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * 示例 3:
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
 * 请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
 * <p>
 * 链接：https://leetcode.cn/problems/longest-substring-without-repeating-characters
 */
public class A3LongestSubstring {

    /**
     * 双指针，滑动窗口
     */
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        int result = 0;
        char[] sChar = s.toCharArray();

        // 子串字符集
        Map<Character, Integer> map = new HashMap<>();
        map.put(sChar[0], 0);
        // 双指针，代表不重复子串的起止位置
        int i = 0, j = 1;

        // 依次移动子串右边位置，直至遇到重复字符
        for (; j < sChar.length; j++) {
            if (map.containsKey(sChar[j])) {
                result = result > (j - i) ? result : (j - i);

                int next = map.get(sChar[j]) + 1;
                // 将子串开始位置移到重复字符的右边一个
                for (; i < next; i++) {
                    map.remove(sChar[i]);
                }
                map.put(sChar[j], j);

            } else {
                map.put(sChar[j], j);
            }
        }

        return result > (j - i) ? result : (j - i);
    }

    public static void main(String[] args) {
        A3LongestSubstring a3LongestSubstring = new A3LongestSubstring();
        int r = a3LongestSubstring.lengthOfLongestSubstring("abcabcbb");
        System.out.println(r);
    }

}
