package com.czc.helper.util.leetcode.a51To100;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/11/1
 *
 * <p>
 * 给你一个字符串 s 、一个字符串 t 。返回 s 中涵盖 t 所有字符的最小子串。如果 s 中不存在涵盖 t 所有字符的子串，则返回空字符串 "" 。
 * 注意：
 * 对于 t 中重复字符，我们寻找的子字符串中该字符数量必须不少于 t 中该字符数量。
 * 如果 s 中存在这样的子串，我们保证它是唯一的答案。
 * 示例 1：
 * 输入：s = "ADOBECODEBANC", t = "ABC"
 * 输出："BANC"
 * 示例 2：
 * 输入：s = "a", t = "a"
 * 输出："a"
 * 示例 3:
 * 输入: s = "a", t = "aa"
 * 输出: ""
 * 解释: t 中两个字符 'a' 均应包含在 s 的子串中，
 * 因此没有符合条件的子字符串，返回空字符串。
 * 提示：
 * 1 <= s.length, t.length <= 105
 * s 和 t 由英文字母组成
 * 进阶：你能设计一个在 o(n) 时间内解决此问题的算法吗？
 * <p>
 * 链接：https://leetcode.cn/problems/minimum-window-substring
 * 最小覆盖子串
 */
public class A76MinimumWindowSubstring {

    private int resultL = 0, resultR, resultLen;

    /**
     * 自己实现，看了提示的利用哈希表来解决
     */
    public String minWindow(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        int total = t.length(), len = s.length();

        // 统计t中所有字符次数，放入map中
        for (int i = 0; i < total; i++) {
            if (map.containsKey(t.charAt(i))) {
                map.put(t.charAt(i), map.get(t.charAt(i)) + 1);
            } else {
                map.put(t.charAt(i), 1);
            }
        }
        int left = 0, right = 0;
        // 找到从0位置开始，s中覆盖t的最短子串
        while (right < len && total > 0) {
            if (map.containsKey(s.charAt(right))) {
                if (map.get(s.charAt(right)) > 0) {
                    total--;
                }
                map.put(s.charAt(right), map.get(s.charAt(right)) - 1);
            }
            right++;
        }
        right--;
        if (total > 0) {
            return "";
        }

        // 找到从0位置开始的最短覆盖子串，初始化返回结果的左，右位置，以及长度
        resultR = right;
        resultLen = right - left + 1;

        // leftGo
        left = leftGo(s, map, left, right);

        while (right + 1 < len) {
            // rightGo
            right++;
            if (map.containsKey(s.charAt(right))) {
                map.put(s.charAt(right), map.get(s.charAt(right)) - 1);
                // leftGo
                left = leftGo(s, map, left, right);
            }
        }

        return s.substring(resultL, resultR + 1);
    }

    private int leftGo(String s, Map<Character, Integer> map, int left, int right) {
        while (true) {
            if (map.containsKey(s.charAt(left))) {
                if (map.get(s.charAt(left)) < 0) {
                    map.put(s.charAt(left), map.get(s.charAt(left)) + 1);
                    left++;
                } else {
                    if (right - left + 1 < resultLen) {
                        resultLen = right - left + 1;
                        resultL = left;
                        resultR = right;
                    }
                    return left;
                }
            } else {
                left++;
            }
        }
    }

    public static void main(String[] args) {
        A76MinimumWindowSubstring substring = new A76MinimumWindowSubstring();
        System.out.println(substring.minWindow("ADOBECODEBANC", "ABC"));
    }
}
