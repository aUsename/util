package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/25
 *
 * <p>
 * 实现 pow(x, n) ，即计算 x 的整数 n 次幂函数（即，x^n ）。
 * 示例 1：
 * 输入：x = 2.00000, n = 10
 * 输出：1024.00000
 * 示例 2：
 * 输入：x = 2.10000, n = 3
 * 输出：9.26100
 * 示例 3：
 * 输入：x = 2.00000, n = -2
 * 输出：0.25000
 * 解释：2^-2 = 1/2^2 = 1/4 = 0.25
 * 提示：
 * -100.0 < x < 100.0
 * -2^31 <= n <= 2^31-1
 * -10^4 <= x^n <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/powx-n
 * Pow(x, n)
 */
public class A50PowxN {
    public double myPow(double x, int n) {
        if (n == 0) return 1;

        double result = 1;
        if (n == Integer.MIN_VALUE) {
            result = result * (1 / x);
            n++;
        }
        if (n < 0) {
            x = 1 / x;
            n = -n;
        }

        while (n > 1) {
            if (n % 2 == 1) {
                result = result * x;
            }
            n = n >> 1;
            x = x * x;
        }
        result = result * x;
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Math.pow(2, 111));
        System.out.println(Math.pow(0.5, 111));
        A50PowxN powxN = new A50PowxN();
        System.out.println(powxN.myPow(2, 111));
    }
}
