package com.czc.helper.util.leetcode.a1To50;

import com.czc.helper.util.leetcode.helper.ListNode;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/9/13
 *
 * <p>
 * 给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
 * 示例 1：
 * 输入：head = [1,2,3,4,5], n = 2
 * 输出：[1,2,3,5]
 * 示例 2：
 * 输入：head = [1], n = 1
 * 输出：[]
 * 示例 3：
 * 输入：head = [1,2], n = 1
 * 输出：[1]
 * 提示：
 * 链表中结点的数目为 sz
 * 1 <= sz <= 30
 * 0 <= Node.val <= 100
 * 1 <= n <= sz
 * 进阶：你能尝试使用一趟扫描实现吗？
 * <p>
 * 链接：https://leetcode.cn/problems/remove-nth-node-from-end-of-list
 */
public class A19RemoveNthNodeFromEndOfList {
    /**
     * 自己实现1，先反转，再删除，再反转
     * 3次扫描
     *
     * @return
     */
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode list1 = reverseList(head);

        if (n == 1) {
            // n=1 直接删除头部
            list1 = list1.next;
        } else {
            // n>1，找到被删除节点前置节点
            ListNode current = list1;
            for (int i = 0; i < n - 2; i++) {
                current = current.next;
            }
            current.next = current.next.next;
        }

        ListNode list2 = reverseList(list1);
        return list2;
    }

    // 反转链表
    private ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode();
        while (head != null) {
            newHead.next = new ListNode(head.val, newHead.next);
            head = head.next;
        }
        return newHead.next;
    }

    /**
     * 自己实现2，遍历第一次，找到总节点数，第二次遍历，找到需要删除节点，再删除
     * 2次扫描
     */
    public ListNode removeNthFromEnd1(ListNode head, int n) {
        int count = 0;
        ListNode current = head;
        while (current != null) {
            count++;
            current = current.next;
        }

        if (count - n == 0) {
            head = head.next;
        } else {
            current = head;
            for (int i = 0; i < count - n - 1; i++) {
                current = current.next;
            }
            current.next = current.next.next;
        }

        return head;
    }

    /**
     * 自己实现3，遍历时，用map存储每个节点，遍历完成后直接找到需要删除节点进行删除
     * 1次遍历
     */
    public ListNode removeNthFromEnd2(ListNode head, int n) {
        Map<Integer, ListNode> map = new HashMap<>();
        int count = 0;
        ListNode current = head;
        while (current != null) {
            map.put(count, current);
            count++;
            current = current.next;
        }
        if (count - n == 0) {
            head = head.next;
        } else {
            current = map.get(count - n - 1);
            current.next = current.next.next;
        }
        return head;
    }

    /**
     * leetcode实现，利用栈
     * 近似一次遍历
     */
    public ListNode removeNthFromEnd3(ListNode head, int n) {
        // 使用哑节点，简化头节点的判断
        ListNode dummy = new ListNode(0, head);
        Deque<ListNode> stack = new LinkedList<>();
        ListNode cur = dummy;
        while (cur != null) {
            stack.push(cur);
            cur = cur.next;
        }
        for (int i = 0; i < n; ++i) {
            stack.pop();
        }
        ListNode prev = stack.peek();
        prev.next = prev.next.next;
        ListNode ans = dummy.next;
        return ans;
    }

    /**
     * leetcode实现，双指针，第二个指针在第一个指针后n个位置
     * 1次遍历
     */
    public ListNode removeNthFromEnd4(ListNode head, int n) {
        ListNode dummy = new ListNode(0, head);
        ListNode first = head;
        ListNode second = dummy;
        for (int i = 0; i < n; ++i) {
            first = first.next;
        }
        while (first != null) {
            first = first.next;
            second = second.next;
        }
        second.next = second.next.next;
        ListNode ans = dummy.next;
        return ans;
    }

}
