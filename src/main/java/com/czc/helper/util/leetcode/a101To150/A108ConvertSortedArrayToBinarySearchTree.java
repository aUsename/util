package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/17
 * <p>
 * 给你一个整数数组 nums ，其中元素已经按 升序 排列，请你将其转换为一棵 高度平衡 二叉搜索树。
 * 高度平衡 二叉树是一棵满足「每个节点的左右两个子树的高度差的绝对值不超过 1 」的二叉树。
 * 示例 1：
 * 输入：nums = [-10,-3,0,5,9]
 * 输出：[0,-3,9,-10,null,5]
 * 解释：[0,-10,5,null,-3,null,9] 也将被视为正确答案：
 * 示例 2：
 * 输入：nums = [1,3]
 * 输出：[3,1]
 * 解释：[1,null,3] 和 [3,1] 都是高度平衡二叉搜索树。
 * 提示：
 * 1 <= nums.length <= 10^4
 * -10^4 <= nums[i] <= 10^4
 * nums 按 严格递增 顺序排列
 * <p>
 * 链接：https://leetcode.cn/problems/convert-sorted-array-to-binary-search-tree
 * 将有序数组转换为二叉搜索树
 */
public class A108ConvertSortedArrayToBinarySearchTree {
    public TreeNode sortedArrayToBST(int[] nums) {
        TreeNode head = new TreeNode();
        next(nums, true, head, 0, nums.length - 1);
        return head.left;
    }

    private void next(int[] nums, boolean left, TreeNode head, int leftIndex, int rightIndex) {
        if (leftIndex > rightIndex) {
            return;
        }
        int mid = (leftIndex + rightIndex) / 2;
        TreeNode current = new TreeNode(nums[mid]);
        if (left) {
            head.left = current;
        } else {
            head.right = current;
        }
        next(nums, true, current, leftIndex, mid - 1);
        next(nums, false, current, mid + 1, rightIndex);
    }


}
