package com.czc.helper.util.leetcode;

/**
 * Created by cuizhongcheng on 2022/9/20
 *
 * <p>
 * 给你一个字符串 s ，请你反转字符串中 单词 的顺序。
 * 单词 是由非空格字符组成的字符串。s 中使用至少一个空格将字符串中的 单词 分隔开。
 * 返回 单词 顺序颠倒且 单词 之间用单个空格连接的结果字符串。
 * 注意：输入字符串 s中可能会存在前导空格、尾随空格或者单词间的多个空格。返回的结果字符串中，单词间应当仅用单个空格分隔，且不包含任何额外的空格。
 * 示例 1：
 * 输入：s = "the sky is blue"
 * 输出："blue is sky the"
 * 示例 2：
 * 输入：s = "  hello world  "
 * 输出："world hello"
 * 解释：反转后的字符串中不能存在前导空格和尾随空格。
 * 示例 3：
 * 输入：s = "a good   example"
 * 输出："example good a"
 * 解释：如果两个单词间有多余的空格，反转后的字符串需要将单词间的空格减少到仅有一个。
 * 提示：
 * 1 <= s.length <= 10^4
 * s 包含英文大小写字母、数字和空格 ' '
 * s 中 至少存在一个 单词
 * 进阶：如果字符串在你使用的编程语言中是一种可变数据类型，请尝试使用 O(1) 额外空间复杂度的 原地 解法。
 * <p>
 * 链接：https://leetcode.cn/problems/reverse-words-in-a-string
 */
public class A151ReverseWordsInAString {

    /**
     * 自己实现trim和翻转，实际有String.trim方法
     * todo 同时string不是可变类型，为了节省空间，应该使用StringBuilder进行字符拼接
     */
    public String reverseWords(String s) {
        s = trim(s);

        int leftL = 0, rightR = s.length() - 1;
        int leftR = leftMove(s, leftL), rightL = rightMove(s, rightR);
        String tempStrL;
        String tempStrR;

        while (leftR <= rightL) {
            tempStrL = s.substring(leftL, leftR);
            tempStrR = s.substring(rightL + 1, rightR + 1);
            s = s.substring(0, leftL)
                    + tempStrR
                    + s.substring(leftR, rightL + 1)
                    + tempStrL
                    + s.substring(rightR + 1, s.length());
            leftL = leftL + tempStrR.length() + 1;
            rightR = rightR - tempStrL.length() - 1;

            leftR = leftMove(s, leftL);
            rightL = rightMove(s, rightR);
        }

        return s;
    }

    private int leftMove(String s, int leftR) {
        while (leftR < s.length() && s.charAt(leftR) != ' ') {
            leftR++;
        }
        return leftR;
    }

    private int rightMove(String s, int rightL) {
        while (rightL >= 0 && s.charAt(rightL) != ' ') {
            rightL--;
        }
        return rightL;
    }

    private String trim(String s) {
        while (s.charAt(0) == ' ') {
            s = s.substring(1);
        }
        while (s.charAt(s.length() - 1) == ' ') {
            s = s.substring(0, s.length() - 1);
        }
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                while (s.charAt(i + 1) == ' ') {
                    s = s.substring(0, i + 1) + s.substring(i + 2, s.length());
                }
            }
        }
        return s;
    }

    public static void main(String[] args) {
        A151ReverseWordsInAString words = new A151ReverseWordsInAString();
        String s = words.reverseWords("  hello world  ");
        System.out.println("|" + s + "|");
    }
}
