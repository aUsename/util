package com.czc.helper.util.leetcode.a1To50;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/10/14
 *
 * <p>
 * 给定一个字符串 s 和一个字符串数组 words。 words 中所有字符串 长度相同。
 * s 中的 串联子串 是指一个包含  words 中所有字符串以任意顺序排列连接起来的子串。
 * 例如，如果 words = ["ab","cd","ef"]， 那么 "abcdef"， "abefcd"，"cdabef"， "cdefab"，"efabcd"， 和 "efcdab" 都是串联子串。 "acdbef" 不是串联子串，因为他不是任何 words 排列的连接。
 * 返回所有串联字串在 s 中的开始索引。你可以以 任意顺序 返回答案。
 * 示例 1：
 * 输入：s = "barfoothefoobarman", words = ["foo","bar"]
 * 输出：[0,9]
 * 解释：因为 words.length == 2 同时 words[i].length == 3，连接的子字符串的长度必须为 6。
 * 子串 "barfoo" 开始位置是 0。它是 words 中以 ["bar","foo"] 顺序排列的连接。
 * 子串 "foobar" 开始位置是 9。它是 words 中以 ["foo","bar"] 顺序排列的连接。
 * 输出顺序无关紧要。返回 [9,0] 也是可以的。
 * 示例 2：
 * 输入：s = "wordgoodgoodgoodbestword", words = ["word","good","best","word"]
 * 输出：[]
 * 解释：因为 words.length == 4 并且 words[i].length == 4，所以串联子串的长度必须为 16。
 * s 中没有子串长度为 16 并且等于 words 的任何顺序排列的连接。
 * 所以我们返回一个空数组。
 * 示例 3：
 * 输入：s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]
 * 输出：[6,9,12]
 * 解释：因为 words.length == 3 并且 words[i].length == 3，所以串联子串的长度必须为 9。
 * 子串 "foobarthe" 开始位置是 6。它是 words 中以 ["foo","bar","the"] 顺序排列的连接。
 * 子串 "barthefoo" 开始位置是 9。它是 words 中以 ["bar","the","foo"] 顺序排列的连接。
 * 子串 "thefoobar" 开始位置是 12。它是 words 中以 ["the","foo","bar"] 顺序排列的连接。
 * 提示：
 * 1 <= s.length <= 10^4
 * 1 <= words.length <= 5000
 * 1 <= words[i].length <= 30
 * words[i] 和 s 由小写英文字母组成
 * <p>
 * 链接：https://leetcode.cn/problems/substring-with-concatenation-of-all-words
 */
public class A30SubstringWithConcatenationOfAllWords {

    /**
     * 看了一遍leetcode实现思路后自己实现
     * todo 需要注意每次右移左指针时，都是右移1位，而不是移动len位
     * 同时可以将reBuild改成左移右指针来还原
     */
    public List<Integer> findSubstring(String s, String[] words) {
        int len = words[0].length();
        List<Integer> result = new ArrayList<>();

        if (s.length() < words.length * len) {
            return result;
        }

        // 统计words中每个单词和数量
        Map<String, Integer> wordsCount = new HashMap<>();
        // 初始化
        for (int i = 0; i < words.length; i++) {
            if (wordsCount.containsKey(words[i])) {
                wordsCount.put(words[i], wordsCount.get(words[i]) + 1);
            } else {
                wordsCount.put(words[i], 1);
            }
        }
        // 双指针
        int left = 0;
        int right = 0;
        for (; left + words.length * len <= s.length(); ) {
            while (right - left <= words.length * len - len) {
                // 右指针右移
                String current = s.substring(right, right + len);

                if (wordsCount.containsKey(current)) {
                    if (wordsCount.get(current) > 0) {
                        right += len;
                        wordsCount.put(current, wordsCount.get(current) - 1);

                        // 满足条件
                        if (right - left == words.length * len) {
                            result.add(left);
                            // 左指针右移一位，重置其他值
                            reBuild(s, wordsCount, words);
                            left += 1;
                            right = left;
                            break;
                        }

                    } else {
                        // wordsCount.get(current) = 0 ，说明该字符串已满，左指针右移一位
                        reBuild(s, wordsCount, words);
                        left += 1;
                        right = left;
                        break;
                    }
                } else {
                    // 遇到了不存在的字符串，左指针右移一位
                    reBuild(s, wordsCount, words);
                    left += 1;
                    right = left;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * 左指针右移
     */
    private void leftLen(String s, Map<String, Integer> wordsCount, int left, int len) {
        String current = s.substring(left, left + len);
        if (wordsCount.containsKey(current)) {
            wordsCount.put(current, wordsCount.get(current) + 1);
        }
    }

    private void reBuild(String s, Map<String, Integer> wordsCount, String[] words) {
        wordsCount.clear();
        for (int i = 0; i < words.length; i++) {
            if (wordsCount.containsKey(words[i])) {
                wordsCount.put(words[i], wordsCount.get(words[i]) + 1);
            } else {
                wordsCount.put(words[i], 1);
            }
        }
    }

}
