package com.czc.helper.util.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/19
 *
 * <p>
 * 给你一个 无重复元素 的整数数组 candidates 和一个目标整数 target ，找出 candidates 中可以使数字和为目标数 target 的 所有 不同组合 ，并以列表形式返回。你可以按 任意顺序 返回这些组合。
 * candidates 中的 同一个 数字可以 无限制重复被选取 。如果至少一个数字的被选数量不同，则两种组合是不同的。
 * 对于给定的输入，保证和为 target 的不同组合数少于 150 个。
 * 示例 1：
 * 输入：candidates = [2,3,6,7], target = 7
 * 输出：[[2,2,3],[7]]
 * 解释：
 * 2 和 3 可以形成一组候选，2 + 2 + 3 = 7 。注意 2 可以使用多次。
 * 7 也是一个候选， 7 = 7 。
 * 仅有这两种组合。
 * 示例 2：
 * 输入: candidates = [2,3,5], target = 8
 * 输出: [[2,2,2,2],[2,3,3],[3,5]]
 * 示例 3：
 * 输入: candidates = [2], target = 1
 * 输出: []
 * 提示：
 * 1 <= candidates.length <= 30
 * 1 <= candidates[i] <= 200
 * candidate 中的每个元素都 互不相同
 * 1 <= target <= 500
 * <p>
 * 链接：https://leetcode.cn/problems/combination-sum
 */
public class A39CombinationSum {
    private List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        next(candidates, target, 0, 0, new ArrayList<>());
        return result;
    }

    private void next(int[] candidates, int target, int index, int nowSum, List<Integer> temp) {
        if (index >= candidates.length) {
            return;
        }

        // 这里也可以不新建List，通过add和remove复用同一个List，只在满足条件时（result.add时）新建
        // 优化实现可以看相似题 A40CombinationSum2
        List<Integer> newList1 = new ArrayList<>(temp.size());
        List<Integer> newList2 = new ArrayList<>(temp.size());
        for (Integer num : temp) {
            newList1.add(num);
            newList2.add(num);
        }

        // todo 可以先排序，排序之后这里就可以直接剪枝return了
        if (nowSum + candidates[index] == target) {
            temp.add(candidates[index]);
            result.add(temp);
        }

        if (nowSum + candidates[index] < target) {
            // 当前值加上
            newList1.add(candidates[index]);
            next(candidates, target, index, nowSum + candidates[index], newList1);
        }

        // 当前值不加，走下一个
        next(candidates, target, index + 1, nowSum, newList2);
    }

    public static void main(String[] args) {
        A39CombinationSum combinationSum = new A39CombinationSum();
        int[] candidates = {4, 2, 8};
        List<List<Integer>> result = combinationSum.combinationSum(candidates, 8);
        for (List<Integer> item : result) {
            for (Integer num : item) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
