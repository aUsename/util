package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/17
 *
 * <p>
 * 整数数组 nums 按升序排列，数组中的值 互不相同 。
 * 在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转，使数组变为 [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]（下标 从 0 开始 计数）。例如， [0,1,2,4,5,6,7] 在下标 3 处经旋转后可能变为 [4,5,6,7,0,1,2] 。
 * 给你 旋转后 的数组 nums 和一个整数 target ，如果 nums 中存在这个目标值 target ，则返回它的下标，否则返回 -1 。
 * 你必须设计一个时间复杂度为 O(log n) 的算法解决此问题。
 * 示例 1：
 * 输入：nums = [4,5,6,7,0,1,2], target = 0
 * 输出：4
 * 示例 2：
 * 输入：nums = [4,5,6,7,0,1,2], target = 3
 * 输出：-1
 * 示例 3：
 * 输入：nums = [1], target = 0
 * 输出：-1
 * 提示：
 * 1 <= nums.length <= 5000
 * -10^4 <= nums[i] <= 10^4
 * nums 中的每个值都 独一无二
 * 题目数据保证 nums 在预先未知的某个下标上进行了旋转
 * -10^4 <= target <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/search-in-rotated-sorted-array
 * 搜索旋转排序数组
 * 类似题 A81SearchInRotatedSortedArray2
 */
public class A33SearchInRotatedSortedArray {

    /**
     * 对子数组来说，若最左边元素<最右边元素，说明中断点不在此子数组中
     */
    public int search(int[] nums, int target) {
        if (nums.length == 0) return -1;
        return search(nums, target, 0, nums.length - 1);
    }

    private int search(int[] nums, int target, int left, int right) {
        if (right == left) {
            if (nums[left] == target) {
                return left;
            } else {
                return -1;
            }
        }

        if (right - left == 1) {
            return Math.max(search(nums, target, left, left), search(nums, target, right, right));
        }

        // 至少有3个元素
        int mid = (left + right) / 2;
        if (nums[mid] == target) return mid;

        // 左子数组 left ~ mid-1
        int leftResult = -1;
        if (nums[left] <= nums[mid - 1]) {
            // 此子数组是连续的
            if (nums[left] <= target && target <= nums[mid - 1]) {
                leftResult = search(nums, target, left, mid - 1);
            }
        } else {
            if (nums[left] <= target || target <= nums[mid - 1]) {
                leftResult = search(nums, target, left, mid - 1);
            }
        }

        // 右子数组 mid+1 ~ right
        int rightResult = -1;
        if (nums[mid + 1] <= nums[right]) {
            // 此子数组是连续的
            if (nums[mid + 1] <= target && target <= nums[right]) {
                rightResult = search(nums, target, mid + 1, right);
            }
        } else {
            if (nums[mid + 1] <= target || target <= nums[right]) {
                rightResult = search(nums, target, mid + 1, right);
            }
        }

        return Math.max(leftResult, rightResult);
    }
}
