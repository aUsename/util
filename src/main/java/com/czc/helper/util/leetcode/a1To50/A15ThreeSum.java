package com.czc.helper.util.leetcode.a1To50;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/9/8
 *
 * <p>
 * 给你一个整数数组 nums ，判断是否存在三元组 [nums[i], nums[j], nums[k]] 满足 i != j、i != k 且 j != k ，同时还满足 nums[i] + nums[j] + nums[k] == 0 。请
 * 你返回所有和为 0 且不重复的三元组。
 * 注意：答案中不可以包含重复的三元组。
 * 示例 1：
 * 输入：nums = [-1,0,1,2,-1,-4]
 * 输出：[[-1,-1,2],[-1,0,1]]
 * 解释：
 * nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0 。
 * nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0 。
 * nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0 。
 * 不同的三元组是 [-1,0,1] 和 [-1,-1,2] 。
 * 注意，输出的顺序和三元组的顺序并不重要。
 * 示例 2：
 * 输入：nums = [0,1,1]
 * 输出：[]
 * 解释：唯一可能的三元组和不为 0 。
 * 示例 3：
 * 输入：nums = [0,0,0]
 * 输出：[[0,0,0]]
 * 解释：唯一可能的三元组和为 0 。
 * 提示：
 * 3 <= nums.length <= 3000
 * -105 <= nums[i] <= 105
 * <p>
 * 链接：https://leetcode.cn/problems/3sum
 */
public class A15ThreeSum {

    /**
     * 这是初版自己实现的，三重循环，时间复杂度太高了
     * 复杂度O(n^3)
     */
    public List<List<Integer>> threeSum(int[] nums) {
        // 先排序是对的，且需要保证对于每一重循环而言，相邻两次枚举的元素不能相同，否则也会造成重复
        Arrays.sort(nums);

        List<List<Integer>> result = new ArrayList<>();
        int len = nums.length;

        for (int i = 0; i <= len - 3; i++) {
            // 相邻两次枚举的元素不能相同
            if ((i > 0) && (nums[i - 1] == nums[i])) {
                continue;
            }
            for (int j = i + 1; j <= len - 2; j++) {
                if ((j > i + 1) && (nums[j - 1] == nums[j])) {
                    continue;
                }
                // todo 这里可以发现，当a和b确定确定后，只有唯一一个c能满足a+b+c=0，所以当b往右移动时，c只需要向左移动就行，且bc相遇时循环结束，这样就等于少了一次循环
                // todo 这其实就是双指针的解决方案，当两个值相互有制约关系时，可以用双指针，减少遍历次数，这样一般可以减少一次循环
                for (int k = j + 1; k <= len - 1; k++) {
                    if ((k > j + 1) && (nums[k - 1] == nums[k])) {
                        continue;
                    }

                    if (nums[i] + nums[j] + nums[k] == 0) {
                        List<Integer> list = new ArrayList<>();
                        list.add(nums[i]);
                        list.add(nums[j]);
                        list.add(nums[k]);
                        result.add(list);
                        // todo 当这里c找到了，就可以跳出本次循环了,所以可以加一个break
                        // break;
                    }
                }
            }
        }

        return result;
    }


    /**
     * 思想 双指针法
     * <p>
     * 复杂度O(n^2)
     * 用双指针思想做优化，可减少一次循环
     */
    public List<List<Integer>> threeSum1(int[] nums) {
        Arrays.sort(nums);

        List<List<Integer>> result = new ArrayList<>();
        int len = nums.length;

        for (int i = 0; i <= len - 3; i++) {
            if ((i > 0) && (nums[i - 1] == nums[i])) {
                continue;
            }

            // 用来缓存上一个找到的c的位置
            int c = len - 1;
            // j <= c-1 因为b一定在c的左边
            for (int j = i + 1; j <= c - 1; j++) {
                if ((j > i + 1) && (nums[j - 1] == nums[j])) {
                    continue;
                }
                // k > j 因为b一定在c的左边
                for (int k = c; k > j; k--) {
                    // todo 这里用break降低循环次数，但其实可以用nums[i] + nums[j] + nums[k] < 0来终止，因为找不到满足条件的c时，当a+b+c<0其实就可以寻找下一个b了
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        List<Integer> list = new ArrayList<>();
                        list.add(nums[i]);
                        list.add(nums[j]);
                        list.add(nums[k]);
                        result.add(list);
                        c = k;
                        break;
                    }
                }
            }
        }

        return result;
    }


    /**
     * 复杂度O(n^2)
     */
    public List<List<Integer>> threeSum2(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        int len = nums.length;

        for (int i = 0; i <= len - 3; i++) {
            if ((i > 0) && (nums[i - 1] == nums[i])) {
                continue;
            }
            // 用来缓存上一个找到的c的位置
            int k = len - 1;
            // j <= len - 1 因为b一定在c的左边
            for (int j = i + 1; j <= len - 2; j++) {
                if ((j > i + 1) && (nums[j - 1] == nums[j])) {
                    continue;
                }
                // 这里直接用 >0 而不是>=0，是因为只要找到第一个=0的就行了
                while (k > j && nums[i] + nums[j] + nums[k] > 0) {
                    k--;
                }

                // 指针重合，直接走下一个i，所以用break而不是continew
                if (k == j) {
                    break;
                }

                // 满足条件，加到结果集，并走下一个j
                if (nums[i] + nums[j] + nums[k] == 0) {
                    List<Integer> list = new ArrayList<>();
                    list.add(nums[i]);
                    list.add(nums[j]);
                    list.add(nums[k]);
                    result.add(list);
                    continue;
                }
            }
        }
        return result;
    }

    /**
     * leetcode官方实现
     *
     * @return
     */
    public List<List<Integer>> threeSum3(int[] nums) {
        int n = nums.length;
        Arrays.sort(nums);
        List<List<Integer>> ans = new ArrayList<>();
        // 枚举 a
        for (int first = 0; first < n; ++first) {
            // 需要和上一次枚举的数不相同
            if (first > 0 && nums[first] == nums[first - 1]) {
                continue;
            }
            // c 对应的指针初始指向数组的最右端
            int third = n - 1;
            int target = -nums[first];
            // 枚举 b
            for (int second = first + 1; second < n; ++second) {
                // 需要和上一次枚举的数不相同
                if (second > first + 1 && nums[second] == nums[second - 1]) {
                    continue;
                }
                // 需要保证 b 的指针在 c 的指针的左侧
                while (second < third && nums[second] + nums[third] > target) {
                    --third;
                }
                // 如果指针重合，随着 b 后续的增加
                // 就不会有满足 a+b+c=0 并且 b<c 的 c 了，可以退出循环
                if (second == third) {
                    break;
                }
                if (nums[second] + nums[third] == target) {
                    List<Integer> list = new ArrayList<Integer>();
                    list.add(nums[first]);
                    list.add(nums[second]);
                    list.add(nums[third]);
                    ans.add(list);
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        A15ThreeSum threeSum = new A15ThreeSum();
        int[] nums = {-1, 0, 1, 2, -1, -4};
//        int[] nums = {0, 0, 0, 0};
//        int[] nums = {-2, 0, 1, 1, 2};
        List<List<Integer>> result = threeSum.threeSum2(nums);
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.get(i).size(); j++) {
                System.out.print(result.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }

}
