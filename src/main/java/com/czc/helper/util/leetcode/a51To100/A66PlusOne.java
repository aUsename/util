package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/10/29
 *
 * <p>
 * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
 * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。
 * 示例 1：
 * 输入：digits = [1,2,3]
 * 输出：[1,2,4]
 * 解释：输入数组表示数字 123。
 * 示例 2：
 * 输入：digits = [4,3,2,1]
 * 输出：[4,3,2,2]
 * 解释：输入数组表示数字 4321。
 * 示例 3：
 * 输入：digits = [0]
 * 输出：[1]
 * 提示：
 * 1 <= digits.length <= 100
 * 0 <= digits[i] <= 9
 * <p>
 * 链接：https://leetcode.cn/problems/plus-one
 * 加一
 */
public class A66PlusOne {
    /**
     * 自己实现，暴力法
     * 官方使用的"找出最长的后缀9"方法会简单很多：后面的9全部设为0，非9位+1即可
     */
    public int[] plusOne(int[] digits) {
        int pd = 0;
        int temp;
        for (int i = digits.length - 1; i >= 0; i--) {
            if (i == digits.length - 1) {
                temp = digits[i] + 1 + pd;
            } else {
                temp = digits[i] + pd;
            }
            digits[i] = temp % 10;
            pd = temp / 10;
        }

        if (pd == 0) {
            return digits;
        } else {
            int[] result = new int[digits.length + 1];
            result[0] = 1;
            for (int i = 0; i < digits.length; i++) {
                result[i + 1] = digits[0];
            }
            return result;
        }
    }
}
