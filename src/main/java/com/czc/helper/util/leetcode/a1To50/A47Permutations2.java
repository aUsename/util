package com.czc.helper.util.leetcode;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/10/21
 *
 * <p>
 * 给定一个可包含重复数字的序列 nums ，按任意顺序 返回所有不重复的全排列。
 * 示例 1：
 * 输入：nums = [1,1,2]
 * 输出：
 * [[1,1,2],
 * [1,2,1],
 * [2,1,1]]
 * 示例 2：
 * 输入：nums = [1,2,3]
 * 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 * 提示：
 * 1 <= nums.length <= 8
 * -10 <= nums[i] <= 10
 * <p>
 * 链接：https://leetcode.cn/problems/permutations-ii
 * 全排列 II
 */
public class A47Permutations2 {

    private List<List<Integer>> res = new ArrayList<>();
    private int len;
    private List<Integer> input = new ArrayList<>();
    private List<Integer> output = new ArrayList<>();


    public List<List<Integer>> permuteUnique(int[] nums) {
        Arrays.sort(nums);

        for (int num : nums) {
            input.add(num);
            output.add(num);
        }

        len = nums.length;

        backtrack(nums, 0);
        return res;
    }

    public void backtrack(int[] nums, int first) {
        // 所有数都填完了
        if (first == len) {
            res.add(new ArrayList<>(output));
            return;
        }

        for (int i = first; i < len; i++) {

            if (i != first && (input.get(i).equals(input.get(i - 1)))) {
                continue;
            }

            int temp = input.get(i);

            output.set(first, temp);
            input.remove(i);
            input.add(0, temp);

            // 继续递归填下一个数
            backtrack(nums, first + 1);

            input.remove(0);
            input.add(i, temp);
        }
    }

    boolean[] vis;

    /**
     * leetcode官方实现，使用vis保存元素是否被访问过
     */
    public List<List<Integer>> permuteUnique1(int[] nums) {
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        List<Integer> perm = new ArrayList<Integer>();
        vis = new boolean[nums.length];
        Arrays.sort(nums);
        backtrack1(nums, ans, 0, perm);
        return ans;
    }

    public void backtrack1(int[] nums, List<List<Integer>> ans, int idx, List<Integer> perm) {
        if (idx == nums.length) {
            ans.add(new ArrayList<Integer>(perm));
            return;
        }
        for (int i = 0; i < nums.length; ++i) {
            if (vis[i] || (i > 0 && nums[i] == nums[i - 1] && !vis[i - 1])) {
                continue;
            }
            perm.add(nums[i]);
            vis[i] = true;
            backtrack1(nums, ans, idx + 1, perm);
            vis[i] = false;
            perm.remove(idx);
        }
    }


    public static void main(String[] args) {
        A47Permutations2 permutations = new A47Permutations2();
        int[] nums = {2, 2, 1, 1};
        List<List<Integer>> result = permutations.permuteUnique(nums);
        for (List<Integer> list : result) {
            for (Integer item : list) {
                System.out.print(item + " ");
            }
            System.out.println();
        }
    }
}
