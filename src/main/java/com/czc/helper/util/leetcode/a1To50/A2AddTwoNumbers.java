package com.czc.helper.util.leetcode.a1To50;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/9/6
 * <p>
 * <p>
 * 给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 * 你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
 * 示例 1：
 * 输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 * 示例 2：
 * 输入：l1 = [0], l2 = [0]
 * 输出：[0]
 * 示例 3：
 * 输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * 输出：[8,9,9,9,0,0,0,1]
 * 提示：
 * 每个链表中的节点数在范围 [1, 100] 内
 * 0 <= Node.val <= 9
 * 题目数据保证列表表示的数字不含前导零
 * <p>
 * 链接：https://leetcode.cn/problems/add-two-numbers
 */
public class A2AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode header = null;
        ListNode current = null;
        // 进位
        int l3 = 0;
        while (l1 != null && l2 != null) {
            // 计算
            int sum = l1.val + l2.val + l3;

            // 处理进位
            if (sum >= 10) {
                l3 = 1;
                sum = sum - 10;
            } else {
                l3 = 0;
            }

            // todo 这里可以不直接用header，而是在header前面加一个前置节点，在前置节点不填数据，返回的时候直接返回前置节点.next
            if (header == null) {
                header = new ListNode(sum);
                current = header;
            } else {
                current.next = new ListNode(sum);
                current = current.next;
            }

            l1 = l1.next;
            l2 = l2.next;
        }

        // todo 这里处理了其中一个链表更长一点，但其实可以在上面的while循环里处理，将&&换成||，然后节点为null的时候，将val当作0
        while (l1 != null) {
            int sum = l1.val + l3;

            if (sum >= 10) {
                l3 = 1;
                sum = sum - 10;
            } else {
                l3 = 0;
            }
            current.next = new ListNode(sum);
            current = current.next;

            l1 = l1.next;
        }

        while (l2 != null) {
            int sum = l2.val + l3;

            if (sum >= 10) {
                l3 = 1;
                sum = sum - 10;
            } else {
                l3 = 0;
            }
            current.next = new ListNode(sum);
            current = current.next;

            l2 = l2.next;
        }

        if (l3 > 0) {
            current.next = new ListNode(l3);
        }

        return header;
    }

}

