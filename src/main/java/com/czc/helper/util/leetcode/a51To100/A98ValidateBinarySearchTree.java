package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/9/21
 * <p>
 * 给你一个二叉树的根节点 root ，判断其是否是一个有效的二叉搜索树。
 * 有效 二叉搜索树定义如下：
 * 1.节点的左子树只包含 小于 当前节点的数。
 * 2.节点的右子树只包含 大于 当前节点的数。
 * 3.所有左子树和右子树自身必须也是二叉搜索树。
 * 示例 1：
 * 输入：root = [2,1,3]
 * 输出：true
 * 示例 2：
 * 输入：root = [5,1,4,null,null,3,6]
 * 输出：false
 * 解释：根节点的值是 5 ，但是右子节点的值是 4 。
 * 提示：
 * 树中节点数目范围在[1, 104] 内
 * -2^31 <= Node.val <= 2^31 - 1
 * <p>
 * 链接：https://leetcode.cn/problems/validate-binary-search-tree
 * 验证二叉搜索树
 */
public class A98ValidateBinarySearchTree {
    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, null, null);
    }

    private boolean isValidBST(TreeNode root, Integer min, Integer max) {
        if (root == null) return true;
        if (root.left != null) {
            // 左子节点不能大于等于父节点
            if (root.left.val >= root.val) {
                return false;
                // 左子节点有最小限制时，需要判断
            } else if (min != null && root.left.val <= min) {
                return false;
            }
        }
        if (root.right != null) {
            // 右子节点不能小于等于父节点
            if (root.right.val <= root.val) {
                return false;
                // 右子节点有最大限制时，需要判断
            } else if (max != null && root.right.val >= max) {
                return false;
            }
        }

        // 左子树所有节点不能大于等于当前节点，右子树所有节点不能小于等于当前节点
        return isValidBST(root.left, min, root.val) && isValidBST(root.right, root.val, max);
    }
}
