package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/27
 *
 * <p>
 * 给你一个 无重叠的 ，按照区间起始端点排序的区间列表。
 * 在列表中插入一个新的区间，你需要确保列表中的区间仍然有序且不重叠（如果有必要的话，可以合并区间）。
 * 示例 1：
 * 输入：intervals = [[1,3],[6,9]], newInterval = [2,5]
 * 输出：[[1,5],[6,9]]
 * 示例 2：
 * 输入：intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
 * 输出：[[1,2],[3,10],[12,16]]
 * 解释：这是因为新的区间 [4,8] 与 [3,5],[6,7],[8,10] 重叠。
 * 示例 3：
 * 输入：intervals = [], newInterval = [5,7]
 * 输出：[[5,7]]
 * 示例 4：
 * 输入：intervals = [[1,5]], newInterval = [2,3]
 * 输出：[[1,5]]
 * 示例 5：
 * 输入：intervals = [[1,5]], newInterval = [2,7]
 * 输出：[[1,7]]
 * 提示：
 * 0 <= intervals.length <= 10^4
 * intervals[i].length == 2
 * 0 <= intervals[i][0] <= intervals[i][1] <= 10^5
 * intervals 根据 intervals[i][0] 按 升序 排列
 * newInterval.length == 2
 * 0 <= newInterval[0] <= newInterval[1] <= 10^5
 * <p>
 * 链接：https://leetcode.cn/problems/insert-interval
 * 插入区间
 */
public class A57InsertInterval {
    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> list = new ArrayList<>();
        list.add(newInterval);
        for (int i = 0; i < intervals.length; i++) {
            if (intervals[i][1] < list.get(list.size() - 1)[0]) {
                // 新数组整体在末尾数组左边,插入在末尾前一个
                list.add(list.size() - 1, new int[]{intervals[i][0], intervals[i][1]});
            } else if (intervals[i][0] > list.get(list.size() - 1)[1]) {
                // 新数组整体在末尾数组右边,插入在末尾，且后续所有数组都会在右边
                list.add(new int[]{intervals[i][0], intervals[i][1]});
            } else {
                // 有重合，修改末尾数组
                list.get(list.size() - 1)[0] = Math.min(list.get(list.size() - 1)[0], intervals[i][0]);
                list.get(list.size() - 1)[1] = Math.max(list.get(list.size() - 1)[1], intervals[i][1]);
            }
        }
        return list.toArray(new int[list.size()][]);
    }
}
