package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.ListNode;
import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.ArrayList;

/**
 * Created by cuizhongcheng on 2022/11/17
 * <p>
 * 给定一个单链表的头节点  head ，其中的元素 按升序排序 ，将其转换为高度平衡的二叉搜索树。
 * 本题中，一个高度平衡二叉树是指一个二叉树每个节点 的左右两个子树的高度差不超过 1。
 * 示例 1:
 * 输入: head = [-10,-3,0,5,9]
 * 输出: [0,-3,9,-10,null,5]
 * 解释: 一个可能的答案是[0，-3,9，-10,null,5]，它表示所示的高度平衡的二叉搜索树。
 * 示例 2:
 * 输入: head = []
 * 输出: []
 * 提示:
 * head 中的节点数在[0, 2 * 10^4] 范围内
 * -10^5 <= Node.val <= 10^5
 * <p>
 * 链接：https://leetcode.cn/problems/convert-sorted-list-to-binary-search-tree
 * 有序链表转换二叉搜索树
 */
public class A109ConvertSortedListToBinarySearchTree {
    public TreeNode sortedListToBST(ListNode head) {
        ArrayList<Integer> nums = new ArrayList<>();
        while (head != null) {
            nums.add(head.val);
            head = head.next;
        }

        TreeNode H = new TreeNode();
        next(nums, true, H, 0, nums.size() - 1);
        return H.left;
    }

    private void next(ArrayList<Integer> nums, boolean left, TreeNode head, int leftIndex, int rightIndex) {
        if (leftIndex > rightIndex) {
            return;
        }
        int mid = (leftIndex + rightIndex) / 2;
        TreeNode current = new TreeNode(nums.get(mid));
        if (left) {
            head.left = current;
        } else {
            head.right = current;
        }
        next(nums, true, current, leftIndex, mid - 1);
        next(nums, false, current, mid + 1, rightIndex);
    }
}
