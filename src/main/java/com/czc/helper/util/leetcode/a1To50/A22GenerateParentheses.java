package com.czc.helper.util.leetcode.a1To50;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/12
 *
 * <p>
 * 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
 * 示例 1：
 * 输入：n = 3
 * 输出：["((()))","(()())","(())()","()(())","()()()"]
 * 示例 2：
 * 输入：n = 1
 * 输出：["()"]
 * 提示：
 * 1 <= n <= 8
 * <p>
 * 链接：https://leetcode.cn/problems/generate-parentheses
 */
public class A22GenerateParentheses {


    /**
     * 自己实现，将")"依次插入，从右往左
     */
    public List<String> generateParenthesis(int n) {
        int[] array = new int[n];
        List<String> result = new ArrayList<>();
        generate(array, n, n - 1, result, 0);
        return result;
    }

    /**
     * 需要一直保证 current < remain
     *
     * @param array
     * @param remain  还剩多少个")"需要放置
     * @param current 已经放置到的位置
     */
    private void generate(int[] array, int remain, int current, List<String> result, int is) {
        int[] newArray = Arrays.copyOf(array, array.length);

        if (is == 1) {
            newArray[current]++;
        }

        while (remain > current) {
            remain--;
            newArray[current]++;
        }
        // 放置完成
        if (remain == 0) {
            getStr(newArray, result);
            return;
        }

        // current位置放置一个
        generate(newArray, remain - 1, current, result, 1);
        // current位置不放，直接左移
        generate(newArray, remain, current - 1, result, 0);
    }

    private void getStr(int[] array, List<String> result) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            builder.append("(");
            for (int j = 0; j < array[i]; j++) {
                builder.append(")");
            }
        }
        result.add(builder.toString());
//        System.out.println(builder.toString());
    }


    /**
     * leetcode实现
     */
    public List<String> generateParenthesis1(int n) {
        if (n <= 0) {
            return res;
        }
        getParenthesis("", n, n);
        return res;
    }

    List<String> res = new ArrayList<>();
    private void getParenthesis(String str, int left, int right) {
        if (left == 0 && right == 0) {
            res.add(str);
            return;
        }
        if (left == right) {
            //剩余左右括号数相等，下一个只能用左括号
            getParenthesis(str + "(", left - 1, right);
        } else if (left < right) {
            //剩余左括号小于右括号，下一个可以用左括号也可以用右括号
            if (left > 0) {
                getParenthesis(str + "(", left - 1, right);
            }
            getParenthesis(str + ")", left, right - 1);
        }
    }

    public static void main(String[] args) {
        A22GenerateParentheses generateParentheses = new A22GenerateParentheses();
        generateParentheses.generateParenthesis(1);
    }

}
