package com.czc.helper.util.leetcode.a1To50;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/10/12
 * <p>
 * 给你一个链表，两两交换其中相邻的节点，并返回交换后链表的头节点。你必须在不修改节点内部的值的情况下完成本题（即，只能进行节点交换）。
 * 示例 1：
 * 输入：head = [1,2,3,4]
 * 输出：[2,1,4,3]
 * 示例 2：
 * 输入：head = []
 * 输出：[]
 * 示例 3：
 * 输入：head = [1]
 * 输出：[1]
 * 提示：
 * 链表中节点的数目在范围 [0, 100] 内
 * 0 <= Node.val <= 100
 * <p>
 * 链接：https://leetcode.cn/problems/swap-nodes-in-pairs
 */
public class A24SwapNodesInPairs {
    /**
     * 自己实现
     * todo 挺经典的链表题目，可以多复习，同时下一题A25ReverseNodesInKGroup是进阶题
     */
    public ListNode swapPairs(ListNode head) {
        // 记录返回结果的头节点，需要初始化为head的头节点
        ListNode result = new ListNode(0, head);
        // 记录当前节点的前置节点
        ListNode currentHead = null;
        ListNode current = head;
        ListNode temp;
        while (current != null && current.next != null) {
            // 只执行一次，记录返回结果的头节点
            if (result.next == head) {
                result.next = current.next;
            }

            if (currentHead == null) {
                // 更新前置节点
                currentHead = head;
            } else {
                // 前置节点指向当前第二个节点
                currentHead.next = current.next;
                // 更新前置节点
                currentHead = current;
            }

            temp = current.next.next;
            current.next.next = current;
            current.next = temp;
            current = temp;
        }
        return result.next;
    }

    /**
     * leetcode实现，相同的思路，但是更好理解
     */
    public ListNode swapPairs1(ListNode head) {
        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;
        ListNode temp = dummyHead;
        while (temp.next != null && temp.next.next != null) {
            ListNode node1 = temp.next;
            ListNode node2 = temp.next.next;
            temp.next = node2;
            node1.next = node2.next;
            node2.next = node1;
            temp = node1;
        }
        return dummyHead.next;
    }
}
