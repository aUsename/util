package com.czc.helper.util.leetcode.a1To50;

import java.util.Arrays;

/**
 * Created by cuizhongcheng on 2022/10/9
 *
 * <p>
 * 给你一个长度为 n 的整数数组 nums 和 一个目标值 target。请你从 nums 中选出三个整数，使它们的和与 target 最接近。
 * 返回这三个数的和。
 * 假定每组输入只存在恰好一个解。
 * 示例 1：
 * 输入：nums = [-1,2,1,-4], target = 1
 * 输出：2
 * 解释：与 target 最接近的和是 2 (-1 + 2 + 1 = 2) 。
 * 示例 2：
 * 输入：nums = [0,0,0], target = 1
 * 输出：0
 * 提示：
 * 3 <= nums.length <= 1000
 * -1000 <= nums[i] <= 1000
 * -10^4 <= target <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/3sum-closest
 */
public class A16ThreeSumClosest {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int result = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < nums.length - 2; i++) {
            int left = i + 1;
            int right = nums.length - 1;

            while (left < right) {
                if (nums[i] + nums[left] + nums[right] == target) {
                    return target;
                } else if (nums[i] + nums[left] + nums[right] > target) {
                    while (left < right && nums[i] + nums[left] + nums[right] > target) {
                        result = minResult(result, target, i, left, right, nums);
                        right--;
                    }
                } else {
                    while (left < right && nums[i] + nums[left] + nums[right] < target) {
                        result = minResult(result, target, i, left, right, nums);
                        left++;
                    }
                }
            }
        }

        return result;
    }

    private int minResult(int result, int target, int num1, int num2, int num3, int[] nums) {
        int re1 = target - result;
        if (re1 < 0) {
            re1 = -re1;
        }
        int re2 = target - nums[num1] - nums[num2] - nums[num3];
        if (re2 < 0) {
            re2 = -re2;
        }

        return re1 <= re2 ? result : nums[num1] + nums[num2] + nums[num3];
    }

    public static void main(String[] args) {
        A16ThreeSumClosest sumClosest = new A16ThreeSumClosest();
        int[] nums = {1, 1, 1, 0};
        System.out.println(sumClosest.threeSumClosest(nums, -100));
    }

}
