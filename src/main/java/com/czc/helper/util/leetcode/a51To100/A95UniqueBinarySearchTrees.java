package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/11/10
 * <p>
 * 给你一个整数 n ，请你生成并返回所有由 n 个节点组成且节点值从 1 到 n 互不相同的不同 二叉搜索树 。可以按 任意顺序 返回答案。
 * 示例 1：
 * 输入：n = 3
 * 输出：[[1,null,2,null,3],[1,null,3,2],[2,1,3],[3,1,null,null,2],[3,2,null,1]]
 * 示例 2：
 * 输入：n = 1
 * 输出：[[1]]
 * 提示：
 * 1 <= n <= 8
 * <p>
 * 链接：https://leetcode.cn/problems/unique-binary-search-trees-ii
 * 不同的二叉搜索树 II
 */
public class A95UniqueBinarySearchTrees {

    private List<TreeNode> result = new ArrayList<>();
    private int[] arr;
    private Map<Integer, TreeNode> treeNodeMap = new HashMap<>();
    private Deque<TreeNode> stack = new LinkedList<>();

    /**
     * 二叉搜索树 左>中>右
     */
    public List<TreeNode> generateTrees(int n) {
        arr = new int[n];
        arr[0] = 1;
        Deque<Integer> deque = new ArrayDeque<>();
        deque.addLast(2);
        deque.addLast(3);
        next(n, 1, deque);
        return result;
    }

    private void next(int n, int index, Deque<Integer> deque) {

        // 构造树
        if (index == n) {
            addResult(n);
            return;
        }

        int current = deque.pollFirst();

        // 当前值加入树中
        arr[index] = current;

        deque.addLast(current * 2);
        deque.addLast(current * 2 + 1);
        // 下一位置
        next(n, index + 1, deque);
        // 还原
        deque.pollLast();
        deque.pollLast();


        // 当前值不加入树中
        if (!deque.isEmpty()) {
            next(n, index, deque);
        }
        deque.addFirst(current);
    }


    private void addResult(int n) {
        // 构造树
        treeNodeMap.clear();
        TreeNode head = new TreeNode();
        treeNodeMap.put(1, head);
        for (int i = 1; i < n; i++) {

            TreeNode temp = new TreeNode();
            treeNodeMap.put(arr[i], temp);

            int pre = arr[i] / 2;
            if (arr[i] % 2 == 1) {
                treeNodeMap.get(pre).right = temp;
            } else {
                treeNodeMap.get(pre).left = temp;
            }
        }

        // 填值，中序遍历
        int index = 1;
        TreeNode temp = head;
        while (temp != null || !stack.isEmpty()) {
            if (temp != null) {
                stack.push(temp);
                temp = temp.left;
            } else {
                temp = stack.pop();
                temp.val = index;
                index++;
                temp = temp.right;
            }
        }

        result.add(head);
    }


    public static void main(String[] args) {
        A95UniqueBinarySearchTrees binarySearchTrees = new A95UniqueBinarySearchTrees();
        binarySearchTrees.generateTrees(8);
    }
}
