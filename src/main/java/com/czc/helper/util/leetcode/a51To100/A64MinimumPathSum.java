package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/9/26
 *
 * <p>
 * 给定一个包含非负整数的 m x n 网格 grid ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
 * 说明：每次只能向下或者向右移动一步。
 * 示例 1：
 * 输入：grid = [[1,3,1],[1,5,1],[4,2,1]]
 * 输出：7
 * 解释：因为路径 1→3→1→1→1 的总和最小。
 * 示例 2：
 * 输入：grid = [[1,2,3],[4,5,6]]
 * 输出：12
 * 提示：
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 200
 * 0 <= grid[i][j] <= 100
 * <p>
 * 链接：https://leetcode.cn/problems/minimum-path-sum
 */
public class A64MinimumPathSum {

    /**
     * 自己实现，动态规划
     * todo 由于需要m + n - 1步，所以使用了(m + n - 1,n)的数组，其实(m,n)的就够了，实际还是对动态规划的状态转移方程没理解好，导致最后代码难编码和难理解很多
     */
    public int minPathSum(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;

        // 总共需要处理m + n - 1步
        int[][] space = new int[m + n - 1][n];
        for (int i = 0; i < m + n - 1; i++) {
            for (int j = 0; j < n; j++) {
                space[i][j] = -1;
            }
        }


        space[0][0] = grid[0][0];
        for (int i = 0; i < m + n - 2; i++) {
            // 遍历grid的 (i-j,j)右边和下边的位置
            for (int j = 0; j < n; j++) {

                // 当前位置有值，往右或往下走
                if (space[i][j] >= 0) {
                    // 未到最右边，可以往右走
                    if (j < n - 1) {
                        int temp = space[i][j] + grid[i - j][j + 1];
                        if (space[i + 1][j + 1] < 0) {
                            space[i + 1][j + 1] = temp;
                        } else {
                            space[i + 1][j + 1] = Math.min(space[i + 1][j + 1], temp);
                        }
                    }
                    // 未到最下边，可以往下走
                    if (i - j < m - 1) {
                        int temp = space[i][j] + grid[i - j + 1][j];
                        if (space[i + 1][j] < 0) {
                            space[i + 1][j] = temp;
                        } else {
                            space[i + 1][j] = Math.min(space[i + 1][j], temp);
                        }
                    }


                }

            }
        }

        return space[m + n - 2][n - 1];
    }


    /**
     * leetcode实现，使用（m,n）的数组，每个位置表示到达此位置的最小长度 dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
     */
    public int minPathSum1(int[][] grid) {
        // 输入检查
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int rows = grid.length, columns = grid[0].length;
        int[][] dp = new int[rows][columns];
        // 初始化一些边界条件
        dp[0][0] = grid[0][0];
        for (int i = 1; i < rows; i++) {
            dp[i][0] = dp[i - 1][0] + grid[i][0];
        }
        for (int j = 1; j < columns; j++) {
            dp[0][j] = dp[0][j - 1] + grid[0][j];
        }
        // 状态转移方程
        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < columns; j++) {
                dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
            }
        }
        return dp[rows - 1][columns - 1];
    }

    public static void main(String[] args) {
        A64MinimumPathSum sum = new A64MinimumPathSum();
        int[][] grid = {{1, 3, 1,}, {1, 5, 1}, {4, 2, 1}};
        System.out.println(sum.minPathSum(grid));
    }
}
