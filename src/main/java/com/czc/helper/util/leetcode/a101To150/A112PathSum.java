package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/9/21
 *
 * <p>
 * 给你二叉树的根节点 root 和一个表示目标和的整数 targetSum 。判断该树中是否存在 根节点到叶子节点 的路径，这条路径上所有节点值相加等于目标和 targetSum 。如果存在，返回 true ；否则，返回 false 。
 * 叶子节点 是指没有子节点的节点。
 * 示例 1：
 * 输入：root = [5,4,8,11,null,13,4,7,2,null,null,null,1], targetSum = 22
 * 输出：true
 * 解释：等于目标和的根节点到叶节点路径如上图所示。
 * 示例 2：
 * 输入：root = [1,2,3], targetSum = 5
 * 输出：false
 * 解释：树中存在两条根节点到叶子节点的路径：
 * (1 --> 2): 和为 3
 * (1 --> 3): 和为 4
 * 不存在 sum = 5 的根节点到叶子节点的路径。
 * 示例 3：
 * 输入：root = [], targetSum = 0
 * 输出：false
 * 解释：由于树是空的，所以不存在根节点到叶子节点的路径。
 * 提示：
 * 树中节点的数目在范围 [0, 5000] 内
 * -1000 <= Node.val <= 1000
 * -1000 <= targetSum <= 1000
 * <p>
 * 链接：https://leetcode.cn/problems/path-sum
 */
public class A112PathSum {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) return false;
        return hasPathSum(root, targetSum, 0);
    }

    private boolean hasPathSum(TreeNode root, int targetSum, int currentSum) {
        // 左子树存在，右子树不存在，选左子树
        if (root.left != null && root.right == null) {
            return hasPathSum(root.left, targetSum, currentSum + root.val);
        }
        // 右子树存在，左子树不存在，选右子树
        if (root.left == null && root.right != null) {
            return hasPathSum(root.right, targetSum, currentSum + root.val);
        }
        // 左右子树都存在
        if (root.left != null && root.right != null) {
            return hasPathSum(root.left, targetSum, currentSum + root.val)
                    || hasPathSum(root.right, targetSum, currentSum + root.val);
        }
        return targetSum == currentSum + root.val;
    }

    /**
     * leetcode实现，判断是叶子节点根据左右子树都为null，且通过targetSum- root.val判断，不需要一直透传targetSum
     */
    public boolean hasPathSum1(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        if (root.left == null && root.right == null) {
            return sum == root.val;
        }
        return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
    }
}
