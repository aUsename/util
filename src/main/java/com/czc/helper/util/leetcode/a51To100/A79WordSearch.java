package com.czc.helper.util.leetcode.a51To100;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/11/2
 * <p>
 * 给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。
 * 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。
 * 示例 1：
 * 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
 * 输出：true
 * 示例 2：
 * 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
 * 输出：true
 * 示例 3：
 * 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
 * 输出：false
 * 提示：
 * m == board.length
 * n = board[i].length
 * 1 <= m, n <= 6
 * 1 <= word.length <= 15
 * board 和 word 仅由大小写英文字母组成
 * <p>
 * 链接：https://leetcode.cn/problems/word-search
 * 单词搜索
 */
public class A79WordSearch {

    private Map<Integer, HashSet<Integer>> map = new HashMap<>();
    private int m, n, len;

    /**
     * 自己实现，使用了Map<Integer, HashSet<Integer>> map来记录当前位置是否被访问过，但其实使用 boolean[][] 即可
     */
    public boolean exist(char[][] board, String word) {
        m = board.length;
        n = board[0].length;
        len = word.length();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // 第一个字符匹配，往下继续判断
                if (board[i][j] == word.charAt(0)) {
                    put(i, j);
                    if (match(board, word, i, j, 1)) {
                        return true;
                    }
                    delete(i, j);
                }
            }
        }
        return false;
    }

    private boolean match(char[][] board, String word, int x, int y, int index) {
        if (index == len) {
            return true;
        }

        if (y + 1 < n && board[x][y + 1] == word.charAt(index) && !contain(x, y + 1)) {
            put(x, y + 1);
            boolean temp = match(board, word, x, y + 1, index + 1);
            if (temp) return true;
            delete(x, y + 1);
        }
        if (x + 1 < m && board[x + 1][y] == word.charAt(index) && !contain(x + 1, y)) {
            put(x + 1, y);
            boolean temp = match(board, word, x + 1, y, index + 1);
            if (temp) return true;
            delete(x + 1, y);
        }
        if (y - 1 >= 0 && board[x][y - 1] == word.charAt(index) && !contain(x, y - 1)) {
            put(x, y - 1);
            boolean temp = match(board, word, x, y - 1, index + 1);
            if (temp) return true;
            delete(x, y - 1);
        }
        if (x - 1 >= 0 && board[x - 1][y] == word.charAt(index) && !contain(x - 1, y)) {
            put(x - 1, y);
            boolean temp = match(board, word, x - 1, y, index + 1);
            if (temp) return true;
            delete(x - 1, y);
        }

        return false;
    }

    private boolean contain(int x, int y) {
        if (map.containsKey(x)) {
            return map.get(x).contains(y);
        } else {
            return false;
        }
    }

    private void put(int x, int y) {
        if (map.containsKey(x)) {
            map.get(x).add(y);
        } else {
            HashSet<Integer> set = new HashSet<>();
            set.add(y);
            map.put(x, set);
        }
    }

    private void delete(int x, int y) {
        map.get(x).remove(y);
    }
}
