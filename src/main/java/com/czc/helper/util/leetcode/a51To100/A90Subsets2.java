package com.czc.helper.util.leetcode.a51To100;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/11/7
 *
 * <p>
 * 给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。
 * 解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
 * 示例 1：
 * 输入：nums = [1,2,2]
 * 输出：[[],[1],[1,2],[1,2,2],[2],[2,2]]
 * 示例 2：
 * 输入：nums = [0]
 * 输出：[[],[0]]
 * 提示：
 * 1 <= nums.length <= 10
 * -10 <= nums[i] <= 10
 * <p>
 * 链接：https://leetcode.cn/problems/subsets-ii
 * 子集 II
 */
public class A90Subsets2 {

    private List<List<Integer>> result = new ArrayList<>();
    private Map<Integer, Integer> map = new HashMap<>();
    private int len;

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        len = nums.length;

        for (int i = 0; i < len; i++) {
            int count = 1;
            int key = i;
            while (i < len - 1 && nums[i] == nums[i + 1]) {
                count++;
                i++;
            }
            map.put(key, count);
        }


        next(nums, 0, new ArrayList<>());
        return result;
    }

    private void next(int[] nums, int point, List<Integer> current) {
        if (point == len) {
            List<Integer> list = new ArrayList<>(current);
            result.add(list);
            return;
        }

        int time = map.get(point);

        for (int i = 0; i <= time; i++) {
            for (int j = 0; j < i; j++) {
                current.add(nums[point]);
            }
            next(nums, point + time, current);
            for (int j = 0; j < i; j++) {
                current.remove(current.size() - 1);
            }
        }
    }

    public static void main(String[] args) {
        A90Subsets2 subsets2 = new A90Subsets2();
        subsets2.subsetsWithDup(new int[]{0});
    }
}
