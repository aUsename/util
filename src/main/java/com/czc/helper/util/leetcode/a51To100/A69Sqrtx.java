package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/9/20
 *
 * <p>
 * 给你一个非负整数 x ，计算并返回 x 的 算术平方根 。
 * 由于返回类型是整数，结果只保留 整数部分 ，小数部分将被 舍去 。
 * 注意：不允许使用任何内置指数函数和算符，例如 pow(x, 0.5) 或者 x ** 0.5 。
 * 示例 1：
 * 输入：x = 4
 * 输出：2
 * 示例 2：
 * 输入：x = 8
 * 输出：2
 * 解释：8 的算术平方根是 2.82842..., 由于返回类型是整数，小数部分将被舍去。
 * 提示：
 * 0 <= x <= 2^31 - 1
 * <p>
 * 链接：https://leetcode.cn/problems/sqrtx
 * x 的平方根
 */
public class A69Sqrtx {

    public int mySqrt(int x) {
        if (x == 1) {
            return 1;
        }
        return sqrt(x, 0, x);
    }

    private int sqrt(int x, int left, int right) {
        if (right - left <= 1) {
            return left;
        }
        int mid = (left + right) / 2;
        if (mid * mid == x) {
            return mid;
            // 这里要使用long，因为可能会越界
        } else if ((long) mid * mid > x) {
            return sqrt(x, left, mid);
        } else {
            return sqrt(x, mid, right);
        }
    }


}
