package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by cuizhongcheng on 2022/11/3
 * <p>
 * 给定一个仅包含 0 和 1 、大小为 rows x cols 的二维二进制矩阵，找出只包含 1 的最大矩形，并返回其面积。
 * 示例 1：
 * 输入：matrix =
 * [
 * ["1","0","1","0","0"],
 * ["1","0","1","1","1"],
 * ["1","1","1","1","1"],
 * ["1","0","0","1","0"]]
 * 输出：6
 * 示例 2：
 * 输入：matrix = []
 * 输出：0
 * 示例 3：
 * 输入：matrix = [["0"]]
 * 输出：0
 * 示例 4：
 * 输入：matrix = [["1"]]
 * 输出：1
 * 示例 5：
 * 输入：matrix = [["0","0"]]
 * 输出：0
 * 提示：
 * rows == matrix.length
 * cols == matrix[0].length
 * 1 <= row, cols <= 200
 * matrix[i][j] 为 '0' 或 '1'
 * <p>
 * 链接：https://leetcode.cn/problems/maximal-rectangle
 * 最大矩形
 */
public class A85MaximalRectangle {

    /**
     * 可以变形为，对二维数组的每个位置，求出以当前位置(x,y)为矩阵左下角的最大矩阵面积，算法同A84LargestRectangleInHistogram，使用单调栈解决
     * 所以我们只需要知道，每个位置上，1的高度即可，当前位置为0时，可以认为高度为0
     */
    public int maximalRectangle(char[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        int result = 0;
        // high存储每个位置向上连续1的个数（包括自己）
        int[][] high = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    high[i][j] = 1 + (i == 0 ? 0 : high[i - 1][j]);
                }
            }
        }

        // 遍历每一行
        for (int i = 0; i < m; i++) {
            int[] left = new int[n];
            int[] right = new int[n];

            Deque<Integer> stack = new ArrayDeque<>();
            for (int j = 0; j < n; j++) {
                while (!stack.isEmpty() && high[i][stack.peek()] >= high[i][j]) {
                    stack.pop();
                }
                left[j] = stack.isEmpty() ? -1 : stack.peek();
                stack.push(j);
            }

            stack.clear();
            for (int j = n - 1; j >= 0; j--) {
                while (!stack.isEmpty() && high[i][stack.peek()] >= high[i][j]) {
                    stack.pop();
                }
                right[j] = stack.isEmpty() ? n : stack.peek();
                stack.push(j);

            }
            for (int j = 0; j < n; ++j) {
                result = Math.max(result, (right[j] - left[j] - 1) * high[i][j]);
            }

        }
        return result;
    }

    public static void main(String[] args) {
        A85MaximalRectangle maximalRectangle = new A85MaximalRectangle();
        char[][] matrix = {
                {'1', '0', '1', '0', '0'},
                {'1', '0', '1', '1', '1'},
                {'1', '1', '1', '1', '1'},
                {'1', '0', '0', '1', '0'}
        };
        System.out.println(maximalRectangle.maximalRectangle(matrix));
    }
}
