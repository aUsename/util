package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/17
 * <p>
 * 给定一个二叉树，判断它是否是高度平衡的二叉树。
 * 本题中，一棵高度平衡二叉树定义为：
 * 一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。
 * 示例 1：
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：true
 * 示例 2：
 * 输入：root = [1,2,2,3,3,null,null,4,4]
 * 输出：false
 * 示例 3：
 * 输入：root = []
 * 输出：true
 * 提示：
 * 树中的节点数在范围 [0, 5000] 内
 * -10^4 <= Node.val <= 10^4
 * 链接：https://leetcode.cn/problems/balanced-binary-tree
 * 平衡二叉树
 */
public class A110BalancedBinaryTree {
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        return dfs(root);
    }

    private boolean dfs(TreeNode root) {
        if (root.left == null && root.right == null) {
            root.val = 1;
            return true;
        }
        if (root.left == null) {
            if (!dfs(root.right)) {
                return false;
            } else {
                if (root.right.val == 1) {
                    root.val = 2;
                    return true;
                } else {
                    return false;
                }
            }
        }
        if (root.right == null) {
            if (!dfs(root.left)) {
                return false;
            } else {
                if (root.left.val == 1) {
                    root.val = 2;
                    return true;
                } else {
                    return false;
                }
            }
        }

        if (
                (!dfs(root.left))
                        ||
                        (!dfs(root.right))
                ) {
            return false;
        }
        if (Math.abs(root.left.val - root.right.val) <= 1) {
            root.val = Math.max(root.left.val, root.right.val) + 1;
            return true;
        } else {
            return false;
        }

    }

    /**
     * 官方实现，不需要修改节点
     */
    public boolean isBalanced1(TreeNode root) {
        return height(root) >= 0;
    }

    public int height(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftHeight = height(root.left);
        int rightHeight = height(root.right);
        if (leftHeight == -1 || rightHeight == -1 || Math.abs(leftHeight - rightHeight) > 1) {
            return -1;
        } else {
            return Math.max(leftHeight, rightHeight) + 1;
        }
    }
}
