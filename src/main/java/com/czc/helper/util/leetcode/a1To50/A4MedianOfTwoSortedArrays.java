package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/9/6
 *
 * <p>
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 * 示例 1：
 * 输入：nums1 = [1,3], nums2 = [2]
 * 输出：2.00000
 * 解释：合并数组 = [1,2,3] ，中位数 2
 * 示例 2：
 * 输入：nums1 = [1,2], nums2 = [3,4]
 * 输出：2.50000
 * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
 * 提示：
 * nums1.length == m
 * nums2.length == n
 * 0 <= m <= 1000
 * 0 <= n <= 1000
 * 1 <= m + n <= 2000
 * -10^6 <= nums1[i], nums2[i] <= 10^6
 * <p>
 * 链接：https://leetcode.cn/problems/median-of-two-sorted-arrays
 */
public class A4MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        // 判断空数组
        int m = nums1.length, n = nums2.length;
        if (m == 0) {
            return findMid(nums2, n);
        }
        if (n == 0) {
            return findMid(nums1, m);
        }
        // 两个数组的左指针
        int left1 = 0, left2 = 0;

        // k =  (m + n + 1) / 2
        double kMid = findKSort(nums1, left1, m, nums2, left2, n, (m + n + 1) / 2);

        // 若 m+n为偶数，取第k和k+1的平均值
        // 若 m+n为奇数，取第k
        if ((m + n) % 2 == 0) {
            double k1Mid = findKSort(nums1, left1, m, nums2, left2, n, (m + n + 2) / 2);
            return (kMid + k1Mid) / 2;
        } else {
            return kMid;
        }
    }


    /**
     * @param left1 数组1的开始位置
     * @param m     m > 0
     * @param left2 数组2的开始位置
     * @param n     n > 0
     * @param k     寻找两个数组第K小元素
     * @return
     */
    private double findKSort(int[] nums1, int left1, int m, int[] nums2, int left2, int n, int k) {
        // 其中一个数组为空时，直接在另一个数组中查找
        if (left1 > m - 1) {
            left2++;
            return nums2[left2 + k - 2];
        }
        if (left2 > n - 1) {
            left1++;
            return nums1[left1 + k - 2];
        }

        // k = 1时，取两个数组的最小元素
        if (k == 1) {
            if (nums1[left1] >= nums2[left2]) {
                left2++;
                return nums2[left2 + k - 2];
            } else {
                left1++;
                return nums1[left1 + k - 2];
            }
        }

        // 准备排除的数组大小，二分法
        int reduce = k / 2;
        // 若数组剩余大小，小于准备排除的数组大小，将数组剩余大小设置为排除大小
        if (m - left1 < reduce) {
            reduce = m - left1;
        }
        if (n - left2 < reduce) {
            reduce = n - left2;
        }


        if (nums1[left1 + reduce - 1] >= nums2[left2 + reduce - 1]) {
            left2 = left2 + reduce;
        } else {
            left1 = left1 + reduce;
        }
        return findKSort(nums1, left1, m, nums2, left2, n, k - reduce);
    }

    private double findMid(int[] nums, int len) {
        // 因为 1 <= m + n 所以这个判断不需要了
        // if (l == 0) return 0;
        if (len % 2 == 1) {
            return nums[(len + 1) / 2 - 1];
        } else {
            return ((double) (nums[len / 2] + nums[(len + 1) / 2 - 1])) / 2;
        }
    }

    public static void main(String[] args) {
        A4MedianOfTwoSortedArrays arrays = new A4MedianOfTwoSortedArrays();
        int[] nums1 = {};
        int[] nums2 = {2,3};
        double result = arrays.findMedianSortedArrays(nums1, nums2);
        System.out.println(result);
    }
}
