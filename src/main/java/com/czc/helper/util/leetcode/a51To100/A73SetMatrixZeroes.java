package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/11/1
 * <p>
 * 给定一个 m x n 的矩阵，如果一个元素为 0 ，则将其所在行和列的所有元素都设为 0 。请使用 原地 算法。
 * 示例 1：
 * 输入：matrix = [[1,1,1],[1,0,1],[1,1,1]]
 * 输出：[[1,0,1],[0,0,0],[1,0,1]]
 * 示例 2：
 * 输入：matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
 * 输出：[[0,0,0,0],[0,4,5,0],[0,3,1,0]]
 * 提示：
 * m == matrix.length
 * n == matrix[0].length
 * 1 <= m, n <= 200
 * -2^31 <= matrix[i][j] <= 2^31 - 1
 * 进阶：
 * 一个直观的解决方案是使用  O(mn) 的额外空间，但这并不是一个好的解决方案。
 * 一个简单的改进方案是使用 O(m + n) 的额外空间，但这仍然不是最好的解决方案。
 * 你能想出一个仅使用常量空间的解决方案吗？
 * <p>
 * 链接：https://leetcode.cn/problems/set-matrix-zeroes
 * 矩阵置零
 */
public class A73SetMatrixZeroes {
    /**
     * 思想：标记变量
     * 自己想不出来，参考答案思路写的：
     * 若使用O(m + n)空间，记录m行n列是否有0就行了，那么不使用额外空间时，需要用第0行，第0列的(m+n)的空间
     * 用两个boolean值（标记变量），分别记录第0行，第0列是否有0
     * 之后使用（x,0)位置，记录第x行是否有0
     * 使用（0,y)位置，记录第y列是否有0
     */
    public void setZeroes(int[][] matrix) {
        // 行数
        int m = matrix.length;
        // 列数
        int n = matrix[0].length;

        // 第0行是否有0
        boolean h = false;
        // 第0列是否有0
        boolean l = false;
        // 处理第0行，共n列
        for (int i = 0; i < n; i++) {
            if (matrix[0][i] == 0) {
                h = true;
                break;
            }
        }
        // 处理第0列，共m行
        for (int i = 0; i < m; i++) {
            if (matrix[i][0] == 0) {
                l = true;
                break;
            }
        }

        // 遍历从（1，1）位置开始的子二维数组，在第0行和0列填当前行是否有0
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }

        // 从第1行开始，填每一行的0
        for (int i = 1; i < m; i++) {
            if (matrix[i][0] == 0) {
                for (int j = 1; j < n; j++) {
                    matrix[i][j] = 0;
                }
            }
        }

        // 从第1列开始，填每一列的0
        for (int j = 1; j < n; j++) {
            if (matrix[0][j] == 0) {
                for (int i = 1; i < m; i++) {
                    matrix[i][j] = 0;
                }
            }
        }

        // 处理第0行
        if (h) {
            for (int i = 0; i < n; i++) {
                matrix[0][i] = 0;
            }
        }
        // 处理第0列
        if (l) {
            for (int i = 0; i < m; i++) {
                matrix[i][0] = 0;
            }
        }

    }
}
