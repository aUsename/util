package com.czc.helper.util.leetcode.a51To100;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by cuizhongcheng on 2022/10/25
 *
 * <p>
 * n 皇后问题 研究的是如何将 n 个皇后放置在 n × n 的棋盘上，并且使皇后彼此之间不能相互攻击。
 * 给你一个整数 n ，返回 n 皇后问题 不同的解决方案的数量。
 * 示例 1：
 * 输入：n = 4
 * 输出：2
 * 解释：如上图所示，4 皇后问题存在两个不同的解法。
 * 示例 2：
 * 输入：n = 1
 * 输出：1
 * 提示：
 * 1 <= n <= 9
 * <p>
 * 链接：https://leetcode.cn/problems/n-queens-ii
 * N皇后 II
 */
public class A52NQueens2 {

    /**
     * 列
     * y
     */
    private Set<Integer> set1 = new HashSet<>();
    /**
     * 右上～左下
     * x + y
     */
    private Set<Integer> set2 = new HashSet<>();
    /**
     * 左上～右下
     * x - y
     */
    private Set<Integer> set3 = new HashSet<>();

    int result = 0;

    /**
     * 用A51NQueens中说的三个集合来实现
     */
    public int totalNQueens(int n) {
        if (n == 0) return 0;
        inputI(0, n);
        return result;
    }

    private void inputI(int i, int n) {
        if (i == n) {
            result++;
            return;
        }
        for (int j = 0; j < n; j++) {
            if (inputQ(i, j)) {
                put(i, j);
                inputI(i + 1, n);
                remove(i, j);
            }
        }
    }

    private boolean inputQ(int x, int y) {
        if (set1.contains(y)) {
            return false;
        }
        if (set2.contains(x + y)) {
            return false;
        }
        if (set3.contains(x - y)) {
            return false;
        }
        return true;
    }

    private void put(int x, int y) {
        set1.add(y);
        set2.add(x + y);
        set3.add(x - y);
    }

    private void remove(int x, int y) {
        set1.remove(y);
        set2.remove(x + y);
        set3.remove(x - y);
    }

    public static void main(String[] args) {
        A52NQueens2 nQueens2 = new A52NQueens2();
        System.out.println(nQueens2.totalNQueens(5));
    }
}
