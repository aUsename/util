package com.czc.helper.util.leetcode.a51To100;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhongcheng on 2022/9/17
 *
 * <p>
 * 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 * 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 * 示例 1：
 * 输入：n = 2
 * 输出：2
 * 解释：有两种方法可以爬到楼顶。
 * 1. 1 阶 + 1 阶
 * 2. 2 阶
 * 示例 2：
 * 输入：n = 3
 * 输出：3
 * 解释：有三种方法可以爬到楼顶。
 * 1. 1 阶 + 1 阶 + 1 阶
 * 2. 1 阶 + 2 阶
 * 3. 2 阶 + 1 阶
 * 提示：
 * 1 <= n <= 45
 * <p>
 * 链接：https://leetcode.cn/problems/climbing-stairs
 * 爬楼梯
 */
public class A70ClimbingStairs {

    private Map<Integer, Integer> map = new HashMap<>();

    public int climbStairs(int n) {
        if (map.get(n) != null) return map.get(n);

        if (n == 1) {
            return 1;
        } else if (n == 2) {
            return 2;
        } else {
            int result = climbStairs(n - 1) + climbStairs(n - 2);
            map.put(n, result);
            return result;
        }
    }
}
