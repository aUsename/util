package com.czc.helper.util.leetcode;

import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.ArrayDeque;

/**
 * Created by cuizhongcheng on 2022/9/21
 *
 * <p>
 * 给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。
 * 示例 1：
 * 输入：root = [4,2,7,1,3,6,9]
 * 输出：[4,7,2,9,6,3,1]
 * 示例 2：
 * 输入：root = [2,1,3]
 * 输出：[2,3,1]
 * 示例 3：
 * 输入：root = []
 * 输出：[]
 * 提示：
 * 树中节点数目范围在 [0, 100] 内
 * -100 <= Node.val <= 100
 * <p>
 * 链接：https://leetcode.cn/problems/invert-binary-tree
 */
public class A226InvertBinaryTree {

    /**
     * 还可以用递归来做
     */
    public TreeNode invertTree(TreeNode root) {
        if (root == null) return root;
        ArrayDeque<TreeNode> deque = new ArrayDeque<>();
        deque.add(root);
        TreeNode temp = new TreeNode();

        while (!deque.isEmpty()) {
            TreeNode currentNode = deque.pop();
            if (currentNode.left != null) {
                deque.add(currentNode.left);
            }
            if (currentNode.right != null) {
                deque.add(currentNode.right);
            }
            temp = currentNode.left;
            currentNode.left = currentNode.right;
            currentNode.right = temp;
        }

        return root;
    }
}
