package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/10/27
 *
 * <p>
 * 给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。
 * 示例 1：
 * 输入：n = 3
 * 1 2 3
 * 8 9 4
 * 7 6 5
 * 输出：[[1,2,3],[8,9,4],[7,6,5]]
 * 示例 2：
 * 输入：n = 1
 * 输出：[[1]]
 * 提示：
 * 1 <= n <= 20
 * <p>
 * 链接：https://leetcode.cn/problems/spiral-matrix-ii
 * 螺旋矩阵 II
 */
public class A59SpiralMatrix2 {
    public int[][] generateMatrix(int n) {
        int[][] result = new int[n][n];
        int out = n;
        int index = 1;
        int x = 0, y = 0;

        // 按圈填，从最外圈往里
        // 同时由于是正方形矩阵，只需要考虑圈数就行
        while (out >= 1) {
            int left = (n - out) / 2;
            int up = left;
            int right = (n + out) / 2 - 1;
            int down = right;

            // 向右
            for (; y <= right; y++) {
                result[x][y] = index;
                index++;
            }
            x++;
            y--;

            // 向下
            for (; x <= down; x++) {
                result[x][y] = index;
                index++;
            }
            y--;
            x--;

            // 向左
            for (; y >= left; y--) {
                result[x][y] = index;
                index++;
            }
            x--;
            y++;

            // 向上
            for (; x > up; x--) {
                result[x][y] = index;
                index++;
            }
            y++;
            x++;

            out -= 2;
        }

        return result;
    }

    public static void main(String[] args) {
        A59SpiralMatrix2 spiralMatrix2 = new A59SpiralMatrix2();
        spiralMatrix2.generateMatrix(3);
    }
}
