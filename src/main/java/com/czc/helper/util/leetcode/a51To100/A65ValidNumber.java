package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/10/29
 *
 * <p>
 * 有效数字（按顺序）可以分成以下几个部分：
 * 1. 一个 小数 或者 整数
 * 2.（可选）一个 'e' 或 'E' ，后面跟着一个 整数
 * <p>
 * 小数（按顺序）可以分成以下几个部分：
 * 1. （可选）一个符号字符（'+' 或 '-'）
 * 2. 下述格式之一：
 * 2.1 至少一位数字，后面跟着一个点 '.'
 * 2.2 至少一位数字，后面跟着一个点 '.' ，后面再跟着至少一位数字
 * 2.3 一个点 '.' ，后面跟着至少一位数字
 * <p>
 * 整数（按顺序）可以分成以下几个部分：
 * 1.（可选）一个符号字符（'+' 或 '-'）
 * 2. 至少一位数字
 * <p>
 * 部分有效数字列举如下：["2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10", "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"]
 * 部分无效数字列举如下：["abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53"]
 * 给你一个字符串 s ，如果 s 是一个 有效数字 ，请返回 true 。
 * 示例 1：
 * 输入：s = "0"
 * 输出：true
 * 示例 2：
 * 输入：s = "e"
 * 输出：false
 * 示例 3：
 * 输入：s = "."
 * 输出：false
 * 提示：
 * 1 <= s.length <= 20
 * s 仅含英文字母（大写和小写），数字（0-9），加号 '+' ，减号 '-' ，或者点 '.' 。
 * <p>
 * 链接：https://leetcode.cn/problems/valid-number
 * 有效数字
 */
public class A65ValidNumber {

    public boolean isNumber(String s) {
        int len = s.length();
        int eIndex = -1;
        for (int i = 0; i < len; i++) {
            if (s.charAt(i) == 'e' || s.charAt(i) == 'E') {
                eIndex = i;
                break;
            }
        }
        // 不包括e
        if (eIndex == -1) {
            return intNumber(s, 0, len) || doubleNumber(s, 0, len);
        } else {
            // 包括e
            return (intNumber(s, 0, eIndex) || doubleNumber(s, 0, eIndex)) && intNumber(s, eIndex + 1, len);
        }
    }

    // 是否是整数
    private boolean intNumber(String s, int start, int end) {
        if (start >= end) return false;
        if (s.charAt(start) == '+' || s.charAt(start) == '-') {
            start++;
        }
        if (start >= end) return false;
        while (start < end) {
            if (s.charAt(start) < '0' || s.charAt(start) > '9') {
                return false;
            }
            start++;
        }
        return true;
    }

    // 是否是小数
    private boolean doubleNumber(String s, int start, int end) {
        if (start >= end) return false;
        if (s.charAt(start) == '+' || s.charAt(start) == '-') {
            start++;
        }
        if (start >= end) return false;

        // 记录小数点前是否已经有数字了
        boolean hasNum = false;
        // 小数点之前，只能是数字
        while (start < end && s.charAt(start) != '.') {
            if (s.charAt(start) < '0' || s.charAt(start) > '9') {
                return false;
            }
            hasNum = true;
            start++;
        }
        // 未找到小数点，失败
        if (start == end) return false;
        // 此时start是小数点
        start++;
        // 若已结束，则hasNum必须为true
        if (start == end) {
            return hasNum;
        } else {
            // 未结束，此时start以及后面只能是数字
            while (start < end) {
                if (s.charAt(start) < '0' || s.charAt(start) > '9') {
                    return false;
                }
                start++;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        A65ValidNumber validNumber = new A65ValidNumber();
        System.out.println(validNumber.isNumber("4.e1"));
    }
}
