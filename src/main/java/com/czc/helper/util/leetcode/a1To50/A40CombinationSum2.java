package com.czc.helper.util.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/10/20
 *
 * <p>
 * 给定一个候选人编号的集合 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
 * candidates 中的每个数字在每个组合中只能使用 一次 。
 * 注意：解集不能包含重复的组合。
 * 示例 1:
 * 输入: candidates = [10,1,2,7,6,1,5], target = 8,
 * 输出:
 * [
 * [1,1,6],
 * [1,2,5],
 * [1,7],
 * [2,6]
 * ]
 * 示例 2:
 * 输入: candidates = [2,5,2,1,2], target = 5,
 * 输出:
 * [
 * [1,2,2],
 * [5]
 * ]
 * 提示:
 * 1 <= candidates.length <= 100
 * 1 <= candidates[i] <= 50
 * 1 <= target <= 30
 * <p>
 * 链接：https://leetcode.cn/problems/combination-sum-ii
 */
public class A40CombinationSum2 {

    private List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        if (candidates.length == 0) return result;
        Arrays.sort(candidates);
        next(candidates, target, 0, 0, new ArrayList<>());
        return result;
    }

    private void next(int[] candidates, int target, int index, int nowSum, List<Integer> temp) {
        if (index >= candidates.length) {
            return;
        }

        if (nowSum + candidates[index] == target) {
            List<Integer> newList = new ArrayList<>(temp.size());
            newList.addAll(temp);
            newList.add(candidates[index]);
            result.add(newList);
            return;
        } else if (nowSum + candidates[index] > target) {
            return;
        } else {
            // 当前值加上
            temp.add(candidates[index]);
            next(candidates, target, index + 1, nowSum + candidates[index], temp);
            temp.remove(temp.size() - 1);

            // 当前值不加
            // 为了避免结果重复，当前值不加时，跳过重复元素
            while (index + 1 < candidates.length && candidates[index] == candidates[index + 1]) {
                index++;
            }
            next(candidates, target, index + 1, nowSum, temp);
        }
    }

    public static void main(String[] args) {
        A40CombinationSum2 combinationSum = new A40CombinationSum2();
        int[] candidates = {10, 1, 2, 7, 6, 1, 5};
        List<List<Integer>> result = combinationSum.combinationSum2(candidates, 8);
        for (List<Integer> item : result) {
            for (Integer num : item) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
