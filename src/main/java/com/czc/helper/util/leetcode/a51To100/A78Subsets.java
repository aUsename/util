package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/2
 *
 * <p>
 * 给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
 * 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。
 * 示例 1：
 * 输入：nums = [1,2,3]
 * 输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
 * 示例 2：
 * 输入：nums = [0]
 * 输出：[[],[0]]
 * 提示：
 * 1 <= nums.length <= 10
 * -10 <= nums[i] <= 10
 * nums 中的所有元素 互不相同
 * <p>
 * 链接：https://leetcode.cn/problems/subsets
 * 子集
 */
public class A78Subsets {

    private List<List<Integer>> result = new ArrayList<>();
    private int len;

    /**
     * 直接回溯
     */
    public List<List<Integer>> subsets(int[] nums) {
        len = nums.length;
        next(nums, 0, new ArrayList<>());
        return result;
    }

    private void next(int[] nums, int point, List<Integer> current) {
        if (point == len) {
            List<Integer> list = new ArrayList<>(current);
            result.add(list);
            return;
        }

        int curNum = nums[point];

        next(nums, point + 1, current);

        current.add(curNum);
        next(nums, point + 1, current);
        current.remove(current.size() - 1);
    }
}
