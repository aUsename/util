package com.czc.helper.util.leetcode.a1To50;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/9/13
 *
 * <p>
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 * 示例 1：
 * 输入：l1 = [1,2,4], l2 = [1,3,4]
 * 输出：[1,1,2,3,4,4]
 * 示例 2：
 * 输入：l1 = [], l2 = []
 * 输出：[]
 * 示例 3：
 * 输入：l1 = [], l2 = [0]
 * 输出：[0]
 * 提示：
 * 两个链表的节点数目范围是 [0, 50]
 * -100 <= Node.val <= 100
 * l1 和 l2 均按 非递减顺序 排列
 * <p>
 * 链接：https://leetcode.cn/problems/merge-two-sorted-lists
 */
public class A21MergeTwoSortedLists {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode head = new ListNode();
        ListNode current = head;
        // todo 这里跟A2AddTwoNumbers又不一样，A2AddTwoNumbers可以用||替换&&，因为如果一个链表为空时，还是需要继续计算，但是这里如果一个链表为空，直接将非空的链表连到current之后就可以了。  即：current.next = l1 == null ? l2 : l1;
        while (list1 != null || list2 != null) {
            if (list1 == null) {
                current.next = new ListNode(list2.val);
                list2 = list2.next;
                current = current.next;
                continue;
            }
            if (list2 == null) {
                current.next = new ListNode(list1.val);
                list1 = list1.next;
                current = current.next;
                continue;
            }
            if (list1.val >= list2.val) {
                // todo 这里应该改成 current.next = list2，减少对象创建
                current.next = new ListNode(list2.val);
                list2 = list2.next;
                current = current.next;
            } else {
                // 这里应该改成 current.next = list1
                current.next = new ListNode(list1.val);
                list1 = list1.next;
                current = current.next;
            }
        }
        return head.next;
    }

    public static void main(String[] args) {
        A21MergeTwoSortedLists mergeTwoSortedLists = new A21MergeTwoSortedLists();
        int[] nums1 = {1, 2, 4};
        ListNode list1 = mergeTwoSortedLists.getList(nums1);
        mergeTwoSortedLists.print(list1);
        System.out.println();
        int[] nums2 = {1, 3, 4};
        ListNode list2 = mergeTwoSortedLists.getList(nums2);
        mergeTwoSortedLists.print(list2);
        System.out.println();

        ListNode result = mergeTwoSortedLists.mergeTwoLists(list1, list2);
        mergeTwoSortedLists.print(result);
    }

    private ListNode getList(int[] nums) {
        ListNode head = new ListNode();
        ListNode current = head;
        for (int num : nums) {
            current.next = new ListNode(num);
            current = current.next;
        }
        return head.next;
    }

    private void print(ListNode list) {
        while (list != null) {
            System.out.println(list.val);
            list = list.next;
        }
    }
}
