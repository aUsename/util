package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/17
 * <p>
 * 给你二叉树的根节点 root ，返回其节点值 自底向上的层序遍历 。 （即按从叶子节点所在层到根节点所在的层，逐层从左向右遍历）
 * 示例 1：
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：[[15,7],[9,20],[3]]
 * 示例 2：
 * 输入：root = [1]
 * 输出：[[1]]
 * 示例 3：
 * 输入：root = []
 * 输出：[]
 * 提示：
 * 树中节点数目在范围 [0, 2000] 内
 * -1000 <= Node.val <= 1000
 * <p>
 * 链接：https://leetcode.cn/problems/binary-tree-level-order-traversal-ii
 * 二叉树的层序遍历 II
 */
public class A107BinaryTreeLevelOrderTraversal2 {
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        // 这里使用了两个队列，实际可以只用一个，然后用一个标志位记录当前层剩余的数目，用size方法即可
        Deque<TreeNode> left = new ArrayDeque<>();
        Deque<TreeNode> right = new ArrayDeque<>();
        Deque<TreeNode> current = left;
        Deque<TreeNode> another = right;
        left.addFirst(root);
        List<Integer> list = new ArrayList<>();

        while (!current.isEmpty()) {
            TreeNode temp = current.removeLast();
            list.add(temp.val);
            if (temp.left != null) {
                another.addFirst(temp.left);
            }
            if (temp.right != null) {
                another.addFirst(temp.right);
            }
            if (current.isEmpty()) {
                result.add(0, new ArrayList<>(list));
                list.clear();
                if (current == left) {
                    current = right;
                    another = left;
                } else {
                    current = left;
                    another = right;
                }
            }
        }
        return result;
    }
}
