package com.czc.helper.util.leetcode.a101To150;

import java.util.*;

/**
 * Created by cuizhongcheng on 2022/11/18
 * <p>
 * 给定一个字符串 s 和一个字符串 t ，计算在 s 的子序列中 t 出现的个数。
 * 字符串的一个 子序列 是指，通过删除一些（也可以不删除）字符且不干扰剩余字符相对位置所组成的新字符串。（例如，"ACE" 是 "ABCDE" 的一个子序列，而 "AEC" 不是）
 * 题目数据保证答案符合 32 位带符号整数范围。
 * 示例 1：
 * 输入：s = "rabbbit", t = "rabbit"
 * 输出：3
 * 解释：
 * 如下图所示, 有 3 种可以从 s 中得到 "rabbit" 的方案。
 * rabb b it
 * ra b bbit
 * rab b bit
 * 示例 2：
 * 输入：s = "babgbag", t = "bag"
 * 输出：5
 * 解释：
 * 如下图所示, 有 5 种可以从 s 中得到 "bag" 的方案。
 * ba b g bag
 * ba bgba g
 * b abgb ag
 * ba b gb ag
 * babg bag
 * 提示：
 * 0 <= s.length, t.length <= 1000
 * s 和 t 由英文字母组成
 * <p>
 * 链接：https://leetcode.cn/problems/distinct-subsequences
 * 不同的子序列
 */
public class A115DistinctSubsequences {

    // 存储s中的所有字符，以及每个字符的出现位置
    private Map<Character, List<Integer>> all = new HashMap<>();

    /**
     * 自己实现，结果正确，但是超时
     */
    public int numDistinct(String s, String t) {
        if (t.length() == 0) {
            return 1;
        }
        if (s.length() == 0) {
            return 0;
        }
        // 初始化 all
        for (int i = 0; i < s.length(); i++) {
            char temp = s.charAt(i);
            if (all.containsKey(temp)) {
                all.get(temp).add(i);
            } else {
                List<Integer> list = new ArrayList<>();
                list.add(i);
                all.put(temp, list);
            }
        }

        // 存储t中字符在s中的位置
        int[] result = new int[t.length()];
        // 找出最左边的result
        int tempMin = -1;
        for (int i = 0; i < t.length(); i++) {
            char temp = t.charAt(i);
            if (all.containsKey(temp)) {
                List<Integer> list = all.get(temp);
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j) > tempMin) {
                        result[i] = list.get(j);
                        tempMin = list.get(j);
                        break;
                    }
                    if (j == list.size() - 1) {
                        return 0;
                    }
                }
            } else {
                return 0;
            }
        }

        int total = 0;
        // num[] 存储当前位置可能取值所在位置，以及每个位置的子序列次数
        // Map<Integer, Integer> 的key表示此位置的取值所在位置，value为子序列次数
        Map<Integer, Integer>[] num = new Map[t.length()];
        // 处理最后一位
        List<Integer> list = all.get(t.charAt(t.length() - 1));
        Map<Integer, Integer> tempMap = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) >= result[t.length() - 1]) {
                total++;
                tempMap.put(list.get(i), 1);
            }
        }
        num[t.length() - 1] = tempMap;

        // 从右边倒数第二位开始，向左动态规划
        for (int i = t.length() - 2; i >= 0; i--) {
            // 当前位置的可能取值
            List<Integer> currentList = all.get(t.charAt(i));
            Map<Integer, Integer> currentMap = new HashMap<>();
            currentMap.put(result[i], total);
            for (int j = 0; j < currentList.size(); j++) {
                // 过滤出当前位置的合理取值
                if (currentList.get(j) > result[i]) {
                    int currentTotal = 0;
                    Map<Integer, Integer> nextMap = num[i + 1];
                    Set<Integer> set = nextMap.keySet();
                    for (Integer item : set) {
                        if (currentList.get(j) < item) {
                            currentTotal = currentTotal + nextMap.get(item);
                        }
                    }
                    total = total + currentTotal;
                    currentMap.put(currentList.get(j), currentTotal);
                }
            }
            num[i] = currentMap;
        }
        return total;
    }

    /**
     * 参考官方的思路，动态规划
     */
    public int numDistinct1(String s, String t) {
        int m = s.length();
        int n = t.length();
        if (n == 0) {
            return 1;
        }
        if (m < n) {
            return 0;
        }
        // (x,y)位置，表示s的前x字符与t的前y字符,有多少种可能的方案
        int[][] result = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            result[i][0] = 1;
        }


        return result[m][n];
    }


    public static void main(String[] args) {
        A115DistinctSubsequences subsequences = new A115DistinctSubsequences();
        System.out.println(subsequences.numDistinct(""
                ,
                ""));
    }


}
