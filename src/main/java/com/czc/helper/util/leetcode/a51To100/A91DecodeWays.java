package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/11/8
 *
 * <p>
 * 一条包含字母 A-Z 的消息通过以下映射进行了 编码 ：
 * 'A' -> "1"
 * 'B' -> "2"
 * ...
 * 'Z' -> "26"
 * 要 解码 已编码的消息，所有数字必须基于上述映射的方法，反向映射回字母（可能有多种方法）。例如，"11106" 可以映射为：
 * "AAJF" ，将消息分组为 (1 1 10 6)
 * "KJF" ，将消息分组为 (11 10 6)
 * 注意，消息不能分组为  (1 11 06) ，因为 "06" 不能映射为 "F" ，这是由于 "6" 和 "06" 在映射中并不等价。
 * 给你一个只含数字的 非空 字符串 s ，请计算并返回 解码 方法的 总数 。
 * 题目数据保证答案肯定是一个 32 位 的整数。
 * 示例 1：
 * 输入：s = "12"
 * 输出：2
 * 解释：它可以解码为 "AB"（1 2）或者 "L"（12）。
 * 示例 2：
 * 输入：s = "226"
 * 输出：3
 * 解释：它可以解码为 "BZ" (2 26), "VF" (22 6), 或者 "BBF" (2 2 6) 。
 * 示例 3：
 * 输入：s = "0"
 * 输出：0
 * 解释：没有字符映射到以 0 开头的数字。
 * 含有 0 的有效映射是 'J' -> "10" 和 'T'-> "20" 。
 * 由于没有字符，因此没有有效的方法对此进行解码，因为所有数字都需要映射。
 * 提示：
 * 1 <= s.length <= 100
 * s 只包含数字，并且可能包含前导零。
 * <p>
 * 链接：https://leetcode.cn/problems/decode-ways
 * 解码方法
 */
public class A91DecodeWays {

    private int len;
    private int result;

    /**
     * 自己实现，暴力回溯，结果正确，但是会超时
     */
    public int numDecodings(String s) {
        len = s.length();
        next(s, 0);
        return result;
    }

    private void next(String s, int point) {
        if (point > len - 1) {
            result++;
            return;
        }

        // 下一个字符是0，当前字符串非法
        if (s.charAt(point) == '0') {
            return;
        }

        if (two(s, point)) {
            next(s, point + 2);
        }
        next(s, point + 1);
    }

    private boolean two(String s, int point) {
        if (point >= len - 1) {
            return false;
        }
        if (s.charAt(point) == '1') {
            return true;
        } else if (s.charAt(point) == '2') {
            return s.charAt(point + 1) >= '0' && s.charAt(point + 1) <= '6';
        }
        return false;
    }


    /**
     * 自己实现，动态规划
     */
    public int numDecodings1(String s) {
        int length = s.length();
        if (length == 0) return 0;
        if (s.charAt(0) == '0') {
            return 0;
        }

        int[] result = new int[length];
        result[0] = 1;

        int index = 1;
        while (index < length) {
            if (s.charAt(index) == '0') {
                if (s.charAt(index - 1) != '1' && s.charAt(index - 1) != '2') {
                    // 不是10或者20，认为是非法字符串
                    return 0;
                } else {
                    result[index] = index == 1 ? 1 : result[index - 2];
                }
            } else {
                if (canTwo(s, index)) {
                    result[index] = index == 1 ? result[index - 1] + 1 : result[index - 1] + result[index - 2];
                } else {
                    result[index] = result[index - 1];
                }
            }
            index++;
        }
        return result[length - 1];
    }

    private boolean canTwo(String s, int point) {
        if (s.charAt(point - 1) == '1') {
            return true;
        } else if (s.charAt(point - 1) == '2') {
            return s.charAt(point) >= '0' && s.charAt(point) <= '6';
        }
        return false;
    }

    public static void main(String[] args) {
        A91DecodeWays decodeWays = new A91DecodeWays();
        System.out.println(decodeWays.numDecodings1("111111111111111111111111111111111111111111111"));
    }
}
