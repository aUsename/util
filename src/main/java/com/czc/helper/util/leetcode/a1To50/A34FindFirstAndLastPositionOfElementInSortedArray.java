package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/10/17
 *
 * <p>
 * 给你一个按照非递减顺序排列的整数数组 nums，和一个目标值 target。请你找出给定目标值在数组中的开始位置和结束位置。
 * 如果数组中不存在目标值 target，返回 [-1, -1]。
 * 你必须设计并实现时间复杂度为 O(log n) 的算法解决此问题。
 * <p>
 * 示例 1：
 * 输入：nums = [5,7,7,8,8,10], target = 8
 * 输出：[3,4]
 * 示例 2：
 * 输入：nums = [5,7,7,8,8,10], target = 6
 * 输出：[-1,-1]
 * 示例 3：
 * 输入：nums = [], target = 0
 * 输出：[-1,-1]
 * <p>
 * 提示：
 * 0 <= nums.length <= 10^5
 * -10^9 <= nums[i] <= 10^9
 * nums 是一个非递减数组
 * -10^9 <= target <= 10^9
 * <p>
 * 链接：https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array
 */
public class A34FindFirstAndLastPositionOfElementInSortedArray {
    public int[] searchRange(int[] nums, int target) {
        int[] result = {-1, -1};
        int len = nums.length;

        if (len == 0) return result;
        if (nums[0] > target || nums[len - 1] < target) return result;

        // 此时 nums[left] <= target 且 target <= nums[right]
        int left = findLeft(nums, target, 0, len - 1);
        if (left != -1) {
            int right = findRight(nums, target, left, len - 1);
            result[0] = left;
            result[1] = right;
        }
        return result;
    }

    public int findLeft(int[] nums, int target, int left, int right) {
        while (left <= right) {
            if (nums[left] == target) {
                return left;
            }
            // 此时 nums[left] < target
            int mid = (left + right) / 2;
            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return -1;
    }

    public int findRight(int[] nums, int target, int left, int right) {
        while (left <= right) {
            if (nums[right] == target) {
                return right;
            }
            // 此时 target < nums[right]
            // todo 这里跟findLeft不一样，寻找right时让mid偏右，寻找left时让mid偏左（二分查找写的时候要特别注意长度是奇数和偶数时）
            int mid = (left + right + 1) / 2;

            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                left = mid;
            }

        }
        return -1;
    }

    public static void main(String[] args) {
        A34FindFirstAndLastPositionOfElementInSortedArray sortedArray = new A34FindFirstAndLastPositionOfElementInSortedArray();
        int[] nums = {5, 7, 7, 8, 8, 10};
        int[] re = sortedArray.searchRange(nums, 8);
        for (int r : re) {
            System.out.println(r);
        }
    }
}
