package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/9/7
 *
 * <p>
 * 给你一个字符串 s，找到 s 中最长的回文子串。
 * 示例 1：
 * 输入：s = "babad"
 * 输出："bab"
 * 解释："aba" 同样是符合题意的答案。
 * 示例 2：
 * 输入：s = "cbbd"
 * 输出："bb"
 * 提示：
 * 1 <= s.length <= 1000
 * s 仅由数字和英文字母组成
 * <p>
 * 链接：https://leetcode.cn/problems/longest-palindromic-substring
 */
public class A5LongestPalindromicSubstring {

    /**
     * 自己实现，中心扩展法
     */
    public String longestPalindrome(String s) {
        int maxLen = 0;
        String subStr = "";

        // 奇数长度回文，以某个字符为中间
        for (int i = 0; i < s.length(); i++) {
            int left = i;
            int right = i;
            while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
            }
            if (maxLen < right - left - 1) {
                maxLen = right - left - 1;
                subStr = s.substring(left + 1, right);
            }

        }
        // 偶数长度回文 i代表中间两个字符左边一个的位置
        for (int i = 0; i < s.length() - 1; i++) {
            int left = i;
            int right = i + 1;
            while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
            }
            if (maxLen < right - left - 1) {
                maxLen = right - left - 1;
                subStr = s.substring(left + 1, right);
            }
        }
        return subStr;
    }

    public static void main(String[] args) {
        A5LongestPalindromicSubstring longestPalindromicSubstring = new A5LongestPalindromicSubstring();
        String re = longestPalindromicSubstring.longestPalindrome("caad");
        System.out.println(re);
    }

}
