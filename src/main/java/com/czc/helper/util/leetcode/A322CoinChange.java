package com.czc.helper.util.leetcode;

import java.util.Arrays;

/**
 * Created by cuizhongcheng on 2022/9/27
 *
 * <p>
 * 给你一个整数数组 coins ，表示不同面额的硬币；以及一个整数 amount ，表示总金额。
 * 计算并返回可以凑成总金额所需的 最少的硬币个数 。如果没有任何一种硬币组合能组成总金额，返回 -1 。
 * 你可以认为每种硬币的数量是无限的。
 * 示例 1：
 * 输入：coins = [1, 2, 5], amount = 11
 * 输出：3
 * 解释：11 = 5 + 5 + 1
 * 示例 2：
 * 输入：coins = [2], amount = 3
 * 输出：-1
 * 示例 3：
 * 输入：coins = [1], amount = 0
 * 输出：0
 * 提示：
 * 1 <= coins.length <= 12
 * 1 <= coins[i] <= 2^31 - 1
 * 0 <= amount <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/coin-change
 */
public class A322CoinChange {

    /**
     * 自己实现，动态规划
     * 需要注意的是coins[i] <= 2^31 - 1，所以要考虑越界
     */
    public int coinChange(int[] coins, int amount) {
        // 边界处理
        if (amount == 0) return 0;

        int[] data = new int[amount];
        for (int temp : coins) {
            if (temp < amount) {
                data[temp] = 1;
            } else if (temp == amount) {
                return 1;
            }
        }
        int min = -1;
        for (int i = 0; i < amount; i++) {
            for (int j = 0; j < coins.length; j++) {
                if (data[i] > 0 && i + coins[j] <= amount) {
                    // 边界处理
                    if (i + coins[j] == amount) {
                        if (min > 0) {
                            min = min <= data[i] + 1 ? min : data[i] + 1;
                        } else {
                            min = data[i] + 1;
                        }

                        // 边界处理
                    } else if (i + coins[j] > 0) {
                        if (data[i + coins[j]] > 0) {
                            data[i + coins[j]] = data[i + coins[j]] <= data[i] + 1 ? data[i + coins[j]] : data[i] + 1;
                        } else {
                            data[i + coins[j]] = data[i] + 1;
                        }
                    }

                }
            }
        }

        return min > 0 ? min : -1;
    }


    /**
     * leetcode实现
     * dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
     */
    public int coinChange1(int[] coins, int amount) {
        int max = amount + 1;
        int[] dp = new int[amount + 1];
        // 数组初始化为 amount + 1，便于比较
        Arrays.fill(dp, max);
        dp[0] = 0;
        for (int i = 1; i <= amount; i++) {
            for (int j = 0; j < coins.length; j++) {
                if (coins[j] <= i) {
                    dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        return dp[amount] > amount ? -1 : dp[amount];
    }


    public static void main(String[] args) {
        A322CoinChange coinChange = new A322CoinChange();
        int[] coins = {1, 2, 5};
        System.out.println(coinChange.coinChange(coins, 0));
    }
}
