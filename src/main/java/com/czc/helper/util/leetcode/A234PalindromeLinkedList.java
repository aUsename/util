package com.czc.helper.util.leetcode;

import com.czc.helper.util.leetcode.helper.ListNode;

/**
 * Created by cuizhongcheng on 2022/9/13
 * <p>
 * 给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。
 * 示例 1：
 * 输入：head = [1,2,2,1]
 * 输出：true
 * 示例 2：
 * 输入：head = [1,2]
 * 输出：false
 * 提示：
 * 链表中节点数目在范围[1, 105] 内
 * 0 <= Node.val <= 9
 * 进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？
 * <p>
 * 链接：https://leetcode.cn/problems/palindrome-linked-list
 */
public class A234PalindromeLinkedList {

    // 找到中间节点，再倒置后半截链表，再判断
    public boolean isPalindrome(ListNode head) {
        if (head == null) return true;
        // 中间节点
        ListNode midNode = middleNode(head);

        if (head == midNode) return true;

        // 倒置后半段
        ListNode reverHead = reverseList(midNode);

        // 判断
        ListNode tempNode = reverHead;
        ListNode tempHead = head;

        while (tempHead.next != midNode) {
            if (tempNode.val != tempHead.val) {
                return false;
            }
            tempNode = tempNode.next;
            tempHead = tempHead.next;
        }
        if (tempNode.val != tempHead.val) {
            return false;
        }
        return true;
    }

    /**
     * 链表倒置
     * 这种倒置不影响原链表，具体可以看 A206ReverseLinkedList
     */
    private ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode();
        while (head != null) {
            newHead.next = new ListNode(head.val, newHead.next);
            head = head.next;
        }
        return newHead.next;
    }


    /**
     * 寻找链表中间节点（偶数时为靠后的那个）
     */
    private ListNode middleNode(ListNode head) {
        ListNode first = head;
        while (first != null && first.next != null) {
            first = first.next.next;
            head = head.next;
        }
        return head;
    }


    public static void main(String[] args) {
        A234PalindromeLinkedList palindromeLinkedList = new A234PalindromeLinkedList();

        int[] nums1 = {1, 2, 2, 1};
        ListNode list1 = palindromeLinkedList.getList(nums1);
        palindromeLinkedList.print(list1);
        System.out.println();


        Boolean result = palindromeLinkedList.isPalindrome(list1);
        System.out.println(result);
    }


    private ListNode getList(int[] nums) {
        ListNode head = new ListNode();
        ListNode current = head;
        for (int num : nums) {
            current.next = new ListNode(num);
            current = current.next;
        }
        return head.next;
    }

    private void print(ListNode list) {
        while (list != null) {
            System.out.println(list.val);
            list = list.next;
        }
    }


}
