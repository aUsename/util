package com.czc.helper.util.leetcode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by cuizhongcheng on 2022/10/20
 *
 * <p>
 * 给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。
 * 示例 1：
 * 输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
 * 输出：6
 * 解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。
 * 示例 2：
 * 输入：height = [4,2,0,3,2,5]
 * 输出：9
 * 提示：
 * n == height.length
 * 1 <= n <= 2 * 10^4
 * 0 <= height[i] <= 10^5
 * <p>
 * 链接：https://leetcode.cn/problems/trapping-rain-water
 */
public class A42TrappingRainWater {
    public int trap(int[] height) {
        int len = height.length;
        if (len <= 2) return 0;
        int result = 0;

        // 单调递减栈
        // 要注意stack里存的是下标，所以高度需要使用height[x]，很容易用错
        Deque<Integer> stack = new LinkedList<>();
        int point = 1;
        stack.add(0);

        while (point < len) {
            if (height[point] < height[stack.peekLast()]) {
                // 新的值更小时
                stack.add(point);
            } else if (height[point] > height[stack.peekLast()]) {
                int min = stack.pollLast();
                // 新的值更大时
                while ((!stack.isEmpty())) {
                    if (height[point] >= height[stack.peekLast()]) {
                        int left = stack.pollLast();
                        result = result + (point - left - 1) * (height[left] - height[min]);
                        min = left;
                    } else {
                        result = result + (point - stack.peekLast() - 1) * (height[point] - height[min]);
                        break;
                    }
                }
                stack.add(point);
            } else {
                // 相同时，以最右边的边来算
                stack.pollLast();
                stack.add(point);
            }
            point++;
        }

        return result;
    }

    public static void main(String[] args) {
        A42TrappingRainWater rainWater = new A42TrappingRainWater();
        int[] height = {5, 2, 1, 2, 1, 5};
        System.out.println(rainWater.trap(height));
    }
}
