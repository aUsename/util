package com.czc.helper.util.leetcode.a1To50;

/**
 * Created by cuizhongcheng on 2022/9/23
 *
 * <p>
 * 给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和 '*' 的正则表达式匹配。
 * '.' 匹配任意单个字符
 * '*' 匹配零个或多个前面的那一个元素
 * 所谓匹配，是要涵盖 整个 字符串 s的，而不是部分字符串。
 * 示例 1：
 * 输入：s = "aa", p = "a"
 * 输出：false
 * 解释："a" 无法匹配 "aa" 整个字符串。
 * 示例 2:
 * 输入：s = "aa", p = "a*"
 * 输出：true
 * 解释：因为 '*' 代表可以匹配零个或多个前面的那一个元素, 在这里前面的元素就是 'a'。因此，字符串 "aa" 可被视为 'a' 重复了一次。
 * 示例 3：
 * 输入：s = "ab", p = ".*"
 * 输出：true
 * 解释：".*" 表示可匹配零个或多个（'*'）任意字符（'.'）。
 * 提示：
 * 1 <= s.length <= 20
 * 1 <= p.length <= 30
 * s 只包含从 a-z 的小写字母。
 * p 只包含从 a-z 的小写字母，以及字符 . 和 *。
 * 保证每次出现字符 * 时，前面都匹配到有效的字符
 * <p>
 * 链接：https://leetcode.cn/problems/regular-expression-matching
 * 正则表达式匹配
 */
public class A10RegularExpressionMatching {

    /**
     * 自己实现，递归回溯法
     */
    public boolean isMatch(String s, String p) {
        return match(s, 0, p, 0);
    }

    private boolean match(String s, int indexS, String p, int indexP) {

        // s到末尾了，p为空合法，p剩余a*时合法，a*a时非法
        if (indexS == s.length()) {
            if (indexP == p.length()) {
                return true;
            }
            if (indexP == p.length() - 1) {
                return false;
            } else if (p.charAt(indexP + 1) == '*') {
                return match(s, indexS, p, indexP + 2);
            } else {
                return false;
            }
        }

        // p到末尾，s必须到末尾
        if (indexP == p.length()) {
            return indexS == s.length();
        }
        // a*时，可匹配，可不匹配
        if (indexP < p.length() - 1 && p.charAt(indexP + 1) == '*') {

            if (s.charAt(indexS) == p.charAt(indexP) || p.charAt(indexP) == '.') {
                return match(s, indexS, p, indexP + 2) || match(s, indexS + 1, p, indexP);
            } else {
                return match(s, indexS, p, indexP + 2);
            }
        }

        if (p.charAt(indexP) == '.') {
            return match(s, indexS + 1, p, indexP + 1);
        } else {
            return s.charAt(indexS) == p.charAt(indexP) && match(s, indexS + 1, p, indexP + 1);
        }

    }

    public static void main(String[] args) {
        A10RegularExpressionMatching matching = new A10RegularExpressionMatching();
        System.out.println(matching.isMatch("ab", ".*c"));
    }
}
