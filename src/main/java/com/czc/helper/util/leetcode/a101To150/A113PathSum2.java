package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/18
 * <p>
 * 给你二叉树的根节点 root 和一个整数目标和 targetSum ，找出所有 从根节点到叶子节点 路径总和等于给定目标和的路径。
 * 叶子节点 是指没有子节点的节点。
 * 示例 1：
 * 输入：root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
 * 输出：[[5,4,11,2],[5,8,4,5]]
 * 示例 2：
 * 输入：root = [1,2,3], targetSum = 5
 * 输出：[]
 * 示例 3：
 * 输入：root = [1,2], targetSum = 0
 * 输出：[]
 * 提示：
 * 树中节点总数在范围 [0, 5000] 内
 * -1000 <= Node.val <= 1000
 * -1000 <= targetSum <= 1000
 * <p>
 * 链接：https://leetcode.cn/problems/path-sum-ii
 */
public class A113PathSum2 {

    private List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<Integer> currentList = new ArrayList<>();
        dfs(root, targetSum, currentList);
        return result;
    }

    private void dfs(TreeNode currentNode, int targetSum, List<Integer> currentList) {
        if (currentNode == null) return;

        currentList.add(currentNode.val);
        // 叶子节点
        if (currentNode.left == null && currentNode.right == null) {
            if (currentNode.val == targetSum) {
                List<Integer> temp = new ArrayList<>(currentList);
                result.add(temp);
            }
            currentList.remove(currentList.size() - 1);
            return;
        }
        dfs(currentNode.left, targetSum - currentNode.val, currentList);
        dfs(currentNode.right, targetSum - currentNode.val, currentList);
        currentList.remove(currentList.size() - 1);
    }
}
