package com.czc.helper.util.leetcode.a51To100;

/**
 * Created by cuizhongcheng on 2022/11/1
 * <p>
 * 编写一个高效的算法来判断 m x n 矩阵中，是否存在一个目标值。该矩阵具有如下特性：
 * 1.每行中的整数从左到右按升序排列。
 * 2.每行的第一个整数大于前一行的最后一个整数。
 * 示例 1：
 * 输入：matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
 * 输出：true
 * 示例 2：
 * 输入：matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
 * 输出：false
 * 提示：
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 100
 * -10^4 <= matrix[i][j], target <= 10^4
 * <p>
 * 链接：https://leetcode.cn/problems/search-a-2d-matrix
 * 搜索二维矩阵
 */
public class A74SearchA2dMatrix {
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length, n = matrix[0].length;
        int left = 0, right = m * n - 1;

        while (left <= right) {
            int mid = (left + right) / 2;
            if (matrix[mid / n][mid % n] == target) {
                // 相等，直接返回
                return true;
            } else if (matrix[mid / n][mid % n] > target) {
                // 在左区间
                right = mid - 1;
            } else {
                // 在右区间
                left = mid + 1;
            }
        }
        return false;
    }
}
