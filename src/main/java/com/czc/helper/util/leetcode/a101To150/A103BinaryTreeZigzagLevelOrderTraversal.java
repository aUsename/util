package com.czc.helper.util.leetcode.a101To150;

import com.czc.helper.util.leetcode.helper.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/14
 * <p>
 * 给你二叉树的根节点 root ，返回其节点值的 锯齿形层序遍历 。（即先从左往右，再从右往左进行下一层遍历，以此类推，层与层之间交替进行）。
 * 示例 1：
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：[[3],[20,9],[15,7]]
 * 示例 2：
 * 输入：root = [1]
 * 输出：[[1]]
 * 示例 3：
 * 输入：root = []
 * 输出：[]
 * 提示：
 * 树中节点数目在范围 [0, 2000] 内
 * -100 <= Node.val <= 100
 * <p>
 * 链接：https://leetcode.cn/problems/binary-tree-zigzag-level-order-traversal
 * 二叉树的锯齿形层序遍历
 */
public class A103BinaryTreeZigzagLevelOrderTraversal {

    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        // 这里使用了两个队列，实际同A102BinaryTreeLevelOrderTraversal 还说可以只用一个+size标志位
        // 对每一层，若从头部取，则将元素加入尾部，若从尾部取，则将元素加入头部
        Deque<TreeNode> left = new ArrayDeque<>();
        Deque<TreeNode> right = new ArrayDeque<>();
        Deque<TreeNode> current = left;
        Deque<TreeNode> another = right;
        left.addFirst(root);
        List<Integer> list = new ArrayList<>();
        boolean turnLeft = false;

        while (!current.isEmpty()) {
            TreeNode temp = current.removeLast();
            list.add(temp.val);
            if (turnLeft) {
                if (temp.right != null) {
                    another.addLast(temp.right);
                }
                if (temp.left != null) {
                    another.addLast(temp.left);
                }
            } else {
                if (temp.left != null) {
                    another.addLast(temp.left);
                }
                if (temp.right != null) {
                    another.addLast(temp.right);
                }
            }

            if (current.isEmpty()) {
                result.add(new ArrayList<>(list));
                list.clear();
                if (current == left) {
                    current = right;
                    another = left;
                } else {
                    current = left;
                    another = right;
                }
                // 反向
                turnLeft = !turnLeft;
            }
        }

        return result;
    }

}
