package com.czc.helper.util.leetcode.a51To100;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuizhongcheng on 2022/11/9
 * <p>
 * 有效 IP 地址 正好由四个整数（每个整数位于 0 到 255 之间组成，且不能含有前导 0），整数之间用 '.' 分隔。
 * 例如："0.1.2.201" 和 "192.168.1.1" 是 有效 IP 地址，但是 "0.011.255.245"、"192.168.1.312" 和 "192.168@1.1" 是 无效 IP 地址。
 * 给定一个只包含数字的字符串 s ，用以表示一个 IP 地址，返回所有可能的有效 IP 地址，这些地址可以通过在 s 中插入 '.' 来形成。你 不能 重新排序或删除 s 中的任何数字。你可以按 任何 顺序返回答案。
 * 示例 1：
 * 输入：s = "25525511135"
 * 输出：["255.255.11.135","255.255.111.35"]
 * 示例 2：
 * 输入：s = "0000"
 * 输出：["0.0.0.0"]
 * 示例 3：
 * 输入：s = "101023"
 * 输出：["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
 * 提示：
 * 1 <= s.length <= 20
 * s 仅由数字组成
 * <p>
 * 链接：https://leetcode.cn/problems/restore-ip-addresses
 * 复原 IP 地址
 */
public class A93RestoreIpAddresses {
    private List<String> result = new ArrayList<>();

    private int[] point = {0, 0, 0, 0};
    private int len;

    /**
     * 自己实现，回溯法
     * 但是剪枝不充分，导致偏慢
     */
    public List<String> restoreIpAddresses(String s) {
        if (s.length() <= 3 || s.length() >= 13) {
            return new ArrayList<>();
        }
        len = s.length();
        next(s, 0);
        return result;
    }

    private void next(String s, int index) {
        if (index == 3) {
            if (correct(s, point[index], len)) {
                String builder = s.substring(point[0], point[1]) + '.' +
                        s.substring(point[1], point[2]) + '.' +
                        s.substring(point[2], point[3]) + '.' +
                        s.substring(point[3], len);
                result.add(builder);
            }
            return;
        }
        if (((len - point[index]) > (4 - index) * 3) || ((len - point[index]) < (4 - index))) {
            return;
        }

        if (correct(s, point[index], point[index] + 1)) {
            point[index + 1] = point[index] + 1;
            next(s, index + 1);
        }
        if (correct(s, point[index], point[index] + 2)) {
            point[index + 1] = point[index] + 2;
            next(s, index + 1);
        }
        if (correct(s, point[index], point[index] + 3)) {
            point[index + 1] = point[index] + 3;
            next(s, index + 1);
        }
    }

    private boolean correct(String s, int left, int right) {
        if (left >= right || right > len || right - left > 3) {
            return false;
        } else if (right - left > 1 && s.charAt(left) == '0') {
            // 不止1位，但首位为0，不满足
            return false;
        }
        int temp = s.charAt(left) - '0';
        for (int i = left + 1; i < right; i++) {
            temp = temp * 10 + s.charAt(i) - '0';
        }
        return temp <= 255;
    }

    public static void main(String[] args) {
        A93RestoreIpAddresses ipAddresses = new A93RestoreIpAddresses();
        List<String> result = ipAddresses.restoreIpAddresses("101023");
        for (String str : result) {
            System.out.println(str);
        }
    }
}
