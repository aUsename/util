package com.czc.helper.util.leetcode.a51To100;

import com.czc.helper.util.leetcode.helper.TreeNode;

/**
 * Created by cuizhongcheng on 2022/11/14
 * <p>
 * 给你二叉搜索树的根节点 root ，该树中的 恰好 两个节点的值被错误地交换。请在不改变其结构的情况下，恢复这棵树 。
 * 示例 1：
 * 输入：root = [1,3,null,null,2]
 * 输出：[3,1,null,null,2]
 * 解释：3 不能是 1 的左孩子，因为 3 > 1 。交换 1 和 3 使二叉搜索树有效。
 * 示例 2：
 * 输入：root = [3,1,4,null,null,2]
 * 输出：[2,1,4,null,null,3]
 * 解释：2 不能在 3 的右子树中，因为 2 < 3 。交换 2 和 3 使二叉搜索树有效。
 * 提示：
 * 树上节点的数目在范围 [2, 1000] 内
 * -2^31 <= Node.val <= 2^31 - 1
 * 进阶：使用 O(n) 空间复杂度的解法很容易实现。你能想出一个只使用 O(1) 空间的解决方案吗？
 * <p>
 * 链接：https://leetcode.cn/problems/recover-binary-search-tree
 * 恢复二叉搜索树
 */
public class A99RecoverBinarySearchTree {

    /**
     * 使用 Morris中序遍历
     */
    public void recoverTree(TreeNode root) {

        TreeNode x = null, y = null, pre = null, predecessor;

        while (root != null) {
            if (root.left != null) {
                // 寻找predecessor
                predecessor = root.left;
                while (predecessor.right != null && predecessor.right != root) {
                    predecessor = predecessor.right;
                }
                if (predecessor.right == null) {
                    // 此时还在持续向左走，还未开始遍历
                    predecessor.right = root;
                    root = root.left;
                } else {
                    // 比较root和pre
                    if (pre != null) {
                        if (pre.val > root.val) {
                            if (x == null) {
                                x = pre;
                            }
                            // 可能是相邻的两个节点，所以y一直设置，x仅在空的时候设置
                            y = root;
                        }
                    }
                    // 设置pre为当前值
                    pre = root;

                    // 向右遍历
                    predecessor.right = null;
                    root = root.right;
                }
            } else {
                // 比较root和pre
                if (pre != null) {
                    if (pre.val > root.val) {
                        if (x == null) {
                            x = pre;
                        }
                        // 可能是相邻的两个节点，所以y一直设置，x仅在空的时候设置
                        y = root;
                    }
                }

                // 设置pre为当前值
                pre = root;

                // 向右遍历
                root = root.right;
            }
        }

        int temp = x.val;
        x.val = y.val;
        y.val = temp;
    }

}
